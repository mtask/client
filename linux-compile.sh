#!/bin/bash
set -e
PDCURSESURL=https://sourceforge.net/projects/pdcurses/files/pdcurses/3.9/PDCurses-3.9.zip

MAKEFLAGS=${MAKEFLAGS:--j}

if [ $# -eq 1 ]
then
	if [ $1 = "-f" ]
	then
		FORCE=true
		MAKEFLAGS="$MAKEFLAGS -B"
	else
		echo "Usage: $0 [-f]"
		exit 1
	fi
fi

cd mtask-rts
cp config.h{.def,}
cp preloaded_tasks.h{.def,}

# Build the regular rts
make $MAKEFLAGS -f Makefile.pc -B
mv client{,-tty}

# Build the curses rts
make $MAKEFLAGS CURSES_INTERFACE=yes -f Makefile.pc -B
mv client{,-curses}

# Build the xcurses rts
if [ ! -d PDCurses-3.9 ]
then
	curl -LSso PDCurses.zip $PDCURSESURL
	unzip -q PDCurses.zip
	rm PDCurses.zip
fi

(
	cd PDCurses-3.9/x11
	if [ -n "$FORCE" -o ! -f Makefile ]
	then
		./configure
	fi
	make $MAKEFLAGS 
	make install || echo "Couldn't install PDCurses, not super user?"
)
make $MAKEFLAGS CURSES_INTERFACE=pdcurses -f Makefile.pc -B
mv client{,-xcurses}

# Build the mqtt rts
(
	cd ../dependencies/WolfMQTT
	if [ ! -f configure ]
	then
		./autogen.sh
	fi
	if [ -n "$FORCE" -o ! -f Makefile ]
	then
		./configure --enable-static --disable-examples --enable-mqtt5 --disable-tls
	fi
	make $MAKEFLAGS

	cd wolfmqtt
	cp options.h.in options.h
	cd ../IDE/ARDUINO
	./wolfmqtt-arduino.sh
)
make $MAKEFLAGS MQTT=yes -f Makefile.pc -B
mv client{,-mqtt}
