#!/bin/bash
set -e

cd WolfMQTT
cp ../vs_settings.h wolfmqtt
./autogen.sh
if [[ $1 == win ]]
then
	./configure --enable-mqtt5 --disable-tls --host=x86_64-pc-mingw32 --disable-examples --enable-static CFLAGS=-Wno-error=attributes LIBS=-lws2_32 CC=x86_64-w64-mingw32-gcc
	make CFLAGS+=-Wno-error DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc
else
	./configure --enable-static --disable-examples --enable-mqtt5 --disable-tls
	make CFLAGS+=-Wno-error 
fi
cd wolfmqtt
cd ../IDE/ARDUINO
./wolfmqtt-arduino.sh
zip -r ../../../wolfmqtt.zip wolfMQTT
