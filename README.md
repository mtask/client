# The mTask client

This repository contains the mTask client source code.
It is written in portable C99 but for testing, `libcheck` is used.
Make sure to either clone this repo recursively
(`git clone --recursive https://gitlab.com/mtask/client`) or initialise and
update the submodules (`git submodule init && git submodule update`).

If you want to build everything, please see `.gitlab-ci.yml` for the
dependencies.
If you only want to build the clients for your platform, run:
`nitrile fetch && nitrile build --only=client`

Automatic builds for the desktop clients can installed using nitrile under the package `mtask-client-desktop`.
Accompanying source code distributions can be found in the package registry: https://gitlab.com/mtask/client/-/packages
It is also possible to automatically fetch the sources on Linux or mac (and arduino dependencies) for the mTask client version installed.
This is done by running `fetch_mtask_client_sources.bash` (available in the `mtask-client-desktop` package) (`bash`, `jq`, `curl`, and `tar` are required).

[[_TOC_]]

## Generated code and configuration

The client depends on the server, since the interface between the client and the server is generated.

To generate only this interface, run: `nitrile build --only=types`.

Furthermore, `config.h` and `preloaded_tasks.h` must be available.
They can be found by customising `config.h.def` and `preloaded_tasks.h.def` respectively.

## Memory layout

The memory is self managed in mTask and can be set with the `MEMSIZE` preprocessor definition.
The memory is divided up into a task heap, a task tree heap and a stack.
The task heap is only modified between executions and thus the stack location changes sometimes.
The `struct MTask` datatype is generated and automatically printed and parsed.

## Device Notes

### linux/windows client

See `windows-compile.sh` and `posix-compile.sh` for instructions.

### macos client

Natively generating the required `types.c` and `types.h` is not possible.
However, you can use docker to generate it using the following command:

```
docker run --rm -it -v "$PWD":/root/bork cleanlang/nitrile:0.4.6 bash -c 'cd /root/bork && nitrile update && nitrile fetch && nitrile build --only=types'
```

Compiling the xcurses variant of the interface requires xquartz as well as the dependencies for linux.
They can be conveniently installed using:

```
brew install xquartz autoconf automake libtool
```

mqtt is not (yet) fully supported

### Arduino compatibles

Currently fully supported:

- Arduino UNO
- LOLIN D1 mini
- NodeMCUv2
- Adafruit Feather M0 WiFi
- ESP32 Firebeetle 32

	For this board, the board preprocessor defines are inconsistent and shared with other similar ESP32 boards so distinguishing is impossible.
	To fix this, add the following line to your [boards.txt](https://arduino.github.io/arduino-cli/0.20/platform-specification/), e.g. on linux: `~/.arduino15/packages/esp32/hardware/esp32/2.0.2/boards.txt`
	
	```
firebeetle32.build.defines=-DARDUINO_ESP32_FIREBEETLE32
	```
	
	in the `firebeetle32` section.

Please use a recent version of the arduino core (N.B. debian stable still has
1.0 in their repositories).


#### MQTT

To prepare WolfMQTT for the Arduino IDE run:

    cd mtask-rts-dependencies/WolfMQTT
    cp wolfmqtt/options.h.in wolfmqtt/options.h
    cd IDE/ARDUINO
    ./wolfmqtt-arduino.sh

And install the library using `Sketch → Include Library → Add ZIP Library` and
point to the the created wolfMQTT folder.  The full path of this folder is:
`./dependencies/WolfMQTT/IDE/ARDUINO/wolfMQTT`

#### IDE

- Install the latest IDE and install all required libraries from `mtask-rts-dependencies` using `Sketch → Include Library → Add ZIP Library`, and point to the directory of the dependency.
	For the MQTT library, see the instructions above.
- Open `mtask-rts/mtask-rts.ino`
- Install the required core(s):
	- AVR (e.g. uno): this core is automatically installed.
	- esp8266 (e.g. NodeMCU, WEMOS D1 mini, ):
		https://github.com/esp8266/Arduino
	- esp32 (e.g. WEMOS d32)
		https://github.com/espressif/arduino-esp32
	- feather (e.g. Adafruit feather M0):
		https://learn.adafruit.com/adafruit-feather-m0-basic-proto/setup

#### Command line

The Arduino IDE is preferred over the CLI approaches.


##### AVR arduinos
Make sure to have `Arduino-mk` installed for avr arduinos
On debian this can be done by running

    apt-get install arduino-mk

Edit the makefile to your liking (e.g. change the port for flashing) and
compile the runtimesystem with

    make -f Makefile.arduino-uno

Upload with:

    make -f Makefile.arduino-uno upload

##### ESP8266 arduinos
Make sure to have `makeEspArduino` installed for esp\* arduinos
This is not in the debian repositories but you can install it from
[here](https://github.com/plerup/makeEspArduino/blob/master/makeEspArduino.mk).

Edit the makefile to your liking (e.g. change the port for flashing) and
compile the runtimesystem with

    make -f Makefile.arduino-lolin-d1-mini

Or 

    make -f Makefile.arduino-nodemcu

Upload with:

    make -f Makefile.arduino-{nodemcu,lolin-d1-mini} flash

##### ESP32 arduinos

Edit the makefile to your liking (e.g. change the port for
flashing) and compile the runtimesystem with

    make -f Makefile.arduino-wemos-d32

Upload with:

    make -f Makefile.arduino-wemos-d32 flash

##### Feather arduinos

There is no(t yet a) command line build available.

#### Wifi

If you want to use wifi, make sure to copy `mtask-rts/config.h.def` to `mtask-rts/config.h` and add the required wifi connection details.

## Customize the firmware

The firmware can be customized using preprocessor macros.

`config.h` contains the global configuration options

`pc.h` contains the linux/windows/macos specific configuration

`arduino.h` contains the Arduino compatible specific configuration

### Customization options

#### General options

- MEMSIZE

  The number of bytes to reserve for the mTask memory
- APINS

  The number of available analog pins
- DPINS

  The number of available digital pins
- LOGLEVEL

  The loglevel (0=silent, 1=info, 2=debug)
- SC(s)

  The method for storing strings (e.g. to store them in progmem)
- REQUIRE\_ALIGNED\_MEMORY\_ACCESS

  Force all pointers dereferences to be aligned (this is automatically set on processors that require this (e.g. ESP8266)).
- PING

  Set a time between pings.
  If set to zero, the device will never send pings

- DEFAULT_SLEEP_TIME

  Sleep time if there are no tasks

- MAX\_SLEEP\_TIME

  Maximum time the device sleeps.
- STACK\_CHECKING

  Enable stack overflow checking (enabled by default, change makefile if you
  want to disable this).
- TRACE\_MEM

  Log the memory allocations

  Maximum time the device sleeps.
- NVM

  Non-volatile memory available for intermittend computing.

#### Communication

Communication is split up into link and communication.

- The link methods is either

  - LINK\_SERIAL for serial port communication
  - LINK\_TCP for TCP socket communication

- The communication is either

  - COMM\_DIRECT for communication without a protocol
  - COMM\_MQTT for communication using MQTT (this requires a broker)

    For running MQTT, define the server in `config.h` and make sure a server is up and running.
    mTask is tested with mosquitto on debian (testing, mosquitto version 1.6.12) that runs in a suitable-for-mtask configuration out of the box.

#### Peripherals

- `HAVE_DHT`

  Have a digital humidity and temperature sensor.
  On arduino, also define either `HAVE_DHT_DHT` or `HAVE_DHT_SHT`.

- `HAVE_LEDMATRIX`

  16x16 LED Matrix is connected
- `HAVE_OLEDSHIELD`

  OLED shield is connected (_TODO_, currently only used for runtime information)
- `HAVE_LIGHTSENSOR`

  Ambient light sensor shield is attached
- `HAVE_AIRQUALITYSENSOR`

  Airquality sensor attached
  On arduino, also define either `HAVE_AIRQUALITYSENSOR_CCS811` or
  `HAVE_AIRQUALITYSENSOR_SGP30`.

  WEMOS D1 Mini I2Cbutton shield attached

- `HAVE_GESTURESENSOR`

  PAJ7620 gesture sensor attached
- `HAVE_NEOPIXEL`

  NeoPixel peripheral attached.

#### PC

- CURSES\_INTERFACE

  If set to `pdcurses` it will build an X11 curses application on linux or macos.
  If set to anything else it will build an ncurses terminal application on
  linux or macos and a `pdcurses` windows application on windows.

  - NOMOUSE

    Disables the mouse support for the curses application

  The curses interface is a bit buggy. To aid debugging, use `gdbserver`.
  This allows you to see the curses interface and interact with `gdb`.

  - Start in one terminal: `gdbserver :12345 ./client`
  - Start in another: `gdb ./client` and type `target remote localhost:12345`
	followed by `c` (continue).

#### Arduino

- BAUDRATE

  Use a different baudrate for the serial connection

## Notes on Arduino compatibles

### UNO compatibles
It is advised to increase the serial buffer a bit since the loop can take relatively long.
This is done by increasing the `SERIAL_RX_BUFFER_SIZE` in `hardware/arduino/avr/cores/arduino/HardwareSerial.cpp`.
Default baudrate is 9600.

With the new task preparation message this shouldn't be necessary anymore.

### ESP8266 boards
The ESP8266 requires aligned LOAD/STORE instructions and will crash otherwise.
To achieve this, make sure to define `REQUIRE_ALIGNED_MEMORY_ACCESS`

### Bluetooth

#### Bluetooth shield v2.2 (Itead studio)
The Bluetooth Shield v2.2 (Itead studio) operates on a baudrate of 38400 so the sketch also requires this.
For operation the tx of the shield needs to be connected to the rx of the arduino and vice versa.

If you want to program the arduino, remove the shield or the tx/rx jumpers.

#### Connect bluetooth on linux
Connect and pair:

```
$ bluetoothctl
[bluetooth]# scan on
...
[bluetooth]# pair 00:00:00:00:00:00
...
[bluetooth]# info 00:00:00:00:00:00
Name: ...
Alias: ...
...
UUID: Serial Port (...)
```

Setup the rfcomm tty

```
$ rfcomm release all # optional
$ rfcomm bind hci0 00:12:12:12:08:30 1
$ ls -l /dev/rfcomm*
crw-rw---- 1 root dialout 216, 0 Oct 31 09:44 /dev/rfcomm0
```

And you are ready to go by selecting /dev/rfcomm0 as a device.
The initial message often takes a little longer than expected.

#### Connect bluetooth on windows

- Click on the bluetooth icon
- Click on add new bluetooth or other device
- Click on bluetooth
- Select the arduino device and enter the pin
- In the bluetooth options click more bluetooth options
- Click on the COM ports tab and there you see an outgoing connection for the device.
  If this is not the case, add it. That outgoing connection is the address you need to enter in the programs.
  e.g. COM4

## Libraries

All library repos are in `dependencies`.

- `Adafruit-GFX-Library`

  Addition graphics routines for the OLED shield
- `Adafruit_SSD1306-esp8266-64x48`

  Patched SDD library for the LOLIN D1 mini
- `DHTlib/libraries/DHTlib`

  Required for DHT
- `WEMOS_Matrix_LED_Shield_Arduino_Library`

  Required for the LED shield
- `WEMOS_SHT3x_Arduino_Library`

  Required for the SHT shield of the LOLIN D1 mini
- `BH1750`

  BH1750 Light sensor
- `LOLIN_OLED_I2C_Button_Library`

  I2C buttons on the LOLIN D1 mini
- `SparkFun_CCS811_Arduino_Library`

  CCS811 air quality sensor
- `Adafruit_SGP30`

  SGP30 air quality sensor
- `WolfMQTT`

  MQTT library. Setup using the instructions in `IDE/ARDUINO` and add the following line: `#define WOLFMQTT_V5` to `mqtt_types.h` after the line containing `//#define ENABLE_MQTT_TLS`
- `Adafruit_NeoPixel`

  NeoPixel peripheral library

## Coding style

For all the C code we adhere to the linux kernel coding style:
https://www.kernel.org/doc/html/v4.10/process/coding-style.html

And there are some sanity style checks performed in `codestyle.bash`:

- `default:` cases in `switch` statements are only allowed in very special
  places (never when using generated enumeration types).
- Indentation with spaces, alignment with tabs.
- Lines must be shorter or equal to 80 chars.
- No trailing whitespace.
- `while`, `if`, `switch` and `for` must have a space before the bracket and a
  space between the bracket and the opening brace.
- macros with `do { ... } while (0)` should not have a semicolon.
