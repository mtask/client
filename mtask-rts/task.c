#include "task.h"

#include "interface.h"
#include "mem.h"
#include "tasktree.h"
#include "scheduler.h"
#include "events.h"
#include "peripheral.h"

extern struct MTaskMeta *current_task;

void task_complete(struct MTaskMeta *t, uintptr_t *stack)
{
	msg_debug(PSTR("Complete task: %s\n"),
		stack[0] == MTNoValue_c ? "No value"
		: stack[0] == MTUnstable_c ? "Unstable"
		: "Stable");

	//stability is equal
	bool same = stack[0] == t->stability;

	//See if they are the same
	if (stack[0] != MTNoValue_c && t->value.size > 0)
		for (uint8_t i = 0; i<t->value.size/2 && same; i++)
			if (stack[i+1] != t->value.elements[i*2]*256u+
					t->value.elements[i*2+1]%256u)
				same = false;

	msg_debug(PSTR("and was %sthe same\n"), same ? PSTR("") : PSTR("not "));

	//Not the same or different stability
	if (!same) {
		msg_debug(PSTR("stability: %u\n"), stack[0]);
		struct TaskValue r;
		t->stability = stack[0];
		if (stack[0] == MTNoValue_c) {
			r.cons = NoValue_c;
		} else {
			r.cons = Value_c;
			//Copy it to the return space
			msg_debug(PSTR("return bytes: %lu\n"), t->value.size);
			msg_log(PSTR("send new task value\n"));
			for (uint8_t i = 0; i<t->value.size/2; i++) {
				t->value.elements[i*2] = stack[i+1] / 256;
				t->value.elements[i*2+1] = stack[i+1] % 256;
				msg_debug(PSTR("%u %u "),
					stack[i+1] / 256, stack[i+1] % 256);
			}
			msg_debug(PSTR("\n"));
			//Prepare the message
			r.data.Value.f0 = &t->value;
			r.data.Value.f1 = stack[0] == MTStable_c;
		}
		//Send the message
		send_message((struct MTMessageFro)
			{ .cons=MTFTaskReturn_c
			, .data={.MTFTaskReturn= { .f0=t->taskid, .f1=r }}
			});
	}
}

void task_remove(struct MTaskMeta *t, uintptr_t *stack)
{
	msg_log(PSTR("REMOVE\n"));
	task_print(t, false);
	// destroy the peripherals
	for (uint32_t i = 0; i<t->peripherals.size; i++) {
		struct BCPeripheral *p = t->peripherals.elements+i;
		peripheral_destroy(p);
	}
	if (t->tree != NULL)
		mem_mark_trash_and_destroy(t->tree, t, stack);
	t->status = MTRemoved_c;
	task_print(t, false);
}

void task_move(struct MTaskMeta *t, uint16_t offset)
{
	//Move value pointer
	t->value.elements = (uint8_t *)
		((uintptr_t)t->value.elements-offset);
	//Move shares pointer
	t->shares.elements = (struct BCShareSpec *)
		((uintptr_t)t->shares.elements-offset);
	//Move shares values
	for (uint32_t i = 0; i<t->shares.size; i++)
		t->shares.elements[i].bcs_value.elements = (uint8_t *)
			((uintptr_t)t->shares.elements[i].bcs_value.elements
				-offset);
	//Move peripheral pointer
	t->peripherals.elements = (struct BCPeripheral *)
		((uintptr_t)t->peripherals.elements-offset);
	//Move instruction pointer
	t->instructions.elements = (uint8_t *)
		((uintptr_t)t->instructions.elements-offset);
	//Move the parent pointers in the tree
	if (t->tree != NULL) {
		struct TaskTree *tree = (struct TaskTree *)t->tree;
		tasktree_set_ptr_task(tree,
			(struct MTaskMeta *)((uintptr_t)t-offset));
	}

}

void task_print(struct MTaskMeta *t, bool verbose)
{
#if LOGLEVEL == 2
	//Server id
	msg_debug(PSTR("register task allocated at %p\n"
		"{taskid=%u, tree=-, stability=%u, peripherals[%lu], "
		"lastrun=%lu, nextrun=(%lu, %lu), status=%u, "
		"execution=(%lu, %lu), lastrun=%lu\n"),
		t, t->taskid, t->stability,
		t->peripherals.size, t->lastrun, t->execution_min,
		t->execution_max,
		t->status, t->execution_min, t->execution_max, t->lastrun);
	msg_debug(PSTR("shares[%u]: ["), t->shares.size);
	for (uint8_t i = 0; i<t->shares.size && verbose; i++) {
		msg_debug(PSTR("%s{id=%u, itask=%s, value=["),
			i == 0 ? "" : ", ",
			t->shares.elements[i].bcs_ident,
			t->shares.elements[i].bcs_itasks ? "yes" : "no");
		for (uint8_t j = 0; j<t->shares.elements[i].bcs_value.size;
				j++) {
			msg_debug(PSTR("%s%u"), j == 0 ? "" : ", ",
				t->shares.elements[i].bcs_value.elements[j]);
		}
		msg_debug(PSTR("]}"));
	}
	msg_debug(PSTR("]\ntaskvalue[%u]: ["), t->value.size);
	for (uint8_t i = 0; i<t->value.size && verbose; i++) {
		msg_debug(PSTR("%s%u"),
			i == 0 ? "" : ", ", t->value.elements[i]);
	}
	msg_debug(PSTR("]\nbytecode[%lu]: ["), t->instructions.size);
	for (uint16_t i = 0; i<t->instructions.size && verbose; i++) {
		if (i <= 3 || i + 3 >= t->instructions.size) {
			msg_debug(PSTR("%s%u"), i == 0 ? "" : ", ",
				t->instructions.elements[i]);
		} else if ( i == 4 ) {
			msg_debug(PSTR(", ..."));
		}
	}
	msg_debug(PSTR("]\n"));
#endif /* LOGLEVEL == 2 */
	(void)t;
	(void)verbose;
}

bool task_initialise_peripherals(struct MTaskMeta *data)
{
	for (uint32_t i = 0; i<data->peripherals.size; i++) {
		struct BCPeripheral *p = data->peripherals.elements+i;
		msg_log(PSTR("peripheral add: %u\n"), p->perinfo.cons);
		if (!peripheral_register(data->taskid, p))
			return false;
	}
	return true;
}

bool task_register(struct MTaskMeta *data)
{
	task_print(data, true);

	//Pad bytecode so that the next task is aligned
#ifdef REQUIRE_ALIGNED_MEMORY_ACCESS
	uintptr_t d = PALIGND(PTRSIZE, mem_task_next_unsafe(data));
	data->instructions.size += d;
	extern uint16_t mem_task;
	mem_task += d;
#endif /* REQUIRE_ALIGNED_MEMORY_ACCESS */

	// Initialize peripherals
	if (!task_initialise_peripherals(data)) {
		return false;
	}

	// Set the last execution time of the task to now
	data->lastrun = getmillis();

	// Add task to the execution queue
	queue_insert_task(data);

	//Ack
	send_message((struct MTMessageFro)
		{ .cons=MTFTaskAck_c, .data={ .MTFTaskAck=data->taskid } });
	return true;
}

struct BCPeripheral *peripheral_get(uint8_t id)
{
	if (id >= current_task->peripherals.size) {
		send_message((struct MTMessageFro)
			{ .cons=MTFException_c
			, .data={.MTFException=
				{ .cons=MTEPeripheralUnknown_c
				, .data={.MTEPeripheralUnknown=
					{.f0=current_task->taskid, .f1=id}
				}}
			}});
		return NULL;
	}
	return current_task->peripherals.elements+id;
}
