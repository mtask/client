#include "interface.h"
#include "mem.h"
#include "task.h"
#include "tasktree.h"
#include "scheduler.h"
#include "preload.h"

/** cast an mTask pointer to a specific pointer type */
#define mem_cast_ptr(t, type) ((type)mem_ptr(t))
/** cast an mTask pointer to a task pointer */
#define mem_cast_task(t) mem_cast_ptr(t, struct MTaskMeta *)
/** cast an mTask pointer to a task tree pointer */
#define mem_cast_tree(t) mem_cast_ptr(t, struct TaskTree *)

extern struct MTaskMeta *current_task;
extern struct TaskTree *events;

uint16_t memsize = MEMSIZE - 1 - (MEMSIZE - 1) % sizeof (void *);
void *mtmem_real[MEMSIZE/sizeof(void *)] = {NULL};
uint8_t *mtmem = (uint8_t *)&mtmem_real;

//Variable pointers
uint16_t mem_task;
uint16_t mem_heap;

// MT_NULL is only used to pretty print pointers
#define MT_NULL 65535
void *mem_ptr(uint16_t i)
{
	return i == MT_NULL ? NULL : (void *)(mtmem + i);
}

uint16_t mem_rptr(void *p)
{
	return p == NULL ? MT_NULL : ((uint8_t *)p)-mtmem;
}
#undef MT_NULL

void mem_print_heap(void)
{
#if LOGLEVEL == 2
	uint16_t num = 0;
	uint16_t walker;
	walker = memsize - sizeof(struct TaskTree);
	while (walker >= mem_heap) {
		struct TaskTree *t = mem_cast_tree(walker);
		msg_debug(PSTR(
			"%02d: %d (rptr: %05u, trash: %d, refr: [%u, %u]): "),
			num++, walker, mem_rptr(t->ptr.tree), t->trash,
			t->refresh_min * (t->seconds ? 1000u : 1u),
			t->refresh_max * (t->seconds ? 1000u : 1u));
		tasktree_print_node(t);
		walker -= sizeof(struct TaskTree);
		msg_debug(PSTR("\n"));
	}
#endif /* LOGLEVEL == 2 */
}

static void mem_print_task_heap(void)
{
#if LOGLEVEL == 2
	msg_debug(PSTR("TASKS\n"));
	struct MTaskMeta *t = mem_task_head();
	while (t != NULL) {
		task_print(t, false);
		t = mem_task_next(t);
	}
	msg_debug(PSTR("ENDTASKS\n"));
#endif /* LOGLEVEL == 2 */
}

void mem_reset(void)
{
	mem_task = 0;
	mem_heap = memsize;

	msg_debug(PSTR("mem_top: %p\n"), mem_ptr(memsize));
	msg_debug(PSTR("mem_bottom: %p\n"), mem_ptr(0));
	msg_debug(PSTR("free bytes: %llu\n"),
		(size_t)mem_heap - (size_t)mem_task);

	mem_print_task_heap();
}

uintptr_t *mem_stack(void)
{
	//This is aligned because tasks are always PTRSIZE bytes aligned
	return mem_ptr(mem_task);
}

uintptr_t *mem_max_sp(void)
{
	return mem_ptr(mem_heap);
}

void mem_set_stack_ptr(void *ptr)
{
	mem_task = mem_rptr(ptr);
}

void *mem_alloc_task(size_t size)
{
	if (size == 0)
		return mem_ptr(mem_task);

	msg_debug(PSTR("mem_alloc_task(%lu) mem_task: %p (%u)\n"),
		size, mem_ptr(mem_task), mem_task);

#ifdef REQUIRE_ALIGNED_MEMORY_ACCESS
	mem_task += PALIGND(size, mem_ptr(mem_task));
#endif /* REQUIRE_ALIGNED_MEMORY_ACCESS */
	void *r = mem_ptr(mem_task);

	if (mem_task + size > mem_heap) {
		send_message((struct MTMessageFro)
			{ .cons=MTFException_c
			, .data={.MTFException=
				{ .cons = MTEOutOfMemory_c
				, .data = {.MTEOutOfMemory=0}
				}
			}});
		return NULL;
	}
	mem_task += size;
#ifdef TRACE_MEM
	msg_log(PSTR("memtrace: task: %u allocated, total %u\n"),
		size, mem_task);
#endif /* TRACE_MEM */
	return r;
}

struct MTaskMeta *mem_task_head(void)
{
	return mem_task == 0 ? NULL : mem_cast_task(0);
}

struct MTaskMeta *mem_task_next_unsafe(struct MTaskMeta *t)
{
	return (struct MTaskMeta *)\
		(t->instructions.elements+t->instructions.size);
}
struct MTaskMeta *mem_task_next(struct MTaskMeta *t)
{
	struct MTaskMeta *r = mem_task_next_unsafe(t);
	if (mem_rptr(r) >= mem_task)
		r = NULL;
	return r;
}

struct TaskTree *mem_alloc_tree(void)
{
	msg_debug(PSTR("general alloc: "));
	if (mem_heap - sizeof(struct TaskTree) < mem_task) {
		msg_log(PSTR("Not enough heap\n"));
		send_message((struct MTMessageFro)
			{ .cons=MTFException_c
			, .data={.MTFException=
				{.cons=MTEHeapUnderflow_c
				,.data={.MTEHeapUnderflow=current_task->taskid}
				}
			}});
		return NULL;
	}
	msg_debug(PSTR("%p\n"), mem_cast_tree(mem_heap));
#ifdef TRACE_MEM
	msg_log(PSTR("memtrace: tree: %u allocated, total %u\n"),
		sizeof(struct TaskTree), memsize-mem_heap);
#endif /* TRACE_MEM */

	mem_heap -= sizeof (struct TaskTree);
	return (struct TaskTree *)mem_ptr(mem_heap);
}

//* Destructively update the pointer
static void mem_update_ptr(
	struct TaskTree *t, struct TaskTree *old, struct TaskTree *new)
{
	switch (t->task_type) {
	case BCStableNode_c:
		t->data.stablenode.next = new;
		break;
	case BCUnstableNode_c:
		t->data.unstablenode.next = new;
		break;
	case BCStep_c:
		t->data.step.lhs = new;
		break;
	case BCStepStable_c:
		t->data.steps.lhs = new;
		break;
	case BCStepUnstable_c:
		t->data.stepu.lhs = new;
		break;
	case BCSeqStable_c:
		t->data.seqs.lhs =
			t->data.seqs.lhs == old ? new : t->data.seqs.lhs;
		t->data.seqs.rhs =
			t->data.seqs.rhs == old ? new : t->data.seqs.rhs;
		break;
	case BCSeqUnstable_c:
		t->data.sequ.lhs =
			t->data.sequ.lhs == old ? new : t->data.sequ.lhs;
		t->data.sequ.rhs =
			t->data.sequ.rhs == old ? new : t->data.sequ.rhs;
		break;
	case BCRepeat_c:
		t->data.repeat.tree =
			t->data.repeat.tree == old ? new : t->data.repeat.tree;
		t->data.repeat.oldtree =
			t->data.repeat.oldtree == old
			? new : t->data.repeat.oldtree;
		break;
	case BCTOr_c:
		t->data.tor.lhs =
			t->data.tor.lhs == old ? new : t->data.tor.lhs;
		t->data.tor.rhs =
			t->data.tor.rhs == old ? new : t->data.tor.rhs;
		break;
	case BCTAnd_c:
		t->data.tand.lhs =
			t->data.tand.lhs == old ? new : t->data.tand.lhs;
		t->data.tand.rhs =
			t->data.tand.rhs == old ? new : t->data.tand.rhs;
		break;
	case BCSdsSet_c:
		t->data.sdsset.data = new;
		break;
	case BCReflect_c:
		t->data.reflect.task = t->data.reflect.task == old
			? new : t->data.reflect.task;
		break;
	case BCRateLimit_c:
		t->data.ratelimit.task = t->data.ratelimit.task == old
			? new : t->data.ratelimit.task;
		t->data.ratelimit.storage = t->data.ratelimit.storage == old
			? new : t->data.ratelimit.storage;
		break;
	case BCSdsUpd_c:
		t->data.sdsupd.ctx = new;
		break;
	case BCEvent_c:
		if (t == NULL) {
			events = new;
		} else {
			t->data.event.next = new;
		}
		break;
	case BCStable0_c:
		// fall through
	case BCStable1_c:
		// fall through
	case BCStable2_c:
		// fall through
	case BCStable3_c:
		// fall through
	case BCStable4_c:
		// fall through
	case BCUnstable0_c:
		// fall through
	case BCUnstable1_c:
		// fall through
	case BCUnstable2_c:
		// fall through
	case BCUnstable3_c:
		// fall through
	case BCUnstable4_c:
		// fall through
	case BCReadD_c:
		// fall through
	case BCWriteD_c:
		// fall through
	case BCReadA_c:
		// fall through
	case BCWriteA_c:
		// fall through
	case BCPinMode_c:
		// fall through
	case BCDelay_c:
		// fall through
	case BCDelayUntil_c:
		// fall through
	case BCSdsGet_c:
		// fall through
	case BCDHTTemp_c:
		// fall through
	case BCDHTHumid_c:
		// fall through
	case BCLEDMatrixDisplay_c:
		// fall through
	case BCLEDMatrixIntensity_c:
		// fall through
	case BCLEDMatrixDot_c:
		// fall through
	case BCLEDMatrixClear_c:
		// fall through
	case BCGetLight_c:
		// fall through
	case BCSetEnvironmentalData_c:
		// fall through
	case BCTVOC_c:
		// fall through
	case BCCO2_c:
		// fall through
	case BCGesture_c:
		// fall through
	case BCNeoSetPixelColor_c:
		// fall through
	case BCInterrupt_c:
		break;
	}
}

void mem_mark_trash_and_destroy(struct TaskTree *tt, struct MTaskMeta *t,
				uintptr_t *uintptr_stack)
{
	struct TaskTree **stack = (struct TaskTree **)uintptr_stack;
	msg_debug(PSTR("mem_mark_trash_and_destroy tree %u task %u stack %u\n"),
		  mem_rptr(tt), mem_rptr(t), mem_rptr(stack));
	uint16_t sp = 0;
	push_stack(stack, sp, NULL);
	push_stack(stack, sp, tt);

	while (stack[--sp] != NULL) {
		struct TaskTree *ctree = stack[sp];
		msg_debug(PSTR("pop: %u "), mem_rptr(stack[sp]));
		tasktree_print(ctree, 0);
		msg_debug(PSTR("\n"));
		switch (ctree->task_type) {
		case BCStableNode_c:
			push_stack(stack, sp, ctree->data.stablenode.next);
			break;
		case BCUnstableNode_c:
			push_stack(stack, sp, ctree->data.unstablenode.next);
			break;
		case BCStep_c:
			push_stack(stack, sp, ctree->data.step.lhs);
			break;
		case BCStepStable_c:
			push_stack(stack, sp, ctree->data.steps.lhs);
			break;
		case BCStepUnstable_c:
			push_stack(stack, sp, ctree->data.stepu.lhs);
			break;
		case BCSeqStable_c:
			push_stack(stack, sp, ctree->data.seqs.lhs);
			push_stack(stack, sp, ctree->data.seqs.rhs);
			break;
		case BCSeqUnstable_c:
			push_stack(stack, sp, ctree->data.sequ.lhs);
			push_stack(stack, sp, ctree->data.sequ.rhs);
			break;
		case BCRepeat_c:
			push_stack(stack, sp, ctree->data.repeat.oldtree);
			if (ctree->data.repeat.tree != NULL) {
				push_stack(stack, sp, ctree->data.repeat.tree);
			}
			break;
		case BCTOr_c:
			push_stack(stack, sp, ctree->data.tor.lhs);
			push_stack(stack, sp, ctree->data.tor.rhs);
			break;
		case BCTAnd_c:
			push_stack(stack, sp, ctree->data.tand.lhs);
			push_stack(stack, sp, ctree->data.tand.rhs);
			break;
		case BCSdsSet_c:
			push_stack(stack, sp, ctree->data.sdsset.data);
			break;
		case BCRateLimit_c:
			push_stack(stack, sp, ctree->data.ratelimit.task);
			if (ctree->data.ratelimit.storage != NULL) {
				push_stack(stack, sp,
					ctree->data.ratelimit.storage);
			}
			break;
		case BCSdsUpd_c:
			push_stack(stack, sp, ctree->data.sdsupd.ctx);
			break;
		case BCReflect_c:
			push_stack(stack, sp, ctree->data.reflect.task);
			break;
		case BCStable0_c:
			// fall through
		case BCStable1_c:
			// fall through
		case BCStable2_c:
			// fall through
		case BCStable3_c:
			// fall through
		case BCStable4_c:
			// fall through
		case BCUnstable0_c:
			// fall through
		case BCUnstable1_c:
			// fall through
		case BCUnstable2_c:
			// fall through
		case BCUnstable3_c:
			// fall through
		case BCUnstable4_c:
			// fall through
		case BCReadD_c:
			// fall through
		case BCWriteD_c:
			// fall through
		case BCReadA_c:
			// fall through
		case BCWriteA_c:
			// fall through
		case BCPinMode_c:
			// fall through
		case BCDelay_c:
			// fall through
		case BCDelayUntil_c:
			// fall through
		case BCSdsGet_c:
			// fall through
		case BCDHTTemp_c:
			// fall through
		case BCDHTHumid_c:
			// fall through
		case BCLEDMatrixDisplay_c:
			// fall through
		case BCLEDMatrixIntensity_c:
			// fall through
		case BCLEDMatrixDot_c:
			// fall through
		case BCLEDMatrixClear_c:
			// fall through
		case BCGetLight_c:
			// fall through
		case BCSetEnvironmentalData_c:
			// fall through
		case BCTVOC_c:
			// fall through
		case BCCO2_c:
			// fall through
		case BCGesture_c:
			// fall through
		case BCNeoSetPixelColor_c:
			// fall through
		case BCEvent_c:
			// fall through
		case BCInterrupt_c:
			break;
		}
		ctree->trash = true;
		destroy_task_node(ctree, t);
	}
}

void mem_node_move(struct TaskTree *to, struct TaskTree *fro)
{
	msg_debug(PSTR("move to: %u - from: %u\n"),
		mem_rptr(to), mem_rptr(fro));
	mem_print_heap();
	msg_debug(PSTR("----\n"));
	//We save the ptr
	bool ptr_is_tree = to->ptr_is_tree;
	union task_or_tree_ptr ptr = to->ptr;

	//t->ptr.tree = f->ptr.tree;
	//Copy everything
	*to = *fro;
	//But restore trashity and parent pointer
	to->trash = 0;
	to->ptr_is_tree = ptr_is_tree;
	to->ptr = ptr;

	//Set the rptrs
	switch (to->task_type) {
	case BCStableNode_c:
		tasktree_set_ptr_tree(to->data.stablenode.next, to);
		break;
	case BCUnstableNode_c:
		tasktree_set_ptr_tree(to->data.unstablenode.next, to);
		break;
	case BCStep_c:
		tasktree_set_ptr_tree(to->data.step.lhs, to);
		break;
	case BCStepStable_c:
		tasktree_set_ptr_tree(to->data.steps.lhs, to);
		break;
	case BCStepUnstable_c:
		tasktree_set_ptr_tree(to->data.stepu.lhs, to);
		break;
	case BCSeqStable_c:
		tasktree_set_ptr_tree(to->data.seqs.lhs, to);
		tasktree_set_ptr_tree(to->data.seqs.rhs, to);
		break;
	case BCSeqUnstable_c:
		tasktree_set_ptr_tree(to->data.sequ.lhs, to);
		tasktree_set_ptr_tree(to->data.sequ.rhs, to);
		break;
	case BCRepeat_c:
		if (to->data.repeat.tree != NULL)
			tasktree_set_ptr_tree(to->data.repeat.tree, to);
		tasktree_set_ptr_tree(to->data.repeat.oldtree, to);
		break;
	case BCTOr_c:
		tasktree_set_ptr_tree(to->data.tor.lhs, to);
		tasktree_set_ptr_tree(to->data.tor.rhs, to);
		break;
	case BCTAnd_c:
		tasktree_set_ptr_tree(to->data.tand.lhs, to);
		tasktree_set_ptr_tree(to->data.tand.rhs, to);
		break;
	case BCSdsSet_c:
		tasktree_set_ptr_tree(to->data.sdsset.data, to);
		break;
	case BCReflect_c:
		tasktree_set_ptr_tree(to->data.reflect.task, to);
		break;
	case BCRateLimit_c:
		tasktree_set_ptr_tree(to->data.ratelimit.task, to);
		if (to->data.ratelimit.storage != NULL)
			tasktree_set_ptr_tree(to->data.ratelimit.storage, to);
		break;
	case BCSdsUpd_c:
		tasktree_set_ptr_tree(to->data.sdsupd.ctx, to);
		break;
	case BCEvent_c:
		if (to->data.event.next != NULL)
			tasktree_set_ptr_tree(to->data.event.next, to);
		events = events == fro ? to : events;
		break;
	case BCStable0_c:
		// fall through
	case BCStable1_c:
		// fall through
	case BCStable2_c:
		// fall through
	case BCStable3_c:
		// fall through
	case BCStable4_c:
		// fall through
	case BCUnstable0_c:
		// fall through
	case BCUnstable1_c:
		// fall through
	case BCUnstable2_c:
		// fall through
	case BCUnstable3_c:
		// fall through
	case BCUnstable4_c:
		// fall through
	case BCReadD_c:
		// fall through
	case BCWriteD_c:
		// fall through
	case BCReadA_c:
		// fall through
	case BCWriteA_c:
		// fall through
	case BCPinMode_c:
		// fall through
	case BCDelay_c:
		// fall through
	case BCDelayUntil_c:
		// fall through
	case BCSdsGet_c:
		// fall through
	case BCDHTTemp_c:
		// fall through
	case BCDHTHumid_c:
		// fall through
	case BCLEDMatrixDisplay_c:
		// fall through
	case BCLEDMatrixIntensity_c:
		// fall through
	case BCLEDMatrixDot_c:
		// fall through
	case BCLEDMatrixClear_c:
		// fall through
	case BCGetLight_c:
		// fall through
	case BCSetEnvironmentalData_c:
		// fall through
	case BCTVOC_c:
		// fall through
	case BCCO2_c:
		// fall through
	case BCGesture_c:
		// fall through
	case BCNeoSetPixelColor_c:
		// fall through
	case BCInterrupt_c:
		break;
	}

	//Mark trash (non recursively)
	fro->trash = true;
	if (fro->ptr_is_tree) {
		fro->ptr.tree = NULL;
	}
	msg_debug(PSTR("done moving\n"));
	mem_print_heap();
}

#define task_size(t) ((uintptr_t)mem_task_next_unsafe(t)-(uintptr_t)t)
void mem_gc_tasks(void)
{
#ifdef TRACE_MEM
	uint16_t oldsize = mem_task;
#endif /* TRACE_MEM */
	mem_print_task_heap();
	msg_debug(PSTR("MEMGCTASKS, mem_task: %lu\n"), mem_task);
	void print_queue(void);
	print_queue();
	uint16_t offset = 0;
	struct MTaskMeta *t = mem_task_head();
	while (t != NULL) {
		msg_debug(PSTR("Task %u, at %p (%u) status: %u, size: %u\n"),
			t->taskid, t, mem_rptr(t), t->status, task_size(t));
		msg_debug(PSTR("offset: %lu\n"), offset);
		struct MTaskMeta *next = mem_task_next(t);
		//Task is removed, so we need to move other tasks
		if (t->status == MTRemoved_c) {
			offset += task_size(t);
		} else {
			struct MTaskMeta *newloc =
				(struct MTaskMeta *)((uintptr_t) t - offset);
			//Update the pointers in the queue
			queue_update_pointer(t, newloc);

			//Calculate the task size
			uint16_t to = mem_rptr(t)+task_size(t);

			//Offset the pointers
			task_move(t, offset);

			// Move the actual task data (struct)
			for (uint16_t i = mem_rptr(t); i<to; i++) {
				mtmem[i-offset] = mtmem[i];
			}
		}
		t = next;
	}
	mem_task -= offset;
	mem_print_task_heap();
	print_queue();
#ifdef TRACE_MEM
	if (oldsize != mem_task)
		msg_log(PSTR("memtrace: after gc task total %u\n"), mem_task);
#endif /* TRACE_MEM */
}

void mem_gc(void)
{
#ifdef TRACE_MEM
	uint16_t oldsize = memsize-mem_heap;
#endif /* TRACE_MEM */
	msg_debug(PSTR("start with mem_gc()\n"));
//	msg_debug(PSTR("MEMGC\n"));
	mem_print_heap();

	uint16_t walker, hole;
	walker = hole = memsize - sizeof(struct TaskTree);
	while (walker >= mem_heap) {
		//Is trash, pass over but keep hole pointer valid
		if (mem_cast_tree(walker)->trash) {
			walker -= sizeof(struct TaskTree);
		//Not trash and no hole
		} else if (walker == hole) {
			hole = (walker -= sizeof(struct TaskTree));
		//Not trash and a hole
		} else {
			msg_debug(PSTR("no trash and hole, move\n"));
			tasktree_print_node(mem_cast_tree(walker));
			msg_debug(PSTR("\nwalker: %u - hole: %u\n"),
				walker, hole);

			struct TaskTree *t = mem_cast_tree(hole);
			//Move and correct rptrs
			tasktree_assign_ptr(t, mem_cast_tree(walker));

			mem_node_move(t, mem_cast_tree(walker));

			//Update parent
			if (t->task_type == BCEvent_c && t->ptr.tree == NULL) {
				events = mem_ptr(hole);
			} else if (t->ptr_is_tree) {
				msg_debug(PSTR("Parent is a tasktree\n"));
				mem_update_ptr(mem_cast_tree(hole)->ptr.tree,
					mem_ptr(walker), mem_ptr(hole));
			} else {
				msg_debug(PSTR("Parent is a task\n"));
				t->ptr.task->tree = mem_ptr(hole);
			}
			hole -= sizeof(struct TaskTree);
			walker -= sizeof(struct TaskTree);
		}
	}
	if (walker != hole) {
		msg_debug(PSTR("mem_heap: %u, walker: %u - hole: %u\n"),
			mem_heap, walker, hole);
		msg_debug(PSTR("Compacted %u bytes\n"), hole - walker);
		mem_heap = hole+sizeof(struct TaskTree);
		msg_debug(PSTR("mem_heap: %u, walker: %u - hole: %u\n"),
			mem_heap, walker, hole);
	}
	msg_debug(PSTR("done with mem_gc()\n"));
	mem_print_heap();
#ifdef TRACE_MEM
	if (oldsize != memsize-mem_heap)
		msg_log(PSTR("memtrace: tree after gc total %u\n"),
			memsize-mem_heap);
#endif /* TRACE_MEM */
}

bool mem_iterate_tasktrees(bool (*f)(struct TaskTree *))
{
	uint16_t walker = memsize - sizeof(struct TaskTree);
	while (walker >= mem_heap) {
		struct TaskTree *t = mem_cast_tree(walker);
		if (!f (t)) {
			return false;
		}
		walker -= sizeof(struct TaskTree);
	}
	return true;
}
