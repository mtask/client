#include "interface.h"
#include "mem.h"

#include "comm_interface.h"
#include "task.h"
#include "tasktree.h"
#include "scheduler.h"
#include "preload.h"

extern struct MTaskMeta *current_task;
extern struct TaskTree *events;

static uint16_t memsize
	= MEMSIZE - 1 - (MEMSIZE - 1) % sizeof (struct TaskTree);
static void *mtmem_real[MEMSIZE/sizeof(void *)] = {NULL};
static uint8_t *mtmem = (uint8_t *)&mtmem_real;

//Variable pointers
uint16_t mem_task;
static uint16_t mem_heap;

void *mem_ptr(uint16_t i)
{
	return i == MT_NULL ? NULL : (void *)(mtmem + i);
}

uint16_t mem_rptr(void *p)
{
	return p == NULL ? MT_NULL : ((uint8_t *)p)-mtmem;
}

static void mem_print_heap()
{
#if LOGLEVEL == 2
	uint16_t walker;
	walker = memsize - sizeof(struct TaskTree);
	while (walker >= mem_heap) {
		struct TaskTree *t = mem_cast_tree(walker);
		msg_debug(PSTR("%d (rptr: %p, trash: %d, refr: [%u, %u]): "),
			walker, t->ptr.tree, t->trash,
			t->refresh_min * (t->seconds ? 1000u : 1u),
			t->refresh_max * (t->seconds ? 1000u : 1u));
		tasktree_print_node(t);
		walker -= sizeof(struct TaskTree);
		msg_debug(PSTR("\n"));
	}
#endif /* LOGLEVEL == 2 */
}

static void mem_print_task_heap()
{
#if LOGLEVEL == 2
	msg_debug(PSTR("TASKS\n"));
	struct MTaskMeta *t = mem_task_head();
	while (t != NULL) {
		task_print(t, false);
		t = mem_task_next(t);
	}
	msg_debug(PSTR("ENDTASKS\n"));
#endif /* LOGLEVEL == 2 */
}

void mem_reset(void)
{
	mem_task = 0;
	mem_heap = memsize;

	msg_log(PSTR("sizeof(struct MTMessageTo)  : %02x\n"),
		sizeof(struct MTMessageTo));
	msg_log(PSTR("sizeof(struct MTMessageFrom): %02x\n"),
		sizeof(struct MTMessageFro));
	msg_log(PSTR("sizeof(struct MTaskMeta)    : %02x\n"),
		sizeof(struct MTaskMeta));
	msg_log(PSTR("sizeof(struct TaskTree)     : %02x\n"),
		sizeof(struct TaskTree));
	msg_log(PSTR("mem_top: %p\n"), mem_ptr(memsize));
	msg_log(PSTR("mem_bottom: %p\n"), mem_ptr(0));
	msg_log(PSTR("free bytes: %llu\n"),
		(size_t)mem_heap - (size_t)mem_task);

	mem_print_task_heap();
}

uint16_t *mem_stack(void)
{
	//This is aligned because tasks are always PTRSIZE bytes aligned
	return mem_ptr(mem_task);
}

uint16_t *mem_max_sp(void)
{
	return mem_ptr(mem_heap);
}

void mem_set_stack_ptr(void *ptr)
{
	mem_task = mem_rptr(ptr);
}

void *mem_alloc_task(size_t size)
{
	msg_debug(PSTR("mem_alloc_task(%lu) mem_task: %p (%u)\n"),
		size, mem_ptr(mem_task), mem_task);

	if (size == 0)
		return mem_ptr(mem_task);

#ifdef REQUIRE_ALIGNED_MEMORY_ACCESS
	mem_task += PALIGND(size, mem_ptr(mem_task));
#endif /* REQUIRE_ALIGNED_MEMORY_ACCESS */
	void *r = mem_ptr(mem_task);

	msg_debug(PSTR("mem_alloc_task(%lu) mem_task: %p (%u)\n"),
		size, r, mem_task);

	if (mem_task + size > mem_heap) {
		send_message((struct MTMessageFro)
			{ .cons=MTFException_c
			, .data={.MTFException=
				{ .cons = MTEOutOfMemory_c
				, .data = {.MTEOutOfMemory=0}
				}
			}});
		return NULL;
	}
	mem_task += size;
#ifdef TRACE_MEM
	msg_log("memtrace: task: %u allocated, total %u\n", size, mem_task);
#endif
	return r;
}

struct MTaskMeta *mem_task_head()
{
	return mem_task == 0 ? NULL : mem_cast_task(0);
}

struct MTaskMeta *mem_task_next_unsafe(struct MTaskMeta *t)
{
	return (struct MTaskMeta *)\
		(t->instructions.elements+t->instructions.size);
}
struct MTaskMeta *mem_task_next(struct MTaskMeta *t)
{
	struct MTaskMeta *r = mem_task_next_unsafe(t);
	if (mem_rptr(r) >= mem_task)
		r = NULL;
	return r;
}

struct TaskTree *mem_alloc_tree()
{
	msg_debug(PSTR("general alloc\n"));
	if (mem_heap - sizeof(struct TaskTree) < mem_task) {
		msg_log(PSTR("Not enough heap\n"));
		send_message((struct MTMessageFro)
			{ .cons=MTFException_c
			, .data={.MTFException=
				{.cons=MTEHeapUnderflow_c
				,.data={.MTEHeapUnderflow=current_task->taskid}
				}
			}});
		return NULL;
	}
	msg_debug(PSTR("at: %p\n"), mem_cast_tree(mem_heap));
#ifdef TRACE_MEM
	msg_log("memtrace: tree: %u allocated, total %u\n",
		sizeof(struct TaskTree), memsize-mem_heap);
#endif

	return (struct TaskTree *)mem_ptr(mem_heap -= sizeof (struct TaskTree));
}

//* Destructively update the pointer
static void mem_update_ptr(
	struct TaskTree *t, struct TaskTree *old, struct TaskTree *new)
{
	switch (t->task_type) {
	case BCStableNode_c:
		t->data.stablenode.next = new;
		break;
	case BCUnstableNode_c:
		t->data.unstablenode.next = new;
		break;
	case BCStep_c:
		t->data.step.lhs = new;
		break;
	case BCStepStable_c:
		t->data.steps.lhs = new;
		break;
	case BCStepUnstable_c:
		t->data.stepu.lhs = new;
		break;
	case BCSeqStable_c:
		t->data.seqs.lhs =
			t->data.seqs.lhs == old ? new : t->data.seqs.lhs;
		t->data.seqs.rhs =
			t->data.seqs.rhs == old ? new : t->data.seqs.rhs;
		break;
	case BCSeqUnstable_c:
		t->data.sequ.lhs =
			t->data.sequ.lhs == old ? new : t->data.sequ.lhs;
		t->data.sequ.rhs =
			t->data.sequ.rhs == old ? new : t->data.sequ.rhs;
		break;
	case BCRepeat_c:
		t->data.repeat.tree =
			t->data.repeat.tree == old ? new : t->data.repeat.tree;
		t->data.repeat.oldtree =
			t->data.repeat.oldtree == old
			? new : t->data.repeat.oldtree;
		break;
	case BCTOr_c:
		t->data.tor.lhs =
			t->data.tor.lhs == old ? new : t->data.tor.lhs;
		t->data.tor.rhs =
			t->data.tor.rhs == old ? new : t->data.tor.rhs;
		break;
	case BCTAnd_c:
		t->data.tand.lhs =
			t->data.tand.lhs == old ? new : t->data.tand.lhs;
		t->data.tand.rhs =
			t->data.tand.rhs == old ? new : t->data.tand.rhs;
		break;
	case BCSdsSet_c:
		t->data.sdsset.data = new;
		break;
	case BCRateLimit_c:
		t->data.ratelimit.task = t->data.ratelimit.task == old
			? new : t->data.ratelimit.task;
		t->data.ratelimit.storage = t->data.ratelimit.storage == old
			? new : t->data.ratelimit.storage;
		break;
	case BCSdsUpd_c:
		t->data.sdsupd.ctx = new;
		break;
	case BCEvent_c:
		if (t == NULL) {
			events = new;
		} else {
			t->data.event.next = new;
		}
		break;
	default:
		break;
	}
}

void mem_mark_trash_and_destroy(struct TaskTree *tt, struct MTaskMeta *t,
				uint16_t *stack)
{
	msg_debug(PSTR("mem_mark_trash_and_destroy tt %p t %p stack %p\n"),
		  tt, t, stack);
	*stack++ = MT_NULL;
	*stack++ = mem_rptr(tt);
	while (*--stack != MT_NULL) {
		struct TaskTree *ctree = mem_cast_tree(*stack);
		msg_debug(PSTR("mem_mark_trash_and_destroy: %u "), *stack);
		tasktree_print(ctree, 0);
		msg_debug(PSTR("\n"));
		switch (ctree->task_type) {
			case BCStableNode_c:
				*stack++ =
					mem_rptr(ctree->data.stablenode.next);
				break;
			case BCUnstableNode_c:
				*stack++ =
					mem_rptr(ctree->data.unstablenode.next);
				break;
			case BCStep_c:
				*stack++ = mem_rptr(ctree->data.step.lhs);
				break;
			case BCStepStable_c:
				*stack++ = mem_rptr(ctree->data.steps.lhs);
				break;
			case BCStepUnstable_c:
				*stack++ = mem_rptr(ctree->data.stepu.lhs);
				break;
			case BCSeqStable_c:
				*stack++ = mem_rptr(ctree->data.seqs.lhs);
				*stack++ = mem_rptr(ctree->data.seqs.rhs);
				break;
			case BCSeqUnstable_c:
				*stack++ = mem_rptr(ctree->data.sequ.lhs);
				*stack++ = mem_rptr(ctree->data.sequ.rhs);
				break;
			case BCRepeat_c:
				*stack++ =
					mem_rptr(ctree->data.repeat.oldtree);
				if (ctree->data.repeat.tree != NULL)
					*stack++ = mem_rptr(
						ctree->data.repeat.tree);
				break;
			case BCTOr_c:
				*stack++ = mem_rptr(ctree->data.tor.lhs);
				*stack++ = mem_rptr(ctree->data.tor.rhs);
				break;
			case BCTAnd_c:
				*stack++ = mem_rptr(ctree->data.tand.lhs);
				*stack++ = mem_rptr(ctree->data.tand.rhs);
				break;
			case BCSdsSet_c:
				*stack++ = mem_rptr(ctree->data.sdsset.data);
				break;
			case BCRateLimit_c:
				*stack++ = mem_rptr(ctree->data.ratelimit.task);
				if (ctree->data.ratelimit.storage != NULL)
					*stack++ = mem_rptr(
						ctree->data.ratelimit.storage);
				break;
			case BCSdsUpd_c:
				*stack++ = mem_rptr(ctree->data.sdsupd.ctx);
				break;
			default:
				break;
		}
		ctree->trash = true;
		destroy_task_node(ctree, t);
	}
}

void mem_node_move(struct TaskTree *t, struct TaskTree *f)
{
	msg_debug(PSTR("move %lu - %lu\n"), mem_rptr(t), mem_rptr(f));
	mem_print_heap();
	msg_debug(PSTR("----\n"));
	//Copy the data
	t->task_type = f->task_type;
	t->trash = 0;
	t->seconds = f->seconds;
	t->refresh_min = f->refresh_min;
	t->refresh_max = f->refresh_max;
	t->data = f->data;
	//We leave the ptr in tact
	//t->ptr.tree = f->ptr.tree;

	//Set the rptrs
	switch (t->task_type) {
	case BCStableNode_c:
		t->data.stablenode.next->ptr.tree = t;
		break;
	case BCUnstableNode_c:
		t->data.unstablenode.next->ptr.tree = t;
		break;
	case BCStep_c:
		t->data.step.lhs->ptr.tree = t;
		break;
	case BCStepStable_c:
		t->data.steps.lhs->ptr.tree = t;
		break;
	case BCStepUnstable_c:
		t->data.stepu.lhs->ptr.tree = t;
		break;
	case BCSeqStable_c:
		t->data.seqs.lhs->ptr.tree = t;
		t->data.seqs.rhs->ptr.tree = t;
		break;
	case BCSeqUnstable_c:
		t->data.sequ.lhs->ptr.tree = t;
		t->data.sequ.rhs->ptr.tree = t;
		break;
	case BCRepeat_c:
		if (t->data.repeat.tree != NULL)
			t->data.repeat.tree->ptr.tree = t;
		t->data.repeat.oldtree->ptr.tree = t;
		break;
	case BCTOr_c:
		t->data.tor.lhs->ptr.tree = t;
		t->data.tor.rhs->ptr.tree = t;
		break;
	case BCTAnd_c:
		t->data.tand.lhs->ptr.tree = t;
		t->data.tand.rhs->ptr.tree = t;
		break;
	case BCSdsSet_c:
		t->data.sdsset.data->ptr.tree = t;
		break;
	case BCRateLimit_c:
		t->data.ratelimit.task->ptr.tree = t;
		if (t->data.ratelimit.storage != NULL)
			t->data.ratelimit.storage->ptr.tree = t;
		break;
	case BCSdsUpd_c:
		t->data.sdsupd.ctx->ptr.tree = t;
		break;
	case BCEvent_c:
		if (t->data.event.next != NULL) {
			t->data.event.next->ptr.tree = t;
		}
		events = events == f ? t : events;
		break;
	default:
		break;
	}

	//Mark trash (non recursively)
	if (mem_rptr(f) >= mem_heap) {
		f->trash = true;
		f->ptr.tree = NULL;
	}
	msg_debug(PSTR("done moving\n"));
	mem_print_heap();
}

#define task_size(t) ((uintptr_t)mem_task_next_unsafe(t)-(uintptr_t)t)
void mem_gc_tasks(void)
{
#ifdef TRACE_MEM
	uint16_t oldsize = mem_task;
#endif
	mem_print_task_heap();
	msg_debug(PSTR("MEMGCTASKS, mem_task: %lu\n"), mem_task);
	void print_queue();
	print_queue();
	uint16_t offset = 0;
	struct MTaskMeta *t = mem_task_head();
	while (t != NULL) {
		msg_debug(PSTR("Task %u, at %p (%u) status: %u, size: %u\n"),
			t->taskid, t, mem_rptr(t), t->status, task_size(t));
		msg_debug(PSTR("offset: %lu\n"), offset);
		struct MTaskMeta *next = mem_task_next(t);
		//Task is removed, so we need to move other tasks
		if (t->status == MTRemoved_c) {
			offset += task_size(t);
		} else {
			struct MTaskMeta *newloc =
				(struct MTaskMeta *)((uintptr_t) t - offset);
			//Update the pointers in the queue
			queue_update_pointer(t, newloc);

			//Calculate the task size
			uint16_t to = mem_rptr(t)+task_size(t);

			//Offset the pointers
			task_move(t, offset);

			// Move the actual task data (struct)
			for (uint16_t i = mem_rptr(t); i<to; i++) {
				mtmem[i-offset] = mtmem[i];
			}
		}
		t = next;
	}
	mem_task -= offset;
	mem_print_task_heap();
	print_queue();
#ifdef TRACE_MEM
	if (oldsize != mem_task)
		msg_log("memtrace: after gc task total %u\n", mem_task);
#endif
}

void mem_gc(void)
{
#ifdef TRACE_MEM
	uint16_t oldsize = memsize-mem_heap;
#endif
//	msg_debug(PSTR("MEMGC\n"));
	mem_print_heap();

	uint16_t walker, hole;
	walker = hole = memsize - sizeof(struct TaskTree);
	while (walker >= mem_heap) {
		//Is trash, pass over but keep hole pointer valid
		if (mem_cast_tree(walker)->trash) {
			walker -= sizeof(struct TaskTree);
		//Not trash and no hole
		} else if (walker == hole) {
			hole = walker -= sizeof(struct TaskTree);
		//Not trash and a hole
		} else {
			msg_debug(PSTR("no trash and hole, move\n"));
			tasktree_print_node(mem_cast_tree(walker));
			msg_debug(PSTR("\nwalker: %u - hole: %u\n"),
				walker, hole);

			struct TaskTree *t = mem_cast_tree(hole);
			//Move and correct rptrs
			t->ptr.tree = mem_cast_tree(walker)->ptr.tree;
			mem_node_move(t, mem_cast_tree(walker));

			//Update parent
			if (t->task_type == BCEvent_c && t->ptr.tree == NULL) {
				events = mem_ptr(hole);
			} else if (mem_rptr(t->ptr.task) < mem_heap) {
				msg_debug(PSTR("Parent is a task\n"));
				t->ptr.task->tree = mem_ptr(hole);
			} else {
				msg_debug(PSTR("Parent is a tasktree\n"));
				mem_update_ptr(mem_cast_tree(hole)->ptr.tree,
					mem_ptr(walker), mem_ptr(hole));
			}
			hole -= sizeof(struct TaskTree);
			walker -= sizeof(struct TaskTree);
		}
	}
	if (walker != hole) {
		msg_debug(PSTR("mem_heap: %u, walker: %u - hole: %u\n"),
			mem_heap, walker, hole);
		msg_debug(PSTR("Compacted %u bytes\n"), hole - walker);
		mem_heap = hole+sizeof(struct TaskTree);
		msg_debug(PSTR("mem_heap: %u, walker: %u - hole: %u\n"),
			mem_heap, walker, hole);
	}
	mem_print_heap();
#ifdef TRACE_MEM
	if (oldsize != memsize-mem_heap)
		msg_log("memtrace: tree after gc total %u\n", memsize-mem_heap);
#endif
}
