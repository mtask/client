/** @file preload.h
 *
 * Contains all the functions to work with device specifications
 */
#ifndef PRELOAD_H
#define PRELOAD_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdbool.h>
#include "types.h"

/**
 * Load a task from program memory, i.e. a preloaded task
 *
 * @param data a struct containing the taskid and the hash
 * @return success
 */
bool preload_task(struct MTaskPrepData data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !PRELOAD_H */

