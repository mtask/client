#include "interface.h"

#if defined(_WIN32) || defined(__APPLE__) || defined (__linux__)\
	|| defined (__unix__)
#ifdef _WIN32
#ifndef WINVER
#define WINVER 0x0501
#endif /* !WINVER */
#endif /* _WIN32 */

#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#else /* _WIN32 */
#include <sys/time.h>
#endif /* _WIN32 */

#include "client.h"
#include "pc/interface.h"
#include "interrupts.h"
#include "mem.h"

// Command line arguments
int gargc;
char **gargv;

// Interface specific arguments
#define MAXICARGC 100
int iargc = 0;
char *iargv[MAXICARGC+1] = {0};

// NVM stuff
char *ic_swapfile = "mtaskswap";

//Restore from sigfpe
jmp_buf fpe;

extern struct SemVer server_version;
extern struct SemVer client_version;

extern char *default_client_id;
extern uint16_t default_keep_alive_time;
extern uint16_t default_will_delay_interval;
extern uint32_t default_ping;
extern char *default_host;
extern uint16_t default_port;

static void real_exit(void)
{
	#ifndef MQTT
		cleanup_communication();
	#endif /* MQTT */
	pc_exit();
	exit(EXIT_SUCCESS);
}

void killHandler(int i)
{
	if (i == SIGFPE) {
#ifdef _WIN32
		signal(SIGFPE,  killHandler);
#endif /* _WIN32 */
		longjmp(fpe, 1);
	}
#ifndef _WIN32
	if (i == SIGPIPE) {
		msg_log("\nSIGPIPE caught, resetting\n", i);
		reset();
		return;
	}
#endif /* _WIN32 */
	msg_log("\n%i caught, Bye...\n", i);

	real_exit();
}

#ifdef _WIN32
int gettimeofday(struct timeval * tp, void *tzp)
{
	(void)tzp;
	static const uint64_t EPOCH = ((uint64_t) 116444736000000000ULL);

	SYSTEMTIME  system_time;
	FILETIME    file_time;
	uint64_t    time;

	GetSystemTime(&system_time);
	SystemTimeToFileTime(&system_time, &file_time);
	time = ((uint64_t)file_time.dwLowDateTime);
	time += ((uint64_t)file_time.dwHighDateTime) << 32;

	tp->tv_sec = (long)((time - EPOCH) / 10000000L);
	tp->tv_usec = (long)(system_time.wMilliseconds * 1000);
	return 0;
}
#endif /* _WIN32 */

uint32_t getmillis(void)
{
	struct timeval tv;
	if (gettimeofday(&tv, NULL) == -1)
		pdie("gettimeofday");
	return tv.tv_sec*1000 + tv.tv_usec/1000;
}

void parse_semantic_version(char *version, struct SemVer *result, char *name)
{
	char *nptr = version, *endptr = NULL;
	long major = 0, minor = 0, patch = 0;
	major = strtol(nptr, &endptr, 10);
	if (*endptr == '.') {
		nptr = endptr + 1;
		minor = strtol(nptr, &endptr, 10);
		if (*endptr == '.') {
			nptr = endptr + 1;
			patch = strtol(nptr, &endptr, 10);
			if (! (*nptr != '\0' && *endptr == '\0')) {
				die("Could not parse %s version: '%s'\n",
					name, version);
			}
		} else if (*nptr != '\0' && *endptr == '\0') {
			patch = 0;
		} else {
			die("Could not parse %s version: '%s'\n",
				name, version);
		}
	} else if (*version != '\0' && *endptr == '\0') {
		minor = 0;
		patch = 0;
	} else {
		die("Could not parse %s version: '%s'\n", name, version);
	}
	if (major < 0 || major > 255)
		die("Major %s version not between 0-255: %ld.\n", name, major);
	if (minor < 0 || minor > 255)
		die("Minor %s version not between 0-255: %ld.\n", name, minor);
	if (patch < 0 || patch > 255)
		die("Minor %s version not between 0-255: %ld.\n", name, patch);
	*result = (struct SemVer) {.major=major, .minor=minor, .patch=patch};
}

bool dpins[DPINS] = {false};
void write_dpin(uint16_t i, bool b)
{
	dpins[i] = b;
	pc_write_dpin(i, b);
}

bool read_dpin(uint16_t i)
{
	pc_read_dpin(i);
	return dpins[i];
}

uint8_t apins[APINS] = {0};
void write_apin(uint16_t i, uint8_t a)
{
	apins[i] = a;
	pc_write_apin(i, a);
}

uint8_t read_apin(uint16_t i)
{
	pc_read_apin(i);
	return apins[i];
}

void set_pinmode(uint16_t p, enum PinMode mode)
{
	pc_set_pinmode(p, mode);
}

uint16_t translate_pin(uint16_t i)
{
	return i>>1;
}

void attach_interrupt(void (*isr)(void), uint16_t pin, enum InterruptMode mode)
{
	msg_log("attach_interrupt pin %d and mode %d\n", pin, mode);
	(void) isr;
	(void) pin;
	(void) mode;
}

void detach_interrupt(uint16_t pin)
{
	(void) pin;
}

void isr(void) {
	// Left empty on purpose, because interrupts are never triggered on PC
}

void (*get_isr(uint16_t pin, enum InterruptMode mode))(void)
{
	(void) mode;

	if (pin > MAX_INTERRUPT_PIN) {
		return NULL;
	}

	return isr;
}

static struct DHTInfo *dht;
void *dht_init(struct DHTInfo a)
{
	dht = malloc(sizeof (struct DHTInfo));
	msg_log("dht init at %p\n", dht);
	*dht = a;
	switch (a.cons) {
	case DHT_DHT_c:
		msg_log("init DHT\n");
		break;
	case DHT_SHT_c:
		msg_log("init SHT on address: 0x%x\n", a.data.DHT_SHT);
		break;
	}
	return dht;
}
void dht_destroy(struct BCPeripheral *p) { free(p->st); }

float temperature = 42.0, humidity = 38.0;
float get_dht_temp(struct BCPeripheral *p)
{
	pc_temperature(p->perinfo.data.BCDHT);
	return temperature;
}

float get_dht_humidity(struct BCPeripheral *p)
{
	pc_humidity(p->perinfo.data.BCDHT);
	return humidity;
}

void *lightsensor_init(LightSensorInfo info)
{
	msg_log("lightsensor init at addr %x\n", info);
	return (void *)1;
	(void)info;
}
void lightsensor_destroy(struct BCPeripheral *p) { (void) p; }

float lightlevel = 4.20;
float get_light(struct BCPeripheral *st)
{
	pc_light();
	return lightlevel;
	(void)st;
}
enum Gesture gesture = GNone_c;
void *gesturesensor_init(GestureSensorInfo info) {
	msg_log("gesturesensor init at addr %x\n", info);
	return (void *)1;
	(void)info;
}
void gesturesensor_destroy(struct BCPeripheral *p) { (void) p; }

enum Gesture get_gesture(struct BCPeripheral *st) {
	return gesture;
	(void)st;
}

int tvoc = 38, eco2 = 42;
void *airqualitysensor_init(struct AirQualitySensorInfo info) {
	switch (info.cons) {
	case AQS_CCS811_c:
		msg_log("init CCS811 on address: 0x%x\n", info.data.AQS_CCS811);
		break;
	case AQS_SGP30_c:
		msg_log("init SGQ30 on address\n");
		break;
	}
	return (void *)1;
}
void airqualitysensor_destroy(struct BCPeripheral *p) { (void) p; }

void set_environmental_data(struct BCPeripheral *st, float humid, float temp) {
	msg_log(PSTR("set environment: %f, %f\n"), humid, temp);
	(void)humid;
	(void)temp;
	(void)st;
}

uint32_t get_tvoc(struct BCPeripheral *p)
{
	pc_tvoc(p->perinfo.data.BCAirQualitySensor);
	return tvoc;
}

uint32_t get_co2(struct BCPeripheral *p)
{
	pc_co2(p->perinfo.data.BCAirQualitySensor);
	return eco2;
}

bool ledmatrix[8][8] = {{false}};
void *ledmatrix_init(struct LEDMatrixInfo x)
{
	return (void *)1;
	(void) x;
}
void ledmatrix_destroy(struct BCPeripheral *p) { (void) p; }

void ledmatrix_dot(struct BCPeripheral *st, uint8_t x, uint8_t y, bool state)
{
	ledmatrix[x][y] = state;
	pc_lmdot(x, y, state);
	(void)st;
}

void ledmatrix_intensity(struct BCPeripheral *st, uint8_t intensity)
{
	pc_lmintensity(intensity);
	(void)st;
}

void ledmatrix_clear(struct BCPeripheral *st)
{
	for (int x = 0; x<8; x++)
		for (int y = 0; y<8; y++)
			ledmatrix[x][y] = false;
	pc_lmclear();
	(void)st;
}

void ledmatrix_display(struct BCPeripheral *st)
{
	pc_lmdisplay();
	(void)st;
}


int neopixel[7][3] = {0};
void* neopixel_init (struct NeoInfo x) {
	return (void *) 1;
	(void) x;
}

void neopixel_destroy (struct BCPeripheral *st) { (void)st; }

void neopixel_setPixelColor (struct BCPeripheral *st,
		uint16_t n, uint8_t r, uint8_t g, uint8_t b) {
	neopixel[n][0] = r;
	neopixel[n][1] = g;
	neopixel[n][2] = b;
	pc_setPixelColor(n, r, g, b);
	(void)st;
}

void msdelay(uint32_t ms)
{
#ifdef _WIN32
	Sleep(ms);
#else /* WIN32 */
	struct timespec ts = {
		.tv_sec=ms/1000,
		.tv_nsec=(ms % 1000) *1000*1000
	};
	nanosleep(&ts, NULL);
#endif /* WIN32 */
}

void mssleep(uint32_t ms)
{
	msdelay(ms);
}

void force_wake_up(void) {}

#ifndef _WIN32
static struct sigaction act;
#endif /* !_WIN32 */

static void usage(FILE *o, char *arg0)
{
	fprintf(o, "Usage: %s [opts] -- [interface specific opts]\n"
		"\n"
		"General options\n"
		"-h, --help                 "
			"Show this help\n"
		"    --version              "
			"Show the version\n"
		"-p, --port=PORT            "
			"PORT Custom port number\n"
		"    --spoof-server=VERSION "
			"Spoof mTask server version.\n"
		"    --spoof-client=VERSION "
			"Spoof mTask client version.\n"
#ifdef NVM
		"-s, --swap=FILE            "
			"Swap file (without extension [default: mtaskswap])\n"
		"-r, --resume               "
			"Resume from swap file\n"
#endif /* NVM */
#if defined(COMM_DIRECT)
		"\n"
		"Direct communication options:\n"
		"    --ping-timeout[=TIME]  "
			"Ping timout in ms, no argument disables it.\n"
#endif /* COMM_DIRECT */
#if defined(COMM_MQTT)
		"\n"
		"MQTT options:\n"
		"    --host=HOST            "
			"Custom mosquitto host.\n"
		"-c, --clean-session        "
			"Use a clean session when connecting to the broker.\n"
		"    --keep-alive=TIME      "
			"Custom keep-alive time.\n"
		"    --client-id=CLIENTID   "
			"Name of this client.\n"
#endif /* COMM_MQTT */
		"\n"
		, arg0);
	pc_usage(o);
}

static void version(FILE *o)
{
	fprintf(o, "mtask client version: %d.%d.%d\n", client_version.major,
		client_version.minor, client_version.patch);
	fprintf(o, "compiled with:\n");
#ifdef _WIN32
	fprintf(o, "os: windows\n");
#elif defined(__APPLE__)
	fprintf(o, "os: apple\n");
#else /* WIN32 or __APPLE__ */
	fprintf(o, "os: linux\n");
#endif /* WIN32 or __APPLE__ */
	fprintf(o, "LOGLEVEL: %u\n", LOGLEVEL);
#ifdef TRACE_MEM
	fprintf(o, "TRACE_MEM\n");
#endif /* TRACE_MEM */
#ifdef CURSES_INTERFACE
	fprintf(o, "interface: curses");
# ifdef NOMOUSE
	fprintf(o, " (mouse support disabled)\n");
# else /* NOMOUSE */
	fprintf(o, "\n");
# endif /* NOMOUSE */
#else /* CURSES_INTERFACE */
# ifdef _WIN32
	fprintf(o, "interface: cmd\n");
# else /* _WIN32 */
	fprintf(o, "interface: tty\n");
# endif /* _WIN32 */
#endif /* CURSES_INTERFACE */
	fprintf(o, "MAX_SLEEP_TIME: %u\n", MAX_SLEEP_TIME);
	fprintf(o, "DEFAULT_SLEEP_TIME: %u\n", DEFAULT_SLEEP_TIME);
	fprintf(o, "\n");
#ifdef LINK_TCP
	fprintf(o, "TCP link settings:\n");
	fprintf(o, "LINK_PORT: %u\n", LINK_PORT);
# ifdef COMM_MQTT
	fprintf(o, "LINK_HOST: %s\n", LINK_HOST);
# endif /* COMM_MQTT */
	fprintf(o, "\n");
# ifdef COMM_DIRECT
	fprintf(o, "direct communication settings\n");
	if (COMM_PING == 0) {
		fprintf(o, "COMM_PING: disable\n");
	} else {
		fprintf(o, "COMM_PING: %u\n", COMM_PING);
	}
# endif /* COMM_DIRECT */
# ifdef COMM_MQTT
	fprintf(o, "MQTT communication settings\n");
	fprintf(o, "KEEP_ALIVE_TIME: %u\n", KEEP_ALIVE_TIME);
	fprintf(o, "WILL_DELAY_INTERVAL: %u\n", WILL_DELAY_INTERVAL);
	fprintf(o, "CLIENT_ID: %s\n",
		CLIENT_ID == NULL ? "randomly generated" : CLIENT_ID);
# endif /* COMM_MQTT */
#endif /* LINK_TCP */
	fprintf(o, "some memory info:\n\n");
	fprintf(o, "memsize: %u\n", memsize);
	fprintf(o, "MEMSIZE (bytes): %d\n", MEMSIZE);
	fprintf(o, "mtmem_real: %p\n", (void *)&mtmem_real[0]);
	fprintf(o, "free bytes: %zd\n", (size_t)mem_heap - (size_t)mem_task);
}

bool real_setup(void)
{
	bool ic_resume = true;
	// init random number generator
	srand((unsigned) time(NULL));

#ifndef _WIN32
	memset(&act, 0, sizeof(act));
	act.sa_handler = killHandler;
	act.sa_flags = SA_NODEFER;
	if (
			sigaction(SIGFPE,  &act, NULL) == -1 ||
			sigaction(SIGINT,  &act, NULL) == -1 ||
			sigaction(SIGTERM, &act, NULL) == -1) {
		pdie("sigaction");
	}
	act.sa_handler = SIG_IGN;
	if (sigaction(SIGPIPE, &act, NULL) == -1) {
		pdie("sigaction");
	}
#else /* !_WIN32 */
	if (
			signal(SIGFPE,  killHandler) == SIG_ERR ||
			signal(SIGINT,  killHandler) == SIG_ERR ||
			signal(SIGTERM,  killHandler) == SIG_ERR) {
		pdie("sigaction");
	}
#endif /* !_WIN32 */

	//Command line arguments
	static struct option lopts[] = {
		/* NAME          ARGUMENT           FLAG  SHORTNAME */
		{"help",         no_argument,       NULL, 'h'},
		{"version",      no_argument,       NULL, 1},
		{"port",         required_argument, NULL, 'p'},
		{"spoof-server", required_argument, NULL, 2},
		{"spoof-client", required_argument, NULL, 3},
#ifdef NVM
		{"no-resume",    no_argument,       NULL, 'r'},
		{"swap",         required_argument, NULL, 's'},
#endif /* NVM */
#ifdef COMM_DIRECT
		{"ping-timeout", optional_argument, NULL, 4},
#endif /* COMM_DIRECT */
#ifdef COMM_MQTT
		{"host",         required_argument, NULL, 5},
		{"keep-alive",   required_argument, NULL, 6},
		{"will-delay",   required_argument, NULL, 7},
		{"client-id",    required_argument, NULL, 8},
#endif /* COMM_DIRECT */
		{NULL,           0,                 NULL, 0}
	};
	// Generate short options string from long options
	char sopts[sizeof(lopts)*2] = {0};
	int c = 0;
	for (long unsigned int i = 0; i<sizeof(lopts)/sizeof(lopts[0]); i++) {
		if (isprint(lopts[i].val)) {
			sopts[c++] = lopts[i].val;
		}
	}
	char *endptr = NULL; /* for strtol */
	long l;
	iargv[iargc++] = gargv[0];
	/* Arguments to pass to the interface */
	while ((c = getopt_long(gargc, gargv, sopts, lopts, NULL)) != -1) {
		switch (c) {
		case 'h':
			usage(stdout, gargv[0]);
			exit(EXIT_SUCCESS);
		case 1: /* version */
			version(stdout);
			exit(EXIT_SUCCESS);
		case 'p': /* port */
			l = strtol(optarg, &endptr, 10);
			if (*endptr != '\0') {
				die("Couldn't parse port: '%s'\n", optarg);
			} else if (l < 1) {
				die("Port numbers are > 1\n");
			} else if (l > UINT16_MAX) {
				die("Port number too big\n");
			}
			default_port = l;
			msg_log(PSTR("Port set to %d\n"), default_port);
			break;
		case 2: /* spoof server version */
			parse_semantic_version(optarg, &server_version,
				"spoof server");
			msg_log("server version set to %d %d %d\n",
				server_version.major,
				server_version.minor,
				server_version.patch);
			break;
		case 3: /* spoof client version */
			parse_semantic_version(optarg, &client_version,
				"spoof client");
			msg_log("client version set to %d %d %d\n",
				client_version.major,
				client_version.minor,
				client_version.patch);
			break;
#ifdef NVM
		case 'r':
			ic_resume = false;
			msg_log(PSTR("Resume set to false\n"));
			break;
		case 's':
			ic_swapfile = optarg;
			msg_log(PSTR("Swapfile set to %s\n"), ic_swapfile);
			break;
#endif /* NVM */
#ifdef COMM_DIRECT
		case 4: /* ping-timeout */
			if (optarg == NULL) {
				l = 0;
			} else {
				l = strtol(optarg, &endptr, 10);
				if (*endptr != '\0') {
					die("Couldn't parse ping timeout: "
						"'%s'\n", optarg);
				}
			}
			if (l == 0) {
				default_ping = 0;
				msg_log(PSTR("Ping disabled\n"));
			} else if (l < 0) {
				die("Ping timeout cannot be negative\n");
			} else if (l > INT_MAX || l == LONG_MAX) {
				die("Ping timeout too large (> %d)\n", INT_MAX);
			} else {
				default_ping = l;
				msg_log(PSTR("Ping timeout set to %u\n"),
					default_ping);
			}
			break;
#endif /* COMM_DIRECT */
#ifdef COMM_MQTT
		case 5: /* mqtt-host */
			default_host = optarg;
			msg_log(PSTR("Host set to %s\n"), default_host);
			break;
		case 6: /* mqtt-keep-alive */
			default_keep_alive_time = strtol(optarg, &endptr, 10);
			if (*endptr != '\0') {
				die("Couldn't parse keep-alive time: "
					"'%s'\n", optarg);
			}
			msg_log(PSTR("Keep-alive time set to %u\n"),
				default_keep_alive_time);
			break;
		case 7: /* mqtt-will-delay */
			default_will_delay_interval =
				strtol(optarg, &endptr, 10);
			if (*endptr != '\0') {
				die("Couldn't parse will delay interval: "
					"'%s'\n", optarg);
			}
			msg_log(PSTR("Will delay interval set to %u\n"),
				default_will_delay_interval);
			break;
		case 8: /* client-id */
			default_client_id = optarg;
			msg_log(PSTR("Client id set to %s\n"),
				default_client_id);
			break;
#endif /* COMM_MQTT */
		case '?': /* error */
			exit(EXIT_FAILURE);
			break;
		}
	}
	/* pass on position arguments to the interface */
	for (; optind < gargc; optind++) {
		iargv[iargc++] = gargv[optind];
	}

	pc_init(iargc, iargv);
	return ic_resume;
}

#if LOGLEVEL > 0
void msg_log(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	pc_msg_log(fmt, ap);
	va_end(ap);
}
#endif /* LOGLEVEL > 0 */

void pdie(const char *s)
{
	pc_exit();
	die("%s: %s\n", s, strerror(errno));
}

void die(const char *fmt, ...)
{
	pc_exit();
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	real_exit();
}

extern int main(int argc, char **argv);
void reset(void)
{
	pc_reset();

	stop_communication(false);

	pc_init(iargc, iargv);
	start_communication(false);
}

void mtask_yield(void)
{
	pc_yield();
}

unsigned int get_random(void)
{
	return (unsigned int) rand();
}

#endif /* defined(_WIN32) || defined(__APPLE__) || defined (__linux__)\
	  || defined (__unix__)*/
