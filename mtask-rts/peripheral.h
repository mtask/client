/** @file
 *
 * Contains peripheral related functions
 */
#ifndef PERIPHERAL_H
#define PERIPHERAL_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "types.h"
#include "interface.h"

/**
 * Register a peripheral (initialise it)
 */
bool peripheral_register(uint8_t taskid, struct BCPeripheral *p);

/**
 * Destroy a peripheral (free the resources)
 */
void peripheral_destroy(struct BCPeripheral *p);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !TASK_H */

