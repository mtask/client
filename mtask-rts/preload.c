#include "interface.h"

#include "preload.h"
#include "preloaded_tasks.h"
#include "mem.h"
#include "task.h"

#if PRELOADED_TASKS_NUM > 0
static uint8_t ct = 0;
static uint16_t cti = 0;

uint8_t read_byte_from_preload()
{
	const uint8_t *task_data = (const uint8_t *)
		pgm_read_ptr(preloaded_tasks+ct);
	return pgm_read_byte(task_data+(cti++));
}

bool preload_task(struct MTaskPrepData data)
{
	msg_log(PSTR("Checking for preloaded tasks\n"));
	for (ct = 0; ct<PRELOADED_TASKS_NUM; ct++) {
		if (preloaded_hashes[ct] == data.hash) {
			msg_log(PSTR("Preloaded task found\n"));
			cti = 0;
			struct MTaskMeta *v = (struct MTaskMeta *)
				parse_MTaskMeta_p(
					read_byte_from_preload,
					mem_alloc_task);
			v->taskid = data.taskid;
			task_register(v);
			return true;
		}
	}
	msg_log(PSTR("Preloaded task not found\n"));
	return false;
}

#endif /* PRELOADED_TASKS_NUM > 0 */
