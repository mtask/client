#ifdef ARDUINO_ARCH_AVR

#include <Arduino.h>
#include <stdbool.h>
#include <stdint.h>

#include "link_interface.h"
#include "interface.h"

bool link_input_available(void)
{
	return Serial.available();
}

uint8_t link_read_byte(void)
{
	while (!link_input_available())
		msdelay(5);
	return Serial.read();
}

void link_write_byte(uint8_t b)
{
	Serial.write(b);
}

void open_link(void)
{
	Serial.begin(BAUDRATE);
}

void close_link(bool temporary)
{
	Serial.end();
	(void)temporary;
}

void cleanup_link() {}

bool link_is_open() {
	return true;
}

#endif /* ARDUINO_ARCH_AVR */
