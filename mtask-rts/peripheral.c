/** @file
 *
 * Contains peripheral related functions
 */
#include "types.h"
#include "interface.h"

static void unsup(uint8_t tid, enum BCPeripheralInfo_c cons, const char *name)
{
	msg_log(PSTR("Failed to setup %s\n"), name);
	send_message((struct MTMessageFro)
		{ .cons=MTFException_c
		, .data={.MTFException=
			{ .cons=MTEUnsupportedPeripheral_c
			, .data={.MTEUnsupportedPeripheral=
				{ .f0=tid
				, .f1=cons
				}
			}}
		}});
	(void)name;
}

bool peripheral_register(uint8_t taskid, struct BCPeripheral *p)
{
	switch (p->perinfo.cons) {
	case BCDHT_c:
#ifdef HAVE_DHT
		p->st = dht_init(p->perinfo.data.BCDHT);
		if (p->st == NULL) {
			unsup(taskid, p->perinfo.cons, "DHT");
			return false;
		}
#else /* HAVE_DHT */
		unsup(taskid, p->perinfo.cons, "DHT");
#endif /* HAVE_DHT */
		break;
	case BCLEDMatrix_c:
#ifdef HAVE_LEDMATRIX
		p->st = ledmatrix_init(p->perinfo.data.BCLEDMatrix);
		if (p->st == NULL) {
			unsup(taskid, p->perinfo.cons, "LEDMatrix");
			return false;
		}
#else /* HAVE_LEDMATRIX */
		unsup(taskid, p->perinfo.cons, "LEDMatrix");
#endif /* HAVE_LEDMATRIX */
		break;
	case BCLightSensor_c:
#ifdef HAVE_LIGHTSENSOR
		p->st = lightsensor_init(p->perinfo.data.BCLightSensor);
		if (p->st == NULL) {
			unsup(taskid, p->perinfo.cons, "Lightsensor");
			return false;
		}
#else /* HAVE_LIGHTSENSOR */
		unsup(taskid, p->perinfo.cons, "Lightsensors");
#endif /* HAVE_LIGHTSENSOR */
		break;
	case BCAirQualitySensor_c:
#ifdef HAVE_AIRQUALITYSENSOR
		p->st = airqualitysensor_init(
			p->perinfo.data.BCAirQualitySensor);
		if (p->st == NULL) {
			unsup(taskid, p->perinfo.cons, "AQS");
			return false;
		}
#else /* HAVE_LIGHTSENSOR */
		unsup(taskid, p->perinfo.cons, "AQS");
#endif /* HAVE_AIRQUALITYSENSOR */
		break;
	case BCGestureSensor_c:
#ifdef HAVE_GESTURESENSOR
		p->st = gesturesensor_init(
			p->perinfo.data.BCGestureSensor);
		if (p->st == NULL) {
			unsup(taskid, p->perinfo.cons, "Gesture");
			return false;
		}
#else /* HAVE_LIGHTSENSOR */
		unsup(taskid, p->perinfo.cons, "Gesture");
#endif /* HAVE_GESTURESENSOR */
		break;
	case BCNeoPixel_c:
#ifdef HAVE_NEOPIXEL
		p->st = neopixel_init(p->perinfo.data.BCNeoPixel);
		if (p->st == NULL) {
			unsup (taskid, p->perinfo.cons, "NeoPixel");
			return false;
		}
#else /* HAVE_NEOPIXEL */
		unsup(taskid, p->perinfo.cons, "NeoPixel");
#endif /* HAVE_NEOPIXEL */
		break;
	}
	return true;
}

void peripheral_destroy(struct BCPeripheral *p)
{
	switch (p->perinfo.cons) {
	case BCDHT_c:
#ifdef HAVE_DHT
		dht_destroy(p);
#endif /* HAVE_DHT */
		break;
	case BCLEDMatrix_c:
#ifdef HAVE_LEDMATRIX
		ledmatrix_destroy(p);
#endif /* HAVE_LEDMATRIX */
		break;
	case BCLightSensor_c:
#ifdef HAVE_LIGHTSENSOR
		lightsensor_destroy(p);
#endif /* HAVE_LIGHTSENSOR */
		break;
	case BCAirQualitySensor_c:
#ifdef HAVE_AIRQUALITYSENSOR
		airqualitysensor_destroy(p);
#endif /* HAVE_AIRQUALITYSENSOR */
		break;
	case BCGestureSensor_c:
#ifdef HAVE_GESTURESENSOR
		gesturesensor_destroy(p);
#endif /* HAVE_GESTURESENSOR */
		break;
	case BCNeoPixel_c:
#ifdef HAVE_NEOPIXEL
		neopixel_destroy(p);
#endif /* HAVE_NEOPIXEL */
		break;
	}
}
