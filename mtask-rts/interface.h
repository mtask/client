/** @file
 *
 * Contains the device specific functions.
 */
#ifndef INTERFACE_H
#define INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>

#if defined(PC)
#include "pc.h"
#include "pc/pgmspace.h"
#elif defined(ARDUINO)
#include "config_arduino.h"
#else /* PC */
#warning "No architecture could be detected, assuming pc"
#include "pc.h"
#include "pc/pgmspace.h"
#endif /* PC */

#include "types.h"
#include "config.h"
#include "preloaded_tasks.h"

// Sanity checks {{{
#if APINS > 255
#error "Maximum number of analog pins is 255"
#endif /* APINS > 255 */

#if DPINS > 255
#error "Maximum number of digital pins is 255"
#endif /* DPINS > 255 */

#if MEMSIZE > UINT16_MAX - 3
#error "Maximum number of memory is 2^16-3"
#endif /* MEMSIZE > UINT16_MAX - 3 */

#if defined(ARDUINO_ARCH_AVR) && LOGLEVEL > 0
#warning Logging on the arduino UNO takes up too much space.\
	Falling back to none
#undef LOGLEVEL
#define LOGLEVEL 0
#endif /* defined(ARDUINO_ARCH_AVR) && LOGLEVEL > 0 */


// }}}

// Macros {{{
/** Macro that returns the maximum of \a a and \b b */
#define mtmax(a, b) ((a) > (b) ? (a) : (b))
/** Macro that returns the minimum of \a a and \b b */
#define mtmin(a, b) ((a) < (b) ? (a) : (b))
// }}

#include "comm_interface.h"

#if DPINS > 0 || APINS > 0
// GPIO pins {{{
/**
 * Set the pinmode of a GPIO pin
 *
 * \note Only required when DPINS>0 || APINS >0
 *
 * @param pin decoded pin number
 * @param mode pin mode
 */
void set_pinmode(uint16_t pin, enum PinMode mode);

/**
 * Translate a bytecode encoded pin into a decoded pin number
 *
 * \note Only required when DPINS>0 || APINS >0
 *
 * @param pin encoded pin
 * @return decoded pin
 */
uint16_t translate_pin(uint16_t pin);
#endif /* DPINS > 0 || APINS > 0 */

#if DPINS > 0
/**
 * Write the value to a given decoded pin number
 *
 * \note Only required when DPINS>0
 *
 * @param pin decoded
 * @param b value
 */
void write_dpin(uint16_t pin, bool b);

/**
 * Read the value from a given decoded pin number
 *
 * \note Only required when DPINS>0
 *
 * @param pin decoded
 * @return value
 */
bool read_dpin(uint16_t pin);
#if APINS > 0
// Analog pins {{{
/**
 * Write the value to a given decoded analog pin number.
 * \note Doesn't work on ESP32 yet due to:
 *	 https://github.com/espressif/arduino-esp32/issues/4
 *
 * \note Only required when APINS>0
 *
 * @param pin decoded pin
 * @param a value
 */
void write_apin(uint16_t pin, uint8_t a);
/**
 * Read the value from a given decoded analog pin number.
 *
 * \note Only required when APINS>0
 *
 * @param pin decoded pin
 * @return value
 */
uint8_t read_apin(uint16_t pin);
//}}}
#endif /* APINS > 0 */
//}}}
#endif /* DPINS > 0  || APINS > 0 */

// Interrupts {{{
/**
 * Attach the interrupt service routine (isr) to a pin
 *
 * @param isr interrupt service routine
 * @param pin decoded pin
 * @param mode interrupt mode
 */
void attach_interrupt(void (*isr)(void), uint16_t pin, enum InterruptMode mode);
/**
 * Detach an interrupt service routine (isr) from a pin
 *
 * @param pin decoded
 */
void detach_interrupt(uint16_t pin);

/**
 * Retrieve the function to retrieve the interrupt service routine (isr) from a
 * pin
 */
void (*get_isr(uint16_t pin, enum InterruptMode mode))(void);
// }}}

#ifdef HAVE_DHT
// DHT {{{
/**
 * Initialise a digital humidity and temperature sensor
 *
 * \note Only required when HAVE_DHT is defined
 *
 * @param i DHT info
 * @return pointer to a state
 */
void *dht_init(struct DHTInfo i);
/**
 * Destroy the dht sensor
 */
void dht_destroy (struct BCPeripheral *st);
/**
 * Get the temperature from a DHT
 *
 * \note Only required when HAVE_DHT is defined
 *
 * @param st pointer to the initialised peripheral
 * @result temperature in °C
 */
float get_dht_temp(struct BCPeripheral *st);
/**
 * Get the humidity from a DHT
 *
 * \note Only required when HAVE_DHT is defined
 *
 * @param st pointer to the initialised peripheral
 * @result humidity in relative percent
 */
float get_dht_humidity(struct BCPeripheral *st);
// }}}
#endif /* HAVE_DHT */

#ifdef HAVE_LEDMATRIX
// LED Matrix {{{
/**
 * Initialise an LED matrix
 *
 * \note Only required when HAVE_LEDMATRIX is defined
 *
 * @param info
 * @return pointer to a state
 */
void *ledmatrix_init(struct LEDMatrixInfo info);
/**
 * Destroy the ledmatrix
 */
void ledmatrix_destroy (struct BCPeripheral *st);
/**
 * Draw a dot on the LED matrix
 *
 * \note Only required when HAVE_LEDMATRIX is defined
 *
 * @param st pointer to the state returned by ledmatrix_init()
 * @param x coordinate
 * @param y coordinate
 * @param onoff state
 */
void ledmatrix_dot(struct BCPeripheral *st, uint8_t x, uint8_t y, bool onoff);
/**
 * Set the intensity of the LED matrix
 *
 * \note Only required when HAVE_LEDMATRIX is defined
 *
 * @param st pointer to the state returned by ledmatrix_init()
 * @param i intensity
 */
void ledmatrix_intensity(struct BCPeripheral *st, uint8_t i);
/**
 * Clear the LED matrix (turn all LEDs off)
 *
 * \note Only required when HAVE_LEDMATRIX is defined
 *
 * @param st pointer to the state returned by ledmatrix_init()
 */
void ledmatrix_clear(struct BCPeripheral *st);
/**
 * Flush the LED matrix's buffer
 *
 * \note Only required when HAVE_LEDMATRIX is defined
 *
 * @param st pointer to the state returned by ledmatrix_init()
 */
void ledmatrix_display(struct BCPeripheral *st);
// }}}
#endif /* HAVE_LEDMATRIX */

#ifdef HAVE_NEOPIXEL
/**
 * Initialise a NeoPixel peripheral
 */
void* neopixel_init (struct NeoInfo info);
/**
 * Destroy the NeoPixel peripheral
 */
void neopixel_destroy (struct BCPeripheral *st);

/**
 * Set a pixel of the LED strip
 *
 * @param p peripheral info
 * @param n pixel number
 * @param r red value
 * @param g green value
 * @param b blue value
 */
void neopixel_setPixelColor(struct BCPeripheral *p,
	uint16_t n, uint8_t r, uint8_t g, uint8_t b);
#endif /* HAVE_NEOPIXEL */

#ifdef HAVE_LIGHTSENSOR
// Lightsensor {{{
/**
 * Initialise an I²C light sensor
 *
 * \note Only required when HAVE_LIGHTSENSOR is defined
 *
 * @param info I²C address
 * @return pointer to a state
 */
void *lightsensor_init(LightSensorInfo info);
/**
 * Destroy the lightsensor
 */
void lightsensor_destroy (struct BCPeripheral *st);
/**
 * Get the light intensity
 *
 * \note Only required when HAVE_LIGHTSENSOR is defined
 *
 * @param st pointer to the state returned by i2c_init()
 * @return light insensity in lux
 */
float get_light(struct BCPeripheral *st);
//}}}
#endif /* HAVE_LIGHTSENSOR */

#ifdef HAVE_AIRQUALITYSENSOR
// Air quality sensor {{{
/**
 * Initialise an eCO2 and tVOC air quality sensor
 *
 * \note Only required when HAVE_AIRQUALITYSENSOR is defined
 * \note On Arduino, either define HAVE_AIRQUALITYSENSOR_SGP32 or
 *     HAVE_AIRQUALITYSENSOR_CCS811
 *
 * @param info structure
 * @return pointer to a state
 */
void *airqualitysensor_init(struct AirQualitySensorInfo info);
/**
 * Destroy the airqualitysensor
 */
void airqualitysensor_destroy (struct BCPeripheral *st);
/**
 * Set the environmental data to calibrate the sensor
 *
 * \note Only required when HAVE_AIRQUALITYSENSOR is defined
 *
 * @param st pointer to the state returned by airqualitysensor_init()
 * @param humid relative humidity in relative percent
 * @param temp temperature in °C
 */
void set_environmental_data(struct BCPeripheral *st, float humid, float temp);
/**
 * Get the Total Volatile Organic Compound (TVOC) level
 *
 * \note Only required when HAVE_AIRQUALITYSENSOR is defined
 *
 * @param st pointer to the state returned by airqualitysensor_init()
 * @result TVOC level in parts per billion
 */
uint32_t get_tvoc(struct BCPeripheral *st);
/**
 * Get the eCO₂ level
 *
 * \note Only required when HAVE_AIRQUALITYSENSOR is defined
 *
 * @param st pointer to the state returned by airqualitysensor_init()
 * @result eCO₂ level in ppm
 */
uint32_t get_co2(struct BCPeripheral *st);
//}}}
#endif /* HAVE_AIRQUALITYSENSOR */

#ifdef HAVE_GESTURESENSOR
// Gesture sensor {{{
/**
 * Initialise an I²C gesture sensore
 *
 * \note Only required when HAVE_GESTURESENSOR is defined
 *
 * @param info I²C address
 * @return pointer to a state
 */
void *gesturesensor_init(GestureSensorInfo info);
/**
 * Destroy the gesturesensor
 */
void gesturesensor_destroy (struct BCPeripheral *st);
/**
 * Get the current detected gesture
 *
 * \note Only required when HAVE_GESTURESENSOR is defined
 *
 * @param st pointer to the state returned by gesturesensor_init()
 * @result detected gesture
 */
enum Gesture get_gesture(struct BCPeripheral *st);
//}}}
#endif /* HAVE_GESTURESENSOR */

#ifdef HAVE_OLEDSHIELD
// Screen {{{
/**
 * Print a message to the display
 *
 * \note Only required when HAVE_OLEDSHIELD is defined
 *
 * @param msg Message
 * @param ... Arguments for the format string
 */
void print_to_display(const char *msg, ...);
/**
 * Flush the display's buffer
 *
 * \note Only required when HAVE_OLEDSHIELD is defined
 */
void flush_display(void);
/**
 * Clear the display
 *
 * \note Only required when HAVE_OLEDSHIELD is defined
 */
void clear_display(void);
#else /* HAVE_OLEDSHIELD */
#define print_to_display(...)
#define flush_display()
#define clear_display()
//}}}
#endif /* HAVE_OLEDSHIELD */

//Sleeping {{{
/**
 * Get the number of milliseconds since boot/epoch
 *
 * @return number of milliseconds
 */
uint32_t getmillis(void);
/**
 * Blockingly wait for the given number of milliseconds
 *
 * @param ms waiting time
 */
void msdelay(uint32_t ms);

/* Sleep */
/**
 * Wait for the given number of milliseconds and allow the device to sleep in
 * the meantime
 *
 * @param ms waiting time
 */
void mssleep(uint32_t ms);
/**
 * Wake up a device, this is getting called from the ISR (see real_isr())
 */
void force_wake_up(void);
//}}}

// Set up and shut down{{{
/**
 * Code to setup the device (called in arduino's setup() or during
 * initalisation)
 *
 * @return whether to resume from nvm or not
 */
bool real_setup(void);
/**
 * Exit with an error message
 *
 * @param msg format string
 * @param ... format string arguments
 */
void die(const char *msg, ...);
/**
 * Resets the client
 */
void reset(void);

//}}}

// Auxilliary {{{
/**
 * Get a random number
 *
 * @return random number
 */
unsigned int get_random(void);

/**
 * Field function so that you can avoid starving dogs. Called very often during
 * rewriting and interpretation.
 */
void mtask_yield(void);
#ifndef RECOVERABLE_ERROR_CODE
/**
 * Code fragment placed in main for example to set longjumps in case of a
 * recoverable error. Used on PC to recover from floating point exceptions.
 */
#define RECOVERABLE_ERROR_CODE
#endif /* RECOVERABLE_ERROR_CODE */

//}}}

// Logging {{{
#if LOGLEVEL == 2
/**
 * Log a message
 *
 * \note Only needs to be defined if LOGLEVEL >= 1
 *
 * @param msg format string
 * @param ... arguments for format string
 */
void msg_log(const char *msg, ...);
/**
 * Log a message to the debug channel
 *
 * \note Only needs to be defined if LOGLEVEL == 2
 */
#define msg_debug(...) msg_log(__VA_ARGS__)

/* Log level: normal */
#elif LOGLEVEL == 1
/**
 * Log a message
 *
 * \note Only needs to be defined if LOGLEVEL >= 1
 *
 * @param msg format string
 * @param ... arguments for format string
 */
void msg_log(const char *msg, ...);
/**
 * Log a message to the debug channel
 *
 * \note Only needs to be defined if LOGLEVEL == 2
 */
#define msg_debug(...) {};

/* Log level: quiet */
#else /* LOGLEVEL == 2 */
/**
 * Log a message
 *
 * \note Only needs to be defined if LOGLEVEL >= 1
 */
#define msg_log(...) {};
/**
 * Log a message to the debug channel
 *
 * \note Only needs to be defined if LOGLEVEL == 2
 */
#define msg_debug(...) {};
#endif /* LOGLEVEL == 2 */
//}}}

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !INTERFACE_H */

