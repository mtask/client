#include "spec.h"
#include "interface.h"
#include "comm_interface.h"
#include "preloaded_tasks.h"

void spec_send(void)
{
	struct MTDeviceSpec spec =
		{ .memory  = MEMSIZE
		, .aPins   = APINS
		, .dPins   = DPINS
		, .haveDHT =
#ifdef HAVE_DHT
			true
#else /* HAVE_DHT */
			false
#endif /* HAVE_DHT */
		, .haveLM=
#ifdef HAVE_LEDMATRIX
			true
#else /* HAVE_LEDMATRIX */
			false
#endif /* HAVE_LEDMATRIX */
		, .haveI2B=
#ifdef HAVE_I2CBUTTON
			true
#else /* HAVE_I2CBUTTON */
			false
#endif /* HAVE_I2CBUTTON */
		, .haveLS=
#ifdef HAVE_LIGHTSENSOR
			true
#else /* HAVE_LIGHTSENSOR */
			false
#endif /* HAVE_LIGHTSENSOR */
		, .haveAQS=
#ifdef HAVE_AIRQUALITYSENSOR
			true
#else /* HAVE_AIRQUALITYSENSOR */
			false
#endif /* HAVE_AIRQUALITYSENSOR */
		, .haveGes=
#ifdef HAVE_GESTURESENSOR
			true
#else /* HAVE_GESTURESENSOR */
			false
#endif /* HAVE_GESTURESENSOR */
		};
	send_message((struct MTMessageFro)
		{ .cons=MTFSpec_c, .data={ .MTFSpec=spec }});
}
