/** @file interpret.h
 *
 * Contains the main interpreter function. Is configured by:
 * - STACK_CHECKING to check for stack overflows
 */
#ifndef INTEPRET_H
#define INTEPRET_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>
#include <stdbool.h>

#include "task.h"

/** Number of stackframe values */
#define STACKMETA 3

void initstackmain(uint16_t *sp, uintptr_t *stack);

/** Helper struct to convert between ints and floats */
union floatint {uint32_t intp; float floatp; };
/** retrieve a float from an uint32_t */
#define uint32_t2float(i) ((float)((union floatint) {.intp=i}).floatp)
/** store a float in an uint32_t */
#define float2uint32_t(f) ((uint32_t)((union floatint) {.floatp=f}).intp)

/**
 * Interpret a program.
 *
 * \note This assumes that the stack has been initialised using initstackmain
 *
 * @param program pointer to the bytecode
 * @param pc program counter
 * @param sp stack pointer
 * @param stack pointer to the stack
 * @return success
 */
bool interpret(uint8_t *program, uint16_t pc, uint16_t sp, uintptr_t *stack);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !INTEPRET_H */

