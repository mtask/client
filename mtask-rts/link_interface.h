/** @file link_interface.h
 *
 * The link interface defines the communication means on the lowest level. From
 * comm_interface.h link interface functions are called.
 *
 * Currently only a TCP link or a serial port link is supported using the
 * following configuration options:
 *
 * - LINK_TCP which uses pc/link.c on the PC or arduino_wifi_link.cpp or
 *   Arduino. They are configured using the configuration from
 *   comm_interface.h.
 *   - LINK_PORT to set the port
 *   - LINK_HOST to set the default host to connect to when in server mode
 * - LINK_SERIAL uses link_serial.cpp on Arduino and is configured using:
 *   - BAUDRATE to set the default baud rate
 */
#ifndef LINK_INTERFACE_H
#define LINK_INTERFACE_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "interface.h"
#include <stdbool.h>
#include <stdint.h>

//Sanity checks for the configuration macros
#if defined(LINK_SERIAL) && defined(LINK_TCP)
#error Multiple link methods defined
#elif defined(LINK_SERIAL)
# ifndef BAUDRATE
#error BAUDRATE not defined
# endif /* BAUDRATE */
#elif defined(LINK_TCP)
# ifndef LINK_PORT
#error No link port is defined. Set LINK_PORT
# endif /* !LINK_PORT */
#else /* LINK_SERIAL */
#error No link mode is set, either set LINK_SERIAL or LINK_TCP
#endif /* LINK_SERIAL */

/**
 * Test whether a byte is available on the input channel
 *
 * @return input available
 */
bool link_input_available(void);
/**
 * Read a single byte from the input channel
 *
 * @return byte
 */
uint8_t link_read_byte(void);
/**
 * Write a single byte to the output channel
 */
void link_write_byte(uint8_t b);
/**
 * Open the link
 */
void open_link(void);
/**
 * Close a link
 *
 * @param temporary close only temporarily
 */
void close_link(bool temporary);
/**
 * Cleanup memory
 */
void cleanup_link(void);

/**
 * Query wether the link is open. Sometimes, during yielding, pings are sent but
 * this can only be done when the link is open.
 */
bool link_is_open(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !LINK_INTERFACE_H */

