/** @file mem.h
 *
 * Contains all the memory management functions
 *
 * In the global segment there is an array of bytes allocated. This is managed
 * by this file.
 *
 * On the stack, pointers in mTask are stored as a 16 bit unsigned integer
 * offset to the managed memory. Translating them happens via mem_rptr() and
 * mem_ptr(). Within the structures, pointers are saved as pointers.
 *
 * Global layout
 *
 * | Address   | Contents  |
 * | --------- | --------- |
 * | MEM_SIZE  | heap      |
 * | ↓         | ↓         |
 * |           |           |
 * |           | ↑         |
 * |           | stack     |
 * |           | task_n    |
 * | ↑         | ...       |
 * | 0         | task_0    |
 *
 * Task (t) layout.
 *
 * The padding is appied when REQUIRE_ALIGNED_MEMORY_ACCESS is defined to make
 * sure the next task starts at an aligned address.
 *
 * | Relative Address           | Contents             |
 * | -------------------------- | -------------------- |
 * |                            | padding (optional)   |
 * | ↑                          | uint8_t *            |
 * | t->instructions.elements   | ^                    |
 * | ↑                          | struct BCPeripheral *|
 * | t->peripherals.elements    | ^                    |
 * | ↑                          | struct BCShareSpec * |
 * | t->shares.elements         | ^                    |
 * | ↑                          | uint8_t *            |
 * | t->value.elements          | ^                    |
 * | sizeof(struct MTaskMeta)   | struct MTaskMeta     |
 * | 0                          | ^                    |
 */
#ifndef MEM_H
#define MEM_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>
#include <stddef.h>

/** mTask's 16 bit null pointer */
#define MT_NULL 65535

#include "interface.h"

#if UINTPTR_MAX == 0xFFFF
/** size of a pointer */
#define PTRSIZE 2
/** store a pointer in a byte array */
#define STOREPTR(b, p) {\
		(b)[0] = (p >> 8) & 0xff;\
		(b)[1] = p & 0xff;\
	}
/** get a pointer from a byte array */
#define GETPTR(b)\
	(void *)(\
		((uintptr_t)(b)[0] << 8) +\
		 (uintptr_t)(b)[1]\
	)
#elif UINTPTR_MAX == 0xFFFFFFFF
/** size of a pointer */
#define PTRSIZE 4
/** store a pointer in a byte array */
#define STOREPTR(b, p) {\
		(b)[0] = (p >> 24) & 0xff;\
		(b)[1] = (p >> 16) & 0xff;\
		(b)[2] = (p >> 8) & 0xff;\
		(b)[3] = p & 0xff;\
	}
/** get a pointer from a byte array */
#define GETPTR(b)\
	(void *)(\
		((uintptr_t)(b)[0] << 24) +\
		((uintptr_t)(b)[1] << 16) +\
		((uintptr_t)(b)[2] << 8) +\
		 (uintptr_t)(b)[3]\
	)
#elif UINTPTR_MAX == 0xFFFFFFFFFFFFFFFFu
/** size of a pointer */
#define PTRSIZE 8
/** store a pointer in a byte array */
#define STOREPTR(b, p) {\
		(b)[0] = (p >> 56) & 0xff;\
		(b)[1] = (p >> 48) & 0xff;\
		(b)[2] = (p >> 40) & 0xff;\
		(b)[3] = (p >> 32) & 0xff;\
		(b)[4] = (p >> 24) & 0xff;\
		(b)[5] = (p >> 16) & 0xff;\
		(b)[6] = (p >> 8) & 0xff;\
		(b)[7] = p & 0xff;\
	}
/** get a pointer from a byte array */
#define GETPTR(b)\
	(void *)(\
		((uintptr_t)(b)[0] << 56) +\
		((uintptr_t)(b)[1] << 48) +\
		((uintptr_t)(b)[2] << 40) +\
		((uintptr_t)(b)[3] << 32) +\
		((uintptr_t)(b)[4] << 24) +\
		((uintptr_t)(b)[5] << 16) +\
		((uintptr_t)(b)[6] << 8) +\
		 (uintptr_t)(b)[7]\
	)
#else /* UINTPTR_MAX == 0xFFFF */
#error TBD pointer size
#endif /* UINTPTR_MAX == 0xFFFF */

/**
 * Convert an mTask pointer to an actual pointer
 *
 * @param ptr mTask pointer
 * @return pointer
 */
void *mem_ptr(uint16_t ptr);
/**
 * Convert an actual pointer to an mTask pointer
 *
 * @param ptr pointer
 * @return mTask pointer
 */
//* Convert pointers to mtask pointers
uint16_t mem_rptr(void *ptr);

/** cast an mTask pointer to a specific pointer type */
#define mem_cast_ptr(t, type) ((type)mem_ptr(t))
/** cast an mTask pointer to a task pointer */
#define mem_cast_task(t) mem_cast_ptr(t, struct MTaskMeta *)
/** cast an mTask pointer to a task tree pointer */
#define mem_cast_tree(t) mem_cast_ptr(t, struct TaskTree *)
/** cast an mTask pointer to a byte pointer */
#define mem_cast_bytes(t) mem_cast_ptr(t, uint8_t *)

#ifdef REQUIRE_ALIGNED_MEMORY_ACCESS
/**
 * Helper macro to get offset to the next aligned cell
 *
 * \note only implemented when REQUIRE_ALIGNED_MEMORY_ACCESS is defined
 *
 * @param sz align to
 * @param p pointer to align
 */
#define PALIGND(sz, p) ((uintptr_t)(p) %\
	mtmin((size_t) PTRSIZE, (size_t) sz) == 0 ? 0 :\
	(mtmin((size_t) PTRSIZE,(size_t) sz)-(uintptr_t)(p) %\
	mtmin((size_t) PTRSIZE, (size_t) sz)))
/**
 * Helper macro to align a pointer of a given type
 *
 * \note only implemented when REQUIRE_ALIGNED_MEMORY_ACCESS is defined
 *
 * @param type to align to
 * @param p pointer to store the result in
 */
#define PALIGN(type, p) ((type *)((uintptr_t)(p) + PALIGND(sizeof(type), p)))
#else /* REQUIRE_ALIGNED_MEMORY_ACCESS */
/**
 * Helper macro to get offset to the next aligned cell
 *
 * \note only implemented when REQUIRE_ALIGNED_MEMORY_ACCESS is defined
 *
 * @param sz align to
 * @param p pointer to align
 */
#define PALIGND(sz, p) 0
/**
 * Helper macro to align a pointer of a given type
 *
 * \note only implemented when REQUIRE_ALIGNED_MEMORY_ACCESS is defined
 *
 * @param type to align to
 * @param p pointer to store the result in
 */
#define PALIGN(type, p) ((type *)(p))
#endif /* REQUIRE_ALIGNED_MEMORY_ACCESS */

/**
 * Reset everything, empty the heap, etc.
 */
void mem_reset(void);

/**
 * Get the stack value for which the stack overflows
 *
 * @return stack pointer
 */
uint16_t *mem_max_sp(void);
/**
 * Get the current start of the stack
 *
 * @return stack pointer
 */
uint16_t *mem_stack(void);
/**
 * Set the stack pointer, i.e. unallocate task space
 *
 * @param ptr stack pointer
 */
void mem_set_stack_ptr(void *ptr);
/**
 * Increase the stack pointer, i.e. allocate task space
 *
 * @param size number of bytes to allocate
 * @return new stack pointer
 */
void *mem_alloc_task(size_t size);

/**
 * Get the head of the task list
 *
 * @return pointer to the head
 */
struct MTaskMeta *mem_task_head(void);
/**
 * Get the next task in the task list.
 *
 * @param head head of the task list
 * @return pointer to the tail
 */
struct MTaskMeta *mem_task_next(struct MTaskMeta *head);
/**
 * Get the tail of the task list but don't check if there is space
 *
 * @param head head of the task list
 * @return pointer to the tail
 */
struct MTaskMeta *mem_task_next_unsafe(struct MTaskMeta *head);
/**
 * Allocate a tasktree
 *
 * @throws MTEOutOfMemory_c if there is no more memory left
 * @return pointer to a task tree
 */
struct TaskTree *mem_alloc_tree(void);
/**
 * Destroy and mark something and all their children as trash
 *
 * @param tt task tree
 * @param t task the tree belongs to
 * @param stack stack to use during marking
 */
void mem_mark_trash_and_destroy(struct TaskTree *tt, struct MTaskMeta *t,
				uint16_t *stack);

/**
 * Move the contents of a tasktree except the ptr while keeping the rptrs of
 * the children in tact
 *
 * @param target target task tree
 * @param source source task tree
 */
void mem_node_move(struct TaskTree *target, struct TaskTree *source);

/**
 * Perform garbage collection on the task list
 */
void mem_gc_tasks(void);
/**
 * Perform garbage collection on the task tree heap
 */
void mem_gc(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !MEM_H */

