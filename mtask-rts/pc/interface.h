/** @file pc_interface.h
 *
 * PC interface to many interface.h functions so that different front ends can
 * implement it differently. This is currently implemented by pc_curses.c and
 * pc_tty.c
 */
#ifndef PC_INTERFACE_H
#define PC_INTERFACE_H

#include <stdint.h>
#include <stdarg.h>
#include "../types.h"

/** @see write_dpin() */
void pc_write_dpin(uint16_t, bool);
/** @see read_dpin() */
void pc_read_dpin(uint16_t);
/** @see write_apin() */
void pc_write_apin(uint16_t, uint8_t);
/** @see read_apin() */
void pc_read_apin(uint16_t);
/** @see set_pinmode() */
void pc_set_pinmode(uint16_t, enum PinMode);

/** @see get_dht_humidity() */
void pc_humidity(struct DHTInfo);
/** @see get_dht_temp() */
void pc_temperature(struct DHTInfo);
/** @see get_light() */
void pc_light(void);
/** @see get_tvoc() */
void pc_tvoc(void);
/** @see get_co2() */
void pc_co2(void);
/** @see get_gesture() */
void pc_gesture(void);
/** @see ledmatrix_dot() */
void pc_lmdot(uint8_t, uint8_t, bool);
/** @see ledmatrix_intensity() */
void pc_lmintensity(uint8_t);
/** @see ledmatrix_clear() */
void pc_lmclear(void);
/** @see ledmatrix_display() */
void pc_lmdisplay(void);

/** @see msg_log() */
void pc_msg_log(const char *, va_list);

/**
 * Initialise the frontend
 */
void pc_init(void);
/**
 * Reset the frontend
 *
 * @see reset()
 */
void pc_reset(void);
/**
 * Exit the frontend
 */
void pc_exit(void);

/** @see real_yield() */
void pc_yield(void);

#endif /* !PC_INTERFACE_H */

