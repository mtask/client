#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "../interface.h"
#include "../nvm.h"

#define my_fputc(c, fh) do {\
	if (fputc((c), (fh)) == EOF) {\
		pdie("fputc");\
	}\
} while (0)
#define my_fgetc(res, fh) do {\
	(res) = fgetc((fh));\
	if (res == EOF) {\
		pdie("fgetc");\
	}\
} while (0)
#define my_fclose(fh) do {\
	if (fclose((fh)) != 0) {\
		pdie("fclose");\
	}\
	fh = NULL;\
} while (0)
#define my_fopen(res, path, mode) do {\
	(res) = fopen((path), (mode));\
	if ((res) == NULL) {\
		pdie("fopen");\
	}\
} while (0)
#define my_malloc(target, sz) do {\
	(target) = malloc((sz));\
	if ((target) == NULL) {\
		pdie(PSTR("malloc"));\
	}\
} while (0)

// from pc.c
extern char *ic_swapfile;
extern bool intermittent;

struct nvm_info {
	FILE *fhandle;
	char *left;
	char *right;
	bool use_left;
	bool readonly;
} info =
	{ .fhandle = NULL
	, .left = NULL
	, .right = NULL
	, .use_left = false
	, .readonly = false
	};

enum nvm_status { valid = 42, invalid = 38};
#if LOGLEVEL > 1
char *nvm_status_string[] = {[valid] = "valid", [invalid] = "invalid"};
#endif /* LOGLEVEL > 1 */

static inline void init_left_right (void)
{
	if (info.left == NULL) {
		size_t len = strlen(ic_swapfile)+4+1;
		my_malloc(info.left, len);
		my_malloc(info.right, len);
		snprintf(info.left, len, "%s.msl", ic_swapfile);
		snprintf(info.right, len, "%s.msr", ic_swapfile);
	}
}

static uint8_t get_status_byte(char *path)
{
	FILE *fhandle = fopen(path, "r");
	if (fhandle == NULL) {
		if (errno == ENOENT) {
			return invalid;
		}
		pdie("fopen");
	}
	uint8_t res = invalid;
	int c = fgetc(fhandle);
	switch (c) {
	case valid:
		res = valid;
		break;
	case EOF:
		// Error
		if (!feof(fhandle)) {
			pdie("fgetc");
		}
	/* fall-through */
	case invalid:
	/* fall-through */
	default: // please
		break;
	}
	my_fclose(fhandle);
	return res;
}

static void set_status_byte(char *path, enum nvm_status status)
{
	msg_debug(PSTR("set_status_byte: %s: %s\n"), path,
		nvm_status_string[status]);
	FILE *fhandle = fopen(path, "r+");
	if (fhandle == NULL) {
		if (errno == ENOENT) {
			my_fopen(fhandle, path, "w");
		} else {
			pdie("fopen");
		}
	}
	my_fputc(status, fhandle);
	my_fclose(fhandle);
}

bool nvm_open_read(void)
{
	if (info.fhandle != NULL) {
		die("nvm was already open\n");
	}
	init_left_right();
	info.readonly = true;

	// try left
	if (get_status_byte(info.left) == valid) {
		// Left was valid, use left
		my_fopen(info.fhandle, info.left, "r");
		info.use_left = true;
	} else if (get_status_byte(info.right) == valid) {
		// Right was valid, use right
		my_fopen(info.fhandle, info.right, "r");
		info.use_left = false;
	} else {
		msg_log(PSTR("left and right are both invalid\n"));
		info.fhandle = NULL;
		return false;
	}
	int res;
	my_fgetc(res, info.fhandle);
	if (res != valid) {
		die("something went horribly wrong\n");
	}
	return true;
}

void nvm_open_write(void)
{
	if (info.fhandle != NULL) {
		die("nvm was already open\n");
	}
	init_left_right();
	info.readonly = false;

	// try left
	if (get_status_byte(info.left) == valid) {
		msg_debug(PSTR("left was valid, use right\n"));
		// Left was valid, use right
		my_fopen(info.fhandle, info.right, "w+");
		info.use_left = false;
	} else {
		msg_debug(PSTR("left was valid, use right\n"));
#if LOGLEVEL > 1
		if (get_status_byte(info.right) == valid) {
			msg_debug(PSTR("right is also valid... strange\n"));
		}
#endif /* LOGLEVEL > 1 */
		// Left was valid, use right
		my_fopen(info.fhandle, info.left, "w+");
		info.use_left = true;
	}
	//invalidate before writing anything
	my_fputc(invalid, info.fhandle);
}

uint8_t nvm_get(void)
{
	if (info.fhandle == NULL) {
		die("nvm_get called on NULL state\n");
	}
	if (!info.readonly) {
		die("nvm_get called on write mode nvm\n");
	}
	int res;
	my_fgetc(res, info.fhandle);
	return res;
}

void nvm_put(uint8_t b)
{
	if (info.fhandle == NULL) {
		die("nvm_put called on NULL state\n");
	}
	if (info.readonly) {
		die("nvm_put called on read mode nvm\n");
	}
	my_fputc(b, info.fhandle);
}

void nvm_close_read(void)
{
	if (info.fhandle == NULL) {
		die("nvm_close_read called on NULL fhandle\n");
	}
	my_fclose(info.fhandle);
}

void nvm_close_write(void)
{
	msg_debug(PSTR("nvm_close_write\n"));
	if (info.fhandle == NULL) {
		die("nvm_close_read called on NULL fhandle\n");
	}
	my_fclose(info.fhandle);
	// Left was used to write, so we invalidate right and validate left
	if (info.use_left) {
		set_status_byte(info.left, valid);
		set_status_byte(info.right, invalid);
	// Right was used to write, so we invalidate left and validate right
	} else {
		set_status_byte(info.right, valid);
		set_status_byte(info.left, invalid);
	}
}

void nvm_invalidate(void)
{
	init_left_right();
	set_status_byte(info.left, invalid);
	set_status_byte(info.right, invalid);
}
