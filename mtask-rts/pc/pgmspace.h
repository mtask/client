/**
 * @file pc/pgmspace.h
 *
 * A port of avr/pgmspace.h for using on linux/windows/mac.
 * \note It only implements the near operations
 */

//If we are in the arduino environment we don't need it
#ifndef ARDUINO

#ifndef PC_PGMSPACE_H
#define PC_PGMSPACE_H

#include <string.h>

#define PROGMEM
#define PGM_P const char *
#define PGM_VOID_P const void *
#define PSTR(str) (str)

#define pgm_read_byte_near(p)  (* (const uint8_t*) (p))
#define pgm_read_word_near(p)  (* (const uint16_t*) (p))
#define pgm_read_dword_near(p) (* (const uint32_t*) (p))
#define pgm_read_float_near(p) (* (const float*) (p))
#define pgm_read_ptr_near(p)   (* (const void* const*) (p))

#define pgm_read_byte(p)  pgm_read_byte_near(p)
#define pgm_read_word(p)  pgm_read_word_near(p)
#define pgm_read_dword(p) pgm_read_dword_near(p)
#define pgm_read_float(p) pgm_read_float_near(p)
#define pgm_read_ptr(p)   pgm_read_ptr_near(p)

#define strcasestr_P strcasestr
#define strcat_P strcat
#define strcpy_P strcpy
#define strncat_P strncat
#define strncpy_P strncpy
#define strpbrk_P strpbrk
#define strsep_P strsep
#define strstr_P strstr
#define strtok_P strtok
#define strchr_P strchr
#define strchrnul_P strchrnul
#define strrchr_P strrchr
#define memchr_P memchr
#define memrchr_P memrchr
#define memcmp_P memcmp
#define strcasecmp_P strcasecmp
#define strcmp_P strcmp
#define strncasecmp_P strncasecmp
#define strncmp_P strncmp
#define strcspn_P strcspn
#define strlcat_P strlcat
#define strlcpy_P strlcpy
#define strnlen_P strnlen
#define strspn_P strspn
#define memccpy_P memccpy
#define memcpy_P memcpy
#define memmem_P memmem

#endif /* !PC_PGMSPACE.H */

#endif /* !ARDUINO */
