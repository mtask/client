/** @file rewrite.h
 *
 * Contains the functions to do task rewriting
 */
#ifndef REWRITE_H
#define REWRITE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

#include "tasktree.h"

/**
 * Destructively rewrites a tasktree
 *
 * @param t task tree to rewrite
 * @param program pointer to the bytecode
 * @param stack stack pointer
 * @param removed flag set when a tasktree is removed
 *	(and gc needs to be called after the run)
 * @return new stack pointer or 0 if there was an error. A task always yields a
 *	task value so it is bigger than 0 always.
 */
uint16_t rewrite(struct TaskTree *t, uint8_t *program, uintptr_t *stack,
	bool *removed);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !REWRITE_H */

