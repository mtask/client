#if defined(ARDUINO_ARCH_esp8266) || defined(ARDUINO_ARCH_ESP8266) ||\
	defined (ARDUINO_ARCH_ESP32) || defined(ADAFRUIT_FEATHER_M0)

#include <Arduino.h>
#include <stdbool.h>
#include <stdint.h>

#include "interface.h"
#include "link_interface.h"
#include "config_arduino.h"
#if defined(ARDUINO_ARCH_ESP8266)
	#include <ESP8266WiFi.h>
	#define TO_IPADDRESS(x) x
#elif defined(ARDUINO_ARCH_ESP32)
	#include <WiFi.h>
	#define TO_IPADDRESS(x) x
#elif defined(ADAFRUIT_FEATHER_M0)
	#include <SPI.h>
	#include <WiFi101.h>
	#define TO_IPADDRESS(x) IPAddress(x)
#else /* ADAFRUIT_FEATHER_M0 */
	#error Wifi not supported on current device
#endif /* ADAFRUIT_FEATHER_M0 */

uint16_t default_port = LINK_PORT;

WiFiClient client;
bool link_continuation = false;

bool link_input_available(void)
{
#ifndef ARDUINO_ARCH_ESP32
	if (!client.connected()) {
		print_to_display(PSTR("Server disconnected\n"));
		delay(1000);
		reset();
	}
#endif /* !defined(ARDUINO_ARCH_ESP32) */
	return client.available();
}

uint8_t link_read_byte(void)
{
	while (!link_input_available())
		msdelay(5);
	return client.read();
}

void link_write_byte(uint8_t b)
{
	client.write(b);
}

void open_link(void)
{
	const char *ssids[] = SSIDS;
	const char *wpas[] = WPAS;

#if defined(ADAFRUIT_FEATHER_M0)
	WiFi.setPins(8, 7, 4, 2);
#else /* ADAFRUIT_FEATHER_M0 */
	WiFi.mode(WIFI_STA);
#  ifdef ARDUINO_ESP32
	WiFi.setSleep(WIFI_PS_MAX_MODEM);
#  elif defined(ARDUINO_ESP8266)
	WiFi.setSleepMode(WIFI_LIGHT_SLEEP);
#  endif
#endif /* ADAFRUIT_FEATHER_M0 */

	int ssid = -1;

	if (!link_continuation) {
		do {
			Serial.println("Scanning...");
			print_to_display("Scanning..\n");
			int n = WiFi.scanNetworks();
			Serial.print("Found ");
			Serial.print(n);
			Serial.println(" networks");
			print_to_display(String(n).c_str());
			print_to_display(" found\n");
			flush_display();
			if (n == 0) {
				print_to_display("none found\n");
				flush_display();
				Serial.println("No networks found");
				delay(1000);
				continue;
			}
			for (unsigned j = 0; j<sizeof(ssids)/sizeof(char *);
					j++) {
				for (int i = 0; i < n; i++) {
					if (WiFi.SSID(i) == ssids[j]) {
						print_to_display("Try ");
						print_to_display(
							WiFi.SSID(i).c_str());
						print_to_display("\n");
						flush_display();
						Serial.print("Connect to: ");
						Serial.println(WiFi.SSID(i));
						ssid = j;
					}
				}
			}
		} while (ssid == -1);
#ifndef ADAFRUIT_FEATHER_M0
		WiFi.scanDelete();
#endif
		WiFi.begin(ssids[ssid], wpas[ssid]);
	} else {
		WiFi.begin();
	}
	while (WiFi.status() != WL_CONNECTED) {
		delay(1000);
		Serial.print(".");
		print_to_display(".");
		flush_display();
	}
	Serial.print("\n");

#ifdef ADAFRUIT_FEATHER_M0
	WiFi.maxLowPowerMode();
#endif

	clear_display();
	print_to_display("Connected to: ");
	print_to_display(ssids[ssid]);
	print_to_display(" ");
	print_to_display(WiFi.localIP().toString().c_str());
	print_to_display(":");
	print_to_display(String(LINK_PORT).c_str());
	print_to_display("\n");
	flush_display();
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(TO_IPADDRESS(WiFi.localIP()));

// With direct communication we operate as a server
#ifdef COMM_DIRECT
	WiFiServer server(LINK_PORT);

	server.begin();
#ifndef ADAFRUIT_FEATHER_M0
	server.setNoDelay(true);
#endif
	Serial.print("Server started on port: ");
	Serial.println(LINK_PORT);

	Serial.println("Waiting for a client to connect.");
	while (!(client = server.available())) {
		delay(10);
	}

	print_to_display("Client:\n");
	print_to_display(client.remoteIP().toString().c_str());
	flush_display();
	Serial.println("Client connected\n");
#else /* COMM_DIRECT */
	if (!client.connect(LINK_HOST, LINK_PORT)) {
		Serial.println("Connecting to server failed.");
		reset();
	}
#endif
}

void close_link(bool temporary)
{
	link_continuation = temporary;
	client.stop();
#ifdef ADAFRUIT_FEATHER_M0
	WiFi.end();
#endif
}

#endif /* defined(ARDUINO_ARCH_esp8266) || defined(ARDUINO_ARCH_ESP8266) ||\
	defined (ARDUINO_ARCH_ESP32) || defined(ADAFRUIT_FEATHER_M0)*/

