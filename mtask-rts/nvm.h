/** @file
 *
 * Contains the high-level non-volatile memory interface.
 */
#ifndef NVM_H
#define NVM_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef NVM
#include "nvm_interface.h"

/**
 * Save the current state to non-volatile memory saved state.
 */
void nvm_save(void);

/**
 * Resume from non-volatile memory saved state.
 */
bool nvm_restore(void);
#endif /* NVM */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !NVM_H */
