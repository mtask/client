/** @file events.h
 *
 * Contains all event handling. Events are saved in the task tree heap so that
 * they are automatically garbage collected when marked as trash
 */
#ifndef EVENTS_H_
#define EVENTS_H_

#include "task.h"
#include "tasktree.h"

/**
 * Return the head of the linked list of event task tree nodes
 *
 * @return head of the list
 */
struct TaskTree *event_list_head(void);
/**
 * Follow the tail of the event task tree node
 *
 * @param e head of the list
 * @return tail of the list
 */
struct TaskTree *event_list_next(struct TaskTree *e);
/**
 * Remove a certain event from the events list
 *
 * @param e element to remove
 */
void remove_event(struct TaskTree *e);

/**
 * Register an event for a given task and their tree. Registers the interrupt if
 * the task tree is an interrupt node.
 *
 * @param t task
 * @param tt task tree
 */
void register_events(struct MTaskMeta *t, struct TaskTree *tt);
/**
 * Deregister an event for a given task and their tree. Deregisters the
 * interrupt if the task tree is an interrupt node and if there are no other
 * tasks waiting for that interrupt.
 *
 * @param t task
 * @param tt task tree
 */
void deregister_events(struct MTaskMeta *t, struct TaskTree *tt);

#endif /* EVENTS_H_ */
