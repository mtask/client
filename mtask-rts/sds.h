/** @file sds.h
 *
 * Contains all functions to work with shared data sources (sdss).
 *
 * \todo make the interface uniform, let them all work on sharespecs
 */
#ifndef SDS_H
#define SDS_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "types.h"

/**
 * Get a sds for a given id.
 *
 * @param id sds id
 * @return pointer to the sds struct
 */
struct BCShareSpec *sds_get(uint8_t id);
/**
 * Update the value of an sds for a given task and sds id from a byte array.
 *
 * @param taskid task id
 * @param sdsid sds id
 * @param value new value
 * @throws MTESdsUnknown if the sds is unknown
 */
void sds_update(uint8_t taskid, uint8_t sdsid, String255 value);
/**
 * Update the value of an sds for a given task and sds id from an mTask stack.
 *
 * @param sds pointer to the sds struct
 * @param stack pointer to the stack where the new value begins
 * @return whether it was the same or not
 */
bool sds_set_mtask(struct BCShareSpec *sds, uintptr_t *stack);
/**
 * Push the current value of an sds on the stack
 *
 * @param sds pointer to the sds struct
 * @param stack pointer to the stack
 * @return stack pointer
 */
uint16_t sds_get_mtask(struct BCShareSpec *sds, uintptr_t *stack);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !SDS_H */

