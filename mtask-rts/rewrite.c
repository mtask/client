#include "rewrite.h"

#include "interpret.h"
#include "mem.h"
#include "events.h"
#include "sds.h"
#include "tasktree.h"
#include "types.h"

extern struct MTaskMeta *current_task;
extern uint32_t now;

//TODO make non-recursive
uint16_t rewrite(struct TaskTree *t, uint8_t *program, uintptr_t *stack,
	bool *removed)
{
	uint16_t sp = 0, sp1 = 0, sp2 = 0, sp3 = 0;
	uintptr_t t16;
	float tfloat;
	uint32_t t32;
#define safe_rewrite(t, stack, res) do {\
	(res) = rewrite(t, program, stack, removed);\
	if ((res) == 0)\
		return 0;\
} while (0)
#define safe_interpret(t, stack, sp) do {\
	if (!interpret(program, t, sp, stack))\
		return 0;\
} while (0)
//	for (uint16_t j = 0; j<sp+2; j++)
//		msg_debug(PSTR("stack[%u]: %u\n"), j, stack[j]);

	msg_debug(PSTR("Rewriting code stack: %p\nrewrite: "), stack);
	tasktree_print_node(t);
	msg_debug(PSTR("\n"));
	switch (t->task_type) {
//Constant node values {{{
	case BCStableNode_c:
		push_stack_rzero(stack, sp, MTStable_c);
		push_stack_rzero(stack, sp, t->data.stablenode.stable[0]);
		// This pushes MTSTable as well
		safe_rewrite(t->data.stablenode.next, stack+sp, sp1);
		// So we overwrite it immediately
		push_stack_rzero(stack, sp, t->data.stablenode.stable[1]);
		// Increment stack pointer with the width of the next node minus
		// the taskvalue state
		inc_sp_rzero(stack, sp, sp1-1);
		break;
	case BCStable0_c:
		push_stack_rzero(stack, sp, MTStable_c);
		break;
	case BCStable1_c:
		push_stack_rzero(stack, sp, MTStable_c);
		push_stack_rzero(stack, sp, t->data.stable[0]);
		break;
	case BCStable2_c:
		push_stack_rzero(stack, sp, MTStable_c);
		push_stack_rzero(stack, sp, t->data.stable[0]);
		push_stack_rzero(stack, sp, t->data.stable[1]);
		break;
	case BCStable3_c:
		push_stack_rzero(stack, sp, MTStable_c);
		push_stack_rzero(stack, sp, t->data.stable[0]);
		push_stack_rzero(stack, sp, t->data.stable[1]);
		push_stack_rzero(stack, sp, t->data.stable[2]);
		break;
	case BCStable4_c:
		push_stack_rzero(stack, sp, MTStable_c);
		push_stack_rzero(stack, sp, t->data.stable[0]);
		push_stack_rzero(stack, sp, t->data.stable[1]);
		push_stack_rzero(stack, sp, t->data.stable[2]);
		push_stack_rzero(stack, sp, t->data.stable[3]);
		break;
	case BCUnstableNode_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		push_stack_rzero(stack, sp, t->data.unstablenode.unstable[0]);
		// This pushes MTSTable as well
		safe_rewrite(t->data.unstablenode.next, stack+sp, sp1);
		// So we overwrite it immediately
		push_stack_rzero(stack, sp, t->data.unstablenode.unstable[1]);
		// Increment stack pointer with the width of the next node minus
		// the taskvalue state
		inc_sp_rzero(stack, sp, sp1-1);
		break;
	case BCUnstable0_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		break;
	case BCUnstable1_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		push_stack_rzero(stack, sp, t->data.unstable[0]);
		break;
	case BCUnstable2_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		push_stack_rzero(stack, sp, t->data.unstable[0]);
		push_stack_rzero(stack, sp, t->data.unstable[1]);
		break;
	case BCUnstable3_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		push_stack_rzero(stack, sp, t->data.unstable[0]);
		push_stack_rzero(stack, sp, t->data.unstable[1]);
		push_stack_rzero(stack, sp, t->data.unstable[2]);
		break;
	case BCUnstable4_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		push_stack_rzero(stack, sp, t->data.unstable[0]);
		push_stack_rzero(stack, sp, t->data.unstable[1]);
		push_stack_rzero(stack, sp, t->data.unstable[2]);
		push_stack_rzero(stack, sp, t->data.unstable[3]);
		break;
//}}}
//Pin IO {{{
	case BCReadD_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		push_stack_rzero(stack, sp, read_dpin(t->data.readd));
		break;
	case BCWriteD_c:
		write_dpin(t->data.writed.pin, t->data.writed.value);

		push_stack_rzero(stack, sp, MTStable_c);
		push_stack_rzero(stack, sp, t->data.writed.value);

		t->task_type = BCStable1_c;
		t->data.stable[0] = t->data.writed.value;

		if (current_task->status != MTUnevaluated_c)
			current_task->status = MTPurged_c;
		break;
	case BCReadA_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		push_stack_rzero(stack, sp, read_apin(t->data.reada));
		break;
	case BCWriteA_c:
		write_apin(t->data.writea.pin, t->data.writea.value);

		push_stack_rzero(stack, sp, MTStable_c);
		push_stack_rzero(stack, sp, t->data.writea.value);

		t->task_type = BCStable1_c;
		t->data.stable[0] = t->data.writea.value;

		if (current_task->status != MTUnevaluated_c)
			current_task->status = MTPurged_c;
		break;
	case BCPinMode_c:
		set_pinmode(t->data.pinmode.pin, t->data.pinmode.pinmode);
		t->task_type = BCStable0_c;
		push_stack_rzero(stack, sp, MTStable_c);

		if (current_task->status != MTUnevaluated_c)
			current_task->status = MTPurged_c;
		break;
	case BCInterrupt_c:
		if (!t->data.interrupt.initialized) {
			register_events(current_task, t);
			t->data.interrupt.initialized = true;
		}

		if (!t->data.interrupt.triggered) {
			push_stack_rzero(stack, sp, MTNoValue_c);
			// Interrupt has width 1
			inc_sp(stack, sp, 1);
		} else {
			t->task_type = BCStable1_c;
			t->data.stable[0] = t->data.interrupt.status;

			push_stack_rzero(stack, sp, MTStable_c);
			push_stack_rzero(stack, sp, t->data.interrupt.status);
		}
		break;
//}}}
// Steps {{{
	case BCStep_c:
		// Safe the original stack pointer
		sp1 = sp;
		// Prepare the stack for interpretation
		initstackmain(&sp, stack);
		// Rewrite (the result are the arguments)
		safe_rewrite(t->data.step.lhs, stack+sp, sp2);
		//Width might be lost if we need to rewrite
		t16 = t->data.steps.w;
		safe_interpret(t->data.step.rhs, stack, sp+sp2);
		//Save the width, it might be destroyed if we had a match
		t16 = t->data.step.w;
		//There was no match...
		if (stack[sp1] == (uintptr_t)NULL) {
			msg_debug(PSTR("Step nomatch\n"));
		} else {
			msg_debug(PSTR("Step match: %u\n"), stack[sp1]);
			//Clean up lhs
			*removed = true;
			mem_mark_trash_and_destroy(t->data.step.lhs,
				current_task, stack+sp1+1);
			//Move the node to t
			mem_node_move(t, (struct TaskTree *)stack[sp1]);
			// Mark as changed
			current_task->status = MTUnevaluated_c;
		}
		// Restore the original stack pointer
		sp = sp1;
		push_stack_rzero(stack, sp, MTNoValue_c);
		// Pad the value up to the width of the rhs
		inc_sp(stack, sp, t16);
		break;
	case BCStepStable_c:
		// Safe the original stack pointer
		sp1 = sp;
		// Prepare the stack for interpretation
		initstackmain(&sp, stack);
		// Rewrite (the result are the arguments)
		safe_rewrite(t->data.steps.lhs, stack+sp, sp2);
		//Width might be lost if we need to rewrite
		t16 = t->data.steps.w;
		//Stable, so we step
		if (stack[sp] == MTStable_c) {
			//Interpret the rhs
			safe_interpret(t->data.steps.rhs, stack, sp+sp2);
			//Clean up lhs
			*removed = true;
			mem_mark_trash_and_destroy(t->data.steps.lhs,
				current_task, stack+sp1+1);
			//Move the node to t
			mem_node_move(t, (struct TaskTree *)stack[sp1]);
			// Mark as changed
			current_task->status = MTUnevaluated_c;
		}
		// Restore the original stack pointer
		sp = sp1;
		// Step never yields a value
		push_stack_rzero(stack, sp, MTNoValue_c);
		// Pad the value up to the width of the rhs
		inc_sp(stack, sp, t16);
		break;
	case BCStepUnstable_c:
		// Safe the original stack pointer
		sp1 = sp;
		// Prepare the stack for interpretation
		initstackmain(&sp, stack);
		// Rewrite (the result are the arguments)
		safe_rewrite(t->data.stepu.lhs, stack+sp, sp2);
		//Width might be lost if we need to rewrite
		t16 = t->data.stepu.w;
		//Stable, so we step
		if (stack[sp] != MTNoValue_c) {
			msg_debug(PSTR("Step match\n"));
			//Interpret the rhs
			safe_interpret(t->data.stepu.rhs, stack, sp+sp2);
			//Clean up lhs
			*removed = true;
			mem_mark_trash_and_destroy(t->data.stepu.lhs,
				current_task, stack+sp1+1);
			//Move the node to t
			mem_node_move(t, (struct TaskTree *)stack[sp1]);
			// Mark as changed
			current_task->status = MTUnevaluated_c;
		} else {
			msg_debug(PSTR("Step nomatch\n"));
		}
		// Restore the original stack pointer
		sp = sp1;
		// Step never yields a value
		push_stack_rzero(stack, sp, MTNoValue_c);
		// Pad the value up to the width of the rhs
		inc_sp_rzero(stack, sp, t16);
		break;
	case BCSeqStable_c:
		// Safe the original stack pointer
		sp = sp1;
		//Rewrite lhs
		safe_rewrite(t->data.seqs.lhs, stack+sp, sp2);
		//Save the width, it might be destroyed if we had a match
		t16 = t->data.seqs.w;
		//There was no match...
		if (stack[sp] != MTStable_c) {
			msg_debug(PSTR("Step nomatch\n"));
		} else {
			msg_debug(PSTR("Step match\n"));
			//Clean up lhs
			*removed = true;
			mem_mark_trash_and_destroy(
				t->data.seqs.lhs, current_task, stack+sp);
			//Move the node to t
			mem_node_move(t, t->data.seqs.rhs);
			// Mark as changed
			current_task->status = MTUnevaluated_c;
		}
		// Restore the original stack pointer
		sp = sp1;
		// Step never yields a value
		push_stack_rzero(stack, sp, MTNoValue_c);
		// Pad the value up to the width of the rhs
		inc_sp_rzero(stack, sp, t16);
		break;
	case BCSeqUnstable_c:
		// Safe the original stack pointer
		sp = sp1;
		//Rewrite lhs
		safe_rewrite(t->data.sequ.lhs, stack+sp, sp2);
		//Save the width, it might be destroyed if we had a match
		t16 = t->data.sequ.w;
		//There was no match...
		if (stack[sp] == MTNoValue_c) {
			msg_debug(PSTR("Step nomatch\n"));
		} else {
			msg_debug(PSTR("Step match\n"));
			//Clean up lhs
			*removed = true;
			mem_mark_trash_and_destroy(
				t->data.sequ.lhs, current_task, stack+sp);
			//Move the node to t
			mem_node_move(t, t->data.sequ.rhs);
			// Mark as changed
			current_task->status = MTUnevaluated_c;
		}
		// Restore the original stack pointer
		sp = sp1;
		// Step never yields a value
		push_stack_rzero(stack, sp, MTNoValue_c);
		// Pad the value up to the width of the rhs
		inc_sp_rzero(stack, sp, t16);
		break;
//}}}
//Repeat, delay {{{
	case BCRepeat_c:
		if (t->data.repeat.tree == NULL) {
			msg_debug(PSTR("Clone repeat tree\n"));
			t->data.repeat.done = false;
			if (t->data.repeat.start == 0)
				t->data.repeat.start = current_task->lastrun;
			t->data.repeat.tree =
				tasktree_clone(t->data.repeat.oldtree, t);
			if (t->data.repeat.tree == NULL)
				return 0;
			tasktree_print(t->data.repeat.tree, 0);
			msg_debug(PSTR("\n"));

			current_task->status = MTUnevaluated_c;
		}
		safe_rewrite(t->data.repeat.tree, stack+sp, sp1);

		// Stable
		if (stack[sp] == MTStable_c) {
			t->data.repeat.done = true;
		}
		stack[sp] = MTNoValue_c;

		// Reinitialize
		if (t->data.repeat.done &&
				current_task->lastrun - t->data.repeat.start >=
				t->refresh_min * (t->seconds ? 1000u : 1u)) {
			//Clean up lhs
			*removed = true;
			mem_mark_trash_and_destroy(
				t->data.repeat.tree, current_task, stack+sp+1);
			t->data.repeat.tree = NULL;
			// Set the next start time to the current time
			// Or the maximum run time + last start time if the task
			// was overdue
			if (current_task->lastrun - t->data.repeat.start >=
					t->refresh_max *
						(t->seconds ? 1000u : 1u)) {
				t->data.repeat.start =
					t->data.repeat.start +
					t->refresh_max *
						(t->seconds ? 1000u : 1u);
			} else {
				t->data.repeat.start = current_task->lastrun;
			}
		}

		if (current_task->status != MTUnevaluated_c &&
			t->data.repeat.done)
			current_task->status = MTPurged_c;
		break;
	case BCDelay_c:
		t->data.until = now + t->data.delay;
		t->task_type = BCDelayUntil_c;
		/*-fallthrough*/
	case BCDelayUntil_c:
		msg_debug(PSTR("now: %lu\n"), now);
		//Overshot time
		t32 = now-t->data.until;
		push_stack_rzero(stack, sp, MTUnstable_c);
		push_stack_rzero(stack, sp, (t32 >> 16) & 0xffff);
		push_stack_rzero(stack, sp, t32 & 0xffff);
		if ((int32_t)t32 >= 0) {
			stack[sp-3] = MTStable_c;
			t->task_type = BCStable1_c;
			t->data.until = (int32_t)t32;
			t->data.stable[0] = stack[sp-2];
			t->data.stable[1] = stack[sp-1];
		}

		if (current_task->status != MTUnevaluated_c)
			current_task->status = MTPurged_c;
		break;
//}}}
//Parallel {{{
	case BCTOr_c:
		safe_rewrite(t->data.tor.lhs, stack+sp, sp1);
		msg_debug(PSTR("rewritten lhs(sp1=%u): %lu %lu\n")
			, sp1, stack[sp], stack[sp+1]);
		if (stack[sp] == MTStable_c) {
			msg_debug(PSTR("lhs is stable, done\n"));
			// Evaluate rhs just for fun
			safe_rewrite(t->data.tor.rhs, stack+sp+sp1, sp2);
			msg_debug(PSTR("rewritten rhs(sp2=%u): %lu %lu\n")
				, sp2, stack[sp+sp1], stack[sp+sp1+1]);
			// Clean up rhs
			*removed = true;
			mem_mark_trash_and_destroy(
				t->data.tor.rhs, current_task, stack+sp1);
			mem_node_move(t, t->data.tor.lhs);
			if (current_task->status != MTUnevaluated_c)
				current_task->status = MTPurged_c;
		} else if (stack[sp] == MTNoValue_c) {
			msg_debug(PSTR("lhs has no value, just eval rhs\n"));
			// This overwrites the lhs completely (which is fine)
			safe_rewrite(t->data.tor.rhs, stack+sp, sp1);
		} else { // stack[sp] == MTUnstable_c
			msg_debug(PSTR("lhs unstable eval rhs and combine\n"));
			// safe the last cell of the left-hand side
			safe_rewrite(t->data.tor.rhs, stack+sp+sp1, sp2);
			msg_debug(PSTR("rewritten rhs(sp1=%u): %lu %lu\n")
				, sp2, stack[sp+sp1], stack[sp+sp1+1]);
			// rhs was stable, overwrite lhs
			if (stack[sp+sp1] == MTStable_c) {
				for (uint8_t i = 0; i<sp1; i++) {
					stack[sp+i] = stack[sp+sp1+i];
				}
				//Clean up lhs
				*removed = true;
				mem_mark_trash_and_destroy(t->data.tor.lhs,
					current_task, stack+sp1);
				//Move the rhs to t
				mem_node_move(t, t->data.tor.rhs);
				if (current_task->status != MTUnevaluated_c)
					current_task->status = MTPurged_c;
			} // if it was unstable, lhs value is used
		}
		sp += sp1;
		break;
	case BCTAnd_c:
		safe_rewrite(t->data.tand.lhs, stack+sp, sp1);
		// safe the last cell of the left-hand side
		t16 = stack[sp+sp1-1];
		// rewrite the rhs, overwrite the left-hand side cell
		safe_rewrite(t->data.tand.rhs, stack+sp+sp1-1, sp2);
		// lhs stability stack[sp]
		// rhs stability stack[sp+sp1]
		// One is novalue, the whole is novalue
		if (stack[sp] == MTNoValue_c
				|| stack[sp+sp1-1] == MTNoValue_c) {
			msg_debug(PSTR("one of the two sides is novalue\n"));
			stack[sp] = MTNoValue_c;
		// One is unstable, the whole is unstable
		} else if (stack[sp] == MTUnstable_c
				|| stack[sp+sp1-1] == MTUnstable_c) {
			msg_debug(PSTR("one of the two sides is unstable\n"));
			stack[sp] = MTUnstable_c;
		} else {// otherwise, both are stable, nothing to do
			msg_debug(PSTR("both sides are stable\n"));
		}
		// restore the last cell of the left-hand side
		stack[sp+sp1-1] = t16;
		// increment the stack pointer with the width of the value
		sp = sp + sp1 - 1 + sp2;
		break;
//}}}
//Sds {{{
	case BCSdsGet_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		sp1 = sds_get_mtask(t->data.sdsget, stack+sp);
		sp += sp1;
		break;
	case BCReflect_c:
		// Rewrite the task
		safe_rewrite(t->data.reflect.task, stack+sp, sp1);
		// stack+sp+1 because we jump over de stability
		sds_set_mtask(t->data.reflect.sds, stack+sp+1);
		// If the result is stable, we replace the reflect node by the
		// child node
		if (stack[sp] == MTStable_c) {
			mem_node_move(t, t->data.reflect.task);
		}
		sp += sp1;
		break;
	case BCSdsSet_c:
		safe_rewrite(t->data.sdsset.data, stack+sp, sp1);
		//Write to the actual share and update server if necessary
		//+1 to jump over the stability
		sds_set_mtask(t->data.sdsset.sds, stack+sp+1);
		//Set this node to be the data
		mem_node_move(t, t->data.sdsset.data);

		if (current_task->status != MTUnevaluated_c) {
			current_task->status = MTPurged_c;
		}
		sp += sp1;
		break;
	case BCSdsUpd_c:
		// Safe the original stack pointer
		sp1 = sp;
		// Prepare the stack for interpretation
		initstackmain(&sp, stack);
		// Push the old value of the sds
		// sp2 = width of old value
		sp2 = sds_get_mtask(t->data.sdsupd.sds, stack+sp);
		// Rewrite the context, it is a task value so we save the last
		// cell
		t16 = stack[sp+sp2-1];
		safe_rewrite(t->data.sdsupd.ctx, stack+sp+sp2-1, sp3);
		// Restore the last cell
		stack[sp+sp2-1] = t16;
		// interpret the function
		safe_interpret(t->data.sdsupd.fun, stack, sp+sp2-1+sp3);
		// Set the stack pointer back to the original TOS
		sp = sp1;
		// This results in a stable node.
		t16 = stack[sp];
		// Push the result to the stack
		safe_rewrite((struct TaskTree *)t16, stack+sp, sp1);
		// Write the new value for the SDS
		sds_set_mtask(t->data.sdsupd.sds, stack+sp+1);
		//Clean up ctx
		*removed = true;
		mem_mark_trash_and_destroy(
			t->data.sdsupd.ctx, current_task, stack+sp+sp1);
		//Set this node to be the data
		mem_node_move(t, (struct TaskTree *)t16);
		if (current_task->status != MTUnevaluated_c)
			current_task->status = MTPurged_c;
		// Increment the stack pointer
		sp = sp1;
		break;
//}}}
//Rate limit {{{
	case BCRateLimit_c:
		if (t->data.ratelimit.storage == NULL ||
			current_task->lastrun -
				t->data.ratelimit.last_execution >=
				t->refresh_min * (t->seconds ? 1000u : 1u)) {
			msg_debug(PSTR("ratelimit task rewritten because %s\n")
				, t->data.ratelimit.storage == NULL
				? "there was no old value"
				: "enough time has passed");
			// Rewrite the task
			safe_rewrite(t->data.ratelimit.task, stack+sp, sp1);
			// Set the last execution
			t->data.ratelimit.last_execution =
				current_task->lastrun;
			// Clean up the old value if there was one
			if (t->data.ratelimit.storage != NULL) {
				mem_mark_trash_and_destroy(
					t->data.ratelimit.storage,
					current_task, stack+sp+sp1);
			}
			// Store this value for future reevaluations
			t->data.ratelimit.storage =
			// -1 and +1 because we skip the stability
				create_result_task(sp1-1, stack+sp+1);
			tasktree_set_ptr_tree(t->data.ratelimit.storage, t);
			// Increment the stack pointer with the value
			sp += sp1;
		} else {
			msg_debug(PSTR(
				"Return stored result, %u"
				"ms until new result\n"),
				(t->data.ratelimit.last_execution +
				 t->refresh_min  * (t->seconds ? 1000u : 1u)) -
					current_task->lastrun);

			safe_rewrite(t->data.ratelimit.storage, stack+sp, sp1);
		}

		if (current_task->status != MTUnevaluated_c)
			current_task->status = MTPurged_c;

		break;
//}}}
//Peripherals {{{
//DHT {{{
#ifdef HAVE_DHT
	case BCDHTTemp_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		tfloat = get_dht_temp(t->data.peripheral.info);
		push_stack_rzero(stack, sp, float2uint32_t(tfloat) >> 16);
		push_stack_rzero(stack, sp, float2uint32_t(tfloat) & 0xffff);
		break;
	case BCDHTHumid_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		tfloat = get_dht_humidity(t->data.peripheral.info);
		push_stack_rzero(stack, sp, float2uint32_t(tfloat) >> 16);
		push_stack_rzero(stack, sp, float2uint32_t(tfloat) & 0xffff);
		break;
#else /* HAVE_DHT */
	case BCDHTTemp_c:
		// fall through
	case BCDHTHumid_c:
		die(PSTR("DHT task tree node in rewrite: %u") , t->task_type);
		break;
#endif /* HAVE_DHT */
//}}}
//LIGHTSENSOR {{{
#ifdef HAVE_LIGHTSENSOR
	case BCGetLight_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		tfloat = get_light(t->data.peripheral.info);
		push_stack_rzero(stack, sp, float2uint32_t(tfloat) >> 16);
		push_stack_rzero(stack, sp, float2uint32_t(tfloat) & 0xffff);
		break;
#else /* HAVE_LIGHTSENSOR */
	case BCGetLight_c:
		die(PSTR("Lightsensor task tree node in rewrite: %u")
			, t->task_type);
		break;
#endif /* HAVE_LIGHTSENSOR */
//}}}
//AIRQUALITYSENSOR {{{
#ifdef HAVE_AIRQUALITYSENSOR
	case BCSetEnvironmentalData_c:
		set_environmental_data(t->data.peripheral.info,
				t->data.peripheral.data.setenvironment.humid,
				t->data.peripheral.data.setenvironment.temp);
		t->task_type = BCStable0_c;
		push_stack_rzero(stack, sp, MTStable_c);

		if (current_task->status != MTUnevaluated_c)
			current_task->status = MTPurged_c;

		break;
	case BCTVOC_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		t32 = get_tvoc(t->data.peripheral.info);
		push_stack_rzero(stack, sp, t32 >> 16);
		push_stack_rzero(stack, sp, t32 & 0xffff);
		break;
	case BCCO2_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		t32 = get_co2(t->data.peripheral.info);
		push_stack_rzero(stack, sp, t32 >> 16);
		push_stack_rzero(stack, sp, t32 & 0xffff);
		break;
#else /* HAVE_AIRQUALITYSENSOR */
	case BCSetEnvironmentalData_c:
		// fall through
	case BCTVOC_c:
		// fall through
	case BCCO2_c:
		die(PSTR("AQS task tree node in rewrite: %u") , t->task_type);
		break;
#endif /* HAVE_AIRQUALITYSENSOR */
//}}}
//GESTURESENSOR {{{
#ifdef HAVE_GESTURESENSOR
	case BCGesture_c:
		push_stack_rzero(stack, sp, MTUnstable_c);
		push_stack_rzero(stack, sp,
			get_gesture(t->data.peripheral.info));
		break;
#else /* HAVE_GESTURESENSOR */
	case BCGesture_c:
		die(PSTR("Gesture task tree node in rewrite: %u")
			, t->task_type);
		break;
#endif /* HAVE_GESTURESENSOR */
//}}}
//LEDMatrix {{{
#ifdef HAVE_LEDMATRIX
	case BCLEDMatrixDisplay_c:
		ledmatrix_display(t->data.peripheral.info);
		push_stack_rzero(stack, sp, MTStable_c);
		break;
	case BCLEDMatrixIntensity_c:
		ledmatrix_intensity(t->data.peripheral.info,
			t->data.peripheral.data.ledmatrixintensity);
		push_stack_rzero(stack, sp, MTStable_c);
		break;
	case BCLEDMatrixDot_c:
		ledmatrix_dot(t->data.peripheral.info,
			t->data.peripheral.data.ledmatrixdot.x,
			t->data.peripheral.data.ledmatrixdot.y,
			t->data.peripheral.data.ledmatrixdot.s);
		push_stack_rzero(stack, sp, MTStable_c);
		break;
	case BCLEDMatrixClear_c:
		ledmatrix_clear(t->data.peripheral.info);
		push_stack_rzero(stack, sp, MTStable_c);
		break;
#else /* HAVE_LEDMATRIX */
	case BCLEDMatrixClear_c:
		// fall through
	case BCLEDMatrixDisplay_c:
		// fall through
	case BCLEDMatrixIntensity_c:
		// fall through
	case BCLEDMatrixDot_c:
		die(PSTR("LED matrix task tree node in rewrite: %u")
			, t->task_type);
		break;
#endif /* HAVE_LEDMATRIX */
//}}}
#ifdef HAVE_NEOPIXEL
	case BCNeoSetPixelColor_c:
		neopixel_setPixelColor(t->data.peripheral.info,
			t->data.peripheral.data.neosetpixel.n,
			t->data.peripheral.data.neosetpixel.r,
			t->data.peripheral.data.neosetpixel.g,
			t->data.peripheral.data.neosetpixel.b);
		push_stack_rzero(stack, sp, MTStable_c);
		break;
#else /* HAVE_NEOPIXEL */
	case BCNeoSetPixelColor_c:
		die(PSTR("Neopixel task tree node in rewrite: %u")
			, t->task_type);
		break;
#endif /* HAVE_NEOPIXEL */
//}}}
	case BCEvent_c:
		die(PSTR("Event task tree node in rewrite: %u"), t->task_type);
		break;
	}
#undef safe_rewrite
#undef safe_interpret
	return sp;
	(void)tfloat;
	(void)t16;
}
