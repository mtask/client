#include "comm_interface.h"

#ifdef COMM_DIRECT

#include "mem.h"

uint32_t default_ping = COMM_PING;

bool input_available(void)
{
	return link_input_available();
}

struct MTMessageTo receive_message(void)
{
	return parse_MTMessageTo(link_read_byte, mem_alloc_task);
}

void send_message(struct MTMessageFro r)
{
	print_MTMessageFro(link_write_byte, r);
}

void start_communication()
{
	open_link();
}

void stop_communication(bool temporary)
{
	close_link(temporary);
}

void cleanup_communication()
{
	cleanup_link();
}

static unsigned long lastping = 0;
void communication_yield(void)
{
	if (getmillis()-lastping > default_ping) {
		struct MTMessageFro r;
		r.cons = MTFPing_c;
		print_MTMessageFro(link_write_byte, r);
		lastping = getmillis();
	}
}
#endif /* COMM_DIRECT */
