#include "events.h"

#include "mem.h"
#include "types.h"
#include "interrupts.h"

// Pointer to the first event
struct TaskTree *events = NULL;

struct TaskTree *event_list_head(void)
{
	return events;
}

struct TaskTree *event_list_next(struct TaskTree *e)
{
	return e->data.event.next;
}

void remove_event(struct TaskTree *e)
{
	// Update return pointer of the next node
	if (e->data.event.next != NULL) {
		tasktree_assign_ptr(e->data.event.next, e);
	}

	// Update the next pointer of the previous node
	if (e->ptr.tree != NULL) {
		e->ptr.tree->data.event.next = e->data.event.next;
	} else {
		events = e->data.event.next;
	}

	e->trash = true;
}

void register_events(struct MTaskMeta *t, struct TaskTree *tt)
{
	// Register Event
	struct TaskTree *e = mem_alloc_tree();
	e->task_type = BCEvent_c;
	e->trash = false;
	tasktree_set_ptr_tree(e, NULL);
	e->data.event.next = events;
	e->data.event.task_id = t->taskid;

	if (events != NULL) {
		tasktree_assign_ptr(events, e);
	}
	events = e;

	switch (tt->task_type) {
		case BCInterrupt_c:
			e->data.event.event_type = tt->data.interrupt.pin * 5 +
						   tt->data.interrupt.mode;
			//Enable interrupt
			enable_interrupt(tt->data.interrupt.pin,
					 tt->data.interrupt.mode);
			break;
		case BCStable0_c:
			// fall through
		case BCStable1_c:
			// fall through
		case BCStable2_c:
			// fall through
		case BCStable3_c:
			// fall through
		case BCStable4_c:
			// fall through
		case BCStableNode_c:
			// fall through
		case BCUnstable0_c:
			// fall through
		case BCUnstable1_c:
			// fall through
		case BCUnstable2_c:
			// fall through
		case BCUnstable3_c:
			// fall through
		case BCUnstable4_c:
			// fall through
		case BCUnstableNode_c:
			// fall through
		case BCReadD_c:
			// fall through
		case BCWriteD_c:
			// fall through
		case BCReadA_c:
			// fall through
		case BCWriteA_c:
			// fall through
		case BCPinMode_c:
			// fall through
		case BCRepeat_c:
			// fall through
		case BCDelay_c:
			// fall through
		case BCDelayUntil_c:
			// fall through
		case BCTAnd_c:
			// fall through
		case BCTOr_c:
			// fall through
		case BCStep_c:
			// fall through
		case BCStepStable_c:
			// fall through
		case BCStepUnstable_c:
			// fall through
		case BCSeqStable_c:
			// fall through
		case BCSeqUnstable_c:
			// fall through
		case BCSdsGet_c:
			// fall through
		case BCSdsSet_c:
			// fall through
		case BCSdsUpd_c:
			// fall through
		case BCReflect_c:
			// fall through
		case BCRateLimit_c:
			// fall through
		case BCDHTTemp_c:
			// fall through
		case BCDHTHumid_c:
			// fall through
		case BCLEDMatrixDisplay_c:
			// fall through
		case BCLEDMatrixIntensity_c:
			// fall through
		case BCLEDMatrixDot_c:
			// fall through
		case BCLEDMatrixClear_c:
			// fall through
		case BCGetLight_c:
			// fall through
		case BCSetEnvironmentalData_c:
			// fall through
		case BCTVOC_c:
			// fall through
		case BCCO2_c:
			// fall through
		case BCGesture_c:
			// fall through
		case BCNeoSetPixelColor_c:
			// fall through
		case BCEvent_c:
			break;
	}
}

void deregister_events(struct MTaskMeta *t, struct TaskTree *tt)
{
	switch (tt->task_type) {
		case BCInterrupt_c: {
			uint16_t pin = tt->data.interrupt.pin;
			enum InterruptMode mode = tt->data.interrupt.mode;

			struct TaskTree *e = event_list_head();
			uint16_t event_type = pin * 5 + mode;

			while (e != NULL) {
				if (e->data.event.event_type == event_type &&
					e->data.event.task_id == t->taskid) {
					remove_event(e);
					disable_interrupt(pin, mode);
					return;
				}
				e = event_list_next(e);
			}
			} break;
		case BCStable0_c:
			// fall through
		case BCStable1_c:
			// fall through
		case BCStable2_c:
			// fall through
		case BCStable3_c:
			// fall through
		case BCStable4_c:
			// fall through
		case BCStableNode_c:
			// fall through
		case BCUnstable0_c:
			// fall through
		case BCUnstable1_c:
			// fall through
		case BCUnstable2_c:
			// fall through
		case BCUnstable3_c:
			// fall through
		case BCUnstable4_c:
			// fall through
		case BCUnstableNode_c:
			// fall through
		case BCReadD_c:
			// fall through
		case BCWriteD_c:
			// fall through
		case BCReadA_c:
			// fall through
		case BCWriteA_c:
			// fall through
		case BCPinMode_c:
			// fall through
		case BCRepeat_c:
			// fall through
		case BCDelay_c:
			// fall through
		case BCDelayUntil_c:
			// fall through
		case BCTAnd_c:
			// fall through
		case BCTOr_c:
			// fall through
		case BCStep_c:
			// fall through
		case BCStepStable_c:
			// fall through
		case BCStepUnstable_c:
			// fall through
		case BCSeqStable_c:
			// fall through
		case BCSeqUnstable_c:
			// fall through
		case BCSdsGet_c:
			// fall through
		case BCSdsSet_c:
			// fall through
		case BCSdsUpd_c:
			// fall through
		case BCReflect_c:
			// fall through
		case BCRateLimit_c:
			// fall through
		case BCDHTTemp_c:
			// fall through
		case BCDHTHumid_c:
			// fall through
		case BCLEDMatrixDisplay_c:
			// fall through
		case BCLEDMatrixIntensity_c:
			// fall through
		case BCLEDMatrixDot_c:
			// fall through
		case BCLEDMatrixClear_c:
			// fall through
		case BCGetLight_c:
			// fall through
		case BCSetEnvironmentalData_c:
			// fall through
		case BCTVOC_c:
			// fall through
		case BCCO2_c:
			// fall through
		case BCGesture_c:
			// fall through
		case BCNeoSetPixelColor_c:
			// fall through
		case BCEvent_c:
			break;
	}
}
