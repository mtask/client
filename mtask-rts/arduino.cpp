#ifdef ARDUINO

#include <Arduino.h>
#include <stdbool.h>
#include <stdint.h>
#include <limits.h>

#include "config_arduino.h"
#include "interface.h"
#include "interrupts.h"
#include "mem.h"
#include "client.h"

#include "comm_interface.h"
#include "types.h"

#if defined(HAVE_LIGHTSENSOR) || defined(HAVE_AIRQUALITYSENSOR)\
	|| defined(HAVE_GESTURESENSOR) || defined(HAVE_DHT_SHT)\
	|| defined(HAVE_OLEDSHIELD)
#define MTASK_USE_WIRE
#include <Wire.h>
bool wire_began = false;
#endif /* defined(HAVE_LIGHTSENSOR) || defined(HAVE_AIRQUALITYSENSOR)\
	|| defined(HAVE_GESTURESENSOR) || defined(HAVE_DHT_SHT)\
	|| defined(HAVE_OLEDSHIELD) */

#if defined(ARDUINO_ARCH_AVR)
const uint8_t apins[] = {A0, A1, A2, A3, A4, A5, A6, A7};
const uint8_t dpins[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

// placement new is not defined for avr arduino
//void * operator new (size_t size, void * ptr) {
//	return ptr;
//	(void)size;
//}

#elif defined(ARDUINO_ARCH_ESP8266)
const uint8_t apins[] = {A0};
const uint8_t dpins[] = {D0, D1, D2, D3, D4, D5, D6, D7, D8};
#include <ESP8266WiFi.h>
#elif defined(ARDUINO_ARCH_ESP32)
const uint8_t apins[] = {A0, A1 ,A2 ,A3 ,A4 ,A5};
const uint8_t dpins[] = {D0, D1, D2, D3, D4, D5, D6, D7, D8, D9, D10};
#include <WiFi.h>
#elif defined(ARDUINO_ARCH_SAMD)
const uint8_t apins[] = {A0, A1 ,A2 ,A3 ,A4 ,A5};
const uint8_t dpins[] = {0, 0, 0, 0, 0, 5, 6, 0, 0, 9, 10, 11, 12, 13};
uint32_t timer_offset = 0;
#include <ArduinoLowPower.h>
#include <new>
#else /* ARDUINO_ARCH_AVR */
#error Unknown arduino device
#endif /* ARDUINO_ARCH_AVR */

#ifdef HAVE_OLEDSHIELD
#ifdef ARDUINO_ESP32_DEV // Lilygo t-wristband
#include <TFT_eSPI.h>

TFT_eSPI display = TFT_eSPI();
#else /* ARDUINO_ESP32_DEV */
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 64 // OLED display width, in pixels
#define SCREEN_HEIGHT 48 // OLED display height, in pixels

//#define OLED_RESET -1
#define OLED_RESET 0
Adafruit_SSD1306 display(OLED_RESET);
//Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
#endif /* ARDUINO_ESP32_DEV */
#endif /* HAVE_OLEDSHIELD */

#ifdef ARDUINO_ARCH_ESP8266
extern "C" void esp_schedule();
#endif /* ARDUINO_ARCH_ESP8266 */

volatile bool wake_up = false;

uint32_t getmillis(void)
{
	uint32_t m = millis();
#ifdef ARDUINO_ARCH_SAMD
	m += timer_offset;
#endif /* ARDUINO_ARCH_ESP8266 */
	return m;
}

void msdelay(uint32_t ms)
{
	delay(ms);
}

void mssleep(uint32_t ms)
{
#ifdef ARDUINO_ARCH_AVR
	wake_up = false;
	uint32_t start = millis();
	while (!wake_up && millis() - start < ms) {
		yield();
	}
#elif defined(ARDUINO_ARCH_SAMD)
#  ifdef COMM_MQTT
	bool disable_wifi = (active_interrupts() && ms > 15563) ||
			    (!active_interrupts() && ms > 14681);
	if (disable_wifi) {
		stop_communication(true);
		ms -= 3438; // Average time to reconnect
	}
#  endif /* COMM_MQTT */
	if (active_interrupts() || ms < 1000) {
		wake_up = false;
		uint32_t start = millis();
		while (!wake_up && millis() - start < ms) {
			LowPower.idle(1);
		}
	} else {
		LowPower.deepSleep(ms);
		timer_offset += ms;
	}
#  ifdef COMM_MQTT
	if (disable_wifi) {
		start_communication(true);
	}
#  endif /* COMM_MQTT */
#elif defined(ARDUINO_ARCH_ESP32)
	if (active_interrupts() || ms < 1000) {
		wake_up = false;
		uint32_t start = millis();
		while (!wake_up && millis() - start < ms) {
			delay(1);
		}
	} else {
		msg_log(PSTR("entering light sleep\n"));
		esp_sleep_enable_timer_wakeup(ms*1000);
		Serial.flush();
		esp_wifi_stop();
		if (esp_light_sleep_start() == ESP_ERR_INVALID_STATE)
			die(PSTR("WiFi or BT wasn't stopped?\n"));
		switch (esp_sleep_get_wakeup_cause()) {
		case ESP_SLEEP_WAKEUP_UNDEFINED:
			msg_log(PSTR("wakeupreason undefined\n")); break;
		case ESP_SLEEP_WAKEUP_ALL:
			msg_log(PSTR("wakeupreason all\n")); break;
		case ESP_SLEEP_WAKEUP_EXT0:
			msg_log(PSTR("wakeupreason ext0\n")); break;
		case ESP_SLEEP_WAKEUP_EXT1:
			msg_log(PSTR("wakeupreason ext1\n")); break;
		case ESP_SLEEP_WAKEUP_TIMER:
			msg_log(PSTR("wakeupreason timer\n")); break;
		case ESP_SLEEP_WAKEUP_TOUCHPAD:
			msg_log(PSTR("wakeupreason touchpad\n")); break;
		case ESP_SLEEP_WAKEUP_ULP:
			msg_log(PSTR("wakeupreason ulp\n")); break;
		case ESP_SLEEP_WAKEUP_GPIO:
			msg_log(PSTR("wakeupreason gpio\n")); break;
		case ESP_SLEEP_WAKEUP_UART:
			msg_log(PSTR("wakeupreason uart\n")); break;
		case ESP_SLEEP_WAKEUP_WIFI:
			msg_log(PSTR("wakeupreason wifi\n")); break;
		case ESP_SLEEP_WAKEUP_COCPU:
			msg_log(PSTR("wakeupreason cocpu\n")); break;
		case ESP_SLEEP_WAKEUP_COCPU_TRAP_TRIG:
			msg_log(PSTR("wakeupreason cocpu crash\n")); break;
		case ESP_SLEEP_WAKEUP_BT:
			msg_log(PSTR("wakeupreason bt\n")); break;
		};
		esp_wifi_start();
		msg_log(PSTR("exiting light sleep\n"));
#else /* !ARDUINO_ARCH_AVR */
	delay(ms);
#endif /* !ARDUINO_ARCH_AVR */
}

void force_wake_up()
{
#ifdef ARDUINO_ARCH_ESP8266
	esp_schedule();
#else /* ARDUINO_ARCH_ESP8266 */
	wake_up = true;
#endif /* ARDUINO_ARCH_ESP8266 */
}

uint8_t read_apin(uint16_t t)
{
	return analogRead(t);
}

void write_apin(uint16_t p, uint8_t v)
{
#ifdef ARDUINO_ARCH_ESP32
	// TODO not implemented in the Arduino libraries for esp32 yet
	// https://github.com/espressif/arduino-esp32/issues/4
	(void)p;
	(void)v;
#else /* ARDUINO_ARCH_ESP32 */
	analogWrite(p, v);
#endif /* ARDUINO_ARCH_ESP32 */
}

bool read_dpin(uint16_t t)
{
	return digitalRead(t);
}

void write_dpin(uint16_t p, bool v)
{
	digitalWrite(p, v ? HIGH : LOW);
}

void set_pinmode(uint16_t p, enum PinMode mode)
{
	pinMode(p, mode == PMInput_c ? INPUT :
		mode == PMOutput_c ? OUTPUT : INPUT_PULLUP);
}

uint16_t translate_pin_s(struct Pin p)
{
	switch (p.cons) {
	case AnalogPin_c:
		return p.data.AnalogPin >= sizeof(apins)
			? 0 : apins[p.data.AnalogPin];
	case DigitalPin_c:
		return p.data.DigitalPin >= sizeof(dpins)
			? 0 : dpins[p.data.DigitalPin];
	}
	return 0;
}

uint16_t translate_pin(uint16_t i)
{
	//Digital pin
	if ((i & 1) > 0) {
		i >>= 1;
		return i >= sizeof(dpins) ? 0 : dpins[i];
	//Analog pin
	} else {
		i >>= 1;
		return i >= sizeof(apins) ? 0 : apins[i];
	}
	return 0;
}

int translate_interrupt_mode(enum InterruptMode mode)
{
	switch (mode) {
		case Change_c:
			return CHANGE;
		case Rising_c:
			return RISING;
		case Falling_c:
			return FALLING;
		case Low_c:
			return LOW;
		case High_c:
			return HIGH;
	}
	return 0;
}

void attach_interrupt(void (*isr)(), uint16_t pin, enum InterruptMode mode)
{
	attachInterrupt(digitalPinToInterrupt(pin), isr,
			translate_interrupt_mode(mode));
}

void detach_interrupt(uint16_t pin)
{
	detachInterrupt(digitalPinToInterrupt(pin));
}

#if defined(HAVE_DHT)

# if defined(HAVE_DHT_DHT)
#include <dhtnew.h>
# endif /* HAVE_DHT_DHT */
# if defined(HAVE_DHT_SHT)
#include <WEMOS_SHT3X.h>
# endif /* HAVE_DHT_SHT */

void *dht_init(struct DHTInfo a)
{
# if defined(HAVE_DHT_DHT)
	if (a.cons == DHT_DHT_c) {
		msg_log(PSTR("init dht with"));
		return new DHTNEW((uint8_t)translate_pin_s(a.data.DHT_DHT.f0));
	}
# endif /* HAVE_DHT_DHT */
# if defined(HAVE_DHT_SHT)
	if (a.cons == DHT_SHT_c) {
		msg_log(PSTR("init sht with: 0x%02x\n"), a.data.DHT_SHT);
		return new SHT3X(a.data.DHT_SHT);
	}
# endif /* HAVE_DHT_DHT */
	return NULL;
}

void dht_destroy(struct BCPeripheral *p)
{
# if defined(HAVE_DHT_DHT)
	if (p->perinfo.data.BCDHT.cons == DHT_DHT_c) {
		delete (DHTNEW *)p->st;
	}
# endif /* HAVE_DHT_DHT */
# if defined(HAVE_DHT_SHT)
	if (p->perinfo.data.BCDHT.cons) {
		delete (SHT3X *)p->st;
	}
# endif /* HAVE_DHT_DHT */
}

float get_dht_temp(struct BCPeripheral *p)
{
# if defined(HAVE_DHT_DHT)
	if (p->perinfo.data.BCDHT.cons == DHT_DHT_c) {
		((DHTNEW *)p->st)->read();
		return ((DHTNEW *)p->st)->getTemperature();
	}
# endif /* HAVE_DHT_DHT */
# if defined(HAVE_DHT_SHT)
	if (p->perinfo.data.BCDHT.cons == DHT_SHT_c) {
		((SHT3X *)p->st)->get();
		return ((SHT3X *)p->st)->cTemp;
	}
# endif /* HAVE_DHT_SHT */
	return 0.0;
}

float get_dht_humidity(struct BCPeripheral *p)
{
# if defined(HAVE_DHT_DHT)
	if (p->perinfo.data.BCDHT.cons == DHT_DHT_c) {
		((DHTNEW *)p->st)->read();
		return ((DHTNEW *)p)->getHumidity();
	}
# endif /* HAVE_DHT_DHT */
# if defined(HAVE_DHT_SHT)
	if (p->perinfo.data.BCDHT.cons == DHT_SHT_c) {
		((SHT3X *)p->st)->get();
		return ((SHT3X *)p->st)->humidity;
	}
# endif /* HAVE_DHT_SHT */
	return 0.0;
}
#endif /* HAVE_DHT */

#ifdef HAVE_LIGHTSENSOR
#include <BH1750.h>

void *lightsensor_init(uint8_t addr) {
	BH1750 *ls = new BH1750(addr);
	if (!ls->begin(BH1750::ONE_TIME_HIGH_RES_MODE)) {
		die(PSTR("Error initializing lightsensor"));
	}

	return ls;
}

void lightsensor_destroy(struct BCPeripheral *p)
{
	delete (BH1750 *)p->st;
}

float get_light(struct BCPeripheral *p)
{
	return ((BH1750 *)p->st)->readLightLevel();
}
#endif /* HAVE_LIGHTSENSOR */

#ifdef HAVE_AIRQUALITYSENSOR

# ifdef HAVE_AIRQUALITYSENSOR_CCS811
#include <SparkFunCCS811.h>
#endif /* HAVE_AIRQUALITYSENSOR_CCS811 */

# ifdef HAVE_AIRQUALITYSENSOR_SGP30
#include <Adafruit_SGP30.h>
/**
 * Return absolute humidity [mg/m^3] with approximation formula.
 * Approximation formula from Sensirion SGP30 Driver Integration chapter 3.15.
 *
 * @param temperature [°C]
 * @param humidity [%RH]
 */
uint32_t getAbsoluteHumidity(float temperature, float humidity) {
	const float absoluteHumidity = 216.7f * ((humidity / 100.0f) * 6.112f \
		* exp((17.62f * temperature) / (243.12f + temperature))
		/ (273.15f + temperature)); // [g/m^3]
	const uint32_t absoluteHumidityScaled
		= static_cast<uint32_t>(1000.0f * absoluteHumidity); // [mg/m^3]
	return absoluteHumidityScaled;
}
# endif /* HAVE_AIRQUALITYSENSOR_SGP30 */

void *airqualitysensor_init(struct AirQualitySensorInfo aqs)
{
	switch (aqs.cons) {
	case AQS_CCS811_c: {
# ifdef HAVE_AIRQUALITYSENSOR_CCS811
		CCS811 *r = new CCS811(aqs.data.AQS_CCS811);
		if (r->beginWithStatus()
				!= CCS811Core::CCS811_Stat_SUCCESS) {
			delete r;
			die(PSTR("Error initialising CCS811 airqualitysensor"));
		}
		return r;
# endif /* HAVE_AIRQUALITYSENSOR_CCS811 */
	}
	case AQS_SGP30_c: {
# ifdef HAVE_AIRQUALITYSENSOR_SGP30
		Adafruit_SGP30 *r = new Adafruit_SGP30();
		if (!r->begin()) {
			delete r;
			die(PSTR("Error initialising SGP30 airqualitysensor"));
		}
		return r;
# endif /* HAVE_AIRQUALITYSENSOR_SGP30 */
	}}
	return NULL;
}

void airqualitysensor_destroy(struct BCPeripheral *p)
{
# ifdef HAVE_AIRQUALITYSENSOR_CCS811
	if (p->perinfo.data.BCAirQualitySensor.cons == AQS_CCS811_c) {
		delete (CCS811 *)p->st;
	}
# endif /* HAVE_AIRQUALITYSENSOR_CCS811 */
# ifdef HAVE_AIRQUALITYSENSOR_SGP30
	if (p->perinfo.data.BCAirQualitySensor.cons == AQS_SGP30_c) {
		delete (Adafruit_SGP30 *)p->st;
	}
# endif /* HAVE_AIRQUALITYSENSOR_SGP30 */
}

void set_environmental_data(struct BCPeripheral *p, float humid, float temp)
{
# ifdef HAVE_AIRQUALITYSENSOR_CCS811
	if (p->perinfo.data.BCAirQualitySensor.cons == AQS_CCS811_c) {
		((CCS811 *)p->st)->setEnvironmentalData(humid, temp);
	}
# endif /* HAVE_AIRQUALITYSENSOR_CCS811 */
# ifdef HAVE_AIRQUALITYSENSOR_SGP30
	if (p->perinfo.data.BCAirQualitySensor.cons == AQS_SGP30_c) {
		msg_log(PSTR("setenv: h:%f t:%f\n"), humid, temp);
		((Adafruit_SGP30 *)p->st)->setHumidity(
			getAbsoluteHumidity(temp, humid));
	}
# endif /* HAVE_AIRQUALITYSENSOR_SGP30 */
}

uint32_t get_tvoc(struct BCPeripheral *p)
{
# ifdef HAVE_AIRQUALITYSENSOR_CCS811
	if (p->perinfo.data.BCAirQualitySensor.cons == AQS_CCS811_c) {
		((CCS811 *)p->st)->readAlgorithmResults();
		return ((CCS811 *)p->st)->getTVOC();
	}
# endif /* HAVE_AIRQUALITYSENSOR_CCS811 */
# ifdef HAVE_AIRQUALITYSENSOR_SGP30
	if (p->perinfo.data.BCAirQualitySensor.cons == AQS_SGP30_c) {
		((Adafruit_SGP30 *)p->st)->IAQmeasure();
		return ((Adafruit_SGP30 *)p->st)->TVOC;
	}
# endif /* HAVE_AIRQUALITYSENSOR_SGP30 */
	return 0;
}

uint32_t get_co2(struct BCPeripheral *p)
{
# ifdef HAVE_AIRQUALITYSENSOR_CCS811
	if (p->perinfo.data.BCAirQualitySensor.cons == AQS_CCS811_c) {
		((CCS811 *)p->st)->readAlgorithmResults();
		return ((CCS811 *)p->st)->getCO2();
	}
# endif /* HAVE_AIRQUALITYSENSOR_CCS811 */
# ifdef HAVE_AIRQUALITYSENSOR_SGP30
	if (p->perinfo.data.BCAirQualitySensor.cons == AQS_SGP30_c) {
		((Adafruit_SGP30 *)p->st)->IAQmeasure();
		msg_log(PSTR("read co2 from sgp: %u\n"),
			((Adafruit_SGP30 *)p->st)->eCO2);
		return ((Adafruit_SGP30 *)p->st)->eCO2;
	}
# endif /* HAVE_AIRQUALITYSENSOR_SGP30 */
	return 0;
}
#endif /* HAVE_AIRQUALITYSENSOR */

#ifdef HAVE_GESTURESENSOR
#include "paj7620.h"
void *gesturesensor_init(uint8_t addr)
{
	uint8_t error = paj7620Init();
	if (error) {
		die(PSTR("Error initialisinggesture paj7620 sensor\n"));
	}
	return (void *)(uintptr_t)addr;
}

void gesturesensor_destroy(struct BCPeripheral *p) { (void) p; }

enum Gesture get_gesture(struct BCPeripheral *p)
{
	uint8_t data = 0, error;
	enum Gesture r = GNone_c;
	if ((error = paj7620ReadReg((int)p->st, 1 , &data))) {
		die(PSTR("Error reading gesture paj7620 sensor\n"));
	} else {
		switch (data) {
		case GES_RIGHT_FLAG:
			r = GRight_c;
			break;
		case GES_LEFT_FLAG:
			r = GLeft_c;
			break;
		case GES_UP_FLAG:
			r = GUp_c;
			break;
		case GES_DOWN_FLAG:
			r = GDown_c;
			break;
		case GES_FORWARD_FLAG:
			r = GForward_c;
			break;
		case GES_BACKWARD_FLAG:
			r = GBackward_c;
			break;
		case GES_CLOCKWISE_FLAG:
			r = GClockwise_c;
			break;
		case GES_COUNT_CLOCKWISE_FLAG:
			r = GCountClockwise_c;
			break;
		default: // please
			r = GNone_c;
			break;
		}
	}
	return r;
}
#endif /* HAVE_GESTURESENSOR */

#ifdef HAVE_LEDMATRIX
#include <WEMOS_Matrix_LED.h>

void *ledmatrix_init(struct LEDMatrixInfo x)
{
	return new MLED(5, translate_pin_s(x.f0), translate_pin_s(x.f1));
}

void ledmatrix_destroy(struct BCPeripheral *p)
{
	((MLED *)p->st)->clear();
	((MLED *)p->st)->display();
	delete (MLED *)p->st;
}

void ledmatrix_dot(struct BCPeripheral *p, uint8_t x, uint8_t y, bool state)
{
	((MLED *)p->st)->dot(x, y, state);
}
void ledmatrix_intensity(struct BCPeripheral *p, uint8_t intensity)
{
	((MLED *)p->st)->intensity = intensity;
}
void ledmatrix_clear(struct BCPeripheral *p)
{
	((MLED *)p->st)->clear();
}
void ledmatrix_display(struct BCPeripheral *p)
{
	((MLED *)p->st)->display();
}
#endif /* HAVE_LEDMATRIX */

#ifdef HAVE_OLEDSHIELD
void print_to_display(const char *msg, ...)
{
	char buf[64];
	va_list args;
	va_start(args, msg);
#ifdef ARDUINO_ARCH_SAMD
	vsnprintf(buf, 128, msg, args);
#else /* ARDUINO_ARCH_SAMD */
	vsnprintf_P(buf, 128, msg, args);
#endif /* ARDUINO_ARCH_SAMD */
	va_end(args);
	display.printf(buf);
}

void flush_display()
{
	display.display();
}

void clear_display()
{
#ifdef ARDUINO_ESP32_DEV //lilygo t-wristband
	display.fillScreen(TFT_BLACK);
#else /* ARDUINO_ESP32_DEV */
	display.clearDisplay();
#endif /* ARDUINO_ESP32_DEV */
	display.setCursor(0, 0);
}
#endif /* HAVE_OLEDSHIELD */

#ifdef HAVE_NEOPIXEL
#include <Adafruit_NeoPixel.h>

void* neopixel_init (struct NeoInfo info) {
	int flags = 0;
	switch (info.f2) {
	case NEO_GRB_c:
		flags += NEO_GRB;
		break;
	case NEO_RGB_c:
		flags += NEO_RGB;
		break;
	case NEO_RGBW_c:
		flags += NEO_RGBW;
		break;
	}
	switch (info.f3) {
	case NEO_KHZ800_c:
		flags += NEO_KHZ800;
		break;
	case NEO_KHZ400_c:
		flags += NEO_KHZ400;
		break;
	}
	Adafruit_NeoPixel *np = new Adafruit_NeoPixel(
		info.f0, translate_pin_s(info.f1), flags);
	np->begin();
	return np;
}

void neopixel_destroy(struct BCPeripheral *p)
{
	delete (Adafruit_NeoPixel *)p->st;
}

void neopixel_setPixelColor(struct BCPeripheral *p,
	uint16_t n, uint8_t r, uint8_t g, uint8_t b)
{
	msg_log(PSTR("set pixel color %u %u %u %u\n"), n, r, g, b);
	((Adafruit_NeoPixel*)p->st)->setPixelColor(n,
		((Adafruit_NeoPixel*)p->st)->Color(r, g, b));
	((Adafruit_NeoPixel*)p->st)->show();
}
#endif /* HAVE_NEOPIXEL */

bool real_setup()
{
//Report peripherals only when the link method is not serial
#if !defined(LINK_SERIAL) && LOGLEVEL > 0
	Serial.begin(BAUDRATE);
#ifdef ARDUINO_ARCH_SAMD
	while (!Serial);
#endif /* ARDUINO_ARCH_SAMD */
	Serial.println(PSTR("Peripherals:"));

#ifdef HAVE_OLEDSHIELD
	Serial.println(PSTR("Have OLED"));
#endif /* HAVE_OLEDSHIELD */
#ifdef HAVE_LEDMATRIX
	Serial.println(PSTR("Have LEDMatrix"));
	//TODO add clearing here?
#endif /* HAVE_LEDMATRIX */
#ifdef HAVE_DHT
	Serial.println(PSTR("Have DHT"));
# ifdef HAVE_DHT_DHT
	Serial.println(PSTR("\tof the DHT type"));
# endif /* HAVE_DHT_DHT */
# ifdef HAVE_DHT_SHT
	Serial.println(PSTR("\tof the SHT type"));
# endif /* HAVE_DHT_SHT */
#endif /* HAVE_DHT */
#ifdef HAVE_AIRQUALITYSENSOR
	Serial.println(PSTR("Have AQS"));
# ifdef HAVE_AIRQUALITYSENSOR_CCS811
	Serial.println(PSTR("\tof the CCS811 type"));
# endif /* HAVE_AIRQUALITYSENSOR_CCS811 */
# ifdef HAVE_AIRQUALITYSENSOR_SGP30
	Serial.println(PSTR("\tof the SGP30 type"));
# endif /* HAVE_AIRQUALITYSENSOR_SGP30 */
#endif /* HAVE_AIRQUALITYSENSOR */
#ifdef HAVE_LIGHTSENSOR
	Serial.println(PSTR("Have Lightsensor"));
#endif /* HAVE_LIGHTSENSOR */
#ifdef HAVE_NEOPIXEL
	// Even if not connected, this doesn't fail.
	Serial.println(PSTR("Have NeoPixel led strip"));
	Adafruit_NeoPixel np(7, D4, NEO_GRB + NEO_KHZ800);
	np.begin();
	np.clear();
	np.show();
#endif /* HAVE_NEOPIXEL */
#endif /* !defined(LINK_SERIAL) && LOGLEVEL > 0 */

#if defined(ARDUINO_ARCH_ESP8266) || defined(ARDUINO_ARCH_ESP32) \
	|| defined(ARDUINO_ARCH_SAMD)
#ifdef HAVE_OLEDSHIELD
#ifdef ARDUINO_ESP32_DEV //Lilygo t-wristband
	display.init();
	display.fillScreen(TFT_BLACK);
#else /* ARDUINO_ESP32_DEV */
//	if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C))
//		die(PSTR("SSD1306 allocation failed"));
	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
	display.display();
	delay(500);
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
#endif /* ARDUINO_ESP32_DEV */
	display.setCursor(0, 0);
#endif /* HAVE_OLEDSHIELD */

#ifdef MTASK_USE_WIRE
	if (!wire_began) {
		Wire.begin();
		wire_began = true;
	}
#endif /* MTASK_USE_WIRE */

#endif /* defined(ARDUINO_ARCH_ESP8266) || defined(ARDUINO_ARCH_ESP32)
	|| defined(ARDUINO_ARCH_SAMD) */

#ifdef LED_BUILTIN
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, LOW);
#endif /* LED_BUILTIN */

#ifdef ARDUINO_ARCH_ESP32
	if (esp_sleep_enable_gpio_wakeup() == ESP_ERR_INVALID_STATE)
		die(PSTR("Unable to set esp_sleep_enable_gpio_wakeup()\n"));
#endif /* ARDUINO_ARCH_ESP32 */

	return false;
}

void mtask_yield(void)
{
#ifdef ARDUINO_ARCH_ESP8266
	yield();
#endif /* ARDUINO_ARCH_ESP8266 */
}

unsigned int get_random()
{
#ifdef ARDUINO_ARCH_ESP8266
	return RANDOM_REG32;
#else /* ARDUINO_ARCH_ESP8266 */
	return random(INT_MAX);
#endif /* ARDUINO_ARCH_ESP8266 */
}

#if LOGLEVEL > 0
void msg_log(const char *fmt, ...)
{
	char buf[128];
	va_list args;
	va_start(args, fmt);
#ifdef ARDUINO_ARCH_SAMD
	vsnprintf(buf,128,fmt,args);
#else /* ARDUINO_ARCH_SAMD */
	vsnprintf_P(buf,128,fmt,args);
#endif /* ARDUINO_ARCH_SAMD */
	va_end(args);
	Serial.write(buf);
	Serial.write('\r');
}
#endif /* LOGLEVEL > 0 */

void die(const char *fmt, ...)
{
#ifdef HAVE_OLEDSHIELD
	#ifdef ARDUINO_ESP32_DEV //lilygo t-wristband
		display.fillScreen(TFT_BLACK);
	#else /* ARDUINO_ESP32_DEV */
		display.clearDisplay();
	#endif /* ARDUINO_ESP32_DEV */
	display.print("Error: ");
	display.print(fmt);
	#ifndef ARDUINO_ESP32_DEV //lilygo t-wristband
		display.display();
	#endif /* ARDUINO_ESP32_DEV */
#endif /* HAVE_OLEDSHIELD */
	Serial.println(fmt);
	while (1) {
		msdelay(1000);
		Serial.print("die");
	}
}

void pdie(char *s)
{
	die(s);
}

void reset(void)
{
	stop_communication(false);
#if defined(ARDUINO_ARCH_ESP8266)
	ESP.restart();
#elif defined(ARDUINO_ARCH_AVR)
	asm volatile ("  jmp 0");
#else /* ARDUINO_ARCH_ESP8266 */
	mem_reset();
	real_setup();
#endif /* ARDUINO_ARCH_ESP8266 */
}

void loop()
{
	real_loop();
}

void setup()
{
	real_main();
}
#endif /* ARDUINO */
