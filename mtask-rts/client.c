#include "client.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "interface.h"
#include "interpret.h"
#include "rewrite.h"
#include "spec.h"
#include "task.h"
#include "tasktree.h"
#include "sds.h"
#include "mem.h"
#include "nvm.h"
#include "scheduler.h"
#include "interrupts.h"
#include "preload.h"
#include "config.h"

struct SemVer client_version = {0, 4, 3};
struct MTaskMeta *current_task;
uint32_t now;

static void read_message(void)
{
	uint8_t waiting_for_task = 0;

	//Find next task
	while (input_available() || waiting_for_task > 0) {
		void *taskptr = mem_stack();
		struct MTMessageTo msg = receive_message();
		bool ok;
		switch ((uint8_t)msg.cons) {
		case MTTTaskPrep_c:
			msg_log(PSTR("Got task prep for id: %u hash: %lld\n"),
				msg.data.MTTTaskPrep.taskid,
				msg.data.MTTTaskPrep.hash);
#if PRELOADED_TASKS_NUM > 0
			//If it wasn't preloaded, we wait for the task
			if (preload_task(msg.data.MTTTaskPrep)) {
				msg_log(PSTR("Task was preloaded, continue\n"));
				break;
			}
#endif /* PRELOADED_TASKS_NUM > 0 */
			send_message((struct MTMessageFro)
				{ .cons=MTFTaskPrepAck_c
				, .data={.MTFTaskPrepAck=
					msg.data.MTTTaskPrep.taskid}
				});
			msg_log(PSTR("Waiting for the actual task\n"));
			waiting_for_task++;
			break;
		case MTTTask_c:
			msg_log(PSTR("Got task register\n"));
			waiting_for_task--;
			ok = task_register(msg.data.MTTTask);
			if (! ok)
				mem_set_stack_ptr(taskptr);
			break;
		case MTTTaskDel_c:
			msg_log(PSTR("Delete task: %u\n"), msg.data.MTTTaskDel);

			// Remove from queue
			queue_remove_task(msg.data.MTTTaskDel);

			// Remove from memory
			current_task = mem_task_head();
			while (current_task != NULL) {
				if (current_task->taskid ==
						msg.data.MTTTaskDel) {
					if (current_task->status
							!= MTRemoved_c) {
						task_remove(current_task,
							mem_stack());
					}
					current_task = NULL;
				} else {
					current_task =
						mem_task_next(current_task);
				}
			}
			send_message((struct MTMessageFro)
				{ .cons=MTFTaskDelAck_c
				, .data={ .MTFTaskDelAck=msg.data.MTTTaskDel }
				});
			mem_gc_tasks();
			break;
		case MTTSpecRequest_c:
			msg_log(PSTR("Sending spec\n"));
			spec_send();
			break;
		case MTTShutdown_c:
			msg_log(PSTR("Got shutdown\n"));
			reset();
			break;
		case MTTSdsUpdate_c:
			msg_log(PSTR("Got upstream sds update\n"));
			sds_update(msg.data.MTTSdsUpdate.f0
				, msg.data.MTTSdsUpdate.f1
				, msg.data.MTTSdsUpdate.f2);
			//Here we have to free the allocs from the sds value
			mem_set_stack_ptr(taskptr);
			break;
		}
	}
}

static void mtask_wait(void)
{
	struct MTaskMeta *nexttask = peek_next_task();
	uint32_t nextrun;

	if (nexttask != NULL) {
		nextrun = nexttask->lastrun + nexttask->execution_max;
	} else {
		nextrun = now + DEFAULT_SLEEP_TIME;
	}

	/* sleep*/
	uint32_t sleep_time = 0;
	sleep_time = nextrun - getmillis();

	if (sleep_time > 0 && sleep_time <= INF_SLEEP_TIME &&
			!pending_interrupts()) {
		sleep_time = mtmin(sleep_time, MAX_SLEEP_TIME);
		if (sleep_time > 500)
			msg_log(PSTR("Sleep for %i ms\n"), sleep_time);
		mssleep(sleep_time);
	}
}

void real_loop(void)
{
	now = getmillis();

	real_yield();
	read_message();
	process_interrupts();
	uint16_t sp = 0;
	uintptr_t *stack = mem_stack();
	bool task_removed = false;
	bool tree_removed = false;

	//Run tasks
	current_task = next_task(now);

	while (current_task != NULL) {
		msg_debug(PSTR("\nExecute task %u, %p\n"),
			current_task->taskid, &current_task);

		current_task->lastrun = now;

		switch (current_task->status) {
		//Unevaluated tasks must first be interpreted
		case MTNeedsInit_c:
			if (current_task->tree != NULL)
				die(PSTR("Unevaluated task with a tree???\n"));

			msg_debug(PSTR("Execute main function, stack on %p\n"),
				stack);

			//Run the main function and set the tasktree
			sp = 0;
			initstackmain(&sp, stack);
			if (interpret(current_task->instructions.elements,
					0, sp, stack)) {
				current_task->tree =
					(struct TaskTree *)stack[0];
				queue_insert_task(current_task);

				msg_debug(PSTR("returned on %p\n"),
					current_task->tree);
				tasktree_print(current_task->tree, 0);
				msg_debug(PSTR("\n"));
				current_task->status = MTUnevaluated_c;
			//Errored
			} else {
				msg_log(PSTR("interpret errored, resetting\n"));
				mem_reset();
			}
			break;
		//In all other cases, just rewrite
		case MTUnevaluated_c:
		case MTEvaluated_c:
		case MTPurged_c:
			if (current_task->tree == NULL)
				die(PSTR("Evaluated task without a tree???\n"));
			msg_debug(PSTR("\n"));

			enum MTaskEvalStatus old_task_status =
				current_task->status;
			current_task->status = MTEvaluated_c;

			if (rewrite(current_task->tree,
					current_task->instructions.elements,
					stack, &tree_removed) != 0) {
				msg_debug(PSTR("print sid: %u, tree: %u\n"),
					current_task->taskid,
					current_task->tree);
				tasktree_print(current_task->tree, 0);
				msg_debug(PSTR("\n"));
				task_complete(current_task, stack);
				if (stack[0] == MTStable_c) {
					msg_log(PSTR(
						"Task is stable, removing\n"));
					task_remove(current_task, stack);
					task_removed = true;
				} else {
					if (current_task->status == MTPurged_c
							|| current_task->status
							!= old_task_status)
						calculate_execution_interval(
							current_task);
					queue_insert_task(current_task);
				}
			} else {
				msg_log(PSTR("rewrite errored, resetting\n"));
				mem_reset();
			}
			break;
		//Removed, should never happen
		case MTRemoved_c:
			die(PSTR("Task removed but not garbage collected??\n"));
			break;
		}
		current_task = next_task(now);
	}
	if (task_removed) {
		mem_gc_tasks();
		mem_gc();
	} else if (tree_removed) {
		mem_gc();
	}

	//Sleep
#ifdef NVM
	nvm_save();
#endif /* NVM */
	mtask_wait();
}

void real_yield(void)
{
	mtask_yield();
	if (link_is_open())
		communication_yield();
}

#ifdef NVM
void ic_resume() {
	if (!real_setup()) {
		// Resume disabled from command line
		nvm_invalidate();
		start_communication(false);
		spec_send();
		mem_reset();
		return;
	}

	if (!nvm_restore()) {
		if (start_communication(true)) {
			// Inform broker that this session should be terminated
			stop_communication(false);
			start_communication(false);
		}
		nvm_invalidate();
		spec_send();
		mem_reset();
		return;
	}

	if (!start_communication(true)) {
		nvm_invalidate();
		spec_send();
	}
}
#endif /* NVM */

void real_main(void)
{

#ifdef NVM
	ic_resume();
#else /* NVM */
	real_setup();
	start_communication(false);
	spec_send();
	mem_reset();
#endif /* NVM */
	RECOVERABLE_ERROR_CODE;

#if !defined(ARDUINO)
	while (true) {
		real_loop();
	}
#endif /* !defined(ARDUINO) */
}
