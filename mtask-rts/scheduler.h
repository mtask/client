/** @file scheduler.h
 *
 * Contains the interface to the scheduler.
 */
#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <stdint.h>

#include "interface.h"
#include "types.h"
#include "task.h"

/** maximum sleep time */
#define INF_SLEEP_TIME 2147483647u
/**
 * Calculate the preferred interval for a task to be executed
 *
 * @param task pointer to the task
 */
void calculate_execution_interval(struct MTaskMeta *task);

/**
 * Peek in the task queue for the next task
 *
 * @return next task or NULL if there is none
 */
struct MTaskMeta *peek_next_task();
/**
 * Pop the next task from the task queue
 *
 * @param now current time
 * @return next task or NULL if there is none
 */
struct MTaskMeta *next_task(uint32_t now);
/**
 * Insert a task in the task queue
 *
 * @param t pointer to the task
 */
void queue_insert_task(struct MTaskMeta *t);
/**
 * Remove a task from the task queue
 *
 * @param id task id of the task to be removed
 */
void queue_remove_task(uint8_t id);
/**
 * Replace a task in the task queue
 *
 * @param oldt old task
 * @param newt new task
 */
void queue_update_pointer(struct MTaskMeta *oldt, struct MTaskMeta *newt);

#endif /* !SCHEDULER_H */

