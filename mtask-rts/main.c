#include "client.h"
#include "interface.h"

#if defined(PC) && !defined(NO_PC_MAIN)
int main(int argc, char *argv[])
{
	gargc = argc;
	gargv = argv;
	real_main();
	return 0;
}
#endif /* PC */
