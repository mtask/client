#include "scheduler.h"

#include "mem.h"
#include "tasktree.h"

uint8_t queue_start = 0;
uint8_t queue_end = 0;
struct MTaskMeta *task_queue[TASK_QUEUE_SIZE];

// Refresh rate calculation
static void calculate_refresh_rate(struct TaskTree *tt, uint64_t lastrun,
	uint32_t *refresh_min, uint32_t *refresh_max)
{
	uint32_t refresh_min2;
	uint32_t refresh_max2;
	uint32_t multiplier = 1;

	if (tt->seconds) {
		multiplier = 1000;
	}

	if (tt->refresh_min <= tt->refresh_max && tt->task_type != BCRepeat_c &&
		tt->task_type != BCRateLimit_c) {
		*refresh_min = tt->refresh_min * multiplier;
		*refresh_max = tt->refresh_max * multiplier;
		return;
	}

	switch (tt->task_type) {
	//Repeat, delay and step {{{
	case BCRepeat_c:
		if (tt->data.repeat.tree == NULL) {
			*refresh_min = 0;
			*refresh_max = 0;
		} else if (tt->data.repeat.done) {
			*refresh_min = (tt->data.repeat.start +
					tt->refresh_min * multiplier) -
						lastrun;
			*refresh_max = (tt->data.repeat.start +
					tt->refresh_max * multiplier) -
						lastrun;
		} else {
			calculate_refresh_rate(tt->data.repeat.tree, lastrun,
				refresh_min, refresh_max);
		}
		break;
	case BCStep_c:
		calculate_refresh_rate(tt->data.step.lhs, lastrun, refresh_min,
			refresh_max);
		break;
	case BCStepStable_c:
		calculate_refresh_rate(tt->data.steps.lhs, lastrun, refresh_min,
			refresh_max);
		break;
	case BCSeqStable_c:
		calculate_refresh_rate(tt->data.seqs.lhs, lastrun, refresh_min,
			refresh_max);
		break;
	case BCStepUnstable_c:
		calculate_refresh_rate(tt->data.stepu.lhs, lastrun, refresh_min,
			refresh_max);
		break;
	case BCSeqUnstable_c:
		calculate_refresh_rate(tt->data.sequ.lhs, lastrun, refresh_min,
			refresh_max);
		break;
	case BCDelayUntil_c:
		if (lastrun < tt->data.until) {
			uint32_t delay = tt->data.until - lastrun;
			*refresh_min = delay;
			*refresh_max = delay;
		}
		break;
		//}}}
		//Parallel {{{
	case BCTOr_c:
	case BCTAnd_c:
		calculate_refresh_rate(tt->data.tor.lhs, lastrun, refresh_min,
						refresh_max);
		calculate_refresh_rate(tt->data.tor.rhs, lastrun, &refresh_min2,
						&refresh_max2);
		if (*refresh_min > refresh_max2) {
			*refresh_max = refresh_max2;
			*refresh_min = refresh_min2;
		} else if (refresh_min2 > *refresh_max) {
			// refresh min and max already have the correct value
		} else {
			*refresh_min = mtmax(*refresh_min, refresh_min2);
			*refresh_max = mtmin(*refresh_max, refresh_max2);
		}

		break;
		//}}}
		//Sds {{{
	case BCSdsGet_c:
		*refresh_min = 0;
		*refresh_max = 2000;
		break;
	case BCSdsSet_c:
	case BCSdsUpd_c:
		*refresh_min = 0;
		*refresh_max = 0;
		break;
	case BCReflect_c:
		calculate_refresh_rate(tt->data.reflect.task, lastrun,
			refresh_min, refresh_max);
		break;
		//}}}
		//Rate limit {{{
	case BCRateLimit_c:
		*refresh_min = (tt->data.ratelimit.last_execution +
			tt->refresh_min * multiplier) -
				lastrun;
		*refresh_max = (tt->data.ratelimit.last_execution +
			tt->refresh_max * multiplier) -
				lastrun;
		break;
		//}}}
		//Pin IO {{{
	case BCReadD_c:
	case BCReadA_c:
		*refresh_min = 0;
		*refresh_max = 100;
		break;
	case BCWriteD_c:
	case BCWriteA_c:
	case BCPinMode_c:
		*refresh_min = 0;
		*refresh_max = 0;
		break;
		//}}}
		// Interrupts {{{
	case BCInterrupt_c:
		*refresh_min = INF_SLEEP_TIME;
		*refresh_max = INF_SLEEP_TIME;
		break;
		//}}}
		//Peripherals {{{
		//DHT {{{
	case BCDHTTemp_c:
	case BCDHTHumid_c:
		*refresh_min = 0;
		*refresh_max = 2000;
		break;
		//}}}
		//LIGHTSENSOR {{{
	case BCGetLight_c:
		*refresh_min = 0;
		*refresh_max = 100;
		break;
		//}}}
		//AIRQUALITYSENSOR {{{
	case BCTVOC_c:
	case BCCO2_c:
		*refresh_min = 0;
		*refresh_max = 2000;
		break;
		//}}}
		//GESTURESENSOR {{{
	case BCGesture_c:
		*refresh_min = 0;
		*refresh_max = 1000;
		break;
		//}}}
		//AIRQUALITYSENSOR {{{
	case BCSetEnvironmentalData_c:
		//}}}
		//LEDMatrix {{{
	case BCLEDMatrixDisplay_c:
	case BCLEDMatrixIntensity_c:
	case BCLEDMatrixDot_c:
	case BCLEDMatrixClear_c:
		//}}}
		//Neopixel {{{
	case BCNeoSetPixelColor_c:
		//}}}
		//}}}
		//Constant node values {{{
	case BCStableNode_c:
	case BCStable0_c:
	case BCStable1_c:
	case BCStable2_c:
	case BCStable3_c:
	case BCStable4_c:
	case BCUnstableNode_c:
	case BCUnstable0_c:
	case BCUnstable1_c:
	case BCUnstable2_c:
	case BCUnstable3_c:
	case BCUnstable4_c:
		*refresh_min = INF_SLEEP_TIME;
		*refresh_max = INF_SLEEP_TIME;
		break;
		//}}}
	case BCEvent_c:
		die(PSTR("Event node encountered in task tree"));
		break;
	case BCDelay_c:
		die(PSTR("Unrewritten delay node encountered in task tree"));
		break;
	}
}

void calculate_execution_interval(struct MTaskMeta *task)
{
	uint32_t refresh_min = 0;
	uint32_t refresh_max = 0;

	if (task->status != MTUnevaluated_c) {
		calculate_refresh_rate(task->tree, task->lastrun, &refresh_min,
			&refresh_max);
	}

	task->execution_min = mtmin(refresh_min, INF_SLEEP_TIME);
	task->execution_max = mtmin(refresh_max, INF_SLEEP_TIME);

	msg_debug(PSTR("execution interval: [%u, %u] \n"), refresh_min,
		refresh_max);
}

// Task queue
void print_queue(void)
{
#if LOGLEVEL == 2
	uint8_t i = queue_start;
	msg_debug(PSTR("task queue: ["));
	while (i != queue_end) {
		if (i != queue_start)
			msg_debug(PSTR(", "));

		msg_debug(PSTR("%u"), task_queue[i]->taskid);
		i = (i + 1) % TASK_QUEUE_SIZE;
	}
	msg_debug(PSTR("]\n"));
#endif /* LOGLEVEL == 2 */
}

struct MTaskMeta *peek_next_task(void)
{
	if (queue_start == queue_end)
		return NULL;

	return task_queue[queue_start];
}

struct MTaskMeta *next_task(uint32_t now)
{
	struct MTaskMeta *out = peek_next_task();

	// Stop condition
	if (out == NULL || out->lastrun == now ||
		now - (out->lastrun + out->execution_min) >= INF_SLEEP_TIME) {
		return NULL;
	}

	queue_start = (uint8_t)(queue_start + 1) % TASK_QUEUE_SIZE;

	print_queue();
	return out;
}
void queue_insert_task(struct MTaskMeta *t)
{
	msg_debug(PSTR("insert task: %u\n"),
		t->taskid, queue_start, queue_end);
	print_queue();

	uint8_t prev_end = queue_end;
	queue_end = (uint8_t)(queue_end + 1) % TASK_QUEUE_SIZE;
	if (queue_end == queue_start) {
		msg_log(PSTR("task queue full, resetting\n"));
		mem_reset();
	}

	uint32_t rr = t->lastrun + t->execution_max;
	uint8_t i = queue_start;
	bool found = false;

	while (i != queue_end) {
		struct MTaskMeta *cur = task_queue[i];

		if (!found &&
			(  i == prev_end
			|| rr - (cur->lastrun + cur->execution_max)
				> INF_SLEEP_TIME)) {
			found = true;
		}

		if (found) {
			task_queue[i] = t;
			t = cur;
		}

		i = (i + 1) % TASK_QUEUE_SIZE;
	}

	print_queue();
}

void queue_remove_task(uint8_t id)
{
	if (queue_start == queue_end) {
		return;
	}

	uint8_t i = queue_start;
	struct MTaskMeta *cur;
	struct MTaskMeta *prev = NULL;
	bool found = false;

	while (i != queue_end) {
		cur = task_queue[i];

		task_queue[i] = prev;

		if (cur->taskid == id) {
			found = true;
			break;
		}

		prev = cur;
		i = (i + 1) % TASK_QUEUE_SIZE;
	}

	if (!found) {
		queue_end = (uint8_t)(queue_end + 1) % TASK_QUEUE_SIZE;
		task_queue[i] = prev;
	}

	queue_start = (uint8_t)(queue_start + 1) % TASK_QUEUE_SIZE;

	print_queue();
}

void queue_update_pointer(struct MTaskMeta *oldt, struct MTaskMeta *newt) {
	uint8_t i = queue_start;
	while (i != queue_end) {
		if (task_queue[i] == oldt) {
			task_queue[i] = newt;
			return;
		}
		i = (i + 1) % TASK_QUEUE_SIZE;
	}
}
