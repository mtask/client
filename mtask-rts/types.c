#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "types.h"
union floatint {uint32_t intp; float floatp; };
union doubleint {uint64_t intp; double doublep; };
Int parse_Int(uint8_t (*get)(), void *(*alloc)(size_t)) {
	Int r;

	r = 0;
	r += (int64_t)get()<<56;
	r += (int64_t)get()<<48;
	r += (int64_t)get()<<40;
	r += (int64_t)get()<<32;
	r += (int64_t)get()<<24;
	r += (int64_t)get()<<16;
	r += (int64_t)get()<<8;
	r += (int64_t)get()<<0;

	return r;
	(void)alloc;
}
void *parse_Int_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	Int *r = (Int *)alloc(sizeof(Int));

	*r = parse_Int(get, alloc);

	return r;
	(void)alloc;
}
UInt8 parse_UInt8(uint8_t (*get)(), void *(*alloc)(size_t)) {
	UInt8 r;

	r = get();

	return r;
	(void)alloc;
}
void *parse_UInt8_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	UInt8 *r = (UInt8 *)alloc(sizeof(UInt8));

	*r = parse_UInt8(get, alloc);

	return r;
	(void)alloc;
}
UInt16 parse_UInt16(uint8_t (*get)(), void *(*alloc)(size_t)) {
	UInt16 r;

	r = (uint16_t)get()<<8;
	r += (uint16_t)get();

	return r;
	(void)alloc;
}
void *parse_UInt16_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	UInt16 *r = (UInt16 *)alloc(sizeof(UInt16));

	*r = parse_UInt16(get, alloc);

	return r;
	(void)alloc;
}
JumpLabel parse_JumpLabel(uint8_t (*get)(), void *(*alloc)(size_t)) {
	JumpLabel r;

	r = parse_UInt16(get, alloc);

	return r;
	(void)alloc;
}
void *parse_JumpLabel_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	JumpLabel *r = (JumpLabel *)alloc(sizeof(JumpLabel));

	*r = parse_JumpLabel(get, alloc);

	return r;
	(void)alloc;
}
struct BCTaskType parse_BCTaskType(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCTaskType r;

	r.cons = (enum BCTaskType_c) get();
	switch(r.cons) {
	case BCStable0_c:
		break;
	case BCStable1_c:
		break;
	case BCStable2_c:
		break;
	case BCStable3_c:
		break;
	case BCStable4_c:
		break;
	case BCStableNode_c:
		r.data.BCStableNode = parse_UInt8(get, alloc);
		break;
	case BCUnstable0_c:
		break;
	case BCUnstable1_c:
		break;
	case BCUnstable2_c:
		break;
	case BCUnstable3_c:
		break;
	case BCUnstable4_c:
		break;
	case BCUnstableNode_c:
		r.data.BCUnstableNode = parse_UInt8(get, alloc);
		break;
	case BCReadD_c:
		break;
	case BCWriteD_c:
		break;
	case BCReadA_c:
		break;
	case BCWriteA_c:
		break;
	case BCPinMode_c:
		break;
	case BCInterrupt_c:
		break;
	case BCRepeat_c:
		break;
	case BCDelay_c:
		break;
	case BCDelayUntil_c:
		break;
	case BCTAnd_c:
		break;
	case BCTOr_c:
		break;
	case BCStep_c:
		r.data.BCStep.f0 = parse_UInt8(get, alloc);
		r.data.BCStep.f1 = parse_JumpLabel(get, alloc);
		break;
	case BCStepStable_c:
		r.data.BCStepStable.f0 = parse_UInt8(get, alloc);
		r.data.BCStepStable.f1 = parse_JumpLabel(get, alloc);
		break;
	case BCStepUnstable_c:
		r.data.BCStepUnstable.f0 = parse_UInt8(get, alloc);
		r.data.BCStepUnstable.f1 = parse_JumpLabel(get, alloc);
		break;
	case BCSeqStable_c:
		r.data.BCSeqStable = parse_UInt8(get, alloc);
		break;
	case BCSeqUnstable_c:
		r.data.BCSeqUnstable = parse_UInt8(get, alloc);
		break;
	case BCSdsGet_c:
		r.data.BCSdsGet = parse_UInt8(get, alloc);
		break;
	case BCSdsSet_c:
		r.data.BCSdsSet = parse_UInt8(get, alloc);
		break;
	case BCSdsUpd_c:
		r.data.BCSdsUpd.f0 = parse_UInt8(get, alloc);
		r.data.BCSdsUpd.f1 = parse_JumpLabel(get, alloc);
		break;
	case BCRateLimit_c:
		break;
	case BCDHTTemp_c:
		r.data.BCDHTTemp = parse_UInt8(get, alloc);
		break;
	case BCDHTHumid_c:
		r.data.BCDHTHumid = parse_UInt8(get, alloc);
		break;
	case BCLEDMatrixDisplay_c:
		r.data.BCLEDMatrixDisplay = parse_UInt8(get, alloc);
		break;
	case BCLEDMatrixIntensity_c:
		r.data.BCLEDMatrixIntensity = parse_UInt8(get, alloc);
		break;
	case BCLEDMatrixDot_c:
		r.data.BCLEDMatrixDot = parse_UInt8(get, alloc);
		break;
	case BCLEDMatrixClear_c:
		r.data.BCLEDMatrixClear = parse_UInt8(get, alloc);
		break;
	case BCAButton_c:
		r.data.BCAButton = parse_UInt8(get, alloc);
		break;
	case BCBButton_c:
		r.data.BCBButton = parse_UInt8(get, alloc);
		break;
	case BCGetLight_c:
		r.data.BCGetLight = parse_UInt8(get, alloc);
		break;
	case BCSetEnvironmentalData_c:
		r.data.BCSetEnvironmentalData = parse_UInt8(get, alloc);
		break;
	case BCTVOC_c:
		r.data.BCTVOC = parse_UInt8(get, alloc);
		break;
	case BCCO2_c:
		r.data.BCCO2 = parse_UInt8(get, alloc);
		break;
	case BCGesture_c:
		r.data.BCGesture = parse_UInt8(get, alloc);
		break;
	case BCEvent_c:
		break;
	}

	return r;
	(void)alloc;
}
void *parse_BCTaskType_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCTaskType *r = (struct BCTaskType *)alloc(sizeof(struct BCTaskType));

	*r = parse_BCTaskType(get, alloc);

	return r;
	(void)alloc;
}
Char parse_Char(uint8_t (*get)(), void *(*alloc)(size_t)) {
	Char r;

	r = (Char)get();

	return r;
	(void)alloc;
}
void *parse_Char_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	Char *r = (Char *)alloc(sizeof(Char));

	*r = parse_Char(get, alloc);

	return r;
	(void)alloc;
}
struct Char_HshArray parse_Char_HshArray(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct Char_HshArray r;

	r.size = 0;
	r.size += (uint32_t)get()<<24;
	r.size += (uint32_t)get()<<16;
	r.size += (uint32_t)get()<<8;
	r.size += (uint32_t)get()<<0;
	r.elements = alloc(r.size*sizeof(Char));
	for (uint32_t i = 0; i<r.size; i++) {
		r.elements[i] = parse_Char(get, alloc);
	}

	return r;
	(void)alloc;
}
void *parse_Char_HshArray_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct Char_HshArray *r = (struct Char_HshArray *)alloc(sizeof(struct Char_HshArray));

	*r = parse_Char_HshArray(get, alloc);

	return r;
	(void)alloc;
}
String255 parse_String255(uint8_t (*get)(), void *(*alloc)(size_t)) {
	String255 r;

	r.size = get();
	r.elements = alloc(r.size);
	for (uint8_t i = 0; i<r.size; i++) {
		r.elements[i] = get();
	}

	return r;
	(void)alloc;
}
void *parse_String255_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	String255 *r = (String255 *)alloc(sizeof(String255));

	*r = parse_String255(get, alloc);

	return r;
	(void)alloc;
}
struct BCInstr parse_BCInstr(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCInstr r;

	r.cons = (enum BCInstr_c) get();
	switch(r.cons) {
	case BCReturn1_0_c:
		break;
	case BCReturn1_1_c:
		break;
	case BCReturn1_2_c:
		break;
	case BCReturn1_c:
		r.data.BCReturn1 = parse_UInt8(get, alloc);
		break;
	case BCReturn2_0_c:
		break;
	case BCReturn2_1_c:
		break;
	case BCReturn2_2_c:
		break;
	case BCReturn2_c:
		r.data.BCReturn2 = parse_UInt8(get, alloc);
		break;
	case BCReturn3_0_c:
		break;
	case BCReturn3_1_c:
		break;
	case BCReturn3_2_c:
		break;
	case BCReturn3_c:
		r.data.BCReturn3 = parse_UInt8(get, alloc);
		break;
	case BCReturn_0_c:
		r.data.BCReturn_0 = parse_UInt8(get, alloc);
		break;
	case BCReturn_1_c:
		r.data.BCReturn_1 = parse_UInt8(get, alloc);
		break;
	case BCReturn_2_c:
		r.data.BCReturn_2 = parse_UInt8(get, alloc);
		break;
	case BCReturn_c:
		r.data.BCReturn.f0 = parse_UInt8(get, alloc);
		r.data.BCReturn.f1 = parse_UInt8(get, alloc);
		break;
	case BCJumpF_c:
		r.data.BCJumpF = parse_JumpLabel(get, alloc);
		break;
	case BCJump_c:
		r.data.BCJump = parse_JumpLabel(get, alloc);
		break;
	case BCLabel_c:
		r.data.BCLabel = parse_JumpLabel(get, alloc);
		break;
	case BCJumpSR_c:
		r.data.BCJumpSR.f0 = parse_UInt8(get, alloc);
		r.data.BCJumpSR.f1 = parse_JumpLabel(get, alloc);
		break;
	case BCTailcall_c:
		r.data.BCTailcall.f0 = parse_UInt8(get, alloc);
		r.data.BCTailcall.f1 = parse_UInt8(get, alloc);
		r.data.BCTailcall.f2 = parse_JumpLabel(get, alloc);
		break;
	case BCArg0_c:
		break;
	case BCArg1_c:
		break;
	case BCArg2_c:
		break;
	case BCArg3_c:
		break;
	case BCArg4_c:
		break;
	case BCArg_c:
		r.data.BCArg = parse_UInt8(get, alloc);
		break;
	case BCArg10_c:
		break;
	case BCArg21_c:
		break;
	case BCArg32_c:
		break;
	case BCArg43_c:
		break;
	case BCArgs_c:
		r.data.BCArgs.f0 = parse_UInt8(get, alloc);
		r.data.BCArgs.f1 = parse_UInt8(get, alloc);
		break;
	case BCStepArg_c:
		r.data.BCStepArg.f0 = parse_UInt16(get, alloc);
		r.data.BCStepArg.f1 = parse_UInt8(get, alloc);
		break;
	case BCMkTask_c:
		r.data.BCMkTask = parse_BCTaskType(get, alloc);
		break;
	case BCTuneRateMs_c:
		break;
	case BCTuneRateSec_c:
		break;
	case BCIsStable_c:
		break;
	case BCIsUnstable_c:
		break;
	case BCIsNoValue_c:
		break;
	case BCIsValue_c:
		break;
	case BCPush1_c:
		r.data.BCPush1 = parse_UInt8(get, alloc);
		break;
	case BCPush2_c:
		r.data.BCPush2.f0 = parse_UInt8(get, alloc);
		r.data.BCPush2.f1 = parse_UInt8(get, alloc);
		break;
	case BCPush3_c:
		r.data.BCPush3.f0 = parse_UInt8(get, alloc);
		r.data.BCPush3.f1 = parse_UInt8(get, alloc);
		r.data.BCPush3.f2 = parse_UInt8(get, alloc);
		break;
	case BCPush4_c:
		r.data.BCPush4.f0 = parse_UInt8(get, alloc);
		r.data.BCPush4.f1 = parse_UInt8(get, alloc);
		r.data.BCPush4.f2 = parse_UInt8(get, alloc);
		r.data.BCPush4.f3 = parse_UInt8(get, alloc);
		break;
	case BCPush_c:
		r.data.BCPush = parse_String255(get, alloc);
		break;
	case BCPushNull_c:
		break;
	case BCPop1_c:
		break;
	case BCPop2_c:
		break;
	case BCPop3_c:
		break;
	case BCPop4_c:
		break;
	case BCPop_c:
		r.data.BCPop = parse_UInt8(get, alloc);
		break;
	case BCRot_c:
		r.data.BCRot.f0 = parse_UInt8(get, alloc);
		r.data.BCRot.f1 = parse_UInt8(get, alloc);
		break;
	case BCDup_c:
		break;
	case BCPushPtrs_c:
		break;
	case BCItoR_c:
		break;
	case BCItoL_c:
		break;
	case BCRtoI_c:
		break;
	case BCRtoL_c:
		break;
	case BCLtoI_c:
		break;
	case BCLtoR_c:
		break;
	case BCAddI_c:
		break;
	case BCSubI_c:
		break;
	case BCMultI_c:
		break;
	case BCDivI_c:
		break;
	case BCAddL_c:
		break;
	case BCSubL_c:
		break;
	case BCMultL_c:
		break;
	case BCDivL_c:
		break;
	case BCAddR_c:
		break;
	case BCSubR_c:
		break;
	case BCMultR_c:
		break;
	case BCDivR_c:
		break;
	case BCAnd_c:
		break;
	case BCOr_c:
		break;
	case BCNot_c:
		break;
	case BCEqI_c:
		break;
	case BCNeqI_c:
		break;
	case BCEqL_c:
		break;
	case BCNeqL_c:
		break;
	case BCLeI_c:
		break;
	case BCGeI_c:
		break;
	case BCLeqI_c:
		break;
	case BCGeqI_c:
		break;
	case BCLeL_c:
		break;
	case BCGeL_c:
		break;
	case BCLeqL_c:
		break;
	case BCGeqL_c:
		break;
	case BCLeR_c:
		break;
	case BCGeR_c:
		break;
	case BCLeqR_c:
		break;
	case BCGeqR_c:
		break;
	}

	return r;
	(void)alloc;
}
void *parse_BCInstr_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCInstr *r = (struct BCInstr *)alloc(sizeof(struct BCInstr));

	*r = parse_BCInstr(get, alloc);

	return r;
	(void)alloc;
}
enum APin parse_APin(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum APin r;

	r = (enum APin) get();

	return r;
	(void)alloc;
}
void *parse_APin_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum APin *r = (enum APin *)alloc(sizeof(enum APin));

	*r = parse_APin(get, alloc);

	return r;
	(void)alloc;
}
enum DPin parse_DPin(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum DPin r;

	r = (enum DPin) get();

	return r;
	(void)alloc;
}
void *parse_DPin_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum DPin *r = (enum DPin *)alloc(sizeof(enum DPin));

	*r = parse_DPin(get, alloc);

	return r;
	(void)alloc;
}
struct Pin parse_Pin(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct Pin r;

	r.cons = (enum Pin_c) get();
	switch(r.cons) {
	case AnalogPin_c:
		r.data.AnalogPin = parse_APin(get, alloc);
		break;
	case DigitalPin_c:
		r.data.DigitalPin = parse_DPin(get, alloc);
		break;
	}

	return r;
	(void)alloc;
}
void *parse_Pin_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct Pin *r = (struct Pin *)alloc(sizeof(struct Pin));

	*r = parse_Pin(get, alloc);

	return r;
	(void)alloc;
}
enum DHTtype parse_DHTtype(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum DHTtype r;

	r = (enum DHTtype) get();

	return r;
	(void)alloc;
}
void *parse_DHTtype_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum DHTtype *r = (enum DHTtype *)alloc(sizeof(enum DHTtype));

	*r = parse_DHTtype(get, alloc);

	return r;
	(void)alloc;
}
I2CAddr parse_I2CAddr(uint8_t (*get)(), void *(*alloc)(size_t)) {
	I2CAddr r;

	r = parse_UInt8(get, alloc);

	return r;
	(void)alloc;
}
void *parse_I2CAddr_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	I2CAddr *r = (I2CAddr *)alloc(sizeof(I2CAddr));

	*r = parse_I2CAddr(get, alloc);

	return r;
	(void)alloc;
}
struct DHTInfo parse_DHTInfo(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct DHTInfo r;

	r.cons = (enum DHTInfo_c) get();
	switch(r.cons) {
	case DHT_DHT_c:
		r.data.DHT_DHT.f0 = parse_Pin(get, alloc);
		r.data.DHT_DHT.f1 = parse_DHTtype(get, alloc);
		break;
	case DHT_SHT_c:
		r.data.DHT_SHT = parse_I2CAddr(get, alloc);
		break;
	}

	return r;
	(void)alloc;
}
void *parse_DHTInfo_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct DHTInfo *r = (struct DHTInfo *)alloc(sizeof(struct DHTInfo));

	*r = parse_DHTInfo(get, alloc);

	return r;
	(void)alloc;
}
struct LEDMatrixInfo parse_LEDMatrixInfo(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct LEDMatrixInfo r;

	r.dataPin = parse_Pin(get, alloc);
	r.clockPin = parse_Pin(get, alloc);

	return r;
	(void)alloc;
}
void *parse_LEDMatrixInfo_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct LEDMatrixInfo *r = (struct LEDMatrixInfo *)alloc(sizeof(struct LEDMatrixInfo));

	*r = parse_LEDMatrixInfo(get, alloc);

	return r;
	(void)alloc;
}
enum _Unit parse__Unit(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum _Unit r;

	r = (enum _Unit) get();

	return r;
	(void)alloc;
}
void *parse__Unit_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum _Unit *r = (enum _Unit *)alloc(sizeof(enum _Unit));

	*r = parse__Unit(get, alloc);

	return r;
	(void)alloc;
}
VoidPointer parse_VoidPointer(uint8_t (*get)(), void *(*alloc)(size_t)) {
	VoidPointer r;

	r	 = NULL;
	(void)get;

	return r;
	(void)alloc;
}
void *parse_VoidPointer_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	VoidPointer *r = (VoidPointer *)alloc(sizeof(VoidPointer));

	*r = parse_VoidPointer(get, alloc);

	return r;
	(void)alloc;
}
struct BCPeripheral parse_BCPeripheral(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCPeripheral r;

	r.cons = (enum BCPeripheral_c) get();
	switch(r.cons) {
	case BCDHT_c:
		r.data.BCDHT = parse_DHTInfo(get, alloc);
		break;
	case BCLEDMatrix_c:
		r.data.BCLEDMatrix = parse_LEDMatrixInfo(get, alloc);
		break;
	case BCI2CButton_c:
		r.data.BCI2CButton = parse_I2CAddr(get, alloc);
		break;
	case BCLightSensor_c:
		r.data.BCLightSensor = parse_I2CAddr(get, alloc);
		break;
	case BCAirQualitySensor_c:
		r.data.BCAirQualitySensor = parse_I2CAddr(get, alloc);
		break;
	case BCGestureSensor_c:
		r.data.BCGestureSensor = parse_I2CAddr(get, alloc);
		break;
	case BCInitialised_c:
		r.data.BCInitialised = parse_VoidPointer(get, alloc);
		break;
	}

	return r;
	(void)alloc;
}
void *parse_BCPeripheral_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCPeripheral *r = (struct BCPeripheral *)alloc(sizeof(struct BCPeripheral));

	*r = parse_BCPeripheral(get, alloc);

	return r;
	(void)alloc;
}
Bool parse_Bool(uint8_t (*get)(), void *(*alloc)(size_t)) {
	Bool r;

	r = (Bool)get();

	return r;
	(void)alloc;
}
void *parse_Bool_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	Bool *r = (Bool *)alloc(sizeof(Bool));

	*r = parse_Bool(get, alloc);

	return r;
	(void)alloc;
}
struct MTDeviceSpec parse_MTDeviceSpec(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTDeviceSpec r;

	r.memory = parse_UInt16(get, alloc);
	r.aPins = parse_UInt8(get, alloc);
	r.dPins = parse_UInt8(get, alloc);
	r.haveDHT = parse_Bool(get, alloc);
	r.haveLM = parse_Bool(get, alloc);
	r.haveI2B = parse_Bool(get, alloc);
	r.haveLS = parse_Bool(get, alloc);
	r.haveAQS = parse_Bool(get, alloc);
	r.haveGes = parse_Bool(get, alloc);

	return r;
	(void)alloc;
}
void *parse_MTDeviceSpec_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTDeviceSpec *r = (struct MTDeviceSpec *)alloc(sizeof(struct MTDeviceSpec));

	*r = parse_MTDeviceSpec(get, alloc);

	return r;
	(void)alloc;
}
struct TaskValue parse_TaskValue(uint8_t (*get)(), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(), void *(*)(size_t))) {
	struct TaskValue r;

	r.cons = (enum TaskValue_c) get();
	switch(r.cons) {
	case NoValue_c:
		break;
	case Value_c:
		r.data.Value.f0 = parse_0(get, alloc);
		r.data.Value.f1 = parse_Bool(get, alloc);
		break;
	}

	return r;
	(void)alloc;
}
void *parse_TaskValue_p(uint8_t (*get)(), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(), void *(*)(size_t))) {
	struct TaskValue *r = (struct TaskValue *)alloc(sizeof(struct TaskValue));

	*r = parse_TaskValue(get, alloc, parse_0);

	return r;
	(void)alloc;
}
struct UInt16_Array parse_UInt16_Array(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct UInt16_Array r;

	r.size = 0;
	r.size += (uint32_t)get()<<24;
	r.size += (uint32_t)get()<<16;
	r.size += (uint32_t)get()<<8;
	r.size += (uint32_t)get()<<0;
	r.elements = alloc(r.size*sizeof(UInt16));
	for (uint32_t i = 0; i<r.size; i++) {
		r.elements[i] = parse_UInt16(get, alloc);
	}

	return r;
	(void)alloc;
}
void *parse_UInt16_Array_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct UInt16_Array *r = (struct UInt16_Array *)alloc(sizeof(struct UInt16_Array));

	*r = parse_UInt16_Array(get, alloc);

	return r;
	(void)alloc;
}
struct MTException parse_MTException(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTException r;

	r.cons = (enum MTException_c) get();
	switch(r.cons) {
	case MTEOutOfMemory_c:
		r.data.MTEOutOfMemory = parse_UInt8(get, alloc);
		break;
	case MTEHeapUnderflow_c:
		r.data.MTEHeapUnderflow = parse_UInt8(get, alloc);
		break;
	case MTEStackOverflow_c:
		r.data.MTEStackOverflow = parse_UInt8(get, alloc);
		break;
	case MTESdsUnknown_c:
		r.data.MTESdsUnknown.f0 = parse_UInt8(get, alloc);
		r.data.MTESdsUnknown.f1 = parse_UInt8(get, alloc);
		break;
	case MTEPeripheralUnknown_c:
		r.data.MTEPeripheralUnknown.f0 = parse_UInt8(get, alloc);
		r.data.MTEPeripheralUnknown.f1 = parse_UInt8(get, alloc);
		break;
	case MTEUnsupportedPeripheral_c:
		r.data.MTEUnsupportedPeripheral.f0 = parse_UInt8(get, alloc);
		r.data.MTEUnsupportedPeripheral.f1 = parse_UInt8(get, alloc);
		break;
	case MTEUnsupportedInterrupt_c:
		r.data.MTEUnsupportedInterrupt.f0 = parse_UInt16(get, alloc);
		r.data.MTEUnsupportedInterrupt.f1 = parse_UInt8(get, alloc);
		break;
	case MTEFPException_c:
		r.data.MTEFPException = parse_UInt8(get, alloc);
		break;
	case MTESyncException_c:
		r.data.MTESyncException = parse_String255(get, alloc);
		break;
	case MTERTSError_c:
		break;
	case MTEUnexpectedDisconnect_c:
		break;
	}

	return r;
	(void)alloc;
}
void *parse_MTException_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTException *r = (struct MTException *)alloc(sizeof(struct MTException));

	*r = parse_MTException(get, alloc);

	return r;
	(void)alloc;
}
struct MTMessageFro parse_MTMessageFro(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTMessageFro r;

	r.cons = (enum MTMessageFro_c) get();
	switch(r.cons) {
	case MTFTaskAck_c:
		r.data.MTFTaskAck = parse_UInt8(get, alloc);
		break;
	case MTFTaskPrepAck_c:
		r.data.MTFTaskPrepAck = parse_UInt8(get, alloc);
		break;
	case MTFTaskDelAck_c:
		r.data.MTFTaskDelAck = parse_UInt8(get, alloc);
		break;
	case MTFTaskReturn_c:
		r.data.MTFTaskReturn.f0 = parse_UInt8(get, alloc);
		r.data.MTFTaskReturn.f1 = parse_TaskValue(get, alloc, parse_String255_p);
		break;
	case MTFSpec_c:
		r.data.MTFSpec = parse_MTDeviceSpec(get, alloc);
		break;
	case MTFSdsUpdate_c:
		r.data.MTFSdsUpdate.f0 = parse_UInt8(get, alloc);
		r.data.MTFSdsUpdate.f1 = parse_UInt8(get, alloc);
		r.data.MTFSdsUpdate.f2 = parse_UInt16_Array(get, alloc);
		break;
	case MTFException_c:
		r.data.MTFException = parse_MTException(get, alloc);
		break;
	case MTFDebug_c:
		r.data.MTFDebug = parse_String255(get, alloc);
		break;
	case MTFPing_c:
		break;
	}

	return r;
	(void)alloc;
}
void *parse_MTMessageFro_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTMessageFro *r = (struct MTMessageFro *)alloc(sizeof(struct MTMessageFro));

	*r = parse_MTMessageFro(get, alloc);

	return r;
	(void)alloc;
}
enum MTaskValueState parse_MTaskValueState(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum MTaskValueState r;

	r = (enum MTaskValueState) get();

	return r;
	(void)alloc;
}
void *parse_MTaskValueState_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum MTaskValueState *r = (enum MTaskValueState *)alloc(sizeof(enum MTaskValueState));

	*r = parse_MTaskValueState(get, alloc);

	return r;
	(void)alloc;
}
struct BCShareSpec parse_BCShareSpec(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCShareSpec r;

	r.bcs_ident = parse_UInt8(get, alloc);
	r.bcs_itasks = parse_Bool(get, alloc);
	r.bcs_value = parse_String255(get, alloc);

	return r;
	(void)alloc;
}
void *parse_BCShareSpec_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCShareSpec *r = (struct BCShareSpec *)alloc(sizeof(struct BCShareSpec));

	*r = parse_BCShareSpec(get, alloc);

	return r;
	(void)alloc;
}
struct BCShareSpec_Array parse_BCShareSpec_Array(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCShareSpec_Array r;

	r.size = 0;
	r.size += (uint32_t)get()<<24;
	r.size += (uint32_t)get()<<16;
	r.size += (uint32_t)get()<<8;
	r.size += (uint32_t)get()<<0;
	r.elements = alloc(r.size*sizeof(struct BCShareSpec));
	for (uint32_t i = 0; i<r.size; i++) {
		r.elements[i] = parse_BCShareSpec(get, alloc);
	}

	return r;
	(void)alloc;
}
void *parse_BCShareSpec_Array_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCShareSpec_Array *r = (struct BCShareSpec_Array *)alloc(sizeof(struct BCShareSpec_Array));

	*r = parse_BCShareSpec_Array(get, alloc);

	return r;
	(void)alloc;
}
struct BCPeripheral_Array parse_BCPeripheral_Array(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCPeripheral_Array r;

	r.size = 0;
	r.size += (uint32_t)get()<<24;
	r.size += (uint32_t)get()<<16;
	r.size += (uint32_t)get()<<8;
	r.size += (uint32_t)get()<<0;
	r.elements = alloc(r.size*sizeof(struct BCPeripheral));
	for (uint32_t i = 0; i<r.size; i++) {
		r.elements[i] = parse_BCPeripheral(get, alloc);
	}

	return r;
	(void)alloc;
}
void *parse_BCPeripheral_Array_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCPeripheral_Array *r = (struct BCPeripheral_Array *)alloc(sizeof(struct BCPeripheral_Array));

	*r = parse_BCPeripheral_Array(get, alloc);

	return r;
	(void)alloc;
}
struct BCInstr_Array parse_BCInstr_Array(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCInstr_Array r;

	r.size = 0;
	r.size += (uint32_t)get()<<24;
	r.size += (uint32_t)get()<<16;
	r.size += (uint32_t)get()<<8;
	r.size += (uint32_t)get()<<0;
	r.elements = alloc(r.size*sizeof(struct BCInstr));
	for (uint32_t i = 0; i<r.size; i++) {
		r.elements[i] = parse_BCInstr(get, alloc);
	}

	return r;
	(void)alloc;
}
void *parse_BCInstr_Array_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct BCInstr_Array *r = (struct BCInstr_Array *)alloc(sizeof(struct BCInstr_Array));

	*r = parse_BCInstr_Array(get, alloc);

	return r;
	(void)alloc;
}
BCInstrs parse_BCInstrs(uint8_t (*get)(), void *(*alloc)(size_t)) {
	BCInstrs r;

	r.size = get()<<8;
	r.size += get();
	r.elements = alloc(r.size);
	for (uint16_t i = 0; i<r.size; i++) {
		r.elements[i] = get();
	}

	return r;
	(void)alloc;
}
void *parse_BCInstrs_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	BCInstrs *r = (BCInstrs *)alloc(sizeof(BCInstrs));

	*r = parse_BCInstrs(get, alloc);

	return r;
	(void)alloc;
}
enum MTaskEvalStatus parse_MTaskEvalStatus(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum MTaskEvalStatus r;

	r = (enum MTaskEvalStatus) get();

	return r;
	(void)alloc;
}
void *parse_MTaskEvalStatus_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum MTaskEvalStatus *r = (enum MTaskEvalStatus *)alloc(sizeof(enum MTaskEvalStatus));

	*r = parse_MTaskEvalStatus(get, alloc);

	return r;
	(void)alloc;
}
UInt32 parse_UInt32(uint8_t (*get)(), void *(*alloc)(size_t)) {
	UInt32 r;

	r = (uint32_t)get()<<24;
	r = (uint32_t)get()<<16;
	r = (uint32_t)get()<<8;
	r += (uint32_t)get();

	return r;
	(void)alloc;
}
void *parse_UInt32_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	UInt32 *r = (UInt32 *)alloc(sizeof(UInt32));

	*r = parse_UInt32(get, alloc);

	return r;
	(void)alloc;
}
struct MTaskPrepData parse_MTaskPrepData(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTaskPrepData r;

	r.taskid = parse_UInt8(get, alloc);
	r.hash = parse_Int(get, alloc);

	return r;
	(void)alloc;
}
void *parse_MTaskPrepData_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTaskPrepData *r = (struct MTaskPrepData *)alloc(sizeof(struct MTaskPrepData));

	*r = parse_MTaskPrepData(get, alloc);

	return r;
	(void)alloc;
}
enum PinMode parse_PinMode(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum PinMode r;

	r = (enum PinMode) get();

	return r;
	(void)alloc;
}
void *parse_PinMode_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum PinMode *r = (enum PinMode *)alloc(sizeof(enum PinMode));

	*r = parse_PinMode(get, alloc);

	return r;
	(void)alloc;
}
enum InterruptMode parse_InterruptMode(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum InterruptMode r;

	r = (enum InterruptMode) get();

	return r;
	(void)alloc;
}
void *parse_InterruptMode_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum InterruptMode *r = (enum InterruptMode *)alloc(sizeof(enum InterruptMode));

	*r = parse_InterruptMode(get, alloc);

	return r;
	(void)alloc;
}
enum ButtonStatus parse_ButtonStatus(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum ButtonStatus r;

	r = (enum ButtonStatus) get();

	return r;
	(void)alloc;
}
void *parse_ButtonStatus_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum ButtonStatus *r = (enum ButtonStatus *)alloc(sizeof(enum ButtonStatus));

	*r = parse_ButtonStatus(get, alloc);

	return r;
	(void)alloc;
}
enum Gesture parse_Gesture(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum Gesture r;

	r = (enum Gesture) get();

	return r;
	(void)alloc;
}
void *parse_Gesture_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	enum Gesture *r = (enum Gesture *)alloc(sizeof(enum Gesture));

	*r = parse_Gesture(get, alloc);

	return r;
	(void)alloc;
}
struct _Tuple4 parse__Tuple4(uint8_t (*get)(), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_1)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_2)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_3)(uint8_t (*)(), void *(*)(size_t))) {
	struct _Tuple4 r;

	r.f0 = parse_0(get, alloc);
	r.f1 = parse_1(get, alloc);
	r.f2 = parse_2(get, alloc);
	r.f3 = parse_3(get, alloc);

	return r;
	(void)alloc;
}
void *parse__Tuple4_p(uint8_t (*get)(), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_1)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_2)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_3)(uint8_t (*)(), void *(*)(size_t))) {
	struct _Tuple4 *r = (struct _Tuple4 *)alloc(sizeof(struct _Tuple4));

	*r = parse__Tuple4(get, alloc, parse_0, parse_1, parse_2, parse_3);

	return r;
	(void)alloc;
}
struct _Tuple6 parse__Tuple6(uint8_t (*get)(), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_1)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_2)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_3)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_4)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_5)(uint8_t (*)(), void *(*)(size_t))) {
	struct _Tuple6 r;

	r.f0 = parse_0(get, alloc);
	r.f1 = parse_1(get, alloc);
	r.f2 = parse_2(get, alloc);
	r.f3 = parse_3(get, alloc);
	r.f4 = parse_4(get, alloc);
	r.f5 = parse_5(get, alloc);

	return r;
	(void)alloc;
}
void *parse__Tuple6_p(uint8_t (*get)(), void *(*alloc)(size_t), void *(*parse_0)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_1)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_2)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_3)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_4)(uint8_t (*)(), void *(*)(size_t)), void *(*parse_5)(uint8_t (*)(), void *(*)(size_t))) {
	struct _Tuple6 *r = (struct _Tuple6 *)alloc(sizeof(struct _Tuple6));

	*r = parse__Tuple6(get, alloc, parse_0, parse_1, parse_2, parse_3, parse_4, parse_5);

	return r;
	(void)alloc;
}
struct MTMessageTo parse_MTMessageTo(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTMessageTo r;

	r.cons = (enum MTMessageTo_c) get();
	switch(r.cons) {
	case MTTTask_c:
		r.data.MTTTask = parse_MTaskMeta_p(get, alloc);
		break;
	case MTTTaskPrep_c:
		r.data.MTTTaskPrep = parse_MTaskPrepData(get, alloc);
		break;
	case MTTTaskDel_c:
		r.data.MTTTaskDel = parse_UInt8(get, alloc);
		break;
	case MTTSpecRequest_c:
		break;
	case MTTShutdown_c:
		break;
	case MTTSdsUpdate_c:
		r.data.MTTSdsUpdate.f0 = parse_UInt8(get, alloc);
		r.data.MTTSdsUpdate.f1 = parse_UInt8(get, alloc);
		r.data.MTTSdsUpdate.f2 = parse_String255(get, alloc);
		break;
	}

	return r;
	(void)alloc;
}
void *parse_MTMessageTo_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTMessageTo *r = (struct MTMessageTo *)alloc(sizeof(struct MTMessageTo));

	*r = parse_MTMessageTo(get, alloc);

	return r;
	(void)alloc;
}
struct MTaskMeta parse_MTaskMeta(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTaskMeta r;

	r.taskid = parse_UInt8(get, alloc);
	r.tree = parse_VoidPointer(get, alloc);
	r.stability = parse_MTaskValueState(get, alloc);
	r.value = parse_String255(get, alloc);
	r.shares = parse_BCShareSpec_Array(get, alloc);
	r.peripherals = parse_BCPeripheral_Array(get, alloc);
	r.instructions = parse_BCInstrs(get, alloc);
	r.status = parse_MTaskEvalStatus(get, alloc);
	r.execution_min = parse_UInt32(get, alloc);
	r.execution_max = parse_UInt32(get, alloc);
	r.lastrun = parse_UInt32(get, alloc);

	return r;
	(void)alloc;
}
void *parse_MTaskMeta_p(uint8_t (*get)(), void *(*alloc)(size_t)) {
	struct MTaskMeta *r = (struct MTaskMeta *)alloc(sizeof(struct MTaskMeta));

	*r = parse_MTaskMeta(get, alloc);

	return r;
	(void)alloc;
}
void print_Int(void (*put)(uint8_t), Int r) {
	put ((r>>56) & 0xff);
	put ((r>>48) & 0xff);
	put ((r>>40) & 0xff);
	put ((r>>32) & 0xff);
	put ((r>>24) & 0xff);
	put ((r>>16) & 0xff);
	put ((r>>8) & 0xff);
	put ((r>>0) & 0xff);

}
void print_Int_p(void (*put)(uint8_t), void *r) {
	print_Int(put, *(Int *)r);
}
void print_UInt8(void (*put)(uint8_t), UInt8 r) {
	put(r);

}
void print_UInt8_p(void (*put)(uint8_t), void *r) {
	print_UInt8(put, *(UInt8 *)r);
}
void print_UInt16(void (*put)(uint8_t), UInt16 r) {
	put(r>>8);
	put(r & 0xff);

}
void print_UInt16_p(void (*put)(uint8_t), void *r) {
	print_UInt16(put, *(UInt16 *)r);
}
void print_JumpLabel(void (*put)(uint8_t), JumpLabel r) {
	print_UInt16(put, r);

}
void print_JumpLabel_p(void (*put)(uint8_t), void *r) {
	print_JumpLabel(put, *(JumpLabel *)r);
}
void print_BCTaskType(void (*put)(uint8_t), struct BCTaskType r) {
	put(r.cons);
	switch(r.cons) {
	case BCStable0_c:
		break;
	case BCStable1_c:
		break;
	case BCStable2_c:
		break;
	case BCStable3_c:
		break;
	case BCStable4_c:
		break;
	case BCStableNode_c:
		print_UInt8(put, r.data.BCStableNode);
		break;
	case BCUnstable0_c:
		break;
	case BCUnstable1_c:
		break;
	case BCUnstable2_c:
		break;
	case BCUnstable3_c:
		break;
	case BCUnstable4_c:
		break;
	case BCUnstableNode_c:
		print_UInt8(put, r.data.BCUnstableNode);
		break;
	case BCReadD_c:
		break;
	case BCWriteD_c:
		break;
	case BCReadA_c:
		break;
	case BCWriteA_c:
		break;
	case BCPinMode_c:
		break;
	case BCInterrupt_c:
		break;
	case BCRepeat_c:
		break;
	case BCDelay_c:
		break;
	case BCDelayUntil_c:
		break;
	case BCTAnd_c:
		break;
	case BCTOr_c:
		break;
	case BCStep_c:
		print_UInt8(put, r.data.BCStep.f0);
		print_JumpLabel(put, r.data.BCStep.f1);
		break;
	case BCStepStable_c:
		print_UInt8(put, r.data.BCStepStable.f0);
		print_JumpLabel(put, r.data.BCStepStable.f1);
		break;
	case BCStepUnstable_c:
		print_UInt8(put, r.data.BCStepUnstable.f0);
		print_JumpLabel(put, r.data.BCStepUnstable.f1);
		break;
	case BCSeqStable_c:
		print_UInt8(put, r.data.BCSeqStable);
		break;
	case BCSeqUnstable_c:
		print_UInt8(put, r.data.BCSeqUnstable);
		break;
	case BCSdsGet_c:
		print_UInt8(put, r.data.BCSdsGet);
		break;
	case BCSdsSet_c:
		print_UInt8(put, r.data.BCSdsSet);
		break;
	case BCSdsUpd_c:
		print_UInt8(put, r.data.BCSdsUpd.f0);
		print_JumpLabel(put, r.data.BCSdsUpd.f1);
		break;
	case BCRateLimit_c:
		break;
	case BCDHTTemp_c:
		print_UInt8(put, r.data.BCDHTTemp);
		break;
	case BCDHTHumid_c:
		print_UInt8(put, r.data.BCDHTHumid);
		break;
	case BCLEDMatrixDisplay_c:
		print_UInt8(put, r.data.BCLEDMatrixDisplay);
		break;
	case BCLEDMatrixIntensity_c:
		print_UInt8(put, r.data.BCLEDMatrixIntensity);
		break;
	case BCLEDMatrixDot_c:
		print_UInt8(put, r.data.BCLEDMatrixDot);
		break;
	case BCLEDMatrixClear_c:
		print_UInt8(put, r.data.BCLEDMatrixClear);
		break;
	case BCAButton_c:
		print_UInt8(put, r.data.BCAButton);
		break;
	case BCBButton_c:
		print_UInt8(put, r.data.BCBButton);
		break;
	case BCGetLight_c:
		print_UInt8(put, r.data.BCGetLight);
		break;
	case BCSetEnvironmentalData_c:
		print_UInt8(put, r.data.BCSetEnvironmentalData);
		break;
	case BCTVOC_c:
		print_UInt8(put, r.data.BCTVOC);
		break;
	case BCCO2_c:
		print_UInt8(put, r.data.BCCO2);
		break;
	case BCGesture_c:
		print_UInt8(put, r.data.BCGesture);
		break;
	case BCEvent_c:
		break;
	}

}
void print_BCTaskType_p(void (*put)(uint8_t), void *r) {
	print_BCTaskType(put, *(struct BCTaskType *)r);
}
void print_Char(void (*put)(uint8_t), Char r) {
	put(r);

}
void print_Char_p(void (*put)(uint8_t), void *r) {
	print_Char(put, *(Char *)r);
}
void print_Char_HshArray(void (*put)(uint8_t), struct Char_HshArray r) {
	put ((r.size>>24) & 0xff);
	put ((r.size>>16) & 0xff);
	put ((r.size>>8) & 0xff);
	put ((r.size>>0) & 0xff);
	for (uint32_t i = 0; i<r.size; i++) {
		print_Char(put, 	r.elements[i]);
	}

}
void print_Char_HshArray_p(void (*put)(uint8_t), void *r) {
	print_Char_HshArray(put, *(struct Char_HshArray *)r);
}
void print_String255(void (*put)(uint8_t), String255 r) {
	put(r.size);
	for (uint8_t i = 0; i<r.size; i++) {
		put(r.elements[i]);
	}

}
void print_String255_p(void (*put)(uint8_t), void *r) {
	print_String255(put, *(String255 *)r);
}
void print_BCInstr(void (*put)(uint8_t), struct BCInstr r) {
	put(r.cons);
	switch(r.cons) {
	case BCReturn1_0_c:
		break;
	case BCReturn1_1_c:
		break;
	case BCReturn1_2_c:
		break;
	case BCReturn1_c:
		print_UInt8(put, r.data.BCReturn1);
		break;
	case BCReturn2_0_c:
		break;
	case BCReturn2_1_c:
		break;
	case BCReturn2_2_c:
		break;
	case BCReturn2_c:
		print_UInt8(put, r.data.BCReturn2);
		break;
	case BCReturn3_0_c:
		break;
	case BCReturn3_1_c:
		break;
	case BCReturn3_2_c:
		break;
	case BCReturn3_c:
		print_UInt8(put, r.data.BCReturn3);
		break;
	case BCReturn_0_c:
		print_UInt8(put, r.data.BCReturn_0);
		break;
	case BCReturn_1_c:
		print_UInt8(put, r.data.BCReturn_1);
		break;
	case BCReturn_2_c:
		print_UInt8(put, r.data.BCReturn_2);
		break;
	case BCReturn_c:
		print_UInt8(put, r.data.BCReturn.f0);
		print_UInt8(put, r.data.BCReturn.f1);
		break;
	case BCJumpF_c:
		print_JumpLabel(put, r.data.BCJumpF);
		break;
	case BCJump_c:
		print_JumpLabel(put, r.data.BCJump);
		break;
	case BCLabel_c:
		print_JumpLabel(put, r.data.BCLabel);
		break;
	case BCJumpSR_c:
		print_UInt8(put, r.data.BCJumpSR.f0);
		print_JumpLabel(put, r.data.BCJumpSR.f1);
		break;
	case BCTailcall_c:
		print_UInt8(put, r.data.BCTailcall.f0);
		print_UInt8(put, r.data.BCTailcall.f1);
		print_JumpLabel(put, r.data.BCTailcall.f2);
		break;
	case BCArg0_c:
		break;
	case BCArg1_c:
		break;
	case BCArg2_c:
		break;
	case BCArg3_c:
		break;
	case BCArg4_c:
		break;
	case BCArg_c:
		print_UInt8(put, r.data.BCArg);
		break;
	case BCArg10_c:
		break;
	case BCArg21_c:
		break;
	case BCArg32_c:
		break;
	case BCArg43_c:
		break;
	case BCArgs_c:
		print_UInt8(put, r.data.BCArgs.f0);
		print_UInt8(put, r.data.BCArgs.f1);
		break;
	case BCStepArg_c:
		print_UInt16(put, r.data.BCStepArg.f0);
		print_UInt8(put, r.data.BCStepArg.f1);
		break;
	case BCMkTask_c:
		print_BCTaskType(put, r.data.BCMkTask);
		break;
	case BCTuneRateMs_c:
		break;
	case BCTuneRateSec_c:
		break;
	case BCIsStable_c:
		break;
	case BCIsUnstable_c:
		break;
	case BCIsNoValue_c:
		break;
	case BCIsValue_c:
		break;
	case BCPush1_c:
		print_UInt8(put, r.data.BCPush1);
		break;
	case BCPush2_c:
		print_UInt8(put, r.data.BCPush2.f0);
		print_UInt8(put, r.data.BCPush2.f1);
		break;
	case BCPush3_c:
		print_UInt8(put, r.data.BCPush3.f0);
		print_UInt8(put, r.data.BCPush3.f1);
		print_UInt8(put, r.data.BCPush3.f2);
		break;
	case BCPush4_c:
		print_UInt8(put, r.data.BCPush4.f0);
		print_UInt8(put, r.data.BCPush4.f1);
		print_UInt8(put, r.data.BCPush4.f2);
		print_UInt8(put, r.data.BCPush4.f3);
		break;
	case BCPush_c:
		print_String255(put, r.data.BCPush);
		break;
	case BCPushNull_c:
		break;
	case BCPop1_c:
		break;
	case BCPop2_c:
		break;
	case BCPop3_c:
		break;
	case BCPop4_c:
		break;
	case BCPop_c:
		print_UInt8(put, r.data.BCPop);
		break;
	case BCRot_c:
		print_UInt8(put, r.data.BCRot.f0);
		print_UInt8(put, r.data.BCRot.f1);
		break;
	case BCDup_c:
		break;
	case BCPushPtrs_c:
		break;
	case BCItoR_c:
		break;
	case BCItoL_c:
		break;
	case BCRtoI_c:
		break;
	case BCRtoL_c:
		break;
	case BCLtoI_c:
		break;
	case BCLtoR_c:
		break;
	case BCAddI_c:
		break;
	case BCSubI_c:
		break;
	case BCMultI_c:
		break;
	case BCDivI_c:
		break;
	case BCAddL_c:
		break;
	case BCSubL_c:
		break;
	case BCMultL_c:
		break;
	case BCDivL_c:
		break;
	case BCAddR_c:
		break;
	case BCSubR_c:
		break;
	case BCMultR_c:
		break;
	case BCDivR_c:
		break;
	case BCAnd_c:
		break;
	case BCOr_c:
		break;
	case BCNot_c:
		break;
	case BCEqI_c:
		break;
	case BCNeqI_c:
		break;
	case BCEqL_c:
		break;
	case BCNeqL_c:
		break;
	case BCLeI_c:
		break;
	case BCGeI_c:
		break;
	case BCLeqI_c:
		break;
	case BCGeqI_c:
		break;
	case BCLeL_c:
		break;
	case BCGeL_c:
		break;
	case BCLeqL_c:
		break;
	case BCGeqL_c:
		break;
	case BCLeR_c:
		break;
	case BCGeR_c:
		break;
	case BCLeqR_c:
		break;
	case BCGeqR_c:
		break;
	}

}
void print_BCInstr_p(void (*put)(uint8_t), void *r) {
	print_BCInstr(put, *(struct BCInstr *)r);
}
void print_APin(void (*put)(uint8_t), enum APin r) {
	put(r);

}
void print_APin_p(void (*put)(uint8_t), void *r) {
	print_APin(put, *(enum APin *)r);
}
void print_DPin(void (*put)(uint8_t), enum DPin r) {
	put(r);

}
void print_DPin_p(void (*put)(uint8_t), void *r) {
	print_DPin(put, *(enum DPin *)r);
}
void print_Pin(void (*put)(uint8_t), struct Pin r) {
	put(r.cons);
	switch(r.cons) {
	case AnalogPin_c:
		print_APin(put, r.data.AnalogPin);
		break;
	case DigitalPin_c:
		print_DPin(put, r.data.DigitalPin);
		break;
	}

}
void print_Pin_p(void (*put)(uint8_t), void *r) {
	print_Pin(put, *(struct Pin *)r);
}
void print_DHTtype(void (*put)(uint8_t), enum DHTtype r) {
	put(r);

}
void print_DHTtype_p(void (*put)(uint8_t), void *r) {
	print_DHTtype(put, *(enum DHTtype *)r);
}
void print_I2CAddr(void (*put)(uint8_t), I2CAddr r) {
	print_UInt8(put, r);

}
void print_I2CAddr_p(void (*put)(uint8_t), void *r) {
	print_I2CAddr(put, *(I2CAddr *)r);
}
void print_DHTInfo(void (*put)(uint8_t), struct DHTInfo r) {
	put(r.cons);
	switch(r.cons) {
	case DHT_DHT_c:
		print_Pin(put, r.data.DHT_DHT.f0);
		print_DHTtype(put, r.data.DHT_DHT.f1);
		break;
	case DHT_SHT_c:
		print_I2CAddr(put, r.data.DHT_SHT);
		break;
	}

}
void print_DHTInfo_p(void (*put)(uint8_t), void *r) {
	print_DHTInfo(put, *(struct DHTInfo *)r);
}
void print_LEDMatrixInfo(void (*put)(uint8_t), struct LEDMatrixInfo r) {
	print_Pin(put, r.dataPin);
	print_Pin(put, r.clockPin);

}
void print_LEDMatrixInfo_p(void (*put)(uint8_t), void *r) {
	print_LEDMatrixInfo(put, *(struct LEDMatrixInfo *)r);
}
void print__Unit(void (*put)(uint8_t), enum _Unit r) {
	put(r);

}
void print__Unit_p(void (*put)(uint8_t), void *r) {
	print__Unit(put, *(enum _Unit *)r);
}
void print_VoidPointer(void (*put)(uint8_t), VoidPointer r) {
	(void)	r	;
	(void)put;

}
void print_VoidPointer_p(void (*put)(uint8_t), void *r) {
	print_VoidPointer(put, *(VoidPointer *)r);
}
void print_BCPeripheral(void (*put)(uint8_t), struct BCPeripheral r) {
	put(r.cons);
	switch(r.cons) {
	case BCDHT_c:
		print_DHTInfo(put, r.data.BCDHT);
		break;
	case BCLEDMatrix_c:
		print_LEDMatrixInfo(put, r.data.BCLEDMatrix);
		break;
	case BCI2CButton_c:
		print_I2CAddr(put, r.data.BCI2CButton);
		break;
	case BCLightSensor_c:
		print_I2CAddr(put, r.data.BCLightSensor);
		break;
	case BCAirQualitySensor_c:
		print_I2CAddr(put, r.data.BCAirQualitySensor);
		break;
	case BCGestureSensor_c:
		print_I2CAddr(put, r.data.BCGestureSensor);
		break;
	case BCInitialised_c:
		print_VoidPointer(put, r.data.BCInitialised);
		break;
	}

}
void print_BCPeripheral_p(void (*put)(uint8_t), void *r) {
	print_BCPeripheral(put, *(struct BCPeripheral *)r);
}
void print_Bool(void (*put)(uint8_t), Bool r) {
	put(r);

}
void print_Bool_p(void (*put)(uint8_t), void *r) {
	print_Bool(put, *(Bool *)r);
}
void print_MTDeviceSpec(void (*put)(uint8_t), struct MTDeviceSpec r) {
	print_UInt16(put, r.memory);
	print_UInt8(put, r.aPins);
	print_UInt8(put, r.dPins);
	print_Bool(put, r.haveDHT);
	print_Bool(put, r.haveLM);
	print_Bool(put, r.haveI2B);
	print_Bool(put, r.haveLS);
	print_Bool(put, r.haveAQS);
	print_Bool(put, r.haveGes);

}
void print_MTDeviceSpec_p(void (*put)(uint8_t), void *r) {
	print_MTDeviceSpec(put, *(struct MTDeviceSpec *)r);
}
void print_TaskValue(void (*put)(uint8_t), struct TaskValue r, void (*print_0)(void (*)(uint8_t), void *)) {
	put(r.cons);
	switch(r.cons) {
	case NoValue_c:
		break;
	case Value_c:
		print_0(put, r.data.Value.f0);
		print_Bool(put, r.data.Value.f1);
		break;
	}

}
void print_TaskValue_p(void (*put)(uint8_t), void *r, void (*print_0)(void (*)(uint8_t), void *)) {
	print_TaskValue(put, *(struct TaskValue *)r, print_0);
}
void print_UInt16_Array(void (*put)(uint8_t), struct UInt16_Array r) {
	put ((r.size>>24) & 0xff);
	put ((r.size>>16) & 0xff);
	put ((r.size>>8) & 0xff);
	put ((r.size>>0) & 0xff);
	for (uint32_t i = 0; i<r.size; i++) {
		print_UInt16(put, 	r.elements[i]);
	}

}
void print_UInt16_Array_p(void (*put)(uint8_t), void *r) {
	print_UInt16_Array(put, *(struct UInt16_Array *)r);
}
void print_MTException(void (*put)(uint8_t), struct MTException r) {
	put(r.cons);
	switch(r.cons) {
	case MTEOutOfMemory_c:
		print_UInt8(put, r.data.MTEOutOfMemory);
		break;
	case MTEHeapUnderflow_c:
		print_UInt8(put, r.data.MTEHeapUnderflow);
		break;
	case MTEStackOverflow_c:
		print_UInt8(put, r.data.MTEStackOverflow);
		break;
	case MTESdsUnknown_c:
		print_UInt8(put, r.data.MTESdsUnknown.f0);
		print_UInt8(put, r.data.MTESdsUnknown.f1);
		break;
	case MTEPeripheralUnknown_c:
		print_UInt8(put, r.data.MTEPeripheralUnknown.f0);
		print_UInt8(put, r.data.MTEPeripheralUnknown.f1);
		break;
	case MTEUnsupportedPeripheral_c:
		print_UInt8(put, r.data.MTEUnsupportedPeripheral.f0);
		print_UInt8(put, r.data.MTEUnsupportedPeripheral.f1);
		break;
	case MTEUnsupportedInterrupt_c:
		print_UInt16(put, r.data.MTEUnsupportedInterrupt.f0);
		print_UInt8(put, r.data.MTEUnsupportedInterrupt.f1);
		break;
	case MTEFPException_c:
		print_UInt8(put, r.data.MTEFPException);
		break;
	case MTESyncException_c:
		print_String255(put, r.data.MTESyncException);
		break;
	case MTERTSError_c:
		break;
	case MTEUnexpectedDisconnect_c:
		break;
	}

}
void print_MTException_p(void (*put)(uint8_t), void *r) {
	print_MTException(put, *(struct MTException *)r);
}
void print_MTMessageFro(void (*put)(uint8_t), struct MTMessageFro r) {
	put(r.cons);
	switch(r.cons) {
	case MTFTaskAck_c:
		print_UInt8(put, r.data.MTFTaskAck);
		break;
	case MTFTaskPrepAck_c:
		print_UInt8(put, r.data.MTFTaskPrepAck);
		break;
	case MTFTaskDelAck_c:
		print_UInt8(put, r.data.MTFTaskDelAck);
		break;
	case MTFTaskReturn_c:
		print_UInt8(put, r.data.MTFTaskReturn.f0);
		print_TaskValue(put, r.data.MTFTaskReturn.f1, print_String255_p);
		break;
	case MTFSpec_c:
		print_MTDeviceSpec(put, r.data.MTFSpec);
		break;
	case MTFSdsUpdate_c:
		print_UInt8(put, r.data.MTFSdsUpdate.f0);
		print_UInt8(put, r.data.MTFSdsUpdate.f1);
		print_UInt16_Array(put, r.data.MTFSdsUpdate.f2);
		break;
	case MTFException_c:
		print_MTException(put, r.data.MTFException);
		break;
	case MTFDebug_c:
		print_String255(put, r.data.MTFDebug);
		break;
	case MTFPing_c:
		break;
	}

}
void print_MTMessageFro_p(void (*put)(uint8_t), void *r) {
	print_MTMessageFro(put, *(struct MTMessageFro *)r);
}
void print_MTaskValueState(void (*put)(uint8_t), enum MTaskValueState r) {
	put(r);

}
void print_MTaskValueState_p(void (*put)(uint8_t), void *r) {
	print_MTaskValueState(put, *(enum MTaskValueState *)r);
}
void print_BCShareSpec(void (*put)(uint8_t), struct BCShareSpec r) {
	print_UInt8(put, r.bcs_ident);
	print_Bool(put, r.bcs_itasks);
	print_String255(put, r.bcs_value);

}
void print_BCShareSpec_p(void (*put)(uint8_t), void *r) {
	print_BCShareSpec(put, *(struct BCShareSpec *)r);
}
void print_BCShareSpec_Array(void (*put)(uint8_t), struct BCShareSpec_Array r) {
	put ((r.size>>24) & 0xff);
	put ((r.size>>16) & 0xff);
	put ((r.size>>8) & 0xff);
	put ((r.size>>0) & 0xff);
	for (uint32_t i = 0; i<r.size; i++) {
		print_BCShareSpec(put, 	r.elements[i]);
	}

}
void print_BCShareSpec_Array_p(void (*put)(uint8_t), void *r) {
	print_BCShareSpec_Array(put, *(struct BCShareSpec_Array *)r);
}
void print_BCPeripheral_Array(void (*put)(uint8_t), struct BCPeripheral_Array r) {
	put ((r.size>>24) & 0xff);
	put ((r.size>>16) & 0xff);
	put ((r.size>>8) & 0xff);
	put ((r.size>>0) & 0xff);
	for (uint32_t i = 0; i<r.size; i++) {
		print_BCPeripheral(put, 	r.elements[i]);
	}

}
void print_BCPeripheral_Array_p(void (*put)(uint8_t), void *r) {
	print_BCPeripheral_Array(put, *(struct BCPeripheral_Array *)r);
}
void print_BCInstr_Array(void (*put)(uint8_t), struct BCInstr_Array r) {
	put ((r.size>>24) & 0xff);
	put ((r.size>>16) & 0xff);
	put ((r.size>>8) & 0xff);
	put ((r.size>>0) & 0xff);
	for (uint32_t i = 0; i<r.size; i++) {
		print_BCInstr(put, 	r.elements[i]);
	}

}
void print_BCInstr_Array_p(void (*put)(uint8_t), void *r) {
	print_BCInstr_Array(put, *(struct BCInstr_Array *)r);
}
void print_BCInstrs(void (*put)(uint8_t), BCInstrs r) {
	put(r.size >> 8);
	put(r.size);
	for (uint16_t i = 0; i<r.size; i++) {
		put(r.elements[i]);
	}

}
void print_BCInstrs_p(void (*put)(uint8_t), void *r) {
	print_BCInstrs(put, *(BCInstrs *)r);
}
void print_MTaskEvalStatus(void (*put)(uint8_t), enum MTaskEvalStatus r) {
	put(r);

}
void print_MTaskEvalStatus_p(void (*put)(uint8_t), void *r) {
	print_MTaskEvalStatus(put, *(enum MTaskEvalStatus *)r);
}
void print_UInt32(void (*put)(uint8_t), UInt32 r) {
	put(r>>24);
	put(r>>16);
	put(r>>8);
	put(r & 0xff);

}
void print_UInt32_p(void (*put)(uint8_t), void *r) {
	print_UInt32(put, *(UInt32 *)r);
}
void print_MTaskPrepData(void (*put)(uint8_t), struct MTaskPrepData r) {
	print_UInt8(put, r.taskid);
	print_Int(put, r.hash);

}
void print_MTaskPrepData_p(void (*put)(uint8_t), void *r) {
	print_MTaskPrepData(put, *(struct MTaskPrepData *)r);
}
void print_PinMode(void (*put)(uint8_t), enum PinMode r) {
	put(r);

}
void print_PinMode_p(void (*put)(uint8_t), void *r) {
	print_PinMode(put, *(enum PinMode *)r);
}
void print_InterruptMode(void (*put)(uint8_t), enum InterruptMode r) {
	put(r);

}
void print_InterruptMode_p(void (*put)(uint8_t), void *r) {
	print_InterruptMode(put, *(enum InterruptMode *)r);
}
void print_ButtonStatus(void (*put)(uint8_t), enum ButtonStatus r) {
	put(r);

}
void print_ButtonStatus_p(void (*put)(uint8_t), void *r) {
	print_ButtonStatus(put, *(enum ButtonStatus *)r);
}
void print_Gesture(void (*put)(uint8_t), enum Gesture r) {
	put(r);

}
void print_Gesture_p(void (*put)(uint8_t), void *r) {
	print_Gesture(put, *(enum Gesture *)r);
}
void print__Tuple4(void (*put)(uint8_t), struct _Tuple4 r, void (*print_0)(void (*)(uint8_t), void *), void (*print_1)(void (*)(uint8_t), void *), void (*print_2)(void (*)(uint8_t), void *), void (*print_3)(void (*)(uint8_t), void *)) {
	print_0(put, r.f0);
	print_1(put, r.f1);
	print_2(put, r.f2);
	print_3(put, r.f3);

}
void print__Tuple4_p(void (*put)(uint8_t), void *r, void (*print_0)(void (*)(uint8_t), void *), void (*print_1)(void (*)(uint8_t), void *), void (*print_2)(void (*)(uint8_t), void *), void (*print_3)(void (*)(uint8_t), void *)) {
	print__Tuple4(put, *(struct _Tuple4 *)r, print_0, print_1, print_2, print_3);
}
void print__Tuple6(void (*put)(uint8_t), struct _Tuple6 r, void (*print_0)(void (*)(uint8_t), void *), void (*print_1)(void (*)(uint8_t), void *), void (*print_2)(void (*)(uint8_t), void *), void (*print_3)(void (*)(uint8_t), void *), void (*print_4)(void (*)(uint8_t), void *), void (*print_5)(void (*)(uint8_t), void *)) {
	print_0(put, r.f0);
	print_1(put, r.f1);
	print_2(put, r.f2);
	print_3(put, r.f3);
	print_4(put, r.f4);
	print_5(put, r.f5);

}
void print__Tuple6_p(void (*put)(uint8_t), void *r, void (*print_0)(void (*)(uint8_t), void *), void (*print_1)(void (*)(uint8_t), void *), void (*print_2)(void (*)(uint8_t), void *), void (*print_3)(void (*)(uint8_t), void *), void (*print_4)(void (*)(uint8_t), void *), void (*print_5)(void (*)(uint8_t), void *)) {
	print__Tuple6(put, *(struct _Tuple6 *)r, print_0, print_1, print_2, print_3, print_4, print_5);
}
void print_MTMessageTo(void (*put)(uint8_t), struct MTMessageTo r) {
	put(r.cons);
	switch(r.cons) {
	case MTTTask_c:
		print_MTaskMeta_p(put, r.data.MTTTask);
		break;
	case MTTTaskPrep_c:
		print_MTaskPrepData(put, r.data.MTTTaskPrep);
		break;
	case MTTTaskDel_c:
		print_UInt8(put, r.data.MTTTaskDel);
		break;
	case MTTSpecRequest_c:
		break;
	case MTTShutdown_c:
		break;
	case MTTSdsUpdate_c:
		print_UInt8(put, r.data.MTTSdsUpdate.f0);
		print_UInt8(put, r.data.MTTSdsUpdate.f1);
		print_String255(put, r.data.MTTSdsUpdate.f2);
		break;
	}

}
void print_MTMessageTo_p(void (*put)(uint8_t), void *r) {
	print_MTMessageTo(put, *(struct MTMessageTo *)r);
}
void print_MTaskMeta(void (*put)(uint8_t), struct MTaskMeta r) {
	print_UInt8(put, r.taskid);
	print_VoidPointer(put, r.tree);
	print_MTaskValueState(put, r.stability);
	print_String255(put, r.value);
	print_BCShareSpec_Array(put, r.shares);
	print_BCPeripheral_Array(put, r.peripherals);
	print_BCInstrs(put, r.instructions);
	print_MTaskEvalStatus(put, r.status);
	print_UInt32(put, r.execution_min);
	print_UInt32(put, r.execution_max);
	print_UInt32(put, r.lastrun);

}
void print_MTaskMeta_p(void (*put)(uint8_t), void *r) {
	print_MTaskMeta(put, *(struct MTaskMeta *)r);
}

