#include "interface.h"
#ifdef NVM
#include <inttypes.h>
#include "mem.h"
#include "tasktree.h"
#include "task.h"
#include "types.h"
#include "nvm_interface.h"

// from events.c
extern struct TaskTree *events;
// from scheduling.c
extern uint8_t queue_start;
extern uint8_t queue_end;
extern struct MTaskMeta *task_queue[TASK_QUEUE_SIZE];

void nvm_save(void)
{
	msg_debug(PSTR("nvm_save\n"));
	nvm_open_write();
	// print original pointer to restore when addresses are randomised
	print_VoidPointer(nvm_put, mtmem_real);
	msg_debug(PSTR("printed mtmem_real\n"));
	// Get the task pointer (stack)
	print_UInt16(nvm_put, mem_task);
	// print the heap pointer
	print_UInt16(nvm_put, mem_heap);
	// print events pointer
	print_VoidPointer(nvm_put, events);
	// Write the task memory
	for (uint16_t i = 0; i<mem_task; i++) {
		nvm_put(mtmem[i]);
	}
	// Write the tasktree memory
	for (uint16_t i = mem_heap; i<memsize; i++) {
		nvm_put(mtmem[i]);
	}
	// Write the scheduling queue
	print_UInt8(nvm_put, queue_start);
	print_UInt8(nvm_put, queue_end);
	for (uint8_t i = queue_start;
			i != queue_end;
			i = (i+1) % TASK_QUEUE_SIZE) {
		print_VoidPointer(nvm_put, task_queue[i]);
	}
	nvm_close_write();
	msg_debug(PSTR("nvm_save done\n"));
}

#define offsetp(p, d) ((void *)((uintptr_t)(p) + (d)))
#define updatep(p, d) do {\
	if ((p) != NULL) {\
		(p) = offsetp(p, d); \
	}\
} while (0)

static ptrdiff_t pd = 0;

bool patch_pointer(struct TaskTree *t)
{
	switch (t->task_type) {
	case BCStableNode_c:
		updatep(t->data.stablenode.next, pd);
		break;
	case BCUnstableNode_c:
		updatep(t->data.unstablenode.next, pd);
		break;
	case BCStep_c:
		updatep(t->data.step.lhs, pd);
		break;
	case BCStepStable_c:
		updatep(t->data.steps.lhs, pd);
		break;
	case BCStepUnstable_c:
		updatep(t->data.stepu.lhs, pd);
		break;
	case BCSeqStable_c:
		updatep(t->data.seqs.lhs, pd);
		updatep(t->data.seqs.rhs, pd);
		break;
	case BCSeqUnstable_c:
		updatep(t->data.sequ.lhs, pd);
		updatep(t->data.sequ.rhs, pd);
		break;
	case BCRepeat_c:
		updatep(t->data.repeat.tree, pd);
		updatep(t->data.repeat.oldtree, pd);
		break;
	case BCTOr_c:
		updatep(t->data.tor.lhs, pd);
		updatep(t->data.tor.rhs, pd);
		break;
	case BCTAnd_c:
		updatep(t->data.tand.lhs, pd);
		updatep(t->data.tand.rhs, pd);
		break;
	case BCSdsSet_c:
		updatep(t->data.sdsset.data, pd);
		break;
	case BCReflect_c:
		updatep(t->data.reflect.task, pd);
		break;
	case BCRateLimit_c:
		updatep(t->data.ratelimit.task, pd);
		updatep(t->data.ratelimit.storage, pd);
		break;
	case BCSdsUpd_c:
		updatep(t->data.sdsupd.ctx, pd);
		break;
	case BCEvent_c:
		updatep(t->data.event.next, pd);
		break;
	case BCStable0_c: // fall through
	case BCStable1_c: // fall through
	case BCStable2_c: // fall through
	case BCStable3_c: // fall through
	case BCStable4_c: // fall through
	case BCUnstable0_c: // fall through
	case BCUnstable1_c: // fall through
	case BCUnstable2_c: // fall through
	case BCUnstable3_c: // fall through
	case BCUnstable4_c: // fall through
	case BCReadD_c: // fall through
	case BCWriteD_c: // fall through
	case BCReadA_c: // fall through
	case BCWriteA_c: // fall through
	case BCPinMode_c: // fall through
	case BCDelay_c: // fall through
	case BCDelayUntil_c: // fall through
	case BCSdsGet_c: // fall through
	case BCDHTTemp_c: // fall through
	case BCDHTHumid_c: // fall through
	case BCLEDMatrixDisplay_c: // fall through
	case BCLEDMatrixIntensity_c: // fall through
	case BCLEDMatrixDot_c: // fall through
	case BCLEDMatrixClear_c: // fall through
	case BCGetLight_c: // fall through
	case BCSetEnvironmentalData_c: // fall through
	case BCTVOC_c: // fall through
	case BCCO2_c: // fall through
	case BCGesture_c: // fall through
	case BCNeoSetPixelColor_c: // fall through
	case BCInterrupt_c:
		break;
	}
	return true;
}

bool nvm_restore(void)
{
	if (!nvm_open_read()) {
		msg_log(PSTR("failed to restore from nvm: open\n"));
		return false;
	}
	// get original pointer to calculate the offset to restore when
	// addresses are randomised
	void *mtmem_real_old_addr = parse_VoidPointer(nvm_get, NULL);
	// Get the task pointer (stack)
	mem_task = parse_UInt16(nvm_get, NULL);
	// Get the heap pointer
	mem_heap = parse_UInt16(nvm_get, NULL);
	// read the events pointer
	events = parse_VoidPointer(nvm_get, NULL);
	// Read the task memory
	for (uint16_t i = 0; i<mem_task; i++) {
		mtmem[i] = nvm_get();
	}
	// Read the tasktree memory
	for (uint16_t i = mem_heap; i<memsize; i++) {
		mtmem[i] = nvm_get();
	}
	// Read the scheduling queue
	queue_start = parse_UInt8(nvm_get, NULL);
	queue_end = parse_UInt8(nvm_get, NULL);
	for (uint8_t i = queue_start;
			i != queue_end;
			i = (i+1) % TASK_QUEUE_SIZE) {
		task_queue[i] = parse_VoidPointer(nvm_get, NULL);
	}

	// difference in bytes
	pd = (intptr_t)mtmem_real - (intptr_t)mtmem_real_old_addr;
	// No ASLR, no translation
	if (pd != 0) {
		msg_debug(PSTR("mtmem_real    : %p\n"), mtmem_real);
		msg_debug(PSTR("mtmem_real old: %p\n"), mtmem_real_old_addr);
		msg_debug(PSTR("diff          : %td\n"), pd);

		// Patch events pointer
		updatep(events, pd);
		// Translate the pointers
		struct MTaskMeta *ct = mem_task_head();
		while (ct != NULL) {
			updatep(ct->tree, pd);
			updatep(ct->value.elements, pd);
			updatep(ct->shares.elements, pd);
			for (uint32_t i = 0; i<ct->shares.size; i++) {
				updatep(ct->shares.elements[i]
					.bcs_value.elements, pd);
			}
			updatep(ct->peripherals.elements, pd);
			updatep(ct->instructions.elements, pd);

			ct = mem_task_next(ct);
		}
		// Translate and re-initialise the tasktrees
		mem_iterate_tasktrees(patch_pointer);
		// Translate the event queue
		for (uint8_t i = queue_start;
				i != queue_end;
				i = (i+1) % TASK_QUEUE_SIZE) {
			updatep(task_queue[i], pd);
		}
	// Addresses are randomised
	} else {
		msg_debug(PSTR("no pointer difference, no offset needed\n"));
	}

	// Initialise the peripherals
	struct MTaskMeta *ct = mem_task_head();
	while (ct != NULL) {
		// Initialise the peripherals
		if (!task_initialise_peripherals(ct)) {
			msg_log(PSTR("failed to restore from nvm: "
				"peripherals\n"));
			return false;
		}
		ct = mem_task_next(ct);
	}

	// Close the nvm
	nvm_close_read();
	return true;
}
#endif /* NVM */
