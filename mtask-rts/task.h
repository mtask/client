/** @file
 *
 * Contains task and task list related functions
 */
#ifndef TASK_H
#define TASK_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>
#include <stdbool.h>

#include "types.h"
#include "interface.h"

/**
 * Complete a task, check if the return value changed and possibly send it to
 * the server.
 *
 * @param t pointer to the task
 * @param stack pointer to the stack where the value starts
 */
void task_complete(struct MTaskMeta *t, uint16_t *stack);
/**
 * Remove a task and mark it as trash.
 *
 * @param t pointer to the task
 * @param stack pointer to the stack, used to recursively mark
 */
void task_remove(struct MTaskMeta *t, uint16_t *stack);
/**
 * Move the metadata of a task down to compress the task heap.
 *
 * @param t the task to move
 * @param offset the number of bytes to move it down
 */
void task_move(struct MTaskMeta *t, uint16_t offset);
/**
 * Print just the top level of the task for debugging purposes
 *
 * @param t pointer to the task
 * @param verbose print verbosely
 */
void task_print(struct MTaskMeta *t, bool verbose);
/**
 * Retrieve a peripheral from a task
 *
 * @param id peripheral id
 * @return state of that peripheral
 */
void *peripheral_get(uint8_t id);
/**
 * Initialise a new task from a received new task message. Initialise the
 * peripherals, pad the bytecode, etc.
 * Finally it acks to the server.
 *
 * @param data MTask data from an MTTTask message
 * @throws MTEUnsupportedPeripheral if a peripheral wasn't supported
 * @return success
 */
bool task_register(struct MTaskMeta *data);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !TASK_H */

