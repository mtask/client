#include "task.h"

#include "comm_interface.h"
#include "mem.h"
#include "scheduler.h"
#include "events.h"

void destroy_task_node(struct TaskTree* tt, struct MTaskMeta *t)
{
	switch (tt->task_type) {
		case BCInterrupt_c:
			deregister_events(t, tt);
			break;
		default:
			break;
	}
}

//TODO make non-recursive
void tasktree_print(struct TaskTree *t, int indent)
{
#if LOGLEVEL == 2
	if (t == NULL) {
		msg_debug(PSTR("(null)\n"));
		return;
	}

	if (t->trash) {
		msg_debug(PSTR("(removed)\n"));
	}

//	for (int i = 0; i<indent; i++)
//		msg_debug(PSTR("\t"));

	switch (t->task_type) {
	case BCStableNode_c:
		msg_debug(PSTR("BCStablenode (%u): ("), t->data.stablenode.w);
		tasktree_print(t->data.stablenode.next, 0);
		msg_debug(PSTR(") %d %d"), t->data.stablenode.stable[0],
			t->data.stablenode.stable[1]);
		break;
	case BCStable0_c:
		msg_debug(PSTR("BCStable0"));
		break;
	case BCStable1_c:
		msg_debug(PSTR("BCStable1: %d"), t->data.stable[0]);
		break;
	case BCStable2_c:
		msg_debug(PSTR("BCStable2: %d %d"), t->data.stable[0],
			t->data.stable[1]);
		break;
	case BCStable3_c:
		msg_debug(PSTR("BCStable3: %d %d %d"), t->data.stable[0],
			t->data.stable[1], t->data.stable[2]);
		break;
	case BCStable4_c:
		msg_debug(PSTR("BCStable4: %d %d %d %d"), t->data.stable[0],
			t->data.stable[1], t->data.stable[2],
			t->data.stable[3]);
		break;
	case BCUnstableNode_c:
		msg_debug(PSTR("BCUnstablenode (%u): ("),
			t->data.unstablenode.w);
		tasktree_print(t->data.unstablenode.next, 0);
		msg_debug(PSTR(") %d %d"), t->data.unstablenode.unstable[0],
			t->data.unstablenode.unstable[1]);
		break;
	case BCUnstable0_c:
		msg_debug(PSTR("BCUnstable0"));
		break;
	case BCUnstable1_c:
		msg_debug(PSTR("BCUnstable1: %d"), t->data.unstable[0]);
		break;
	case BCUnstable2_c:
		msg_debug(PSTR("BCUnstable2: %d %d"), t->data.unstable[0],
			t->data.unstable[1]);
		break;
	case BCUnstable3_c:
		msg_debug(PSTR("BCUnstable3: %d %d %d"), t->data.unstable[0],
			t->data.unstable[1], t->data.unstable[2]);
		break;
	case BCUnstable4_c:
		msg_debug(PSTR("BCUnstable4: %d %d %d %d"),
			t->data.unstable[0], t->data.unstable[1],
			t->data.unstable[2], t->data.unstable[3]);
		break;
	case BCReadD_c:
		msg_debug(PSTR("readD %d"), t->data.readd);
		break;
	case BCWriteD_c:
		msg_debug(PSTR("writeD %d %d"), t->data.writed.pin,
			t->data.writed.value);
		break;
	case BCReadA_c:
		msg_debug(PSTR("readA %d"), t->data.readd);
		break;
	case BCWriteA_c:
		msg_debug(PSTR("writeA %d %d"), t->data.writed.pin,
			t->data.writed.value);
		break;
	case BCPinMode_c:
		msg_debug(PSTR("pinmode %d %d"), t->data.pinmode.pin,
			t->data.pinmode.pinmode);
		break;
	case BCInterrupt_c:
		msg_debug(PSTR("interrupt %d %d"), t->data.interrupt.pin,
			t->data.interrupt.mode);
		break;
	case BCRepeat_c:
		msg_debug(PSTR("repeat ("));
		tasktree_print(t->data.repeat.tree, indent+1);
		msg_debug(PSTR(")"));
		break;
	case BCDelay_c:
		msg_debug(PSTR("delay (%u)"), t->data.delay);
		break;
	case BCDelayUntil_c:
		msg_debug(PSTR("delayuntil (%u)"), t->data.until);
		break;
	case BCStep_c:
		msg_debug(PSTR("("));
		tasktree_print(t->data.step.lhs, indent+1);
		msg_debug(PSTR(") >>* %lu"), t->data.step.rhs);
		break;
	case BCStepStable_c:
		msg_debug(PSTR("("));
		tasktree_print(t->data.steps.lhs, indent+1);
		msg_debug(PSTR(") >>= %lu"), t->data.steps.rhs);
		break;
	case BCStepUnstable_c:
		msg_debug(PSTR("("));
		tasktree_print(t->data.stepu.lhs, indent+1);
		msg_debug(PSTR(") >>~ %lu"), t->data.stepu.rhs);
		break;
	case BCSeqStable_c:
		msg_debug(PSTR("("));
		tasktree_print(t->data.seqs.lhs, indent+1);
		msg_debug(PSTR(") >>| "));
		tasktree_print(t->data.seqs.rhs, indent+1);
		msg_debug(PSTR(")"));
		break;
	case BCSeqUnstable_c:
		msg_debug(PSTR("("));
		tasktree_print(t->data.sequ.lhs, indent+1);
		msg_debug(PSTR(") >>. ("));
		tasktree_print(t->data.sequ.rhs, indent+1);
		msg_debug(PSTR(")"));
		break;
	case BCTOr_c:
		msg_debug(PSTR("("));
		tasktree_print(t->data.tor.lhs, indent+1);
		msg_debug(PSTR(") -||- ("));
		tasktree_print(t->data.tor.rhs, indent+1);
		msg_debug(PSTR(")"));
		break;
	case BCTAnd_c:
		msg_debug(PSTR("("));
		tasktree_print(t->data.tand.lhs, indent+1);
		msg_debug(PSTR(") -&&- ("));
		tasktree_print(t->data.tand.rhs, indent+1);
		msg_debug(PSTR(")"));
		break;
	case BCSdsSet_c:
		msg_debug(PSTR("setSDS %u"), t->data.sdsset.sds->bcs_ident);
		break;
	case BCSdsGet_c:
		msg_debug(PSTR("getSDS %u"), t->data.sdsget->bcs_ident);
		break;
	case BCRateLimit_c:
		msg_debug(PSTR("rateLimit ("));
		tasktree_print(t->data.ratelimit.task, indent+1);
		msg_debug(PSTR(")"));
		break;
	case BCSdsUpd_c:
		msg_debug(PSTR("updSDS %u"), t->data.sdsupd.sds->bcs_ident);
		break;
	default:
		break;
	}
#else /* LOGLEVEL == 2 */
	(void)t;
	(void)indent;
#endif /* LOGLEVEL == 2 */
}

void tasktree_print_node(struct TaskTree *t)
{
#if LOGLEVEL == 2
	switch (t->task_type) {
		case BCStable0_c:
		case BCStable1_c:
		case BCStable2_c:
		case BCStable3_c:
		case BCStable4_c:
			msg_debug(PSTR("Stable"));
			break;
		case BCStableNode_c:
			msg_debug(PSTR("StableNode %p"),
				t->data.stablenode.next);
			break;
		case BCUnstable0_c:
		case BCUnstable1_c:
		case BCUnstable2_c:
		case BCUnstable3_c:
		case BCUnstable4_c:
			msg_debug(PSTR("Unstable"));
			break;
		case BCUnstableNode_c:
			msg_debug(PSTR("UnstableNode %p"),
				t->data.unstablenode.next);
			break;
		case BCReadD_c:
			msg_debug(PSTR("Readd"));
			break;
		case BCWriteD_c:
			msg_debug(PSTR("Writed"));
			break;
		case BCReadA_c:
			msg_debug(PSTR("Reada"));
			break;
		case BCWriteA_c:
			msg_debug(PSTR("Writea"));
			break;
		case BCPinMode_c:
			msg_debug(PSTR("PinMode"));
			break;
		case BCInterrupt_c:
			msg_debug(PSTR("Interrupt"));
			break;
		case BCRepeat_c:
			msg_debug(PSTR("Repeat %p"), t->data.repeat.oldtree);
			break;
		case BCDelayUntil_c:
			msg_debug(PSTR("DelayUntil"));
			break;
		case BCDelay_c:
			msg_debug(PSTR("Delay"));
			break;
		case BCTAnd_c:
			msg_debug(PSTR("And %p %p"),
				t->data.tand.lhs, t->data.tand.rhs);
			break;
		case BCTOr_c:
			msg_debug(PSTR("Or %p %p"),
				t->data.tor.lhs, t->data.tor.rhs);
			break;
		case BCStep_c:
			msg_debug(PSTR("Step %p (%u, %u"),
				t->data.step.lhs, t->data.step.w,
				t->data.step.rhs);
			break;
		case BCStepStable_c:
			msg_debug(PSTR("StepStable %p (%u, %u)"),
				t->data.steps.lhs, t->data.steps.w,
				t->data.steps.rhs);
			break;
		case BCStepUnstable_c:
			msg_debug(PSTR("StepUnstable %p) (%u, %u)"),
				t->data.stepu.lhs, t->data.stepu.w,
				t->data.stepu.rhs);
			break;
		case BCSeqStable_c:
			msg_debug(PSTR("Seq stable %p %p"),
				t->data.seqs.lhs, t->data.seqs.rhs);
			break;
		case BCSeqUnstable_c:
			msg_debug(PSTR("Seq unstable %p %p"),
				t->data.sequ.lhs, t->data.sequ.rhs);
			break;
		case BCSdsGet_c:
			msg_debug(PSTR("Sdsget"));
			break;
		case BCSdsSet_c:
			msg_debug(PSTR("Sdsset"));
			break;
		case BCRateLimit_c:
			msg_debug(PSTR("RateLimit %p"), t->data.ratelimit.task);
			break;
		case BCSdsUpd_c:
			msg_debug(PSTR("Sdsupd"));
			break;
#ifdef HAVE_DHT
		case BCDHTTemp_c:
			msg_debug(PSTR("BCDhttemp"));
			break;
		case BCDHTHumid_c:
			msg_debug(PSTR("BCDhthumid"));
			break;
#endif /* HAVE_DHT */
#ifdef HAVE_AIRQUALITYSENSOR
		case BCSetEnvironmentalData_c:
			msg_debug(PSTR("BCSetEnvironmental"));
			break;
		case BCTVOC_c:
			msg_debug(PSTR("BCTVOC"));
			break;
		case BCCO2_c:
			msg_debug(PSTR("BCTCO2"));
			break;
#endif /* HAVE_AIRQUALITYSENSOR */
#ifdef HAVE_GESTURESENSOR
		case BCGesture_c:
			msg_debug(PSTR("BCGesture"));
			break;
#endif /* HAVE_GESTURESENSOR */
#ifdef HAVE_I2CBUTTON
		case BCAButton_c:
			msg_debug(PSTR("BCAButton"));
			break;
		case BCBButton_c:
			msg_debug(PSTR("BCBButton"));
			break;
#endif /* HAVE_I2CBUTTON */
#ifdef HAVE_LEDMATRIX
		case BCLEDMatrixDisplay_c:
			msg_debug(PSTR("BCLEDMatrixDisplay"));
			break;
		case BCLEDMatrixIntensity_c:
			msg_debug(PSTR("BCLEDMatrixIntensity %u"),
				t->data.ledmatrixintensity.intensity);
			break;
		case BCLEDMatrixDot_c:
			msg_debug(PSTR("BCLEDMatrixDot %u %u %u"),
				t->data.ledmatrixdot.x,
				t->data.ledmatrixdot.y,
				t->data.ledmatrixdot.s);
			break;
		case BCLEDMatrixClear_c:
			msg_debug(PSTR("BCLEDMatrixClear"));
			break;
#endif /* HAVE_LEDMATRIX */
		case BCEvent_c:
			msg_debug(PSTR("Event %u %u %u"),
				t->data.event.task_id,
				t->data.event.event_type,
				mem_rptr(t->data.event.next));
			break;
		default:
			break;
	}
#else /* LOGLEVEL == 2 */
	(void)t;
#endif /* LOGLEVEL == 2 */
}

struct TaskTree *tasktree_clone(struct TaskTree *treep, struct TaskTree *rptr)
{
	struct TaskTree *tree = treep;
	struct TaskTree *new = mem_alloc_tree();
	if (new == NULL)
		return NULL;
	*new = *tree;
	new->ptr.tree = rptr;
	new->seconds = tree->seconds;
	new->refresh_min = tree->refresh_min;
	new->refresh_max = tree->refresh_max;

//Safe clone
#define CLONE(to, source) {\
	to = tasktree_clone(source, new);\
	if (to == NULL)\
		return NULL;\
	}

	switch (tree->task_type) {
	case BCStableNode_c:
		CLONE(new->data.stablenode.next, tree->data.stablenode.next);
		break;
	case BCUnstableNode_c:
		CLONE(new->data.unstablenode.next,
			tree->data.unstablenode.next);
		break;
	case BCRepeat_c:
		CLONE(new->data.repeat.tree, tree->data.repeat.tree);
		CLONE(new->data.repeat.oldtree, tree->data.repeat.oldtree);
		break;
	case BCTAnd_c:
		CLONE(new->data.tand.lhs, tree->data.tand.lhs);
		CLONE(new->data.tand.rhs, tree->data.tand.rhs);
		break;
	case BCTOr_c:
		CLONE(new->data.tor.lhs, tree->data.tor.lhs);
		CLONE(new->data.tor.rhs, tree->data.tor.rhs);
		break;
	case BCStep_c:
		CLONE(new->data.step.lhs, tree->data.step.lhs);
		break;
	case BCStepStable_c:
		CLONE(new->data.steps.lhs, tree->data.steps.lhs);
		break;
	case BCStepUnstable_c:
		CLONE(new->data.stepu.lhs, tree->data.stepu.lhs);
		break;
	case BCSeqStable_c:
		CLONE(new->data.seqs.lhs, tree->data.seqs.lhs);
		CLONE(new->data.seqs.rhs, tree->data.seqs.rhs);
		break;
	case BCSeqUnstable_c:
		CLONE(new->data.sequ.lhs, tree->data.sequ.lhs);
		CLONE(new->data.sequ.rhs, tree->data.sequ.rhs);
		break;
	case BCSdsSet_c:
		CLONE(new->data.sdsset.data, tree->data.sdsset.data);
		break;
	case BCRateLimit_c:
		CLONE(new->data.ratelimit.task, tree->data.ratelimit.task);
		CLONE(new->data.ratelimit.storage,
			tree->data.ratelimit.storage);
		new->data.ratelimit.last_execution =
			tree->data.ratelimit.last_execution;
		break;
	case BCSdsUpd_c:
		CLONE(new->data.sdsupd.ctx, tree->data.sdsupd.ctx);
		break;
	case BCDelay_c:
		new->data.delay = tree->data.delay;
		break;
	case BCDelayUntil_c:
		new->data.until = tree->data.until;
		break;
	default:
		break;
	}
	return new;
}

struct TaskTree *create_result_task(uint16_t *sp, uint16_t *stack)
{
	if (stack == sp)
		return NULL;

	int left = stack - sp;
	struct TaskTree *t;
	struct TaskTree *next;

	// Select node type
	uint8_t node_type = BCUnstable0_c;
	node_type += mtmin(left, 5);

	t = mem_alloc_tree();
	if (t == NULL) {
		msg_log(PSTR("error during tree allocation, resetting\n"));
		mem_reset();
	}

	t->task_type = node_type;
	t->trash = false;

	uint8_t capacity = 0;
	if (node_type == BCUnstableNode_c) {
		t->data.unstablenode.next = NULL;
		t->data.unstablenode.w = left - 2;
		t->data.unstablenode.unstable[0] = *sp++;
		t->data.unstablenode.unstable[1] = *sp++;

		next = create_result_task(sp, stack);

		next->ptr.tree = t;
		t->data.unstablenode.next = next;
	} else {
		capacity = mtmin(left, 4);
		for (int s = 0; s < capacity; s++) {
			t->data.unstable[s] = *sp++;
		}
	}

	return t;
}
