/** @file
 *
 * Contains task and task list related functions
 */
#ifndef TASKTREE_H
#define TASKTREE_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>
#include <stdbool.h>

#include "types.h"
#include "interface.h"

/**
 * Tagged union containing all task tree nodes
 */
struct TaskTree {
	/** type of task (tag) */
	enum BCTaskType_c task_type;
	/** is trash? */
	bool trash:1;
	/** whether the refresh interval is milliseconds (false) or seconds
	 *  (true) */
	bool seconds:1;
	/** minimum of the refresh interval */
	uint16_t refresh_min;
	/** maximum of the refresh interval */
	uint16_t refresh_max;
	/** Pointer to the parent */
	bool ptr_is_tree:1;
	union task_or_tree_ptr {
		struct TaskTree *tree;
		struct MTaskMeta *task;
	} ptr;
	union {
// Constant node values {{{
		uint16_t stable[4];
		uint16_t unstable[4];
		struct {
			struct TaskTree *next;
			uint8_t w;
			uint16_t stable[2];
		} stablenode;
		struct {
			struct TaskTree *next;
			uint8_t w;
			uint16_t unstable[2];
		} unstablenode;
//}}}
//Pin IO {{{
		//Read pin
		uint16_t readd;
		uint16_t reada;
		struct {
			uint16_t pin;
			uint8_t pinmode;
		} pinmode;
		struct {
			//Pin to write
			uint16_t pin;
			//Value to write
			bool value;
		} writed;
		struct {
			//Pin to write
			uint16_t pin;
			//Value to write
			uint8_t value;
		} writea;
//}}}
//Interrupt {{{
		struct {
			//Pin
			uint16_t pin;
			//Interrupt mode
			enum InterruptMode mode;
			//Is the interrupt initialized
			bool initialized;
			//Triggered
			bool triggered;
			//Pin status
			bool status;
		} interrupt;
//}}}
//Repeat, delay and step {{{
		struct {
			//Pointer to the tree
			struct TaskTree *tree;
			//Pointer to the original tree
			struct TaskTree *oldtree;
			//Start time of the task
			uint32_t start;
			//Inner task is done
			bool done;
		} repeat;
		//Waiting time
		uint64_t delay;
		//Milliseconds since boot
		uint64_t until;
		struct {
			//Pointer to lhs
			struct TaskTree *lhs;
			//Pointer to function for the rhs
			uint16_t rhs;
			//Return size of rhs
			uint8_t w;
		} step;
		struct {
			//Pointer to lhs
			struct TaskTree *lhs;
			//Pointer to function for the rhs
			uint16_t rhs;
			//Return size of rhs
			uint8_t w;
		} steps;
		struct {
			//Pointer to lhs
			struct TaskTree *lhs;
			//Pointer to function for the rhs
			uint16_t rhs;
			//Return size of rhs
			uint8_t w;
		} stepu;
		struct {
			//Pointer to lhs
			struct TaskTree *lhs;
			//Pointer to rhs
			struct TaskTree *rhs;
			//Width of the rhs
			uint8_t w;
		} seqs;
		struct {
			//Pointer to lhs
			struct TaskTree *lhs;
			//Pointer to the rhs
			struct TaskTree *rhs;
			//Width of the rhs
			uint8_t w;
		} sequ;
//}}}
//Parallel {{{
		struct {
			//Pointer to lhs
			struct TaskTree *lhs;
			//Pointer to rhs
			struct TaskTree *rhs;
		} tand;
		struct {
			//Pointer to lhs
			struct TaskTree *lhs;
			//Pointer to rhs
			struct TaskTree *rhs;
		} tor;
//}}}
//Sds {{{
		//Pointer to the sds
		struct BCShareSpec *sdsget;
		struct {
			//Pointer to the sds
			struct BCShareSpec *sds;
			//Pointer to the data that we write
			struct TaskTree *data;
		} sdsset;
		struct {
			//Pointer to the function
			uint16_t fun;
			//Pointer to the context
			struct TaskTree *ctx;
			//Pointer to the actual sds
			struct BCShareSpec *sds;
		} sdsupd;
		struct {
			//Pointer to the sds
			struct BCShareSpec *sds;
			// Pointer to the reflected task
			struct TaskTree *task;
		} reflect;
//}}}
//Rate limit {{{
		struct {
			//Last execution
			uint32_t last_execution;
			//The ratelimited task
			struct TaskTree *task;
			//Storage of intermediate results
			struct TaskTree *storage;
		} ratelimit;
//}}}
//Peripherals {{{
		//The void * are the device specific states
		struct {
			struct BCPeripheral *info;
			union {
				uint8_t ledmatrixintensity;
				struct {
					uint8_t x;
					uint8_t y;
					bool s;
				} ledmatrixdot;
				struct {
					float humid;
					float temp;
				} setenvironment;
				struct {
					uint8_t n;
					uint8_t r;
					uint8_t g;
					uint8_t b;
				} neosetpixel;
			} data;
		} peripheral;
//}}}
//Events {{{
		struct {
			uint8_t event_type;
			uint8_t task_id;
			struct TaskTree *next;

		} event;
//}}}
	} data;
};

#define tasktree_assign_ptr(tt, t) do {\
	(tt)->ptr_is_tree = (t)->ptr_is_tree;\
	(tt)->ptr = (t)->ptr;\
} while (0)
#define tasktree_set_ptr_tree(tt, p) do {\
	(tt)->ptr_is_tree = true;\
	(tt)->ptr.tree = (p);\
} while (0)
#define tasktree_set_ptr_task(tt, p) do {\
	(tt)->ptr_is_tree = false;\
	(tt)->ptr.task = (p);\
} while (0)

/**
 * Print a task tree recursively for debugging purposes
 *
 * @param tt pointer to the task tree
 * @param indent indentation to print in
 */
void tasktree_print(struct TaskTree *tt, int indent);
/**
 * Print just the top level of a task tree for debugging purposes
 *
 * @param tt pointer to the task tree
 */
void tasktree_print_node(struct TaskTree *tt);
/**
 * Clone a task tree (deepcopy) and give it a parent
 *
 * @param tree pointer to the task tree
 * @param rptr pointer to the parent
 * @return cloned task tree
 */
struct TaskTree *tasktree_clone(struct TaskTree *tree, struct TaskTree *rptr);
/**
 * Destroy the task tree
 *
 * @param tt pointer to the task tree
 * @param t pointer to the task
 */
void destroy_task_node(struct TaskTree* tt, struct MTaskMeta *t);
/**
 * Create task tree containing the value on the stack
 *
 * @param todo cells to pop from the stack
 * @param stack start of the stack pointer
 * @return pointer to the created task tree
 */
struct TaskTree *create_result_task(uint16_t todo, uintptr_t *stack);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !TASKTREE_H */

