/** @file config_arduino.h
 *
 * This file contains all the configuration for various arduino compatibles.
 *
 * To add a device, the following must be done:
 *
 * ## In this file
 *
 * In the "Architecture" section, the architecture, the chip must be added.
 * In the "Variants" section, the specific device variant must be defined, i.e.
 *	which perpipherals are attached, how many pins are available, etc.
 *	N.B. MAX_INTERRUPT_PIN must be the highest pin for which an interrupt is
 *	supported, this should not be a macro (e.g. D3) but an integer. You can
 *	find the mapping from macro to pin number in the pins_arduino.h file
 *	located in:
 *	`~/.arduino15/packages/ARCH/hardware/ARCH/VERSION/variants/VARIANT`
 *
 * ## In arduino.cpp
 *
 * Some functionality may not be defined for your architecture/variant so you
 * need to go through there to add the device specific implementation.
 * Most importantly, when adding a new architecture, make sure to define the
 * dpins and apins arrays
 *
 * ## In link_arduino_wifi.cpp
 *
 * If using WiFi as a link layer, add the WiFi implementation for your
 * architecture and variant to this file. It is readily available for esp8266,
 * esp32 and feather m0.
 *
 * ## In arduino_interrupts.c
 *
 * This file defines all the available interrupt vectors. To save space, only
 * the available isrs are defined per architecture.
 * It also contains a mapping from pin number to isr.
 * Both the ISRs and the mapping can be generated using MakeISR from mTask/tools
 */
#ifndef ARDUINO_CONFIG_H
#define ARDUINO_CONFIG_H
#ifdef ARDUINO

#if defined(ARDUINO_ESP8266_NODEMCU_ESP12) && !defined(ARDUINO_ESP8266_NODEMCU)
#define ARDUINO_ESP8266_NODEMCU
#endif /* defined(ARDUINO_ESP8266_NODEMCU_ESP12)
	&& !defined(ARDUINO_ESP8266_NODEMCU) */

#ifdef ARDUINO_ARCH_esp8266
#undef ARDUINO_ARCH_ESP8266
#define ARDUINO_ARCH_ESP8266
#endif /* ARDUINO_ARCH_esp8266 */

/*
 * Architectures
 */
#if defined(ARDUINO_ARCH_AVR)
	#include <Arduino.h>
	#include <WString.h>
	#define MEMSIZE 1024
	#define TASK_QUEUE_SIZE 10
#elif defined(ARDUINO_ARCH_ESP8266) || defined (ARDUINO_ARCH_ESP32)\
		|| defined (ARDUINO_ARCH_esp8266)
	#include <pgmspace.h>

	#define MEMSIZE 16384
	#define TASK_QUEUE_SIZE 50
	#define REQUIRE_ALIGNED_MEMORY_ACCESS
#elif defined(ARDUINO_ARCH_SAMD)
	#include <Arduino.h>
	#define MEMSIZE 8384
	#define TASK_QUEUE_SIZE 50
	#define REQUIRE_ALIGNED_MEMORY_ACCESS
#else /* ARDUINO_ARCH_AVR */
	#error Unknown arduino architecture
#endif /* ARDUINO_ARCH_AVR */

/*
 * Variant
 */
#if defined(ARDUINO_AVR_UNO)

	#define APINS 6
	#define DPINS 14
	#define MAX_INTERRUPT_PIN 3
	#define HAVE_DHT
	#define HAVE_DHT_DHT

#elif defined(ARDUINO_ESP8266_NODEMCU)
	#define APINS 1
	#define DPINS 8
	#define MAX_INTERRUPT_PIN 15

	#define HAVE_DHT
	#define HAVE_DHT_DHT

#elif defined(ARDUINO_ESP8266_WEMOS_D1MINI)
	#define APINS 1
	#define DPINS 8
	#define MAX_INTERRUPT_PIN 15

	#define HAVE_LEDMATRIX
	#define HAVE_OLEDSHIELD
	#define HAVE_DHT
	#define HAVE_DHT_DHT
	#define HAVE_DHT_SHT
	#define HAVE_LIGHTSENSOR
	#define HAVE_AIRQUALITYSENSOR
	#define HAVE_AIRQUALITYSENSOR_SGP30
	#define HAVE_AIRQUALITYSENSOR_CCS811
	#define HAVE_GESTURESENSOR
	#define HAVE_NEOPIXEL
#elif defined(ARDUINO_LOLIN_D32_PRO)
	#define APINS 1
	#define DPINS 1

	#define MAX_INTERRUPT_PIN 0
#elif defined(ARDUINO_ESP32_DEV)
# ifdef ARDUINO_ESP32_FIREBEETLE32
	#define APINS 5
	#define DPINS 10
	#define MAX_INTERRUPT_PIN 27
# else /* ARDUINO_ESP32_FIREBEETLE32 */
	 #define APINS 1
	 #define DPINS 0
	 #define MAX_INTERRUPT_PIN 0

	 #define HAVE_OLEDSHIELD
# endif /* ARDUINO_ESP32_FIREBEETLE32 */
#elif defined(ADAFRUIT_FEATHER_M0)
	#define APINS 6
	#define DPINS 7
	#define MAX_INTERRUPT_PIN 19
#elif defined(ARDUINO_STAMP_PICO)
	#include <M5StickC.h>

	#define APINS 1
	#define DPINS 1

	#define MAX_INTERRUPT_PIN 0
#else /* ARDUINO_AVR_UNO */
	#error Unknown arduino device
#endif /* ARDUINO_AVR_UNO */

#if LOGLEVEL > 0 && defined (ARDUINO_ARCH_AVR)
#warning Are you sure you want to enable logging? This takes up a lot of space.
#endif /* LOGLEVEL > 0 && ARDUINO_ARCH_AVR */

#endif /* ARDUINO */
#endif /* !ARDUINO_CONFIG_H */
