#include <stdbool.h>

#include "interface.h"
#include "interrupts.h"
#include "events.h"
#include "mem.h"
#include "scheduler.h"
#include "tasktree.h"

static uint64_t enabled_interrupts[2] = { 0, 0 };
static volatile uint64_t triggered_interrupts[2] = { 0, 0 };

static volatile uint32_t pin_status = 0;

void real_isr(uint16_t pin, enum InterruptMode mode)
{
	switch (mode) {
		case Change_c: ;
			bool val = read_dpin(pin);
			pin_status = pin_status | (uint32_t) val << pin;
			break;
		case Rising_c:
			pin_status = pin_status | 1u << pin;
			break;
		case Falling_c:
			pin_status = pin_status | 0u << pin;
			break;
		case Low_c:
			pin_status = pin_status | 0u << pin;
			break;
		case High_c:
			pin_status = pin_status | 1u << pin;
			break;
	}

	SET_BIT(triggered_interrupts, pin, mode);

	force_wake_up();
}

void enable_interrupt(uint16_t pin, enum InterruptMode mode)
{
	msg_log(PSTR("enable %d interrupt on pin %d\n"), mode, pin);
	// Check if the interrupt was already enabled
	if (GET_BIT(enabled_interrupts, pin, mode) != 0) {
		msg_log(PSTR("was already enabled\n"));
		return;
	}

	// Check if there is another type of interrupt attached to this pin
	bool overlap = false;
	for (enum InterruptMode m = Change_c; m <= High_c; ++m) {
		if (m == mode) {
			continue;
		}

		if (GET_BIT(enabled_interrupts, pin, m) != 0) {
			msg_log(PSTR("detected interrupt overlap\n"));
			overlap = true;
			break;
		}
	}

	void (*isr)(void) = get_isr(pin, mode);

	if (isr == NULL || overlap) {
		send_message((struct MTMessageFro) {
			.cons = MTFException_c,
			.data = {
				.MTFException = {
					.cons = MTEUnsupportedInterrupt_c,
					.data = { .MTEUnsupportedInterrupt = {
							  .f0 = pin,
							  .f1 = mode }
						}
					}
				}
			});
		return;
	}

	attach_interrupt(isr, pin, mode);
	SET_BIT(enabled_interrupts, pin, mode)
}

void disable_interrupt(uint16_t pin, enum InterruptMode mode)
{
	msg_debug(PSTR("disable %d interrupt on pin %d\n"), mode, pin);
	// Check if it is safe to disable the interrupt
	struct TaskTree *e = event_list_head();
	uint16_t event_type = pin * 5 + mode;
	while (e != NULL) {
		if (e->data.event.event_type == event_type) {
			return;
		}
		e = event_list_next(e);
	}

	detach_interrupt(pin);
	CLEAR_BIT(enabled_interrupts, pin, mode)
}

bool pending_interrupts(void)
{
	return triggered_interrupts[0] != 0 || triggered_interrupts[1] != 0;
}

bool active_interrupts(void)
{
	return enabled_interrupts[0] != 0 || enabled_interrupts[1] != 0;
}

static bool store_in_task_tree(struct TaskTree *t, uint16_t pin,
			       enum InterruptMode mode)
{
	uintptr_t *stack = mem_stack();
	uint16_t sp = 0;
	push_stack(stack, sp, (uintptr_t)NULL);
	push_stack(stack, sp, (uintptr_t)t);
	while (stack[--sp] != (uintptr_t)NULL) {
		struct TaskTree *ctree = (struct TaskTree *)(*stack);
		switch (ctree->task_type) {
		case BCStep_c:
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.step.lhs);
			break;
		case BCStepStable_c:
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.steps.lhs);
			break;
		case BCStepUnstable_c:
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.stepu.lhs);
			break;
		case BCSeqStable_c:
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.seqs.lhs);
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.seqs.rhs);
			break;
		case BCSeqUnstable_c:
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.sequ.lhs);
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.sequ.rhs);
			break;
		case BCRepeat_c:
			if (ctree->data.repeat.tree != NULL)
				push_stack_rfalse(stack, sp,
					(uintptr_t)ctree->data.repeat.tree);
			break;
		case BCTOr_c:
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.tor.lhs);
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.tor.rhs);
			break;
		case BCTAnd_c:
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.tand.lhs);
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.tand.rhs);
			break;
		case BCSdsSet_c:
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.sdsset.data);
			break;
		case BCSdsUpd_c:
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.sdsupd.ctx);
			break;
		case BCReflect_c:
			push_stack_rfalse(stack, sp,
				(uintptr_t)ctree->data.reflect.task);
			break;
		case BCInterrupt_c:
			if (ctree->data.interrupt.mode == mode &&
				ctree->data.interrupt.pin == pin) {
				ctree->data.interrupt.triggered = true;
				ctree->data.interrupt.status =
					pin_status & 1u << pin;
			}
			break;
		case BCStable0_c:
			// fall through
		case BCStable1_c:
			// fall through
		case BCStable2_c:
			// fall through
		case BCStable3_c:
			// fall through
		case BCStable4_c:
			// fall through
		case BCStableNode_c:
			// fall through
		case BCUnstable0_c:
			// fall through
		case BCUnstable1_c:
			// fall through
		case BCUnstable2_c:
			// fall through
		case BCUnstable3_c:
			// fall through
		case BCUnstable4_c:
			// fall through
		case BCUnstableNode_c:
			// fall through
		case BCReadD_c:
			// fall through
		case BCWriteD_c:
			// fall through
		case BCReadA_c:
			// fall through
		case BCWriteA_c:
			// fall through
		case BCPinMode_c:
			// fall through
		case BCDelay_c:
			// fall through
		case BCDelayUntil_c:
			// fall through
		case BCSdsGet_c:
			// fall through
		case BCRateLimit_c:
			// fall through
		case BCDHTTemp_c:
			// fall through
		case BCDHTHumid_c:
			// fall through
		case BCLEDMatrixDisplay_c:
			// fall through
		case BCLEDMatrixIntensity_c:
			// fall through
		case BCLEDMatrixDot_c:
			// fall through
		case BCLEDMatrixClear_c:
			// fall through
		case BCGetLight_c:
			// fall through
		case BCSetEnvironmentalData_c:
			// fall through
		case BCTVOC_c:
			// fall through
		case BCCO2_c:
			// fall through
		case BCGesture_c:
			// fall through
		case BCNeoSetPixelColor_c:
			// fall through
		case BCEvent_c:
			break;
		}
	}
	return true;
}

static struct MTaskMeta *update_task(uint16_t id, uint16_t pin,
	enum InterruptMode mode)
{
	struct MTaskMeta *t = mem_task_head();
	while (t != NULL) {
		if (t->taskid != id) {
			t = mem_task_next(t);
			continue;
		}

		t->execution_max = 0;
		t->execution_min = 0;
		if (!store_in_task_tree(t->tree, pin, mode))
			return NULL;
		return t;
	}

	return NULL;
}

void process_interrupts(void)
{
	if (!pending_interrupts()) {
		return;
	}

	for (uint16_t pin = 0; pin <= MAX_INTERRUPT_PIN; pin++) {
		for (enum InterruptMode mode = Change_c; mode <= High_c;
				mode++) {
			if (GET_BIT(triggered_interrupts, pin, mode)) {
				CLEAR_BIT(triggered_interrupts, pin, mode)

				msg_debug(PSTR(
					"interrupt triggered on pin: %u"
					"with mode: %u\n"),
					pin, mode);

				struct TaskTree *event = event_list_head();

				while (event != NULL) {
					if (event->data.event.event_type !=
							pin * 5 + mode) {
						event = event_list_next(event);
						continue;
					}

					struct MTaskMeta *t = update_task(
						event->data.event.task_id, pin,
						mode);
					if (t == NULL) {
						die(PSTR(
							"error updating task"));
					}

					queue_remove_task(
						event->data.event.task_id);
					queue_insert_task(t);

					remove_event(event);
					event = event_list_next(event);
				}

				disable_interrupt(pin, mode);
			}
		}
	}
}
