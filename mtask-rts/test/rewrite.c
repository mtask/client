#include <check.h>
#include "../mem.h"
#include "../rewrite.h"

#define ck_assert_rewrite(t, p, stack, answer, len) do {\
	bool _removed = false;\
	uint16_t _rsp = rewrite((t), (p), &(stack)[0], &_removed);\
	ck_assert_int_ne(_rsp, 0);\
	ck_assert_int_eq(_rsp, (len));\
	ck_assert_mem_eq((stack), (answer), (len)*sizeof(uintptr_t));\
} while(0);

/**
 * Test whether a stable task pushes the correct task value on the step after a
 * rewrite operation.
 */
START_TEST (test_stable)
{
	mem_reset();
	struct TaskTree t =
		(struct TaskTree)
		{ .task_type=BCStable1_c
		, .trash=false
		, .seconds=false
		, .refresh_min=0
		, .refresh_max=99
		, .ptr_is_tree=false
		, .ptr={.task=NULL}
		, .data={.stable={1, 2, 3, 4}}
		};
	enum BCTaskType_c sts[] = {BCStable0_c, BCStable1_c
		, BCStable2_c, BCStable3_c, BCStable4_c};
	enum BCTaskType_c uts[] = {BCUnstable0_c, BCUnstable1_c
		, BCUnstable2_c, BCUnstable3_c, BCUnstable4_c};

	for (int i = 0; i<=4; i++) {
		uintptr_t *stack = mem_stack();
		uintptr_t answer[10] = {MTStable_c, 1, 2, 3, 4};
		uint8_t program[10] = {0};
		t.task_type = sts[i];
		ck_assert_rewrite(&t, &program[0], stack, answer, i+1);

		t.task_type = uts[i];
		answer[0] = MTUnstable_c;
		ck_assert_rewrite(&t, &program[0], stack, answer, i+1);
	}
}
END_TEST

Suite *test_rewrite()
{
	Suite *s1 = suite_create("test rewrite.h");
	TCase *tc2 = tcase_create("Stable");
	suite_add_tcase(s1, tc2);
	tcase_add_test(tc2, test_stable);
	return s1;
}

