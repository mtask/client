#ifndef TEST_MEM_H
#define TEST_MEM_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <check.h>

Suite *test_mem(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !TEST_MEM_H */
