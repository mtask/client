#ifndef TEST_NVM_H
#define TEST_NVM_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <check.h>

Suite *test_nvm(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !TEST_NVM_H */
