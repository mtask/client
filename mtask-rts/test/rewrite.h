#ifndef TEST_REWRITE_H
#define TEST_REWRITE_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <check.h>

Suite *test_rewrite(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !TEST_MEM_H */
