#include <check.h>
#include "../nvm_interface.h"

#ifdef NVM
/**
 * Test that writing and reading bytes to the nvm is working
 */
START_TEST (test_nvm_open_close) {
	uint8_t data[] = {1, 0, 255, 42, 38, 128};
	nvm_open_write();
	for (long unsigned int i = 0; i<sizeof(data); i++) {
		nvm_put(data[i]);
	}
	nvm_close_write();

	ck_assert_int_eq(nvm_open_read(), true);
	for (long unsigned int i = 0; i<sizeof(data); i++) {
		ck_assert_int_eq(nvm_get(), data[i]);
	}
	nvm_close_read();
} END_TEST
#endif


Suite *test_nvm()
{
	Suite *s1 = suite_create("test nvm.h");
#ifdef NVM
	TCase *tc1 = tcase_create("writing nvm↔readeng nvm");
	tcase_add_test(tc1, test_nvm_open_close);
	suite_add_tcase(s1, tc1);
#endif
	return s1;
}
