#include <stdio.h>
#include <check.h>
#include "../mem.h"
#include "../tasktree.h"

extern void *mem_ptr(uint16_t i);
extern uint16_t mem_heap;
extern void mem_print_heap(void);

/**
 * Test wether mem_rptr and mem_ptr are inverses of eachother.
 */
START_TEST (test_mem_rptr_ptr_id) {
	uint16_t testvalues[] =
		{ 0, 1, 2, UINT16_MAX-2, UINT16_MAX-1, UINT16_MAX};
	for (unsigned long i = 0; i<sizeof(testvalues); i++) {
		ck_assert_int_eq(
			mem_rptr(mem_ptr(testvalues[i])),
			testvalues[i]);
	}
} END_TEST

/**
 * Test that the stack moves when task space is allocated.
 */
START_TEST (test_alloc_task_moves_sp) {
	mem_reset();
	uintptr_t *stack = mem_stack();
	mem_alloc_task(0);
	ck_assert_ptr_eq(stack, mem_stack());

	mem_alloc_task(42);
	ck_assert_int_eq(
		(ptrdiff_t)((uint8_t *)mem_stack()-(uint8_t *)stack), 42);
	mem_reset();
	ck_assert_ptr_eq(stack, mem_stack());
} END_TEST

/**
 * Test that the heap pointer moves when tasktrees are allocated.
 */
START_TEST (test_alloc_heap_moves_sp) {
	mem_reset();
	uintptr_t *heap = mem_max_sp();
	mem_alloc_tree();
	ck_assert_int_eq((ptrdiff_t)((uint8_t *)heap-(uint8_t *)mem_max_sp())
		, sizeof(struct TaskTree));
	mem_alloc_tree();
	ck_assert_int_eq((ptrdiff_t)((uint8_t *)heap-(uint8_t *)mem_max_sp())
		, 2*sizeof(struct TaskTree));
	mem_reset();
	ck_assert_ptr_eq(heap, mem_max_sp());
} END_TEST

/**
 * Test that the rptr of a tasktree is restored when performing a move operation
 * in the heap.
 */
START_TEST (test_move_tasktree_restores_rptr) {
	mem_reset();
	struct TaskTree *t0 = mem_alloc_tree();
	memset(t0, 0, sizeof(struct TaskTree));
	tasktree_set_ptr_tree(t0, (void *)42);

	struct TaskTree *t1 = mem_alloc_tree();
	memset(t1, 0, sizeof(struct TaskTree));
	tasktree_set_ptr_tree(t1, (void *)38);

	ck_assert_msg(t0->ptr_is_tree, "t0 ptr is tree");
	ck_assert_msg(t1->ptr_is_tree, "t1 ptr is tree");

	mem_node_move(t0, t1);
	ck_assert_msg(t0->ptr_is_tree, "t0 ptr is tree");
	ck_assert_msg(t1->ptr_is_tree, "t1 ptr is tree");
	ck_assert_msg(!t0->trash, "to is not trash");
	ck_assert_msg(t1->trash, "from is trash now");
	// ptr.tree is NULL now
	ck_assert_ptr_null(t1->ptr.tree);

	// ptr.tree points to the original parent
	ck_assert_int_eq((uintptr_t)t0->ptr.tree, 42);
}

/**
 * Test that the child pointers of a tasktree are updated when a tasktree is
 * moved, i.e. the children's parent pointers point to the new location.
 */
START_TEST (test_move_tasktree_updates_child_pointers) {
	mem_reset();
	struct TaskTree *t0 = mem_alloc_tree();
	memset(t0, 0, sizeof(struct TaskTree));

	struct TaskTree *t1 = mem_alloc_tree();
	memset(t1, 0, sizeof(struct TaskTree));
	t1->task_type = BCStableNode_c;

	struct TaskTree *t2 = mem_alloc_tree();
	memset(t2, 0, sizeof(struct TaskTree));
	tasktree_set_ptr_tree(t2, t1);
	t2->task_type = BCStable0_c;

	t1->data.stablenode.next = t2;
	t1->data.stablenode.w = 2;

	ck_assert_ptr_eq(t2->ptr.tree, t1);
	mem_node_move(t0, t1);
	ck_assert_ptr_eq(t2->ptr.tree, t0);
}

Suite *test_mem()
{
	Suite *s1 = suite_create("test mem.h");
	TCase *tc1 = tcase_create("ptr↔rptr");
	tcase_add_test(tc1, test_mem_rptr_ptr_id);
	suite_add_tcase(s1, tc1);

	TCase *tc2 = tcase_create("alloc_moves_task_sp");
	tcase_add_test(tc2, test_alloc_task_moves_sp);
	suite_add_tcase(s1, tc2);

	TCase *tc3 = tcase_create("alloc_moves_tree_sp");
	tcase_add_test(tc3, test_alloc_heap_moves_sp);
	suite_add_tcase(s1, tc3);

	TCase *tc4 = tcase_create("move_tasktree_restores_rptr");
	tcase_add_test(tc4, test_move_tasktree_restores_rptr);
	suite_add_tcase(s1, tc4);

	TCase *tc5 = tcase_create("move_tasktree_updates_child_pointers");
	tcase_add_test(tc5, test_move_tasktree_updates_child_pointers);
	suite_add_tcase(s1, tc5);
	return s1;
}
