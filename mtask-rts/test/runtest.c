#include <check.h>

#include "rewrite.h"
#include "mem.h"
#include "nvm.h"

int main(int argc, char **argv)
{
	Suite *s1 = suite_create("Core");

	SRunner *sr = srunner_create(s1);

	srunner_add_suite(sr, test_rewrite());
	srunner_add_suite(sr, test_mem());
	srunner_add_suite(sr, test_nvm());

	srunner_run_all(sr, CK_ENV);
	int nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return nf == 0 ? 0 : 1;
	(void)argc;
	(void)argv;
}
