/** @file
 *
 * Contains the low-level non-volatile memory interface.
 */
#ifndef NVM_INTERFACE_H
#define NVM_INTERFACE_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifdef NVM
#include <stdint.h>
#include <stdbool.h>

/**
 * Open the nvm for reading
 *
 * @result success. May fail if there are only invalid swapfiles.
 */
bool nvm_open_read(void);
/**
 * Open the nvm for writing
 */
void nvm_open_write(void);

/**
 * Fetch a byte from the nvm. First open the nvm using \see nvm_open_read
 *
 * @result byte read
 */
uint8_t nvm_get(void);

/**
 * Write a byte to the nvm. First open the nvm using \see nvm_open_write
 *
 * @param b byte to write.
 */
void nvm_put(uint8_t b);

/**
 * Close the nvm.
 */
void nvm_close_read(void);
/**
 * Close the nvm.
 */
void nvm_close_write(void);
/**
 * Invalidate the nvm storage (because we didn't restore from it)
 */
void nvm_invalidate(void);

#endif /* NVM */

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !NVM_INTERFACE_H */
