/** @file client.h
 * Contains the highest level general purpose client functions
 */
#ifndef CLIENT_H
#define CLIENT_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * Export loop function so that arduino's can call it in in their loop()
 */
void real_loop(void);

/**
 * Export main function so that arduino's can call it in in their setup() and
 * pc's in their main()
 */
void real_main(void);

/**
 * Export yield function so that components can call it during things that take
 * a lot of time.
 */
void real_yield(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !CLIENT_H */

