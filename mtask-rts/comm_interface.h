/** @file comm_interface.h
 *
 * The communication interface defines the communication means on the message
 * level. It uses link_interface.h to delegate the actual communication work.
 * comm_interface.h link interface functions are called.
 *
 * Currently only direct communication (using serial or TCP) or mqtt
 * communication (using TCP) is supported via the following configuration
 * options:
 *
 * - COMM_DIRECT to use direct communication. They are configured using:
 *   - PING set the default time between pings
 * - COMM_MQTT to use MQTT communication using the wolfmqtt library. They are
 *   configure using:
 *   - KEEP_ALIVE_TIME to set the keepalive time
 *   - WILL_DELAY_INTERVAL to set the will delay interval
 *   - CLIENT_ID to set the client identifier, if set to zero a random ID will
 *     be generated
 */
#ifndef COMM_INTERFACE_H
#define COMM_INTERFACE_H
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdbool.h>
#include <stdint.h>

#include "interface.h"
#include "types.h"
#include "link_interface.h"

//Sanity checks for the configuration macros
#if defined(COMM_DIRECT) && defined(COMM_MQTT)
# error Multiple communication methods defined
#elif defined(COMM_DIRECT)
# ifndef COMM_PING
#  error No ping timeout is defined. Set COMM_PING
# endif /* COMM_PING */
#elif defined(COMM_MQTT)
# ifndef LINK_HOST
#  error No hostname set. Set LINK_HOST
# endif /* !LINK_HOST */
# ifndef KEEP_ALIVE_TIME
#  error No mqtt keepalive time set. Set KEEP_ALIVE_TIME
# endif /* !KEEP_ALIVE_TIME */
# ifndef WILL_DELAY_INTERVAL
#  error No mqtt will delay interval set. Set WILL_DELAY_INTERVAL
# endif /* !WILL_DELAY_INTERVAL */
# ifndef CLIENT_ID
#  warn No mqtt client id set. Assume NULL
#  define CLIENT_ID NULL
# endif /* !CLIENT_ID */
#else /* COMM_DIRECT */
# error No communication mode is set, either set COMM_DIRECT or COMM_MQTT
#endif /* !COMM_MQTT && !COMM_DIRECT */

/**
 * Test whether there is input available on the channel
 *
 * @return input available
 */
bool input_available(void);
/**
 * Receive a message on the input channel
 *
 * @return message
 */
struct MTMessageTo receive_message(void);
/**
 * Send a message on the output channel
 *
 * @param msg the message
 */
void send_message(struct MTMessageFro msg);
/**
 * Start the communication with the given settings. This will also set the
 * settings globally.
 *
 * @param resume flag to denote resumption (restore from nvm)
 * @return resumption successful
 */
bool start_communication(bool resume);
/**
 * Stop the communication
 *
 * @param temporary flag to denote whether it should only be stopped temporarily
 */
void stop_communication(bool temporary);
/**
 * Cleanup after an error in the communication
 */
void cleanup_communication(void);
/**
 * Communication yield function, use this to send pings or maintain a connection
 * with a server by sending keepalive messages.
 */
void communication_yield(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* !COMM_INTERFACE_H */

