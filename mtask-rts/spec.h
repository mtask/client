/** @file spec.h
 *
 * Contains all the functions to work with device specifications
 */
#ifndef SPEC_H
#define SPEC_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/**
 * Send the specification to the server
 */
void spec_send(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !SPEC_H */

