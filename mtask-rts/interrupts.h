/** @file interrupts.h
 *
 * Contains the interface to mTask's interrupt registration.
 */
#ifndef INTERRUPTS_H_
#define INTERRUPTS_H_

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#include <stdint.h>
#include "types.h"

/** get the interrupt mask for a given pin and mode */
#define GET_MASK(pin, mode) ((uint64_t)1 << (pin * 5 + mode))
/** set the mask for a pin and mode */
#define SET_BIT(mask, pin, mode)\
	if (pin < 12) {\
		mask[0] |= GET_MASK(pin, mode);\
	} else {\
		mask[1] |= GET_MASK((pin - 12), mode);\
	}
/** clear the mask for a pin and mode */
#define CLEAR_BIT(mask, pin, mode)\
	if (pin < 12) {\
		mask[0] &= ~GET_MASK(pin, mode);\
	} else {\
		mask[1] &= ~GET_MASK((pin - 12), mode);\
	}
/** get the mask for a pin and mode */
#define GET_BIT(mask, pin, mode)\
	((pin < 12) ? mask[0] & GET_MASK(pin, mode) :\
		mask[1] & GET_MASK((pin - 12), mode))

/**
 * The actual interrupt service routine (isr) used.
 * This is called from the registered isr
 *
 * @param pin decoded pin
 * @param mode interrupt mode
 */
void real_isr(uint16_t pin, enum InterruptMode mode);

/**
 * Enable an interrupt on a pin
 *
 * @param pin decoded pin
 * @param mode interrupt mode
 * @throws MTEUnsupportedInterrupt_c if the interrupt is not supported
 */
void enable_interrupt(uint16_t pin, enum InterruptMode mode);
/**
 * Disables an interrupt on a pin
 *
 * @param pin decoded pin
 * @param mode interrupt mode
 */
void disable_interrupt(uint16_t pin, enum InterruptMode mode);
/**
 * Handle the registered interrupts. I.e. update the corresponding task trees
 * and refresh intervals
 */
void process_interrupts();
/**
 * Check if there are pending interrupts.
 */
bool pending_interrupts();
/**
 * Check if there are any interrupts active.
 */
bool active_interrupts();

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* INTERRUPTS_H_ */
