/** @file pc.h
 *
 * Contains some interfacing functions for the PC
 */
#ifndef PC_H
#define PC_H

#include <setjmp.h>
#include "pc/pgmspace.h"

#define APINS 6
#define DPINS 14
#define MAX_INTERRUPT_PIN 14
#define MEMSIZE 50024
#define TASK_QUEUE_SIZE 255
#define HAVE_DHT
#define HAVE_LEDMATRIX
#define HAVE_LIGHTSENSOR
#define HAVE_I2CBUTTON
#define HAVE_AIRQUALITYSENSOR
#define HAVE_GESTURESENSOR

extern int gargc;
extern char **gargv;
extern jmp_buf fpe;
extern uint16_t default_port;

/**
 * The actual handler for the signals
 *
 * @param signo signal number
 */
void killHandler(int signo);

/**
 * Macro containing the code to be called when a SIGFPE has occurred
 */
#define RECOVERABLE_ERROR_CODE {\
	/*Restore point for SIGFPE exceptions*/\
	if (setjmp(fpe) == 1) {\
		send_message((struct MTMessageFro)\
			{ .cons=MTFException_c\
			, .data={.MTFException=\
				{.cons=MTEFPException_c\
				,.data={current_task->taskid}}\
				}\
			});\
		msg_log(PSTR("Caught SIGFPE, resetting\n"));\
		mem_reset();\
	}\
}

/**
 * Exit with the system's error message
 *
 * @param msg message prefix
 */
void pdie(const char *msg);

#endif /* !PC_H */

