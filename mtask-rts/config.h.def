/**@file config.h.def
 *
 * This file contains all the configuration options. Copy it to config.h and
 * adapt it to your needs.
 */
#ifndef CONFIG_H
#define CONFIG_H

/** Define the loglevel (0=none, 1=info, 2=debug) */
#define LOGLEVEL 1

/** Enable if you want to trace memory after GC and allocation */
//#define TRACE_MEM

/** Maximum time to sleep */
//#define MAX_SLEEP_TIME 4294967u
#define MAX_SLEEP_TIME 50u

/** Default time to sleep when there is something to do */
#define DEFAULT_SLEEP_TIME 5u

/******************************************************************************
 * Sane automagic arduino defaults
 *****************************************************************************/
#ifdef ARDUINO
# if defined(ARDUINO_ARCH_AVR)
#  define LINK_SERIAL
#  ifndef BAUDRATE
#   define BAUDRATE 9600
#  endif /* BAUDRATE */
#  ifndef COMM_DIRECT
#   define COMM_DIRECT
#  endif
# elif defined(ARDUINO_ARCH_ESP8266) || defined (ARDUINO_ARCH_ESP32)\
	|| defined (ARDUINO_ARCH_esp8266) || defined(ARDUINO_ARCH_SAMD)
#  if !defined(LINK_TCP) && !defined(LINK_SERIAL)
#   define LINK_TCP
#  endif
#  if !defined(COMM_DIRECT) && !defined(COMM_MQTT)
#   define COMM_DIRECT
#  endif
# else /* ARDUINO_ARCH_AVR */
#  error Unknown arduino architecture
# endif /* ARDUINO_ARCH_AVR */
#endif

/******************************************************************************
 * LINK METHOD
 *****************************************************************************/
/* TCP with arduino WIFI*/
//#define LINK_TCP
#ifdef ARDUINO
/** Array of SSIDS */
#define SSIDS {"darknet"}
/** Array of corresponding passwords */
#define WPAS  {"hunter2"}
#endif /* ARDUINO */

/* TCP on PC */
//#define LINK_TCP
#ifdef LINK_TCP
/** Port to listen to */
#define LINK_PORT 8123
#endif /* LINK_TCP */

/* Serial */
//#define LINK_SERIAL
#if defined(ARDUINO)
/** Baudrate, make sure it is an allowed one */
#define BAUDRATE 9600
#endif /* LINK_SERIAL */

/******************************************************************************
 * COMMUNICATION METHOD
 *****************************************************************************/
/* Direct communication */
//#define COMM_DIRECT
#ifdef COMM_DIRECT
/** Time between pings */
#define COMM_PING 0
#endif /* COMM_DIRECT */

/* MQTT communication */
#ifdef COMM_MQTT
/** The default link port is changed to mosquitto's default port */
#undef LINK_PORT
#define LINK_PORT 1883
/** The hostname to connect to */
#define LINK_HOST "mqtt.example.org"
/** Start with a clean session */
#define CLEAN_SESSION false
/** Keepalive time in seconds */
#define KEEP_ALIVE_TIME 60
/** Name of the client, if NULL, a random name is generated */
#define CLIENT_ID NULL
#endif /* COMM_MQTT */

#endif /* CONFIG_H */
