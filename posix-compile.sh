#!/usr/bin/env bash
set -e
PDCURSESURL=https://sourceforge.net/projects/pdcurses/files/pdcurses/3.9/PDCurses-3.9.zip

MAKEFLAGS=${MAKEFLAGS:--j}

if [ $# -eq 1 ]
then
	if [ $1 = "-f" ]
	then
		FORCE=true
		MAKEFLAGS="$MAKEFLAGS -B"
	else
		echo "Usage: $0 [-f]"
		exit 1
	fi
fi

cd mtask-rts
cp config.h{.def,}
cp preloaded_tasks.h{.def,}

# Build the regular rts, first with clang to test, then with cc
if [[ "$(uname)" = "Linux" ]]
then
	make CC=clang $MAKEFLAGS -B
fi
make $MAKEFLAGS -B
mv client mtask-client-tty

# Build the curses rts
if [[ "$(uname)" = "Linux" ]]
then
	make CC=clang $MAKEFLAGS CURSES_INTERFACE=yes -B
fi
make $MAKEFLAGS CURSES_INTERFACE=yes -B
mv client mtask-client-curses

# Build the xcurses rts
if [ ! -d PDCurses-3.9 ]
then
	curl --retry 3 -LSso PDCurses.zip $PDCURSESURL
	unzip -q PDCurses.zip
	rm PDCurses.zip
fi

(
	cd PDCurses-3.9/x11
	if [ -n "$FORCE" -o ! -f Makefile ]
	then
		./configure
	fi
	make $MAKEFLAGS
	make install || echo "Couldn't install PDCurses, not super user?"
)
make $MAKEFLAGS CURSES_INTERFACE=pdcurses -B
mv client mtask-client-xcurses

# Build the mqtt rts
(
	cd ../dependencies/
	bash prepare_wolf.sh
)
if [[ "$(uname)" = "Linux" ]]
then
	make CC=clang $MAKEFLAGS NVM=yes MQTT=yes -B
	make CC=clang $MAKEFLAGS MQTT=yes -B
fi
make $MAKEFLAGS NVM=yes MQTT=yes -B
make $MAKEFLAGS MQTT=yes -B
mv client mtask-client-mqtt
