#!/usr/bin/env bash
set -e
version="$(grep -Po "(?<=^version: ).*" nitrile.yml)"
version_minor=$(<<< $version cut -d'.' -f 2)
code="struct SemVer client_version = {${version%%.*}, ${version_minor}, ${version##*.}}"
grep -q "$code" -F mtask-rts/client.c
