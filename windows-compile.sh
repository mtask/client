#!/usr/bin/env bash
set -e
PDCURSESURL=https://sourceforge.net/projects/pdcurses/files/pdcurses/3.9/PDCurses-3.9.zip

MAKEFLAGS=${MAKEFLAGS:--j}
MAKEFLAGS="$MAKEFLAGS DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc"

if [ $# -eq 1 ]
then
	if [ $1 = "-f" ]
	then
		FORCE=true
		MAKEFLAGS="$MAKEFLAGS -B"
	else
		echo "Usage: $0 [-f]"
		exit 1
	fi
fi

cd mtask-rts
cp config.h{.def,}
cp preloaded_tasks.h{.def,}

# Build the regular rts
make $MAKEFLAGS -B
mv client.exe mtask-client-cmd.exe

# Build the xcurses rts
if [ ! -d PDCurses-3.9 ]
then
	curl --retry 3 -LSso PDCurses.zip $PDCURSESURL
	unzip -q PDCurses.zip
	rm PDCurses.zip
fi
make $MAKEFLAGS -C PDCurses-3.9/wincon CC=x86_64-w64-mingw32-gcc
make $MAKEFLAGS CURSES_INTERFACE=pdcurses -B
mv client.exe mtask-client-curses.exe

# Build the mqtt rts (skip for now)
(
	cd ../dependencies
	bash prepare_wolf.sh win
)
make $MAKEFLAGS MQTT=yes -B
mv client.exe mtask-client-mqtt.exe
