#!/bin/bash
set -e
PDCURSESURL=https://sourceforge.net/projects/pdcurses/files/pdcurses/3.9/PDCurses-3.9.zip

MAKEFLAGS=${MAKEFLAGS:--j}
MAKEFLAGS="$MAKEFLAGS DETECTED_OS=Windows CC=x86_64-w64-mingw32-gcc"

if [ $# -eq 1 ]
then
	if [ $1 = "-f" ]
	then
		FORCE=true
		MAKEFLAGS="$MAKEFLAGS -B"
	else
		echo "Usage: $0 [-f]"
		exit 1
	fi
fi

cd mtask-rts
cp config.h{.def,}
cp preloaded_tasks.h{.def,}

# Build the regular rts
make $MAKEFLAGS -f Makefile.pc -B
mv client{,-cmd}.exe

# Build the xcurses rts
if [ ! -d PDCurses-3.9 ]
then
	curl -LSso PDCurses.zip $PDCURSESURL
	unzip -q PDCurses.zip
	rm PDCurses.zip
fi
make $MAKEFLAGS -C PDCurses-3.9/wincon CC=x86_64-w64-mingw32-gcc
make $MAKEFLAGS CURSES_INTERFACE=pdcurses -f Makefile.pc -B
mv client{,-curses}.exe

# Build the mqtt rts
(
	cd ../dependencies/WolfMQTT
	cp ../vs_settings.h wolfmqtt
	if [ ! -f configure ]
	then
		./autogen.sh
	fi
	if [ -n "$FORCE" -o ! -f Makefile ]
	then
		./configure --enable-mqtt5 --disable-tls --host=x86_64-pc-mingw32 --disable-examples --enable-static CFLAGS=-Wno-error=attributes LIBS=-lws2_32 CC=x86_64-w64-mingw32-gcc
	fi
	make $MAKEFLAGS

	cd wolfmqtt
	cp options.h.in options.h
	cd ../IDE/ARDUINO
	./wolfmqtt-arduino.sh
)
make $MAKEFLAGS MQTT=yes -f Makefile.pc -B
mv client{,-mqtt}.exe
