#!/usr/bin/env bash
set -e

BASENAME=mtask-client-sources-$(grep "^version" nitrile.yml | cut -d' ' -f2)

TARGET=${TARGET:-$BASENAME}

rm -Irf "$TARGET"
mkdir -p "$TARGET"/mtask-rts{,-dependencies}

cp mtask-rts/{Makefile*,*.{c,h,cpp,ino}} "$TARGET"/mtask-rts

for dep in \
		Adafruit-GFX-Library\
		Adafruit_BusIO\
		Adafruit_NeoPixel\
		Adafruit_SGP30\
		Adafruit_SSD1306\
		DHTNEW\
		Gesture_PAJ7620\
		LOLIN_OLED_I2C_Button_Library\
		SparkFun_CCS811_Arduino_Library\
		WEMOS_Matrix_LED_Shield_Arduino_Library\
		WEMOS_SHT3x_Arduino_Library
do
	cp -R dependencies/$dep "$TARGET"/mtask-rts-dependencies
done

#Special libraries
mkdir "$TARGET"/mtask-rts-dependencies/{DHTlib,WolfMQTT}

cp -R dependencies/WolfMQTT/LICENSE "$TARGET"/mtask-rts-dependencies/WolfMQTT/
cp -R dependencies/WolfMQTT/IDE/ARDUINO/wolfMQTT "$TARGET"/mtask-rts-dependencies/WolfMQTT/

tar -czf $BASENAME.tar.gz "$TARGET"
