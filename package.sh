#!/bin/bash
set -e

if [ $# -eq 1 ]
then
	if [ $1 = "-w" ]
	then
		WINDOWS=true
	elif [ $1 = "-l" ]
	then
		WINDOWS=
	else
		echo "Usage: $0 [-l|-w]"
		exit 1
	fi
fi


TARGET=${TARGET:-mtask-client}

rm -Irf "$TARGET"
mkdir -p "$TARGET"/mtask-rts{,-dependencies}

cp mtask-rts/{Makefile*,client-*,*.{c,h,cpp,ino}} "$TARGET"/mtask-rts

for dep in \
		Adafruit_BusIO\
		Adafruit-GFX-Library\
		Adafruit_SSD1306\
		DHTNEW\
		Gesture_PAJ7620\
		LOLIN_OLED_I2C_Button_Library\
		SparkFun_CCS811_Arduino_Library\
		WEMOS_Matrix_LED_Shield_Arduino_Library\
		WEMOS_SHT3x_Arduino_Library
do
	cp -R dependencies/$dep "$TARGET"/mtask-rts-dependencies
done

#Special libraries
mkdir "$TARGET"/mtask-rts-dependencies/{DHTlib,WolfMQTT}

cp -R dependencies/WolfMQTT/LICENSE "$TARGET"/mtask-rts-dependencies/WolfMQTT/
cp -R dependencies/WolfMQTT/IDE/ARDUINO/wolfMQTT "$TARGET"/mtask-rts-dependencies/WolfMQTT/

#Package
if [ -z $WINDOWS ]
then
	tar -czf mtask-client-linux-x64.tar.gz "$TARGET"
else
	zip -r mtask-client-windows-x64.zip "$TARGET"
fi
