module MakeTypes

import StdEnv
import Data.GenType
import Data.GenType.CCode
import Data.Either
import Data.VoidPointer
import System.CommandLine
import System.FilePath

import Data.UInt
import mTask.Interpret.Instructions
import mTask.Interpret.Message
import mTask.Interpret.Peripheral
import mTask.Interpret.String255
import mTask.SemVer

derive gType BCInstrs, BCInstr, JumpLabel, BCTaskType, PinMode, I2CAddr, InterruptMode, Gesture
derive gType LEDMatrixInfo, DHTInfo, Pin, DHTtype, APin, DPin, MTaskValueState, BCPeripheral, MTaskEvalStatus, BCPeripheralInfo
derive gType MTDeviceSpec, MTMessageFro, MTException, TaskValue, MTMessageTo, MTaskMeta, BCShareSpec, MTaskPrepData
derive gType AirQualitySensorInfo, GestureSensorInfo, LightSensorInfo
derive gType NeoInfo, NeoPixelFreq, NeoPixelType

// This function makes some types always pointers (this makes parsing easier)
forceMTaskPointer :: ![Type] ![[Type]] -> [[Type]]
forceMTaskPointer acc [[t]:ts]
	| isMember (typeName t) ["MTaskMeta", "MTMessageTo"]
		= forceMTaskPointer [t:acc] ts
forceMTaskPointer acc [t:ts] = [t:forceMTaskPointer acc ts]
forceMTaskPointer acc [] = [acc]

writeToFile :: ![String] !String !*World -> *World
writeToFile data fp w
	# (ok, f, w) = fopen fp FWriteText w
	| not ok = abort ("Couldn't open " +++ fp +++ " for writing\n")
	# f = foldl (flip fwrites) f data
	# (ok, w) = fclose f w
	| not ok = abort ("Couldn't close " +++ fp +++ "\n")
	= w

die :: !String !*World -> *World
die msg w = snd (fclose (stderr <<< msg <<< "\n") w)

currentVersionCCode :: [String]
currentVersionCCode = [
	"#define SERVER_VERSION_MAJOR ",
	toString (CURRENT_SERVER_VERSION.major),
	"\n#define SERVER_VERSION_MINOR ",
	toString (CURRENT_SERVER_VERSION.minor),
	"\n#define SERVER_VERSION_PATCH ",
	toString (CURRENT_SERVER_VERSION.patch),
	"\nstruct SemVer server_version = ",
	"{SERVER_VERSION_MAJOR, SERVER_VERSION_MINOR, SERVER_VERSION_PATCH};\n"]

Start :: !*World -> *World
Start w
	# ([argv0:args], w) = getCommandLine w
	# (io, w) = stdio w
	| not (args =: [_])
		= die "Please supply the target directory as the only argument" w
	# td = hd args
	= case generateCCode opts types of
		Left e = die ("Error generating c code: " +++ e) w
		Right (h, c)
			# w = writeToFile h (td </> "types.h") w
			# io = io <<< "Header file written at: " <<< (td </> "types.h") <<< "\n"
			# w = writeToFile (c ++ currentVersionCCode) (td </> "types.c") w
			# io = io <<< "C file written at: " <<< (td </> "types.c") <<< "\n"
			= snd (fclose (io <<< "Done\n") w)
where
	opts :: GenCCodeOptions
	opts = { zero
		& basename = "types"
		, customDeps = forceMTaskPointer []
		, overrides =
			[ uint8gType, uint16gType, uint32gType
			, int8gType, int16gType, int32gType
			, string255gType, bcinstrsgType, voidPointergType
			]
		}

	// The extra types are required as they are always pushed
	types :: Box GType (MTMessageTo, MTMessageFro, (InterruptMode, PinMode, Gesture))
//	types :: Box GType (T Int)
	types = gType{|*|}
