# Changelog

#### Unreleased

- Add nvm support for PC (with mqtt)

#### 0.4.3

- Support server version 0.6.

#### 0.4.2

- Add version to specification.
- Cleanup curses interface
- Fix drawing on too small terminals
- Fix forms in curses
- Fix window resizing in curses
- Add Neopixel to curses

#### 0.4.1

- Fix MQTT code (many buffer overflows).
- Fix NeoPixel destroy thing in setup on Arduino.
- Rewrite many functions using the stack such as the rewriting engine to check
  for stack overflows.
- Add `--except`/`-x` flag to the test suite to exclude certain suites.

### 0.4

- Fix sds retrieving from identifier.
- Change: Add `bool link_is_open(void)` to the link interface.
- Fix SIGPIPE handling on PC and accidental communication yields when there was
  no connection set up yet. It is handled by ignoring the signal and checking
  the return code of `write`.
- Make pointers to parents safer.
- Make stackcells of pointerwidth instead of uint16_t to save a lot of
  conversions.
- Add testing framework (`libcheck`).
- Improved debugging prints by doing it earlier, more compact and more general.
- Add stackoverflow checking in other places (e.g. `mem.c`) using stacks as
  well.

#### 0.3.2

- Fix bug in DHT temperature sensor.

#### 0.3.1

- Fix overflow in SGP30 air quality sensor readings.
- Fix bug in temperature sensors.

### 0.3

- Support new peripheral layout (from server)
- Support SGP30 airquality sensor
- Support Neopixel

#### 0.2.1

- Make compatible with clang and macos
- Fix buffer overflow in title generation of curses interface
- Reflect interface options in --version (on linux)
- Add script to fetch the corresponding sources
- Fix some error messages
- Fix curses interface on linux being unresponsive while waiting for connection
- Fix curses interface not showing the errors

### 0.2

- Initial specification sending instead of waiting for a message
- Generate `types.[ch]` in CI

#### 0.1.2

- Remove temperature drift on desktop so that tests pass

#### 0.1.1

- Fix bug with sdsUpd

### 0.1

- Initial nitrile version
