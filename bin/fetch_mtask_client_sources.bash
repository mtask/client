#!/usr/bin/env bash
set -e

usage () {
	echo "Usage: $0 [-q] [-o DIR] [-v VERSION | -c CLIENT]"
	echo "       $0 [-h]"
	echo ""
	echo "Options:"
	echo "	-h         show this help"
	echo "	-v VERSION download VERSION instead of the detected version"
	echo "	-c CLIENT  detect client version from CLIENT binary"
	echo "	-o DIR     directory to extract (default: ./mtask-client-sources-VERSION)"
}

VERSION=
CLIENT=
ODIR=
while getopts "hv:c:o:" o
do
	case "$o" in
		h)
			usage
			exit 0
			;;
		c)
			if [[ -n "$VERSION" ]]
			then
				echo "Cannot use -v in conjunction with -c" >&2
				exit 1
			fi
			CLIENT="$OPTARG"
			;;
		v)
			if [[ -n "$CLIENT" ]]
			then
				echo "Cannot use -v in conjunction with -c" >&2
				exit 1
			fi
			VERSION="$OPTARG"
			;;
		o)
			ODIR="$OPTARG"
			;;
		*)
			usage >&2
			exit 1
			;;
	esac
done

if [[ -z "$VERSION" ]]
then
	CLIENT="${CLIENT:-mtask-client-tty}"
	echo "version not specified, detecting using $CLIENT"
	VERSION="$("$CLIENT" --version | grep "mtask client version: .*" | cut -f4 -d' ')"
fi

ODIR="${ODIR:-mtask-client-sources-$VERSION}"

package_id="$(curl -sSL "https://gitlab.com/api/v4/projects/mtask%2Fclient/packages?package_name=mtask-client-sources" | jq ".[] | select (.version == \"$VERSION\") | .id")"
if [[ -z "$package_id" ]]
then
	echo "Cannot find package for version $VERSION" >&2
	exit 1
fi
echo "package id: $package_id"
file_id="$(curl -sSL "https://gitlab.com/api/v4/projects/mtask%2Fclient/packages/$package_id/package_files" | jq '.[0].id')"
echo "file id: $file_id"

mkdir -p "$ODIR"
curl -sSL "https://gitlab.com/mtask/client/-/package_files/$file_id/download" | tar -xz --strip-components=1 -C "$ODIR"
echo "mtask client sources downloaded to $ODIR"
