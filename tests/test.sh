#!/usr/bin/env bash
if [ $# -lt 1 ]
then
	echo "please provide the client to test as the first argument" >&2
	exit 1
fi
set -e
client="$1"
shift
"$client" 2>err.log 1>out.log &
pid="$!"
sleep 1
trap "kill $pid || true" EXIT
timeout 30m tests/TestSuite "${@}"
sleep 1
