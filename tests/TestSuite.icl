module TestSuite

import Data.Real
import iTasks.Internal.Task
import iTasks.Internal.Util
import StdEnv

import Data.Func
import Data.Functor
import Data.GenHash
import Data.List => qualified group
import Data.Tuple
import Control.GenBimap
import qualified Data.Map
import System.GetOpt
import Text
import Text.GenPrint
import Text.GenParse

import iTasks.Extensions.MQTT

import mTask.Language
import mTask.Interpret
import mTask.Interpret.Devices

import iTasks
import iTasks.Extensions.DateTime

import Gast => qualified label, >=., >., <., <=.

derive gPrint MTD, MQTTSettings, TCPSettings, TTYSettings, MQTTAuth, Parity, ByteSize, BaudRate, Port
derive gParse MTD, MQTTSettings, TCPSettings, TTYSettings, MQTTAuth, Parity, ByteSize, BaudRate, Port

derive class Gast Button, APin, DPin, Gesture, Pin, TaskValue
derive gPrint Long, UInt8, UInt16, UInt32, Int8, Int16, Int32
derive genShow Long, UInt8, UInt16, UInt32, Int8, Int16, Int32

gmm mn mx = [!mn, mx, one, ~one, zero:[!inc mn..dec (dec mx)]]

ggen{|Long|}   s = [!LONG_MAX, LONG_MIN, zero, one, ~one:[!zero..dec LONG_MAX]]
ggen{|UInt8|}  s = gmm UINT8_MIN UINT8_MAX
ggen{|UInt16|} s = gmm UINT16_MIN UINT16_MAX
ggen{|UInt32|} s = gmm UINT32_MIN UINT32_MAX
ggen{|Int8|}   s = gmm INT8_MIN INT8_MAX
ggen{|Int16|}  s = gmm INT16_MIN INT16_MAX
ggen{|Int32|}  s = gmm INT32_MIN INT32_MAX

int16s :: [Int]
int16s = [i\\(Int16 i)<|-ggen{|*|} genState]

realeq :: Real Real -> Bool
realeq a b
	| a == b = True
	= abs (a-b) <= max (abs a) (abs b) * eps

realprop :: Real Real -> Property
realprop x y = check realeq x y

reals :== [0.001, -0.001, 1.23E10, -1.23E10, 1.23E20, -1.23E20:map fromInt ints]
ints  :== [0,1,-1,42,-42,100,-100]
longs :== map Long [20000, -20000:ints]

eps :== 1.19E-07

/*
 * Parametrically test an mTask
 *
 * @param Name of the test
 * @param arguments
 * @param expected result
 * @param function producing the task
 * @param device
 * @result name and pass/fail
 */
//testTask` :: (b b -> Bool) combinator String a b (a -> Main (BCInterpret (TaskValue b))) MTDevice -> Task (String, Bool) | type b & gText{|*|} a
testTask` p c s args expected_result task dev
	=   c (liftmTask (task args) dev)
	\r->if (p r expected_result)
		(treturn True)
		(traceValue (concat
			[ "Test ", s, " failed for "
			, toSingleLineText args
			, " expected "
			, toSingleLineText expected_result
			, " but got "
			, toSingleLineText r]) >-| treturn False)

testiTask s t p c expected_result = c t \r->if (p r expected_result)
	(treturn True)
	(traceValue (concat
		[ "Test ", s, " failed"
		, " expected "
		, toSingleLineText expected_result
		, " but got "
		, toSingleLineText r]) >-| treturn False)

testTask   = testTask` (==) (>>-)
testrTask  = testTask` realeq (>>-)
testuTask  = testTask` (==) (>>~)
testruTask = testTask` realeq (>>~)

ntests_default :== 20

take` num slist = take num [s\\s<|-slist]

Start :: !*World -> *World
Start w = doTasksWithOptions (withDefaultEngineCLIOptions eopts ppos task usage) w
where
	updUserOpt :: String ((?[String]) -> [String]) EngineOptions -> EngineOptions
	updUserOpt k v eo = {eo & userOpts='Data.Map'.put k (v $ 'Data.Map'.get k eo.userOpts) eo.userOpts}

	addUserOpt :: String [String] -> (EngineOptions -> EngineOptions)
	addUserOpt k v = updUserOpt k \_->v

	eopts :: [OptDescr ((?EngineOptions) -> ?EngineOptions)]
	eopts =
		[ Option ['n'] ["ntest"] (ReqArg (\a->fmap $ addUserOpt "ntests" [a]) "NTEST")
			"Specify the maximum number of testcases per test"
		, Option ['d'] ["device"] (ReqArg (\a->fmap $ addUserOpt "device" [a]) "DEVICESPEC") $ concat
			[ "Specify how to connect to the SUT. Examples:\n"
			, "\n"
			, printToString (TCP {host="192.168.1.1",port=Port 8123,pingTimeout= ?None}), "\n"
			, printToString (Serial zero), "\n"
			, printToString (MQTT {MQTTSettings | host="localhost", port=Port 1883, mcuId="mcu", serverId="mTask-server", auth=NoAuth}), "\n"
			]
		, Option ['l'] ["list"] (NoArg $ fmap $ addUserOpt "list" [])
			"List the available testsuites"
		, Option ['x'] ["except"] (ReqArg (\a->fmap $ updUserOpt "except" (\mx->[a:fromMaybe [] mx])) "SUITE")
			"Exclude certain suites from the tests"
		]

	ppos :: [String] -> MaybeError [String] ((?EngineOptions) -> ?EngineOptions)
	ppos [] = Ok id
	ppos pos = Ok $ fmap $ addUserOpt "selectedTests" pos

	usage :: String -> String
	usage argv0 = concat3 "Usage: " argv0 " [OPTS] [SUITE1 [SUITE2 ...]]"

	task :: EngineOptions -> MaybeError [String] StartableTask
	task eo
		# ntests = case 'Data.Map'.get "ntests" eo.userOpts of
			?None = Ok ntests_default
			?Just s = case parseString $ hd s of
				?None = Error ["ntests must be an integer"]
				?Just i = Ok i
		| isError ntests = liftError ntests
		# ntests = fromOk ntests
		# device = case 'Data.Map'.get "device" eo.userOpts of
			?None = Ok dev
			?Just s = case parseString $ hd s of
				?None = Error ["device must be a valid device spec"]
				?Just d = Ok d
		| isError device = liftError device
		# device = fromOk device
		# selectedTests = case 'Data.Map'.get "selectedTests" eo.userOpts of
			?None = map fst (tests ntests)
			?Just s = s
		| 'Data.Map'.member "list" eo.userOpts
			= Ok $ onStartup $ traceValue $ join "\n" $ map summarySuite $ tests ntests
		# missing = filter (\name->isNothing $ find ((==)name o fst) $ tests ntests) selectedTests
		| not (isEmpty missing)
			= Error ["Some testsuites name aren't known: ":intersperse "," missing]
		# excludedTests = case 'Data.Map'.get "except" eo.userOpts of
			?None = []
			?Just s = s
		# missing = filter (\name->not (isMember name selectedTests)) excludedTests
		# selectedTests = [t \\ t <- selectedTests | not (isMember t excludedTests)]
		| not (isEmpty missing)
			= Error ["Some excluded testsuites weren't included or known: ":intersperse "," missing]
		= Ok $ onStartup $ withDevice` False device (\dev->
				test selectedTests ntests dev
			>>- \verdict->traceValue ("verdict: " +++ toString verdict)
			>-| waitForTimer False 1
			>-| treturn verdict
			) >>- \verdict->shutDown (if verdict 0 1)
	where
		dev = TCP {TCPSettings|host="localhost", port=Port 8123, pingTimeout= ?None}
		summarySuite (name, ts) = concat
			[ name, " with "
			, toString $ length ts, " tests and "
			, toString $ sum [length t\\(_, t)<-ts], " cases"
			]

		test :: [String] Int MTDevice -> Task Bool
		test suites ntests dev
			| suites =: [] = traceValue "Nothing to do" >-| return True
			= sequence (map (testSuite dev) toTest) @ or
		where
			toTest = [test\\test=:(name, _)<-tests ntests | isMember name suites]

		testSuite :: MTDevice (String, [(String, [MTDevice -> Task Bool])]) -> Task Bool
		testSuite dev (suiteName, tests)
			=   traceValue ("Running testSuite " +++ suiteName)
			>-| sequence (map (testTest dev) tests)
			>>- \r->traceValue r
			@ and

		testTest :: MTDevice (String, [MTDevice -> Task Bool]) -> Task Bool
		testTest dev (testName, cases)
			=   traceValue ("\ttest: " +++ testName)
			>-| sequence (map (flip id dev) cases)
			@ and

		testCase :: MTDevice (MTDevice -> Task Bool) -> Task Bool
		testCase dev testcase = testcase dev

	tests :: Int -> [(String, [(String, [MTDevice -> Task Bool])])]
	tests ntests =
		[("stable", stableTests)
		,("multiTaskTests", multiTests)
		,("unstable", unstableTests)
		,("boolarith", boolarithTests)
		,("casts", castsTests)
		,("comparisons", comparisonsTests)
		,("intArith", intArith)
		,("longArith", longArith)
		,("realArith", realArith)
		,("conditionalTests", conditionalTests)
		,("functionTests", functionTests)
		,("parallelTests", parallelTests)
		,("stepTests", stepTests)
		,("rpeatTests", rpeatTests)
		,("rateLimitTests", rateLimitTests)
		,("stepPredicates", stepPredicates)
		,("stepContexti", stepContexts 42)
		,("stepContextl", stepContexts (Long 42))
		,("sdsTests", sdsTests)
		,("sdsLiftTests", sdsLiftTests)
		,("sdsReflectTests", sdsReflectTests)
		,("peripheralTests", peripheralTests)
		]
	where
		stableTests = 
			[ ("ret()", [testTask "ret()" () () \()->{main=rtrn (lit ())}])
			, ("reti",  [testTask "reti" i i \i->{main=rtrn (lit i)}\\i<-take` ntests int16s])
			, ("retl",  [testTask "retl" i i \i->{main=rtrn (lit i)}\\i<-tl [Long 0:take` ntests (ggen{|*|} genState)]])
			, ("retb",  [testTask "retb" i i \i->{main=rtrn (lit i)}\\i<-[True, False]])
			, ("retr",  [testrTask "retr" i i \i->{main=rtrn (lit i)}\\i<-reals])
			, ("rettuple",
				[testTask "irett" (1,2) (1,2) \i->{main=rtrn (lit i)}
				,testTask "irettt" (1,(2,3)) (1,(2,3)) \i->{main=rtrn (lit i)}
				,testTask "irettt" ((1,2),3) ((1,2),3) \i->{main=rtrn (lit i)}
				,testTask "iretttt" ((1,2),(3,4)) ((1,2),(3,4)) \i->{main=rtrn (lit i)}
				,testTask "lrett" (Long 1,Long 2) (Long 1,Long 2) \i->{main=rtrn (lit i)}
				,testTask "lrettt" (Long 1,(Long 2,Long 3)) (Long 1,(Long 2,Long 3)) \i->{main=rtrn (lit i)}
				,testTask "lrettt" ((Long 1,Long 2),Long 3) ((Long 1,Long 2),Long 3) \i->{main=rtrn (lit i)}
				,testTask "lretttt" ((Long 1,Long 2),(Long 3,Long 4)) ((Long 1,Long 2),(Long 3,Long 4)) \i->{main=rtrn (lit i)}
				])
			]

		unstableTests =
			[ ("unsi", [testuTask "unsi" i i \i->{main=unstable (lit i)}\\i<-take` ntests int16s])
			, ("unsl", [testuTask "unsl" i i \i->{main=unstable (lit i)}\\i<-tl [Long 0:take` ntests (ggen{|*|} genState)]])
			, ("unsb", [testuTask "unsb" i i \i->{main=unstable (lit i)}\\i<-[True, False]])
			, ("unsr", [testruTask "unsr" i i \i->{main=unstable (lit i)}\\i<-reals])
			, ("unstuple",
				[ testuTask "iunst" (1,2) (1,2) \i->{main=unstable (lit i)}
				, testuTask "iunstt" (1,(2,3)) (1,(2,3)) \i->{main=unstable (lit i)}
				, testuTask "iunstt" ((1,2),3) ((1,2),3) \i->{main=unstable (lit i)}
				, testuTask "iunsttt" ((1,2),(3,4)) ((1,2),(3,4)) \i->{main=unstable (lit i)}
				, testuTask "lunst" (Long 1,Long 2) (Long 1,Long 2) \i->{main=unstable (lit i)}
				, testuTask "lunstt" (Long 1,(Long 2,Long 3)) (Long 1,(Long 2,Long 3)) \i->{main=unstable (lit i)}
				, testuTask "lunstt" ((Long 1,Long 2),Long 3) ((Long 1,Long 2),Long 3) \i->{main=unstable (lit i)}
				, testuTask "lunsttt" ((Long 1,Long 2),(Long 3,Long 4)) ((Long 1,Long 2),(Long 3,Long 4)) \i->{main=unstable (lit i)}
				])
			]
		
		boolarithTests =
			[ ("not", [testTask "not" i (not i) (\i->{main=rtrn (Not (lit i))})\\i<-take` ntests (ggen{|*|} genState)])
			, ("&&",  [testTask "&&" (i,j) (i && j) (\(i,j)->{main=rtrn (lit i &. lit j)})\\(i,j)<-take` ntests (ggen{|*|} genState)])
			, ("||",  [testTask "||" (i,j) (i || j) (\(i,j)->{main=rtrn (lit i |. lit j)})\\(i,j)<-take` ntests (ggen{|*|} genState)])
			]

		castsTests =
			[ ("i2i", [testTask "i2i" i (toInt i)  (\i->{main=rtrn (int (lit i))})\\i<-ints])
			, ("r2i", [testTask "r2i" i (toInt i)  (\i->{main=rtrn (int (lit i))})\\i<-[-1.0, 1.0, -1.1, 1.1, 0.0, -100.0, -100.1, 100.0, 100.1]])
			, ("l2i", [testTask "l2i" i (toInt i)  (\i->{main=rtrn (int (lit i))})\\i<-[1,-1,0,100,105]])
			, ("i2r", [testrTask "i2r" i (toReal i) (\i->{main=rtrn (real (lit i))})\\i<-ints])
			, ("r2r", [testrTask "r2r" i (toReal i) (\i->{main=rtrn (real (lit i))})\\i<-[-1.0, 1.0, -1.1, 1.1, 0.0, -100.0, -100.1, 100.0, 100.1]])
			, ("l2r", [testrTask "l2r" i (toReal i) (\i->{main=rtrn (real (lit i))})\\i<-[1,-1,0,100,105]])
			]
			
		comparisonsTests =
			[ ("i>", [testTask "i>"  (i,j) (i > j)  (\(i,j)->{main=rtrn (lit i >.  lit j)})\\i<-ints, j<-ints])
			, ("i<", [testTask "i<"  (i,j) (i < j)  (\(i,j)->{main=rtrn (lit i <.  lit j)})\\i<-ints, j<-ints])
			, ("i>=",[testTask "i>=" (i,j) (i >= j) (\(i,j)->{main=rtrn (lit i >=. lit j)})\\i<-ints, j<-ints])
			, ("i<=",[testTask "i<=" (i,j) (i <= j) (\(i,j)->{main=rtrn (lit i <=. lit j)})\\i<-ints, j<-ints])
			, ("l>", [testTask "l>"  (i,j) (i > j)  (\(i,j)->{main=rtrn (lit i >.  lit j)})\\i<-longs, j<-longs])
			, ("l<", [testTask "l<"  (i,j) (i < j)  (\(i,j)->{main=rtrn (lit i <.  lit j)})\\i<-longs, j<-longs])
			, ("l>=",[testTask "l>=" (i,j) (i >= j) (\(i,j)->{main=rtrn (lit i >=. lit j)})\\i<-longs, j<-longs])
			, ("l<=",[testTask "l<=" (i,j) (i <= j) (\(i,j)->{main=rtrn (lit i <=. lit j)})\\i<-longs, j<-longs])
			, ("r>", [testTask "r>"  (i,j) (i > j)  (\(i,j)->{main=rtrn (lit i >.  lit j)})\\i<-reals, j<-reals])
			, ("r<", [testTask "r<"  (i,j) (i < j)  (\(i,j)->{main=rtrn (lit i <.  lit j)})\\i<-reals, j<-reals])
			, ("r>=",[testTask "r>=" (i,j) (i >= j) (\(i,j)->{main=rtrn (lit i >=. lit j)})\\i<-reals, j<-reals])
			, ("r<=",[testTask "r<=" (i,j) (i <= j) (\(i,j)->{main=rtrn (lit i <=. lit j)})\\i<-reals, j<-reals])
			]

//			//Overflow:(
		intArith =
			[ ("i+", [testTask "i+" (i,j) (i + j) (\(i,j)->{main=rtrn (lit i +. lit j)})\\i<-ints, j<-ints])
			, ("i-", [testTask "i-" (i,j) (i - j) (\(i,j)->{main=rtrn (lit i -. lit j)})\\i<-ints, j<-ints])
			, ("i*", [testTask "i*" (i,j) (i * j) (\(i,j)->{main=rtrn (lit i *. lit j)})\\i<-ints, j<-ints])
			, ("i/", [testTask "i/" (i,j) (i / j) (\(i,j)->{main=rtrn (lit i /. lit j)})\\i<-ints, j<-filter ((<>)0) ints])
			]

		longArith =
			[ ("l+", [testTask "l+" (i,j) (i + j) (\(i,j)->{main=rtrn (lit i +. lit j)})\\i<-longs, j<-longs])
			, ("l-", [testTask "l-" (i,j) (i - j) (\(i,j)->{main=rtrn (lit i -. lit j)})\\i<-longs, j<-longs])
			, ("l*", [testTask "l*" (i,j) (i * j) (\(i,j)->{main=rtrn (lit i *. lit j)})\\i<-longs, j<-longs])
			, ("l/", [testTask "l/" (i,j) (i / j) (\(i,j)->{main=rtrn (lit i /. lit j)})\\i<-longs, j<-filter ((<>)(Long 0)) longs])
			]

		realArith =
			[ ("r+", [testrTask "r+" (i,j) (i + j) (\(i,j)->{main=rtrn (lit i +. lit j)})\\i<-reals, j<-reals])
			, ("r-", [testrTask "r-" (i,j) (i - j) (\(i,j)->{main=rtrn (lit i -. lit j)})\\i<-reals, j<-reals])
			, ("r*", [testrTask "r*" (i,j) (i * j) (\(i,j)->{main=rtrn (lit i *. lit j)})\\i<-reals, j<-reals])
			, ("r/", [testrTask "r/" (i,j) (i / j) (\(i,j)->{main=rtrn (lit i /. lit j)})\\i<-reals, j<-filter ((<>)0.0) reals])
			]

		conditionalTests =
			[ ("if1", [testTask "if" (i, b) (if b i 0) (\(i,b)->{main=rtrn (If (lit b) (lit i) (lit 0))})\\i<-ints, b<-[True, False]])
			, ("if2", [testTask "if" (i, b) (if b 0 i) (\(i,b)->{main=rtrn (If (lit b) (lit 0) (lit i))})\\i<-ints, b<-[True, False]])
			]

		functionTests =
			[ ("const",  [testTask "const" i i (\i->
					fun \const=(\()->lit i) In {main=rtrn (const ())})\\i<-ints])
			, ("constL", [testTask "constL" i i (\i->
					fun \const=(\()->lit i) In {main=rtrn (const ())})\\i<-longs])
			, ("constR", [testrTask "constR" i i (\i->
					fun \const=(\()->lit i) In {main=rtrn (const ())})\\i<-reals])
			, ("increment", [testTask "increment" i (i+1) (\i->
					fun \inc=(\i->i +. (lit 1)) In {main=rtrn (inc (lit i))})\\i<-ints])
			, ("incrementL", [testTask "incrementL" i (i+(Long 1)) (\i->
					fun \inc=(\i->i +. (lit (Long 1))) In {main=rtrn (inc (lit i))})\\i<-longs])
			, ("plus", [testTask "plus" (i,j) (i+j) (\(i,j)->
					fun \plus=(\(i,j)->i +. j) In {main=rtrn (plus (lit i, lit j))})\\i<-ints, j<-ints])
			, ("plusL", [testTask "plusL" (i,j) (i+j) \(i,j)->
					fun \plus=(\(i,j)->i +. j) In {main=rtrn (plus (lit i, lit j))}\\i<-longs, j<-longs])
			, ("plusR", [testrTask "plusR" (i,j) (i+j) \(i,j)->
					fun \plus=(\(i,j)->i +. j) In {main=rtrn (plus (lit i, lit j))}\\i<-reals, j<-reals])
			, ("countdownTl", [testTask "countdownTail" 1000 0 \i->
					fun \countdown=(\i->If (i ==. lit 0) (lit 0) (countdown (i -. lit 1)))
					In {main=rtrn (countdown (lit i))}])
			, ("countdownLTl", [testTask "countdownTailL" (Long 1000) (Long 0) \i->
					fun \countdown=(\i->If (i ==. lit zero) (lit zero) (countdown (i -. lit one)))
					In {main=rtrn (countdown (lit i))}])
			, ("fact", [testTask "factorial" 5 120 \i->
					fun \fac=(\i=If (i ==. lit zero) (lit one) (i *. fac (i -. lit one)))
					In {main=rtrn (fac (lit i))}])
			, ("factL", [testTask "factorialL" (Long 5) (Long 120) \i->
					fun \fac=(\i=If (i ==. lit zero) (lit one) (i *. fac (i -. lit one)))
					In {main=rtrn (fac (lit i))}])
			, ("factR", [testTask "factorialR" 5.0 120.0 \i->
					fun \fac=(\i=If (i ==. lit zero) (lit one) (i *. fac (i -. lit one)))
					In {main=rtrn (fac (lit i))}])
			, ("factTl", [testTask "factorialTl" 5 120 \i->
					fun \facacc=(\(n,a)->If (n ==. lit zero) a (facacc (n -. lit one, n *. a))) In
					fun \fac=(\i=facacc (i, lit one))
					In {main=rtrn (fac (lit i))}])
			, ("factLTl", [testTask "factorialTlL" (Long 5) (Long 120) \i->
					fun \facacc=(\(n,a)->If (n ==. lit zero) a (facacc (n -. lit one, n *. a))) In
					fun \fac=(\i=facacc (i, lit one))
					In {main=rtrn (fac (lit i))}])
			, ("factRTl", [testTask "factorialTl" 5.0 120.0 \i->
					fun \facacc=(\(n,a)->If (n ==. lit zero) a (facacc (n -. lit one, n *. a))) In
					fun \fac=(\i=facacc (i, lit one))
					In {main=rtrn (fac (lit i))}])
			]

		parallelTests = let (i, j) = (42, 43) in
			let (li, lj) = (Long 42, Long 43) in
			[ ("i&&", [testTask "i&&" (i,j) (i,j) (\(i,j)->{main=rtrn (lit i)     .&&. rtrn (lit j)})])
			, ("i||", [testTask "i||" (i,j)  i    (\(i,j)->{main=rtrn (lit i)     .||. rtrn (lit j)})])
			, ("i||ur", [testTask "i||" (i,j)  i    (\(i,j)->{main=rtrn (lit i)     .||. unstable (lit j)})])
			, ("i||ul", [testTask "i||" (i,j)  j    (\(i,j)->{main=unstable (lit i) .||. rtrn (lit j)})])
			, ("l&&", [testTask "l&&" (li,lj) (li,lj) (\(li,lj)->{main=rtrn (lit li)     .&&. rtrn (lit lj)})])
			, ("l&&", [testTask "l||" (li,lj)  li    (\(li,lj)->{main=rtrn (lit li)     .||. rtrn (lit lj)})])
			, ("l&&", [testTask "l||" (li,lj)  li    (\(li,lj)->{main=rtrn (lit li)     .||. unstable (lit lj)})])
			, ("l&&", [testTask "l||" (li,lj)  lj    (\(li,lj)->{main=unstable (lit li) .||. rtrn (lit lj)})])
			]

		stepTests = let i = 42 in let j = 34 in let li = Long 42 in
			[ ("i>>=", [testTask "i>>=" i i (\i->{main=rtrn (lit i) >>=. rtrn})])
			, ("i>>~", [testTask "i>>~" i i (\i->{main=rtrn (lit i) >>~. rtrn})])
			, ("i>>|", [testTask "i>>|" i i (\i->{main=rtrn (lit (i+one)) >>|. rtrn (lit i)})])
			, ("iu>>.",[testTask "iu>>." i i (\i->{main=unstable (lit (i+one)) >>.. rtrn (lit i)})])
			, ("l>>=", [testTask "l>>=" li li (\i->{main=rtrn (lit i) >>=. rtrn})])
			, ("l>>~", [testTask "l>>~" li li (\i->{main=rtrn (lit i) >>~. rtrn})])
			, ("l>>|", [testTask "l>>|" li li (\i->{main=rtrn (lit (i+one)) >>|. rtrn (lit i)})])
			, ("lu>>.",[testTask "lu>>." li li (\i->{main=unstable (lit (i+one)) >>.. rtrn (lit i)})])
			, ("t>>=", [testTask "t>>="  (i,j) (i,j) (\(i,j)->{main=rtrn (lit (i,j)) >>=. rtrn})])
			, ("t>>~", [testTask "t>>~"  (i,j) (i,j) (\(i,j)->{main=rtrn (lit (i,j)) >>~. rtrn})])
			, ("t>>|", [testTask "t>>|"  (i,j) (i,j) (\(i,j)->{main=rtrn (lit (i+one,j+one)) >>|. rtrn (lit (i,j))})])
			, ("tu>>.",[testTask "tu>>." (i,j) (i,j) (\(i,j)->{main=unstable (lit (i+one,j+one)) >>.. rtrn (lit (i,j))})])
			, ("if>>=", [testTask "if>>="  (i,j) i (\(i,j)->{main=rtrn (lit (i,j)) >>=. rtrn o first})])
			, ("if>>~", [testTask "if>>~"  (i,j) i (\(i,j)->{main=rtrn (lit (i,j)) >>~. rtrn o first})])
			, ("if>>|", [testTask "if>>|"  (i,j) i (\(i,j)->{main=rtrn (lit (i+one,j+one)) >>|. rtrn (first $ lit (i,j))})])
			, ("ifu>>.",[testTask "ifu>>." (i,j) i (\(i,j)->{main=unstable (lit (i+one,j+one)) >>.. rtrn (first $ lit (i,j))})])
			, ("is>>=", [testTask "is>>="  (i,j) j (\(i,j)->{main=rtrn (lit (i,j)) >>=. rtrn o second})])
			, ("is>>~", [testTask "is>>~"  (i,j) j (\(i,j)->{main=rtrn (lit (i,j)) >>~. rtrn o second})])
			, ("is>>|", [testTask "is>>|"  (i,j) j (\(i,j)->{main=rtrn (lit (i+one,j+one)) >>|. rtrn (second $ lit (i,j))})])
			, ("isu>>.",[testTask "isu>>." (i,j) j (\(i,j)->{main=unstable (lit (i+one,j+one)) >>.. rtrn (second $ lit (i,j))})])
			]

		rpeatTests = []
//			[ ("rpeat", [testuTask "rpeat" 42 42 \i->{main=rpeat (rtrn (lit i))}])
//			, ("rpeatustep", [testTask "rpeat" 42 42 \i->{main=rpeat (rtrn (lit i)) >>~. rtrn}])
//			, ("rpeatstep", [testTask "rpeat" 42 42 \i->{main=rpeat (rtrn (lit i)) >>*. [IfValue ((==.)(lit i)) rtrn]}])
//			]

		rateLimitTests = let i = 42 in
			[ ("rlreturn", [testuTask "rlreturn" i i (\i->sds \s1=i In {main=getSds` (ExactMs (lit 200)) s1})])
			]

		stepPredicates = let i = 42 in
			[ (">>*us", [testTask ">>*us" i i (\i->{main=rtrn (lit i) >>*. [IfUnstable (const true) (\i->rtrn (i +. lit 1)), IfStable (const true) rtrn]})])
			, (">>*aw", [testTask ">>*aw" i i (\i->{main=rtrn (lit (i+1)) >>*. [Always (rtrn (lit i))]})])
			, (">>*pgt", [testTask ">>*pgt" i i (\i->{main=rtrn (lit i) >>*. [IfValue (\i->i >. lit 1) rtrn, IfValue (\i->i <=. lit 1) (\i->rtrn (i +. lit 1))]})])
			, (">>*ple", [testTask ">>*ple" i i (\i->{main=rtrn (lit i) >>*. [IfValue (\i->i <=. lit 1) (\i->rtrn (i +. lit 1)), IfValue (\i->i >. lit 1) rtrn]})])
			, (">>*plt", [testTask ">>*plt" i (i+1) (\i->{main=rtrn (lit i) >>*. [IfValue (\i->i <. lit 1) rtrn, IfValue (\i->i >. lit 1) (\i->rtrn (i +. lit 1))]})])
			, (">>*pge", [testTask ">>*pge" i (i+1) (\i->{main=rtrn (lit i) >>*. [IfValue (\i->i >=. lit 1) (\i->rtrn (i +. lit 1)), IfValue (\i->i <=. lit 1) rtrn]})])
			]

		stepContexts i =
			[("ctx11>>=", [testTask "ctx11>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn x})])
			,("ctx21>>=", [testTask "ctx21>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx22>>=", [testTask "ctx22>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})])
			,("ctx22>(=", [testTask "ctx22>(=" i i (\i->{main=(rtrn (lit 0) >>=. \_->rtrn (lit i)) >>=. \x->rtrn x})])
			,("ctx31>>=", [testTask "ctx31>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx31>(=", [testTask "ctx31>(=" i i (\i->{main=rtrn (lit i) >>=. \x->(rtrn (lit 0) >>=. \_->rtrn (lit 0)) >>=. \_->rtrn x})])
			,("ctx31>(=", [testTask "ctx31>(=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->(rtrn (lit 0) >>=. \_->rtrn x)})])
			,("ctx31>(=", [testTask "ctx31>(=" i i (\i->{main=rtrn (lit i) >>=. \x->(rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x)})])
			,("ctx32>>=", [testTask "ctx32>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx32>>=", [testTask "ctx32>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->(rtrn (lit 0) >>=. \_->rtrn x)})])
			,("ctx32>>=", [testTask "ctx32>>=" i i (\i->{main=(rtrn (lit 0) >>=. \_->rtrn (lit i)) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx33>>=", [testTask "ctx33>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})])
			,("ctx41>>=", [testTask "ctx41>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx42>>=", [testTask "ctx42>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx43>>=", [testTask "ctx43>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx44>>=", [testTask "ctx44>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})])
			,("ctx51>>=", [testTask "ctx51>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx52>>=", [testTask "ctx52>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx53>>=", [testTask "ctx53>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx54>>=", [testTask "ctx54>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx55>>=", [testTask "ctx55>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})])
			,("ctx61>>=", [testTask "ctx61>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx62>>=", [testTask "ctx62>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx63>>=", [testTask "ctx63>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx64>>=", [testTask "ctx64>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx65>>=", [testTask "ctx65>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx66>>=", [testTask "ctx66>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})])
			,("ctx71>>=", [testTask "ctx71>>=" i i (\i->{main=rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx72>>=", [testTask "ctx72>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx73>>=", [testTask "ctx73>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx74>>=", [testTask "ctx74>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx75>>=", [testTask "ctx75>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx76>>=", [testTask "ctx76>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn (lit 0) >>=. \_->rtrn x})])
			,("ctx77>>=", [testTask "ctx77>>=" i i (\i->{main=rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit 0) >>=. \_->rtrn (lit i) >>=. \x->rtrn x})])
			]
//			//mixed steps and binds
//			//Functions
//			//TODO
//			//Shares

		sdsTests = let cShare i = mapWrite (\_ _-> ?None) ?None $ constShare i in
			[ ("sds1",   [testuTask "sds1"     i i (\i->sds \s1=i   In {main=getSds s1})\\i<-take ntests int16s])
			, ("sds21",  [testuTask "sds21"    i i (\i->sds \s1=i  In sds \s2=99 In {main=getSds s1})\\i<-take ntests int16s])
			, ("sds22",  [testuTask "sds22"    i i (\i->sds \s1=99 In sds \s2=i  In {main=getSds s2})\\i<-take ntests int16s])
			, ("sds31",  [testuTask "sds31"    i i (\i->sds \s1=i  In sds \s2=99 In sds \s3=99 In {main=getSds s1})\\i<-take ntests int16s])
			, ("sds32",  [testuTask "sds32"    i i (\i->sds \s1=99 In sds \s2=i  In sds \s3=99 In {main=getSds s2})\\i<-take ntests int16s])
			, ("sds33",  [testuTask "sds33"    i i (\i->sds \s1=99 In sds \s2=99 In sds \s3=i  In {main=getSds s3})\\i<-take ntests int16s])
			, ("lsds1",  [testuTask "lsds1"    i i (\i->lowerSds \s1=cShare i In {main=getSds s1})\\i<-take ntests int16s])
			, ("lsds21", [testuTask "lsds21"   i i (\i->lowerSds \s1=cShare i  In lowerSds \s2=cShare 99 In {main=getSds s1})\\i<-take ntests int16s])
			, ("lsds22", [testuTask "lsds22"   i i (\i->lowerSds \s1=cShare 99 In lowerSds \s2=cShare i  In {main=getSds s2})\\i<-take ntests int16s])
			, ("lsds31", [testuTask "lsds31"   i i (\i->lowerSds \s1=cShare i  In lowerSds \s2=cShare 99 In lowerSds \s3=cShare 99 In {main=getSds s1})\\i<-take ntests int16s])
			, ("lsds32", [testuTask "lsds32"   i i (\i->lowerSds \s1=cShare 99 In lowerSds \s2=cShare i  In lowerSds \s3=cShare 99 In {main=getSds s2})\\i<-take ntests int16s])
			, ("lsds34", [testuTask "lsds33"   i i (\i->lowerSds \s1=cShare 99 In lowerSds \s2=cShare 99 In lowerSds \s3=cShare i  In {main=getSds s3})\\i<-take ntests int16s])
			, ("usds1",  [testTask  "usds1"    i (i+1) (\i->sds \s1=i In {main=updSds s1 ((+.)(lit 1))})\\i<-take ntests ints])
			, ("usds21", [testTask  "usds21"   i (i+1) (\i->sds \s1=i  In sds \s2=99 In {main=updSds s1 ((+.)(lit 1))})\\i<-take ntests ints])
			, ("usds22", [testTask  "usds22"   i (i+1) (\i->sds \s1=99 In sds \s2=i  In {main=updSds s2 ((+.)(lit 1))})\\i<-take ntests ints])
			, ("usds31", [testTask  "usds31"   i (i+1) (\i->sds \s1=i  In sds \s2=99 In sds \s3=99 In {main=updSds s1 ((+.)(lit 1))})\\i<-take ntests ints])
			, ("usds32", [testTask  "usds32"   i (i+1) (\i->sds \s1=99 In sds \s2=i  In sds \s3=99 In {main=updSds s2 ((+.)(lit 1))})\\i<-take ntests ints])
			, ("usds33", [testTask  "usds33"   i (i+1) (\i->sds \s1=99 In sds \s2=99 In sds \s3=i  In {main=updSds s3 ((+.)(lit 1))})\\i<-take ntests ints])
			, ("usdsatom", [testuTask "usdsatom" i i (\i->sds \s1=i In {main=updSds s1 (flip (+.) (lit 1)) .&&. updSds s1 (flip (-.) (lit 1)) >>|. getSds s1})\\i<-take ntests ints])
			]

		sdsLiftTests =
			//TODO: improve error messages
			[ ("sdsboi", [ \dev->
					withShared 42 \sh->
					liftmTask (lowerSds \lsds=sh In {main=setSds lsds (lit 38)}) dev
					>-| get sh @ \v->v == 38])
			, ("sdsbor", [ \dev->
					withShared 42.0 \sh->
					liftmTask (lowerSds \lsds=sh In {main=setSds lsds (lit 38.0)}) dev
					>-| get sh @ \v->approximatelyEqual 0.1 v 38.0])
			, ("sdsbol", [ \dev->
					withShared (Long 42) \sh->
					liftmTask (lowerSds \lsds=sh In {main=setSds lsds (lit (Long 38))}) dev
					>-| get sh @ \v->v == Long 38])
			]

		sdsReflectTests =
			[ ("reflectInt", [ testTask "reflectInt" i (i, i) \i->sds \sdsi=99 In {main=reflect (rtrn (lit i)) sdsi .&&. (delay (lit 150) >>|. getSds sdsi >>~. rtrn)} \\ i<-take ntests ints])
			]

		peripheralTests =
			[ ("dht", [testrTask "dht" () 80.0 \()->dht (DHT_DHT (DigitalPin D4) DHT11) \dht->{main=humidity` r dht >>~. \h->temperature` r dht >>~. \t->rtrn (h +. t)}\\r<-[Default, RangeMs (lit 50) (lit 100)]])
			, ("dht", [testrTask "dht" () 80.0 \()->dht (DHT_SHT (i2c 0x5b)) \dht->{main=humidity` r dht >>~. \h->temperature` r dht >>~. \t->rtrn (h +. t)}\\r<-[Default, RangeMs (lit 50) (lit 100)]])
			, ("writea", [testuTask "writea" i i \i->{main=writeA (lit p) (lit i) >>|. readA` r (lit p)}\\p<-[A0, A1, A2, A3, A4, A5], i<-[0,1,254,255], r<-[Default, RangeMs (lit 50) (lit 100)]])
			, ("writed", [testuTask "writed" i i \i->{main=writeD (lit p) (lit i) >>|. readD` r (lit p)}\\p<-[A0, A1, A2, A3, A4, A5], i<-[True, False], r<-[Default, RangeMs (lit 50) (lit 100)]])
			, ("interrupt", [testTask "interrupt" () True \()->{main=interrupt (lit m) (lit p) >>*. [IfNoValue (rtrn (lit True))]}\\m<-[Change, Rising, Falling, High, Low], p<-[D0,D1, D2, D3, D4, D5, D6, D7, D8]])
			, ("pinmodea", [testuTask "pinmodea" () () \i->{main=pinMode (lit pm) (lit p)}\\p<-[A0, A1, A2, A3, A4, A5], pm<-[PMInputPullup, PMInput, PMOutput]])
			, ("pinmoded", [testuTask "pinmoded" () () \i->{main=pinMode (lit pm) (lit p)}\\p<-[D0, D1, D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12, D13], pm<-[PMInputPullup, PMInput, PMOutput]])
			, ("aqsco2-ccs", [testuTask "asq co2-ccs" () (Long 42) \()->airqualitySensor (AQS_CCS811 (i2c 0x5b)) \aqs->{main=co2` r aqs}\\r<-[Default, RangeMs (lit 50) (lit 100)]])
			, ("aqstvoc-ccs", [testuTask "aqstvoc-ccs" () (Long 38) \()->airqualitySensor (AQS_CCS811 (i2c 0x5b)) \aqs->{main=tvoc` r aqs}\\r<-[Default, RangeMs (lit 50) (lit 100)]])
			, ("aqsboth-ccs", [testuTask "aqsboth-ccs" () (Long 80) \()->airqualitySensor (AQS_CCS811 (i2c 0x5b)) \aqs->{main=tvoc` r aqs >>~. \v->co2 aqs >>~. \w->rtrn (v +. w)}\\r<-[Default, RangeMs (lit 50) (lit 100)]])
			, ("aqs-ccs", [testTask "asq-ccs" () (Long 80) \()->airqualitySensor (AQS_CCS811 (i2c 0x5b)) \aqs->{main=setEnvironmentalData aqs (lit 4.20) (lit 4.20) >>=. \_->co2 aqs >>~. \c->tvoc aqs >>~. \v->rtrn (v +. c)}])
			, ("aqsdht-ccs", [testTask "aqsdht-ccs" () (Long 80) \()->dht (DHT_DHT (DigitalPin D4) DHT11) \dht->airqualitySensor (AQS_CCS811 (i2c 0x5b)) \aqs->{main=setEnvFromDHT aqs dht >>=. \_->co2 aqs >>~. \c->tvoc aqs >>~. \v->rtrn (v +. c)}])
			, ("aqsco2-sgp", [testuTask "asq co2-sgp" () (Long 42) \()->airqualitySensor (AQS_CCS811 (i2c 0x5b)) \aqs->{main=co2` r aqs}\\r<-[Default, RangeMs (lit 50) (lit 100)]])
			, ("aqstvoc-sgp", [testuTask "aqstvoc-sgp" () (Long 38) \()->airqualitySensor (AQS_CCS811 (i2c 0x5b)) \aqs->{main=tvoc` r aqs}\\r<-[Default, RangeMs (lit 50) (lit 100)]])
			, ("aqsboth-sgp", [testuTask "aqsboth-sgp" () (Long 80) \()->airqualitySensor (AQS_CCS811 (i2c 0x5b)) \aqs->{main=tvoc` r aqs >>~. \v->co2 aqs >>~. \w->rtrn (v +. w)}\\r<-[Default, RangeMs (lit 50) (lit 100)]])
			, ("aqs-sgp", [testTask "asq-sgp" () (Long 80) \()->airqualitySensor (AQS_CCS811 (i2c 0x5b)) \aqs->{main=setEnvironmentalData aqs (lit 4.20) (lit 4.20) >>=. \_->co2 aqs >>~. \c->tvoc aqs >>~. \v->rtrn (v +. c)}])
			, ("aqsdht-sgp", [testTask "aqsdht-sgp" () (Long 80) \()->dht (DHT_DHT (DigitalPin D4) DHT11) \dht->airqualitySensor (AQS_CCS811 (i2c 0x5b)) \aqs->{main=setEnvFromDHT aqs dht >>=. \_->co2 aqs >>~. \c->tvoc aqs >>~. \v->rtrn (v +. c)}])
			, ("neo", [testTask "neo" () () \()->neopixel (NeoInfo (UInt8 7) (DigitalPin D7) NEO_GRB NEO_KHZ800) \neo->{main=setPixelColor neo (lit i) (lit i) (lit i) (lit i)} \\ i <- [0,10,255]])
			, ("gesture", [testuTask "gesture" () GNone \()->gestureSensor (Ges_PAJ7620 (i2c 0x45)) \ges->{main=gesture ges}])
			]

		multiTests =
			[ ("2returnReturn",
				[ \dev->testiTask "2returnReturn -||" (liftmTask {main=rtrn (lit 42)} dev -|| liftmTask {main=rtrn (lit 38)} dev) (==) (>>-) 42
				, \dev->testiTask "2returnReturn ||-" (liftmTask {main=rtrn (lit 42)} dev ||- liftmTask {main=rtrn (lit 38)} dev) (==) (>>-) 38
				])
//			, ("2unstableUnstable",
//				[ \dev->testiTask "2unstableUnstable -||" (liftmTask {main=unstable (lit 42)} dev -|| liftmTask {main=unstable (lit 38)} dev) (==) (>>~) 42
//				, \dev->testiTask "2unstableUnstable ||-" (liftmTask {main=unstable (lit 42)} dev ||- liftmTask {main=unstable (lit 38)} dev) (==) (>>~) 38
//				])
//			, ("2longShortDelay",
//				[ \dev->testiTask "2longShortDelay -||-" (liftmTask {main=delay (lit $ Long 500)} dev -||- liftmTask {main=delay (lit $ Long 100)} dev) (\_ _->True) (>>-) ()
//				, \dev->testiTask "2longShortDelay -||-" (liftmTask {main=delay (lit $ Long 100)} dev -||- liftmTask {main=rtrn (lit $ Long 500)} dev) (\_ _->True) (>>-) ()
//				])
			]
