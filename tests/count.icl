module count

import StdEnv

import iTasks
import iTasks.Extensions.MQTT

import mTask.Language
import mTask.Interpret
import mTask.Interpret.Devices

Start w = doTasks countI w

mqttSettings =
	{ MQTTSettings
	| host = "localhost"
	, port = Port 1883
	, mcuId = "mcu"
	, serverId = "mTask-server"
	, auth = NoAuth
	}

countI :: Task Int
countI = withShared 0 \sds->
	    updateInformation [] mqttSettings
	>>? flip withDevice \dev->
		updateSharedInformation [] sds
		||- liftmTask (countM sds) dev

countM :: (Shared sds Int) -> Main (MTask v Int) | mtask, lowerSds v & RWShared sds & TC (sds () Int Int)
countM sds =
	lowerSds \counterSds=sds
	In fun \count=(\x->
		     setSds counterSds x
		>>|. delay (lit 500)
		>>|. count (x +. lit 1)
	) In {main=count (lit 0)}
