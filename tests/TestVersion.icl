module TestVersion

import StdEnv
import iTasks
import iTasks.Extensions.DateTime

import mTask.Interpret
import mTask.Interpret.Device.TCP

Start :: !*World -> *World
Start w = doTasks (onStartup main) w

main :: Task ()
main = try (withDevice spec \dev->waitForTimer False 1 @! 2)
	(\e -> case e of
		MTEServerVersionMismatch _ = return 0
		_ = return 1
	)
	>>- \ec->waitForTimer False 1 >-| shutDown ec
where
	spec = {TCPSettings | host="localhost", port=Port 8123, pingTimeout= ?None}
