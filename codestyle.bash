#!/usr/bin/env bash
set -e

code=0
for file in mtask-rts/*.{c,cpp,h,ino} mtask-rts/pc/*.[ch]
do
	if [[ "$file" =~ (types|preloaded_tasks).[ch] ]]
	then
		continue
	fi
	if expand "$file" | grep -nE ".{81}"
	then
		echo "$file has a line longer than 80 chars"
		code=1
	fi
	if grep -nE "[[:space:]]$" "$file"
	then
		echo "$file has trailing whitespace"
		code=1
	fi
	if grep -nE "(while|if|switch|for)\(" "$file"
	then
		echo "$file misses a space between if/while/switch for and ("
		code=1
	fi
	if grep -nE "\)\{" "$file"
	then
		echo "$file misses a space between the ) and the {"
		code=1
	fi
	if grep -nE "^\t* +[^*]" "$file"
	then
		echo "$file has indentation with spaces"
		code=1
	fi
	if grep -nE "^[[:space:]]*default:" "$file" | grep -nvE "\/\/[[:space:]]please"
	then
		echo "$file has a default case label"
		code=1
	fi
	if grep -nE "while[[:space:]]*\(0\)[[:space:]]*;" "$file"
	then
		echo "$file has a do { .. } while (0) with a semicolon"
		code=1
	fi
	if grep -nE "\#[[:space:]]*(else|endif)[[:space:]]*$" "$file"
	then
		echo "$file has a #endif or #else without comment"
		code=1
	fi
	if [[ ! ( "$file" =~ (pc.c|pc/.*) ) ]]
	then
		if grep -nE "(msg_(log|debug)|die)\(\"" "$file"
		then
			echo "$file has a log, debug or die messages without using PSTR"
			code=1
		fi
	fi
done

exit $code
