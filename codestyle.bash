#!/bin/bash
set -e

code=0
for file in mtask-rts/*.{c,cpp,h,ino}
do
	if [[ "$file" =~ (types|preloaded_tasks).[ch] ]]
	then
		continue
	fi
	if expand "$file" | grep -nE ".{81}"
	then
		echo "$file has a line longer than 80 chars"
		code=1
	fi
	if grep -nE "[[:space:]]$" "$file"
	then
		echo "$file has trailing whitespace"
		code=1
	fi
	if grep -nE "(while|if|switch|for)\(" "$file"
	then
		echo "$file misses a space between if/while/switch for and ("
		code=1
	fi
	if grep -nE "\)\{" "$file"
	then
		echo "$file misses a space between the ) and the {"
		code=1
	fi
	if grep -nE "^\t* +[^*]" "$file"
	then
		echo "$file has indentation with spaces"
		code=1
	fi
done

exit $code
